<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Timestamp
    |--------------------------------------------------------------------------
    |
    */
    'PROJECT_NAME' => 'Pitch Your Concepts',
    'PROJECT_EDITO' => 'Pitch Your Concepts',
    'DATE_YYYY_MM_DD_H_M_S_24' => 'Y-m-d-H:i:s', // Output: 1988-09-30-04:30:00
    'DATE_YYYY_MM_DD' => 'Y-m-d', // Output: 1988-09-30-04
];
