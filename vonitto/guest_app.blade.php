<!DOCTYPE html>
<html lang="en">
   <head>
      <!-- include header files  -->
      @include('layouts.header-include-files')
   </head>
   <body class="homepage">
     @if(Session::has('message'))
       <p id="alertMessage" class="alert {{ Session::get('alert-class', 'alert-success') }}" style="margin-bottom:0px !important;">{{ Session::get('message') }}</p>
     @endif
     @if(Session::get("errormessage")!=null)
      <p id="alertMessage" class="alert alert-danger" style="margin-bottom:0px !important;">{{Session::get("errormessage")}}</p>
     @endif
      <!-- include body data -->
      <!-- SIGN UP Modal -->
      <div class="mymodal modal fade" id="signup_model" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
         <div class="modal-dialog" role="document">
            <div class="modal-content">
               <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
               <div class="clearfix"></div>
               <div class="modal-body">
                  <div class="signupmodel_inner_container">
                     <div class="facebokbtn_dv"> <a href="{{url('/redirect')}}"> <i class="fa fa-facebook"></i>Sign up with Facebook</a> </div>
                     <div class="ordv"><span>Or</span></div>
                     <form class="form-horizontal" role="form" method="POST" action="{{ url('/register') }}" id="frmRegistration">
                       @if($errors->has('email'))
                        <label class="error">{{ $errors->first('email') }}</label>
                       @endif
                       <input type="hidden" value="{{csrf_token()}}" id="_token" name="_token">
                       <div class="formholder_dv">
                          {{--*/$date = DateTime::createFromFormat("Y-m-d", date('Y-m-d'))/*--}}
                          {{--*/$sYear = $date->format('Y')-100/*--}}
                          {{--*/$eYear = $date->format('Y')-18/*--}}
                          {{--*/$sDay = 1/*--}}
                          {{--*/$sMonth = 1/*--}}
                          <ul>
                             <li>
                                <input type="text" class="txt @if($errors->has('first_name')) has-error @endif" placeholder="Alex |" name="first_name" value="{{old('first_name')}}">
                                <i class="fa fa-user"></i>
                             </li>
                             <li>
                                <input type="text" class="txt @if($errors->has('last_name')) has-error @endif" placeholder="Last Name" name="last_name" value="{{old('last_name')}}">
                                <i class="fa fa-user"></i>
                             </li>
                             <li>
                                <input type="email" class="txt @if($errors->has('email')) has-error @endif" placeholder="Email ID" name="email" value="{{old('email')}}">
                                <i class="fa fa-envelope"></i>
                             </li>
                             <li>
                                <input type="password" class="txt @if($errors->has('password')) has-error @endif" placeholder="Password" name="password">
                                <i class="fa fa-lock"></i>
                             </li>
                             <li class="dvd_3">
                                <label>Birthday</label>
                                <select class="selectpicker @if($errors->has('month')) has-error @endif" name="month">
                                  <option value="">Month</option>
                                  @while($sMonth<=12)
                                   <option value="{{$sMonth}}" @if(old('month')==$sMonth) selected @endif >{{$sMonth}}</option>
                                   {{--*/$sMonth++/*--}}
                                  @endwhile
                                </select>
                                <select class="selectpicker @if($errors->has('day')) has-error @endif" name="day">
                                  <option value="">Day</option>
                                  @while($sDay<=31)
                                   <option value="{{$sDay}}" @if(old('day')==$sDay) selected @endif>{{$sDay}}</option>
                                   {{--*/$sDay++/*--}}
                                  @endwhile
                                </select>
                                <select class="selectpicker @if($errors->has('year')) has-error @endif" name="year">
                                  <option value="">Year</option>
                                  @while($sYear<$eYear)
                                   <option value="{{$sYear}}" @if(old('year')==$sYear) selected @endif>{{$sYear}}</option>
                                   {{--*/$sYear++/*--}}
                                  @endwhile
                                </select>
                                <div class="clearfix"></div>
                             </li>
                             <li class="text"> By signing up, I agree to Vaction Rental's <a href="#">Terms of Service</a> &amp; <a href="#">Privacy Policy.</a> </li>
                             <li class="signupbtn">
                                <button type="submit" id="btnRegistration">Sign up</button>
                             </li>
                             <li class="text"> Already a member ? <a href="#" data-toggle="modal" data-target="#login_model" data-dismiss="modal">Log In </a> </li>
                          </ul>
                       </div>
                     </form>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- login Modal -->
      <div class="mymodal modal fade" id="login_model" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
         <div class="modal-dialog" role="document">
            <div class="modal-content">
               <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
               <div class="clearfix"></div>
               <div class="modal-body">
                  <div class="signupmodel_inner_container">
                     <div class="facebokbtn_dv"> <a href="{{url('/redirect')}}"> <i class="fa fa-facebook"></i>Sign up with Facebook</a> </div>
                     <div class="ordv"><span>Or</span></div>
                     <form id="frmLogin" class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
                       <input type="hidden" name="_token" id="token" value="{{csrf_token()}}">
                       <div class="formholder_dv working_chk_bx">
                         @if($errors->has('email'))
                          <label class="error">{{ $errors->first('email') }}</label>
                         @endif
                          <ul>
                             <li>
                                <input type="email" class="txt @if($errors->has('email')) has-error @endif" placeholder="Alex.Mercer@outlook.com" name="email">
                                <i class="fa fa-envelope"></i>
                             </li>
                             <li>
                                <input type="password" class="txt @if($errors->has('password')) has-error @endif" placeholder="Password" name="password">
                                <i class="fa fa-lock"></i>
                             </li>
                             <li class="rember_pwd">
                                <input tabindex="1" type="checkbox" id="input-1" checked>
                                <label for="input-1">Remember me</label>
                                <a href="#" data-toggle="modal" data-target="#forgot_model" data-dismiss="modal"> Forgot Password ?</a>
                                <div class="clearfix"></div>
                             </li>
                             <li class="signupbtn">
                                <button type="submit" id="login">Log In</button>
                             </li>
                             <li class="text"> Don’t you have an account ? <a href="#" data-toggle="modal" data-target="#signup_model" data-dismiss="modal">Sign up </a> </li>
                          </ul>
                       </div>
                     </form>
                  </div>
               </div>
            </div>
         </div>
      </div>
      
      <!-- Forgot password -->
      <div class="mymodal modal fade" id="forgot_model" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
         <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Forgot Password</h4>
      </div>
               <div class="clearfix"></div>
               <div class="modal-body">
                  <div class="signupmodel_inner_container">
                     @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                     @endif
                     <form class="form-horizontal" role="form" method="POST" action="{{ url('/password/reset') }}">
                        

                        <input type="hidden" name="_token" id="token" value="{{csrf_token()}}">

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-8">
                                <input type="email" class="form-control" name="email" value="{{ old('email') }}" id="email">

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary" id="btnSubmit">
                                    <i class="fa fa-btn fa-envelope" ></i>Submit
                                </button>
                            </div>
                        </div>
                    </form>
                  </div>
               </div>
            </div>
         </div>
      </div>

      <!-- End Forgot Password -->	
      <header class="header">
        <!-- include header code -->
        @include('layouts.header')
      </header>
      <section class="banner">
        <!-- include banner section -->
        @include('layouts.banner')
      </section>
      <!-- main content -->
      @yield('content')
      <footer class="footer_Sec" >
        @include('layouts.footer')
      </footer>

      <!-- footer include files here.  -->
      @include('layouts.footer-include-files')
</body>
</html>
