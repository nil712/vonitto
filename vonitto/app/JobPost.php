<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

class JobPost extends Model
{
  protected $table = "job_post";
  protected $primaryKey = 'id';
  /**
   * The attributes that are mass assignable.
   * @var array
   */
  protected $fillable = ['id','job_project_id','is_featured','title','description','project_deadline','category_id','location_city','location_country','budget','file','skills','how_long','is_one_time','job_poster_id','is_active','what_you_want','job_logo','postcode','currency'];

  public function category()
    {
        return $this->belongsTo('App\Category','category_id','id');
    }
    public function getposter(){
      return $this->belongsTo('App\User','job_poster_id','id');
    }
    /**
     * @Author: LN
     * @Created: March 14 2016
     * @Modified By: Ankit Sompura
     * @Comment:
     * @Todo: return json for datatable
     */
     public function getListing($Data){
       $RequestData = $Data;
       $Columns     = array(
           // Datatable column index  => Database column name
           0 => 'Id',
           1 => 'Category',
           2 => 'Title',
           3 => 'Description',
           3 => 'Project Deadline',
       );

       $TotalData     = DB::table("job_post")->count();
       $TotalFiltered = $TotalData; // When there is no search parameter then total number rows = total number filtered rows.

       $sql = "SELECT t.*,jc.name FROM job_post as t left join job_category as jc on jc.id = t.category_id";

       if (!empty($RequestData['search']['value'])) {
           // If there is a search parameter, $RequestData['search']['value'] contains search parameter
           $sql .= " WHERE (t.id LIKE '" . $RequestData['search']['value'] . "%' ";
           $sql .= " OR jc.name LIKE '" . $RequestData['search']['value'] . "%' ";
           $sql .= " OR t.title LIKE '" . $RequestData['search']['value'] . "%' ";
           $sql .= " OR t.description LIKE '" . $RequestData['search']['value'] . "%') ";
       }

       $TempData      = DB::select($sql);
       $TotalFiltered = count($TempData);
       $sql .= " ORDER BY " . $Columns[$RequestData['order'][0]['column']] . "   " . $RequestData['order'][0]['dir'] . "  LIMIT " . $RequestData['start'] . " ," . $RequestData['length'] . "   ";

       $GetData = DB::select($sql);
       $data    = array();
       foreach ($GetData as $row) { // Preparing an array
           $sqlCategoryData = "SELECT id,name FROM job_category where id='".$row->category_id."'";
           $CategoryData      = DB::select($sqlCategoryData);

           $Edit = '<td style="padding: 2px 2px;"><a title="Edit" style="width:90px;"  href="'.url("/").'/admin/job/'.$row->id.'/edit" data-page-id="'.$row->id.'"> <i class="fa fa-edit" style="color:#000;"></i></a></td>';
           $Delete = '<td style="padding: 2px 2px;"><a title="Delete" style="width:90px;" class="deleteCourseUnitBtn" href="javascript:void(0);" data-page-id="'.$row->id.'"> <i class="fa fa-trash-o" style="color:#000;"></i></a></td>';

           $featured="";

           if($row->is_featured == 0)
                $featured='<td><input type="checkbox" class="isFeatured" data-id="'.$row->id.'" value="Featured" /></td>';
           else
              $featured='<td><input type="checkbox" class="isFeatured" data-id="'.$row->id.'" value="Featured" checked /></td>';

           $NestedData   = array();
           $NestedData[] = $row->id;
           $NestedData[] = ($CategoryData[0]->name)?$CategoryData[0]->name:'';
           $NestedData[] = $row->title;
           $NestedData[] = $row->description;
           $NestedData[] = $featured;
           $NestedData[] = '<table>
                             <tr>
                                 '.$Edit.'
                                 '.$Delete.'
                             </tr>

                           </table>';

           $data[] = $NestedData;

       }

       $JsonData = array(
           "draw" => intval($RequestData['draw']), // For every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
           "recordsTotal" => intval($TotalData), // Total number of records
           "recordsFiltered" => intval($TotalFiltered), // Total number of records after searching, if there is no searching then totalFiltered = totalData
           "data" => $data // Total data array
       );
       return json_encode($JsonData); // Send data as json format
     }

     public function getMaxOrder(){
       return DB::table($this->table)->count();
     }
}
