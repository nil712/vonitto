<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;
use App\Http\Requests\Request;

class Category extends Model
{
  protected $table = "job_category";
  protected $primaryKey = 'id';
  /**
   * The attributes that are mass assignable.
   * @var array
   */
  protected $fillable = ['name'];
  protected $hidden = ['created_at','updated_at'];

  public function jobs()
  {
        return $this->hasMany('App\JobPost', 'category_id', 'id');
  }

  /**
    * @Author: LN
    * @Created: March 14 2016
    * @Modified By: Ankit Sompura
    * @Comment:
    * @Todo: return json for datatable
    */
    public function getListing($Data){
      $RequestData = $Data;
      $Columns     = array(
          // Datatable column index  => Database column name
          0 => 'Id',
          1 => 'Name',
      );

      $TotalData     = DB::table("job_category")->count();
      $TotalFiltered = $TotalData; // When there is no search parameter then total number rows = total number filtered rows.

      $sql = "SELECT * FROM job_category";

      if (!empty($RequestData['search']['value'])) {
          // If there is a search parameter, $RequestData['search']['value'] contains search parameter
          $sql .= " WHERE (id LIKE '" . $RequestData['search']['value'] . "%' ";
          $sql .= " OR name LIKE '" . $RequestData['search']['value'] . "%')";
      }

      $TempData      = DB::select($sql);
      $TotalFiltered = count($TempData);
      $sql .= " ORDER BY " . $Columns[$RequestData['order'][0]['column']] . "   " . $RequestData['order'][0]['dir'] . "  LIMIT " . $RequestData['start'] . " ," . $RequestData['length'] . "   ";

      $GetData = DB::select($sql);
      $data    = array();
      foreach ($GetData as $row) { // Preparing an array

          $Edit = '<td style="padding: 2px 2px;"><a title="Edit" style="width:90px;"  href="'.url("/").'/admin/category/'.$row->id.'/edit" data-page-id="'.$row->id.'"> <i class="fa fa-edit" style="color:#000;"></i></a></td>';

          $Delete = '<td style="padding: 2px 2px;"><a title="Delete" style="width:90px;" class="deleteCourseUnitBtn" href="javascript:void(0);" data-page-id="'.$row->id.'"> <i class="fa fa-trash-o" style="color:#000;"></i></a></td>';


          $NestedData   = array();
          $NestedData[] = $row->id;
          $NestedData[] = $row->name;
          $NestedData[] = '<table>
                            <tr>
                                '.$Edit.'
                                '.$Delete.'
                            </tr>

                          </table>';

          $data[] = $NestedData;

      }

      $JsonData = array(
          "draw" => intval($RequestData['draw']), // For every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
          "recordsTotal" => intval($TotalData), // Total number of records
          "recordsFiltered" => intval($TotalFiltered), // Total number of records after searching, if there is no searching then totalFiltered = totalData
          "data" => $data // Total data array
      );
      return json_encode($JsonData); // Send data as json format
    }

    public function getMaxOrder(){
      return DB::table($this->table)->count();
    }

    public function GetCategoryList(){
      return DB::table('job_category')->select('id', 'name')->get();
    }
}
