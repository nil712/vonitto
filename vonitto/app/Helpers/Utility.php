<?php
namespace App\Helpers;
use App\User;
use URL;
use Auth;
class Utility
{
  public function getUserRoleName($id){
    $data = User::where('id','=',$id)->get();
    if(isset($data[0]->role) && !empty($data[0]->role)){
      return $data[0]->role;
    }
  }
  public function addUrls($request_data){

    $role = $this->getUserRoleName(Auth::user()->id);
    if($role=="JOB_POSTER"){
      $dir = URL::asset('resources/assets/upload/jobposter/');
    }else {
      $dir = URL::asset('resources/assets/upload/jobseeker/');
    }
    if(isset($request_data) && !empty($request_data) && $request_data[0])
    {
      foreach ($request_data as $key=>$data) {
        ($data->image=="")?$data->image="no-image-found.png":$data->image;
        $request_data[$key]->image = $dir."/".$data->image;
      }
      return $request_data;
    }else {
        ($request_data['image']=="")?$request_data['image']="no-image-found.png":$request_data['image'];
        $request_data['image'] = $dir."/".$request_data['image'];
        return $request_data;
    }
  }
}
?>
