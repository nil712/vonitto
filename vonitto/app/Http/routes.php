<?php
use App\JobPost;
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
// normal user login`.
Route::get('/', function () {
    if(!Auth::check()){
      $jobPostFeatured = JobPost::where('is_featured',DB::raw("'1'"))->get();
      $mapJobs = JobPost::all(['location_city','location_country','id','title'])->toArray();
      //JobPost
      return view('home',compact('jobPostFeatured','mapJobs'));
      // return view('auth/register');
    }else{
      return redirect('checklogintype');
    }
});

Route::get('contact_us','ContactUsController@index');//added by richa for contactus page
Route::post('contact_us','ContactUsController@store');//added by richa for contactus page send mai
// redirect to home page for end user.
// redirect to home page for end user.

Route::post('auth/login','Auth\AuthController@login');

Route::auth();
Route::controllers([
     'auth'=>'Auth\AuthController',
     'password'=>'Auth\PasswordController',
]);
Route::group(['middleware' => 'RoleBaseLogin'], function()
{
  Route::get('/checklogintype',function(){
    return true;
  });
});
// This will use to decide user's role and actual redirect page.
// Bellow urls only access by Normal Users.
Route::group(['middleware' => 'CheckUserRole:JOB_POSTER'], function()
{
   Route::post('jobposter/assignJob', 'WelcomeController@assignJob');
   Route::resource('jobposter/home', 'WelcomeController');

   Route::get('myaccount/loadAwardedJobs/{id}','MyAccountController@loadAwardedJobs');
   Route::get('myaccount/posterJobs/{id}','MyAccountController@loadPosterJobs');
   Route::get('myaccount/loadPosterPitchJobs/{id}','MyAccountController@loadPosterPitchJobs');
   Route::get('myaccount/load-completed-job/{id}','MyAccountController@loadPosterCompletedJob');


   Route::post('loadJobsPitcherList','JobPostController@loadJobsPitcherList');
   Route::post('browse/seeker','JobPostController@searchBrowseSeeker');
   Route::get('browse/seeker','JobPostController@browseSeeker');
});

// here is common url for Admin and User
Route::group(['middleware' => 'CheckUserRole:JOB_SEEKER'], function()
{
    Route::resource('jobseeker/home', 'WelcomeController');
    Route::resource('package/payment', 'PaymentController');
    Route::get('myaccount/loadPitchJobs/{id}','MyAccountController@loadPitchJobs');
    Route::get('myaccount/loadJobsPackage/{id}','MyAccountController@loadJobsPackage');
    Route::get('myaccount/loadJobseekerAwardedJobs/{id}','MyAccountController@loadJobseekerAwardedJobs');
    Route::get('myaccount/loadJobseekerCompletedJobs/{id}','MyAccountController@loadAwardedCompletedJobs');

    Route::resource('jobseeker/myaccount','MyAccountController');
    Route::resource('payment/success','PagesController@thanks');

});


// Bellow urls only access by Admin.
/*
    Developer Name : Krunal Sojitra
    Date : 1-June-2016
    Description : Authantication login with role based of admin panel
*/
Route::get('admin', 'Auth\AdminAuthController@getLogin');
Route::post('admin/auth/login', 'Auth\AdminAuthController@postLogin');
Route::get('admin/auth/logout', 'Auth\AdminAuthController@getLogout');
/* -------------------- End Super Admin ----------------------- */
Route::group(['middleware'=>['auth']],function(){
    Route::group(['prefix'=>'admin','middleware' => 'CheckUserRole:sAdmin'], function()
    {
      Route::get('/profile/edit/{id}', 'DashboardController@edit');
      Route::resource('/dashboard', 'DashboardController');

      Route::get('users', 'admin\UserController@index'); // Load users
      Route::post('users/datatable', 'admin\UserController@getDataTable'); // List all users
      Route::post('users/changeStatus', 'admin\UserController@changeStatus'); // Change user status
      Route::get('user/edit/{id}', 'admin\UserController@getUserData'); // Edit user data
      Route::get('user/view/{id}', 'admin\UserController@viewUserData'); // View user profile
      Route::post('user/general/update', 'admin\UserController@updateGeneralData'); // Update user general data
      Route::post('user/media/update', 'admin\UserController@updateMediaData'); // Update user media data
      Route::get('user/create', 'admin\UserController@loadNewUser'); // Load add user view
      Route::post('user/add', 'admin\UserController@addUser'); // Add new user
      Route::post('user/deleteUser', 'admin\UserController@deleteRecord'); // Delete user


      Route::get('userdetail/GetStat', 'admin\UserDetailController@userDetailListing');
      Route::resource('userdetail', 'admin\UserDetailController',array('only'=>array('index','create','store','edit','update','destroy')));
      Route::get('userdetail/datatable', 'admin\UserDetailController@userDetailListing');
      Route::get('userdetail/view/{id}', 'admin\UserDetailController@viewUserData'); // View user profile

      //Start Category Route
      Route::resource('category', 'admin\CategoryController',array('only'=>array('index','create','store','edit','update','destroy')));
      Route::get('category/datatable', 'admin\CategoryController@categoryListing');
      //End Category Route

      //Start Page Cms Route
      Route::resource('pagecms', 'admin\PageCmsController',array('only'=>array('index','create','store','edit','update','destroy')));
      Route::get('pagecms/datatable', 'admin\PageCmsController@pagecmsListing');
      //End Page Cms Route

      //admin google ads routes start
      Route::resource('google_ads', 'admin\GoogleAdsController',array('only'=>array('index','create','store','edit','update','destroy')));
      Route::get('google_ads/datatable', 'admin\GoogleAdsController@GoogleAdsListing');
      //admin google ads routes end

      //Start Job Route
      Route::resource('job', 'admin\JobController',array('only'=>array('index','create','store','edit','update','destroy')));
      Route::get('job/datatable', 'admin\JobController@jobListing');

      /* Hitesh Tank featured or not featured */
        Route::post('job/featured', 'admin\JobController@featuredFunctionality');
      /* Hitesh Tank featured or not featured */
      //End Job  Route

      //Start Subscription Route
      Route::resource('subscription', 'admin\SubscriptionController',array('only'=>array('index','create','store','edit','update','destroy')));
      Route::get('subscription/datatable', 'admin\SubscriptionController@subscriptionListing');
      //End Subscription Route

      /*********************** Review & Rating  By Hitesh Tank********************************/
      Route::resource('review', 'admin\ReviewController',array('only'=>array('index','create','store','edit','update','destroy')));
        Route::get('review/datatable', 'admin\ReviewController@getAll');
      /*********************** Review & Rating  ********************************/
    });
});
/* -------------------- End Super Admin ----------------------- */


Route::group(['middleware' => 'auth'], function()
{
    Route::group(['prefix' => 'jobseeker', ['middleware' => 'role:JOB_SEEKER']], function() {
        Route::post('pitch', 'JobPostController@pitch');
        Route::post('myaccount/unsubscribe', 'UserController@unsubscribe');
    });

    Route::get('download/documents/{filename}','DocumentController@downloadDoc');
    Route::get('download/seeker-documents/{filename}','DocumentController@downloadSeekerDoc');
    Route::get('user/profile/{userid}','UserController@userDetail');
});
// Jobseeker & Jobposter Login & Register Route
Route::get('register', 'RegisterController@signup');
Route::get('login', 'RegisterController@login');
Route::get('/jobseeker/signup', 'JobseekerController@index');
Route::get('/jobposter/signup', 'JobposterController@index');
Route::get('/jobseeker/login', 'JobseekerController@login');
Route::get('/jobposter/login', 'JobposterController@login');
Route::resource('jobseeker', 'JobseekerController');
Route::resource('jobposter', 'JobposterController');
Route::get('logout', 'RegisterController@getLogout');
Route::get('/register/verify/{verifyLink}','UserController@verifyUser');

// Edit Profile Route
Route::get('/jobseeker/editprofile/{id}','JobSeekerProfileController@index');
Route::post('/jobseeker/editprofile/{id}','JobSeekerProfileController@update');
Route::get('/jobposter/editprofile/{id}','JobPosterProfileController@index');
Route::post('/jobposter/editprofile/{id}','JobPosterProfileController@update');

// Job Post Route
Route::get('jobpost','JobPostController@index');

Route::get('browse','JobPostController@browse');
Route::post('browse/search','JobPostController@searchbrowse');
Route::get('browse/search','JobPostController@browse');

Route::get('job/detail/{id}','JobPostController@jobDetail')->middleware('auth');

Route::post('jobpost','JobPostController@store');
Route::get('jobpostsuccess',function(){
  return View::make('jobpost.jobpostsuccess');
});


/*
  * Developer Name : Hitesh Tank
  * Date : 29-July-2016
*/
Route::group(['middleware' => 'auth'], function()
{
    Route::group(['prefix' => 'jobseeker', ['middleware' => 'role:JOB_SEEKER']], function() {
        Route::post('/myaccount/job-status-update','MyAccountController@changeJobStatus');
        Route::get('/myaccount/loadJobseekerFavourite/{id}','MyAccountController@loadFavouriteJobs');
        /*Bookmark and unbookmark*/
        Route::post('/job/mark-action','BookMarkController@bookMarkAction');
        Route::post('job-post/upload-documents','JobPostController@uploadseekerFiles');

    });
    Route::group(['prefix' => 'jobposter', ['middleware' => 'role:JOB_POSTER']], function() {
        Route::resource('/job-seeker/review','SeekerReviewRatingController',array('only'=>array('store')));
        Route::resource('jobpost','JobPostController',array('only'=>array('edit','update')));
        Route::post('job-post/upload-files','JobPostController@uploadFiles');
        Route::post('job-post/delete-post-file','JobPostController@deleteFile');
    });
});
/*
  * Developer Name : Hitesh Tank
  * Date : 29-July-2016
*/
Route::get('post-updated/email','JobPostController@sentUpdateEmail');
Route::get('cms/{slug}','CmsController@cmsPage');
