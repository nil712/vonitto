<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use App\BookMark;
use Input;
use DB;
use App\JobPost;
use Exception;
use Validator;

class BookMarkController extends Controller
{
    public $bookmarkObj;

    public function __construct(){

      $this->bookmarkObj=new BookMark();
    }

    public function bookMarkAction(Request $request){
      $data=$request->json()->get('data');
      $response=['data'=>[]];
      $statusCode=200;
      try{
        $rules = array(
          'job_id' => 'required',
          'poster_id'=>'required',
          'status'=>'required',
        );
        $messages = [
           'job_id.required' => 'Please choose proper job',
           'poster_id.required' => 'Unable to bookmark',
           'status.required' => 'Bookmark is not working',
          ];

          $validator = Validator::make($data, $rules,$messages);
          if($validator->fails()) {
            $response['status'] = "0";
            $response['data'] = array();
            $response['message'] = $validator->errors()->first();
          }else{
              if($findData=BookMark::where('job_id',$data['job_id'])->where('poster_id',$data['poster_id'])->where('seeker_id',Auth::id())->first())
              {
                  $this->bookmarkObj=BookMark::find($findData->id);
                  $this->bookmarkObj->bookmark_status=$data['status'];
                  $this->bookmarkObj->save();
              }else{
                $this->bookmarkObj->poster_id=$data['poster_id'];
                $this->bookmarkObj->job_id=$data['job_id'];
                $this->bookmarkObj->seeker_id=Auth::id();
                $this->bookmarkObj->bookmark_status=$data['status'];
                $this->bookmarkObj->save();
              }
              $response['status']='1';
              $response['updated']=$this->bookmarkObj->bookmark_status;
          }
      }catch(Exception $e){
        $response['status']='0';
        $response['message']=$e->getMessage();
      }finally{
        return response()->json($response,$statusCode);
      }
    }
}
