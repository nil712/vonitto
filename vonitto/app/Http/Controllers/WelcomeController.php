<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Auth,Utility,DB;
use App\User;
use App\JobPost;
use App\JobAwarded;
use Mail;

class WelcomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $objUtility = new Utility;

        $userId = Auth::user()->id;
        $userDetail = User::find($userId);
        $userDetail = $objUtility->addUrls($userDetail);
        $limit = 10;
        $offset = 0;

        $myJobs = DB::select("SELECT job_post.currency,job_post.id as job_id,job_post.job_project_id,job_post.project_deadline,job_post.budget,job_post.title,job_post.created_at,job_pitched.job_seeker_id,CASE WHEN(COUNT(job_pitched.id)>=0) THEN COUNT(job_pitched.id) ELSE 0 END AS pitched_request,job_post.progress_status FROM `job_post`
        LEFT JOIN job_pitched ON job_pitched.job_id = job_post.id
        WHERE job_poster_id = ".$userId." GROUP BY job_post.id order by job_id DESC LIMIT ".$offset.",".$limit."");

        $totalJobs = DB::select("SELECT job_post.id as job_id,job_post.job_project_id,job_post.project_deadline,job_post.budget,job_post.title,job_post.created_at,job_pitched.job_seeker_id,CASE WHEN(COUNT(job_pitched.id)>=0) THEN COUNT(job_pitched.id) ELSE 0 END AS pitched_request,job_post.progress_status FROM `job_post`
        LEFT JOIN job_pitched ON job_pitched.job_id = job_post.id
        WHERE job_poster_id = ".$userId." GROUP BY job_post.id order by job_id DESC");

        $jobsPages = ceil(sizeof($totalJobs)/$limit);


        $pitchedRequest = DB::select("SELECT job_post.id as job_id,job_pitched.job_seeker_id,users.first_name,users.last_name,job_post.job_project_id,job_post.title,job_post.description,job_post.created_at,case when(job_awarded.id>0) then '1' else '0' END as is_awarded,
        CASE WHEN (job_awarded.job_seeker_id = job_pitched.job_seeker_id)THEN '1' ELSE '0' END AS is_assigned FROM job_post
        RIGHT JOIN job_pitched ON job_pitched.job_id = job_post.id
        LEFT JOIN users ON users.id = job_pitched.job_seeker_id
	      LEFT JOIN job_awarded ON job_awarded.job_id = job_post.id
        WHERE job_post.job_poster_id =".$userId." order by created_at LIMIT ".$offset.",".$limit."");

        $totalRequest = DB::select("SELECT job_post.id as job_id,job_pitched.job_seeker_id,users.first_name,users.last_name,job_post.job_project_id,job_post.title,job_post.description,job_post.created_at,case when(job_awarded.id>0) then '1' else '0' END as is_awarded,
        CASE WHEN (job_awarded.job_seeker_id = job_pitched.job_seeker_id)THEN '1' ELSE '0' END AS is_assigned FROM job_post
        RIGHT JOIN job_pitched ON job_pitched.job_id = job_post.id
        LEFT JOIN users ON users.id = job_pitched.job_seeker_id
	      LEFT JOIN job_awarded ON job_awarded.job_id = job_post.id
        WHERE job_post.job_poster_id =".$userId." order by created_at");

        $requestPages = ceil(sizeof($totalRequest)/$limit);

        /**
         * @Method		  :	POST
         * @Params		  :
         * @author        :
         * @created		  :	29 July 2016
         * @Modified by	  :Hitesh Tank
         * @Comment		  : return awarded job which have status of in-progress and assigned
         **/
        $awardedJobs = DB::select("SELECT job_post.id as job_id,job_post.title,users.id as user_id,users.first_name,users.last_name,job_awarded.created_at,job_post.progress_status
        from job_awarded
        left join users on users.id = job_awarded.job_seeker_id
        left join job_post on job_post.id = job_awarded.job_id
        where job_awarded.job_poster_id = ".$userId." AND (job_post.progress_status='In-Progress' OR job_post.progress_status='Assigned') order by job_awarded.created_at LIMIT ".$offset.",".$limit."");

        /**
         * @Method		  :	POST
         * @Params		  :
         * @author        :
         * @created		  :	29 July 2016
         * @Modified by	  :Hitesh Tank
         * @Comment		  : return awarded job count which have status of in-progress and assigned
         **/
        $totalAwardedJobs = DB::select("SELECT job_post.id as job_id,job_post.title,users.id as user_id,users.first_name,users.last_name,job_awarded.created_at,job_post.progress_status
        from job_awarded
        left join users on users.id = job_awarded.job_seeker_id
        left join job_post on job_post.id = job_awarded.job_id
        where (job_post.progress_status='In-Progress' OR job_post.progress_status='Assigned') AND job_awarded.job_poster_id = ".$userId." order by job_awarded.created_at");
        $awardedJobsPages = ceil(sizeof($totalAwardedJobs)/$limit);

        /**
         * @Method		  :	POST
         * @Params		  :
         * @author        :
         * @created		  :	29 July 2016
         * @Created by	  :Hitesh Tank
         * @Comment		  : return completed job data
         **/
        //get Completed Job Data
        $completedJob = JobAwarded::where('job_awarded.job_poster_id','=',$userId)->where('job_post.progress_status','Completed')->leftJoin('job_post','job_post.id','=','job_awarded.job_id')->leftJoin('users','users.id','=','job_awarded.job_seeker_id')->leftJoin('job_review_rating','job_post.id','=','job_review_rating.job_post_id')->limit($limit)->get();
        //  dd($completedJob);
        /**
         * @Method		  :	POST
         * @Params		  :
         * @author        :
         * @created		  :	29 July 2016
         * @Created by	  :Hitesh Tank
         * @Comment		  : return completed job count
         **/
        //Total Completed job pages
        $totalCompletedJob = JobAwarded::where('job_awarded.job_poster_id','=',$userId)->where('job_post.progress_status','Completed')->leftJoin('job_post','job_post.id','=','job_awarded.job_id')->leftJoin('users','users.id','=','job_awarded.job_poster_id')->leftJoin('job_review_rating','job_post.id','=','job_review_rating.job_post_id')->count();
        $completedJobsPages = ceil($totalCompletedJob/$limit);

        return view('welcome',compact('userDetail','myJobs','pitchedRequest','requestPages','jobsPages','awardedJobs','awardedJobsPages','completedJobsPages','completedJob'));

    }
    public function assignJob(){
      extract($_POST);
      $jobAwardedData = JobAwarded::where('job_seeker_id','=',$job_seeker_id)->where('job_id','=',$job_id)->count();
      if($jobAwardedData){ // reward job
        // delete awarded data.
        JobAwarded::where('job_seeker_id','=',$job_seeker_id)->where('job_id','=',$job_id)->delete();
        // update a job status Assigned to null
        $objPostJob = JobPost::find($job_id);
        $objPostJob->progress_status = null;
        $objPostJob->update();
        return "2";
      }else {

        $objJobAwarded = new JobAwarded;
        $objJobAwarded->job_seeker_id = $job_seeker_id;
        $objJobAwarded->job_id = $job_id;
        $objJobAwarded->job_poster_id = Auth::user()->id;


        if($objJobAwarded->save()){
          $objPostJob = JobPost::find($job_id);
          $objPostJob->progress_status = "Assigned";
          $objPostJob->update();
          $seeker_information=User::where("id",$job_seeker_id)->first();
          $info['name']=Auth::user()->first_name." ".Auth::user()->last_name;
          $info['phone']=Auth::user()->phone;
          $info['email']=Auth::user()->email;
          $info['receiver_email']=$seeker_information->email;
          $info['job_title']=$job_title;
          Mail::send('emails._job_poster_awarded', ['info' =>$info], function ($m) use ($info){
            $m->to($info['receiver_email'])->subject('Pitch Your Concepts Awarded Job');
          });
          return "1";
        }else {
          return "0";
        }
      }
    }
}
