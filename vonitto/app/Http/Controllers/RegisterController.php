<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Session;
use Auth;

class RegisterController extends Controller
{
    public function __construct()
    {
      $this->middleware('guest', ['except' => ['logout', 'getLogout']]);
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function signup()
    {
        return view('register.signup');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function login()
    {
        return view('register.login');
    }

    /**
    * Log the user out of the application.
    *
    * @return \Illuminate\Http\Response
    */
    public function getLogout()
    {
         Session::flush();
         Auth::logout();
         return redirect('/');
    }
}
