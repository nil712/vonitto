<?php

namespace App\Http\Controllers;

define('IMAGE_DIR_PATH',base_path()."/resources/assets/upload/jobposter/document/");
define('IMAGE_SEEKER_DIR_PATH',base_path()."/resources/assets/upload/jobposter/seeker_documents/");

use App\Http\Requests;
use Illuminate\Http\Request;
use Validator;
use App\User;
ini_set('max_execution_time', 30000); //30000 seconds = 500 minutes
use DB;
use Session;
use Mail;
use App\PitchDocuments;
use App\JobPost;
use App\Category;
use App\GoogleAds;
use View,Auth;
use App\JobPitched;
use App\BookMarkEmail;
use URL;
use Illuminate\Support\Facades\Input;
class JobPostController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::all('name','id')->toArray();

        //dd($categories);
        return view('jobpost.jobpost',compact('categories'));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function login()
    {
        return view('jobposter.login');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request){
          $response=['data'=>[]];
          $statusCode=200;
          $data = array();
          $rules = array(
              'title' => 'required',
              'description' => 'required',
              'category_id' => 'required',
              'how_long' => 'required'
          );

          $validator = Validator::make($request->all(), $rules);

          if(!$validator->fails()) {
            $data = $request->all();
            // Multiple Job Post Image Uplod Code

             if($request->hasFile('job_logo')) {
                 $file = $request->file('job_logo');
                 $filename = time().rand(1,100).".".$file->getClientOriginalExtension();
                 $logPath = base_path()."/resources/assets/upload/joblogo/";
                 if($file->move($logPath, $filename))
                 {
                   $data['job_logo'] = $filename;
                 }
             }else {
                 // set default image as per category selected.
                 $jobLogoData = DB::table('job_default_logos')->select('logo')->where('job_category_id','=',$data['category_id'])->first();
                //  dd($jobLogoData);
                 $data['job_logo'] = $jobLogoData->logo;
             }
            unset($data['_token']);

            $data['project_deadline'] = date('Y-m-d h:m:s',strtotime($data['project_deadline']));
            $data['file']=$data['file_names'];
            $data['job_poster_id']=Auth::id();
            unset($data['file_names']);
            //  dd($data);
            if($jobposterdata = JobPost::create($data))
            {

              // dd($jobposterdata);
                $jobProjectID = 'PYC_'.date('Ymd')."_".$jobposterdata->id;
                $jobposterdata->job_project_id = $jobProjectID;
                $jobposterdata->save();
                $name = $jobposterdata->first_name." ".$jobposterdata->last_name;
                Session::flash('message', 'Success! Your job was submitted.');
                // send mail to registered user with verification link
                $response['status']="1";
                $response['message']='Success! Your job was submitted.';

            } else {
              $response['status']="0";
              $response['message']='Something Wrong.. Please try again later..!!';
            }
          } else {
            $response['status']="0";
            $response['message']=$validator->error()->first();
          }
          return response()->json($response,$statusCode);
    }

    /**
       * Display the specified resource.
       *
       * @param  int  $id
       * @return \Illuminate\Http\Response
       */
      public function show($id)
      {
        $jobseekerData = User::find($id);
        return view('jobposter.jobposterdetail')->with($jobseekerData);
      }

      /**
       * Update the specified resource in storage.
       *
       * @param  \Illuminate\Http\Request  $request
       * @param  int  $id
       * @return \Illuminate\Http\Response
       */
      public function update(Request $request, $id)
      {
          $response=['data'=>[]];
          $statusCode=200;
          try{
              $rules = array(
                  'title' => 'required',
                  'description' => 'required',
                  'category_id' => 'required',
                  'how_long' => 'required'
              );
              $validator = Validator::make($request->all(), $rules);
              if($validator->fails()) {
                $response['status']='0';
                $response['message']=$validator->errors()->first();
              }else{
                  if(JobPost::where('id',$id)->where('job_poster_id',Auth::id())->count()){
                      $jobObj=JobPost::find($id);



                       if($request->hasFile('job_logo')) {
                           $file = $request->file('job_logo');
                           $filename = time().rand(1,100).".".$file->getClientOriginalExtension();
                           $logPath = base_path()."/resources/assets/upload/joblogo/";
                           if($file->move($logPath, $filename))
                           {
                             $jobObj->job_logo = $filename;
                           }
                       }
                      unset($request->_token);
                      $jobObj->project_deadline = date('Y-m-d h:m:s',strtotime($request->project_deadline));
                      $jobObj->title=addslashes($request->title);
                      $jobObj->what_you_want=addslashes($request->what_you_want);
                      $jobObj->category_id=addslashes($request->category_id);
                      $jobObj->budget=addslashes($request->budget);
                      $jobObj->skills =addslashes($request->skills);
                      $jobObj->how_long = addslashes($request->how_long);
                      $jobObj->location_country = addslashes($request->location_country);
                      $jobObj->location_city = addslashes($request->location_city);
                      $jobObj->description=addslashes($request->description);
                      $jobObj->file=$request->file_names;
                      $jobObj->postcode=$request->postcode;
                      $jobObj->currency=$request->currency;
                      //  dd($data);
                      if($jobObj->save())
                      {
                          $user_id=Auth::id();
                          $output=DB::select("SELECT mark.*,user.email,user.first_name,user.last_name FROM `job_post` as post JOIN job_bookmark as mark ON post.id=mark.job_id JOIN users as user ON mark.seeker_id=user.id where mark.bookmark_status='bookmark' AND post.job_poster_id=$user_id AND post.id=$id" );
                          foreach($output as $bookmark_info){
                              $emailObj=new BookMarkEmail();
                              $emailObj->job_id=$id;
                              $emailObj->seeker_id=$bookmark_info->seeker_id;
                              $emailObj->poster_id=Auth::id();
                              $emailObj->email=$bookmark_info->email;
                              $emailObj->content=Auth::user()->first_name.' '.Auth::user()->last_name.' has updated following job.';
                              $emailObj->url=url('job/detail/'.$id);
                              $emailObj->save();
                          }

                          $response['status']='1';
                          $response['message']='Your job has been updated.';
                      } else {
                        $response['status']='0';
                        $response['message']='Something went wrong, please try again..';
                      }

                  }else{
                    $response['status']='0';
                    $response['message']='Unable to update post';
                  }
              }
          }catch(Exception $e)
          {
            $response['status']='0';
            //$response['message']='Bad Request Error';
            $response['message']=$e->getMessage();
          }finally{
            return response()->json($response,$statusCode);
          }
      }



            /**
             * Show the form for editing the specified resource.
             *
             * @param  int  $id
             * @return \Illuminate\Http\Response
             */
            public function edit($id)
            {
                  $isUserPost=false;
                  $postDetail=array();
                  $categories = Category::all('name','id')->toArray();
                  if(isset($id) && !empty($id) && is_numeric($id)){
                      //check post is active poster login user
                      if(JobPost::where('id',$id)->where('job_poster_id',Auth::id())->count())
                      {
                          $postDetail=JobPost::find($id);
                          $isUserPost=true;
                      }
                      return view('jobpost.editpost',compact('postDetail','isUserPost','categories'));
                  }else{
                    abort('404');
                  }
            }

      /**
       * Update the specified resource in storage.
       *
       * @param  \Illuminate\Http\Request  $request
       * @param  int  $id
       * @return \Illuminate\Http\Response
       */
      public function browse()
      {
        // $jobs = JobPost::where('job_post.progress_status','=',null)->join('users','job_post.job_poster_id','=','users.id')->leftJoin('job_bookmark','job_post.id','=','job_bookmark.job_id')->select('job_post.*','job_bookmark.seeker_id','job_bookmark.poster_id','job_bookmark.bookmark_status','users.company_name')->orderBy('job_post.id','DESC')->get();
        // $categories = Category::all('name','id')->toArray();
        //
        // return View::make("jobpost/browse", compact('jobs','categories'));
        $jobs = JobPost::where('job_post.progress_status', '=', null)->join('users', 'job_post.job_poster_id', '=', 'users.id')->leftJoin('job_bookmark', 'job_post.id', '=', 'job_bookmark.job_id')->select('job_post.*', 'job_bookmark.seeker_id', 'job_bookmark.poster_id', 'job_bookmark.bookmark_status', 'users.company_name')->orderBy('job_post.id', 'DESC')->get();
        $categories = Category::all('name', 'id')->toArray();
        $google_ads = GoogleAds::all()->toArray();

        return View::make("jobpost/browse", compact('jobs', 'categories', 'google_ads'));
      }

      public function browseSeeker(){
        $seekerDetail = User::where('role','=','JOB_SEEKER')->get();
        return View::make("jobpost/browse-seeker", compact('seekerDetail'));
      }

      public function searchBrowseSeeker(Request $request){
        $seekerDetail=array();
          $query=User::where('role','=','JOB_SEEKER');
          $query->where(function($query) use ($request){
              if($request->searchBy == 'location')
              {
                $query->orWhere('city','LIKE','%'.$request->search_keywords.'%');
                $query->orWhere('state','LIKE','%'.$request->search_keywords.'%');
                $query->orWhere('country','LIKE','%'.$request->search_keywords.'%');
              }
              else if($request->searchBy == 'name'){
                $query->orWhere(DB::raw('CONCAT(first_name, " ", last_name)'),'LIKE','%'.$request->search_keywords.'%');
              }else if($request->searchBy == 'postcode'){
                $query->orWhere('zip','LIKE','%'.$request->search_keywords.'%');
              }
              else if($request->searchBy == 'skill'){
                $query->orWhere('computer_skill','LIKE','%'.$request->search_keywords.'%');
              }
              else if($request->searchBy == 'experience'){
                $query->orWhere('experience','LIKE','%'.$request->search_keywords.'%');
              }
              else if($request->searchBy == 'qualification'){
                $query->orWhere('graduation','LIKE','%'.$request->search_keywords.'%');
                $query->orWhere('post_graduation','LIKE','%'.$request->search_keywords.'%');
              }
              else if($request->searchBy == 'project'){
                $query->orWhere('project_category_specialization','LIKE','%'.$request->search_keywords.'%');
              }
          });
          if($request->searchBy == 'rating'){
            // $query->leftJoin('job_review_rating','users.id','=','job_review_rating.seeker_id');
            // $query->select('users.*',DB::raw('round((ROUND(SUM(job_review_rating.rating)/COUNT(users.id),2))*2,0)/2 as rating'));
            // $query->groupBy('users.id');
            // $query->orderBy('rating','DESC');
            // $seekerDetail=$query->get();
          }else{
            $query->orderBy('first_name',$request->sort_by);
            $seekerDetail=$query->get();
          }
          // SELECT user.id,round((ROUND(SUM(rate.rating)/COUNT(user.id),2))*2,0)/2 as
          // rating FROM users as user left join job_review_rating as rate
          // ON user.id=rate.seeker_id  group by user.id ORDER BY rating DESC

          // dd($seekerDetail);
          $searchKeyWords['search_keywords'] = $request->search_keywords;
          $searchBy['searchBy']=$request->searchBy;
          $searchKeyWords['sort_by'] = $request->sort_by;

          return View::make("jobpost/browse-seeker", compact('seekerDetail','searchKeyWords','searchBy'));
      }

      public function searchbrowse(Request $request){

          $jobkeyword = $request['search_keywords'];
          $locationkeyword = $request['search_location'];
          $catid = $request['category_id'];

          if(isset($locationkeyword) && $locationkeyword != ''){
              $locationColumn = "job_post.location_city";
              $locationValue = $locationkeyword;
          }else{
              $locationColumn = "job_post.location_city";
              $locationValue = "";
          }

          if(isset($catid) && $catid != ''){
              $categoryValue = $catid;
          }else{
              $categoryValue = "";
          }
          $id=Auth::id();
          if(isset($request['sort_by']) && $request['sort_by'] != ''){
            if(isset($catid) && $catid != ''){
              $query = JobPost::join('users','job_post.job_poster_id','=','users.id')
                      ->leftJoin('job_bookmark',function($join) use ($id) {
                          $join->on('job_post.id','=','job_bookmark.job_id')
                          ->where('job_bookmark.seeker_id','=',$id);
                      });
                      if(isset($jobkeyword) && !empty($jobkeyword)){
                        $query->where(function($innerQuery) use ($jobkeyword){
                            $poster_names=explode(" ",$jobkeyword);
                            $poster_name=array("0"=>"","1"=>"");
                            if(isset($poster_names[0]) && !empty($poster_names[0]))
                                  $poster_name[0]=$poster_names[0];
                            if(isset($poster_names[1]) && !empty($poster_names[1]))
                                  $poster_name[1]=$poster_names[1];

                            $innerQuery->orWhere('job_post.title','LIKE','%'.$jobkeyword.'%');
                            $innerQuery->orWhere('job_post.budget','LIKE','%'.$jobkeyword.'%');
                            $innerQuery->orWhere('job_post.id','LIKE','%'.$jobkeyword.'%');
                            $innerQuery->orWhere('users.first_name','LIKE','%'.$poster_name[0].'%');
                            if(isset($poster_name[1]) && !empty($poster_name[1]))
                              $innerQuery->orWhere('users.last_name','LIKE','%'.$poster_name[1].'%');
                        });
                      }
                      $query->where($locationColumn, 'LIKE', '%' . $locationValue . '%');
                      $query->where('job_post.category_id',$categoryValue);
                      $query->select('users.company_name','job_post.*','job_bookmark.seeker_id','job_bookmark.poster_id','job_bookmark.bookmark_status');
                      $query->orderBy('job_post.project_deadline',$request['sort_by']);
                      $jobs=$query->get();
            }else{
              $query= JobPost::join('users','job_post.job_poster_id','=','users.id');
                      $query->leftJoin('job_bookmark',function($join) use ($id) {
                          $join->on('job_post.id','=','job_bookmark.job_id')
                          ->where('job_bookmark.seeker_id','=',$id);
                      });
                      if(isset($jobkeyword) && !empty($jobkeyword)){
                        $query->where(function($innerQuery) use ($jobkeyword){
                            $poster_names=explode(" ",$jobkeyword);
                            $poster_name=array("0"=>"","1"=>"");
                            if(isset($poster_names[0]) && !empty($poster_names[0]))
                                  $poster_name[0]=$poster_names[0];
                            if(isset($poster_names[1]) && !empty($poster_names[1]))
                                  $poster_name[1]=$poster_names[1];

                            $innerQuery->orWhere('job_post.title','LIKE','%'.$jobkeyword.'%');
                            $innerQuery->orWhere('job_post.budget','LIKE','%'.$jobkeyword.'%');
                            $innerQuery->orWhere('job_post.id','LIKE','%'.$jobkeyword.'%');
                            $innerQuery->orWhere('users.first_name','LIKE','%'.$poster_name[0].'%');
                            if(isset($poster_name[1]) && !empty($poster_name[1]))
                              $innerQuery->orWhere('users.last_name','LIKE','%'.$poster_name[1].'%');
                        });
                      }
                      $query->where($locationColumn, 'LIKE', '%' . $locationValue . '%');
                      $query->select('users.company_name','job_post.*','job_bookmark.seeker_id','job_bookmark.poster_id','job_bookmark.bookmark_status');
                      $query->orderBy('project_deadline',$request['sort_by']);
                      $jobs=$query->get();

            }
          }else {

            if(isset($catid) && $catid != ''){
              $query=JobPost::join('users','job_post.job_poster_id','=','users.id')
                      ->leftJoin('job_bookmark',function($join) use ($id) {
                          $join->on('job_post.id','=','job_bookmark.job_id')
                          ->where('job_bookmark.seeker_id','=',$id);
                      });
                      $query->where($locationColumn, 'LIKE', '%' . $locationValue . '%');
                      $query->where('category_id',$categoryValue);
                      if(isset($jobkeyword) && !empty($jobkeyword)){
                        $query->where(function($innerQuery) use ($jobkeyword){
                            $poster_names=explode(" ",$jobkeyword);
                            $poster_name=array("0"=>"","1"=>"");
                            if(isset($poster_names[0]) && !empty($poster_names[0]))
                                  $poster_name[0]=$poster_names[0];
                            if(isset($poster_names[1]) && !empty($poster_names[1]))
                                  $poster_name[1]=$poster_names[1];

                            $innerQuery->orWhere('job_post.title','LIKE','%'.$jobkeyword.'%');
                            $innerQuery->orWhere('job_post.budget','LIKE','%'.$jobkeyword.'%');
                            $innerQuery->orWhere('job_post.id','LIKE','%'.$jobkeyword.'%');
                            $innerQuery->orWhere('users.first_name','LIKE','%'.$poster_name[0].'%');
                            if(isset($poster_name[1]) && !empty($poster_name[1]))
                              $innerQuery->orWhere('users.last_name','LIKE','%'.$poster_name[1].'%');
                        });
                      }
                      $query->select('users.company_name','job_post.*','job_bookmark.seeker_id','job_bookmark.poster_id','job_bookmark.bookmark_status');
                      $jobs=$query->get();
            }else{

              $query = JobPost::join('users','job_post.job_poster_id','=','users.id');
                      $query->leftJoin('job_bookmark',function($join) use ($id) {
                          $join->on('job_post.id','=','job_bookmark.job_id')
                          ->where('job_bookmark.seeker_id','=',$id);
                      });
                      $query->where($locationColumn, 'LIKE', '%' . $locationValue . '%');
                      if(isset($jobkeyword) && !empty($jobkeyword)){
                        $query->where(function($innerQuery) use ($jobkeyword){
                            $poster_names=explode(" ",$jobkeyword);
                            $poster_name=array("0"=>"","1"=>"");
                            if(isset($poster_names[0]) && !empty($poster_names[0]))
                                  $poster_name[0]=$poster_names[0];
                            if(isset($poster_names[1]) && !empty($poster_names[1]))
                                  $poster_name[1]=$poster_names[1];

                            $innerQuery->orWhere('job_post.title','LIKE','%'.$jobkeyword.'%');
                            $innerQuery->orWhere('job_post.budget','LIKE','%'.$jobkeyword.'%');
                            $innerQuery->orWhere('job_post.id','LIKE','%'.$jobkeyword.'%');
                            $innerQuery->orWhere('users.first_name','LIKE','%'.$poster_name[0].'%');
                            if(isset($poster_name[1]) && !empty($poster_name[1]))
                              $innerQuery->orWhere('users.last_name','LIKE','%'.$poster_name[1].'%');
                        });
                      }
                      $query->select('users.company_name','job_post.*','job_bookmark.seeker_id','job_bookmark.poster_id','job_bookmark.bookmark_status');
                      $jobs=$query->get();


            }
          }
          // echo "<pre>";
          // print_r($jobs);exit;
          // if(isset($catid) && $catid != ''){
          //   $jobs = JobPost::where($column, 'LIKE', '%' . $value . '%')
          //           ->where($locationColumn, 'LIKE', '%' . $locationValue . '%')
          //           ->where('category_id',$categoryValue)
          //           ->get()->toArray();
          // }else{
          //   $jobs = JobPost::where($column, 'LIKE', '%' . $value . '%')
          //           ->where($locationColumn, 'LIKE', '%' . $locationValue . '%')
          //           ->get()->toArray();
          // }


          $searchKeyWords = $request->toArray();
          if($jobs){
             $categories = Category::all('name','id')->toArray();
             return View::make("jobpost/browse", compact('jobs','categories','searchKeyWords','catid'));
          }else{
             $categories = Category::all('name','id')->toArray();
             return View::make("jobpost/browse", compact('jobs','categories','searchKeyWords','catid'));
          }

      }

      /**
       * Update the specified resource in storage.
       *
       * @param  \Illuminate\Http\Request  $request
       * @param  int  $id
       * @return \Illuminate\Http\Response
       */
      public function jobDetail($id)
      {
        //$jobDetail = JobPost::find($id)->toArray();
        $userId = Auth::user()->id;

        $limit = 2;
        $offset = 0;
        $is_pitched = JobPitched::where('job_seeker_id','=',$userId)->where('job_id','=',$id)->count();

        // ->where($locationColumn, 'LIKE', '%' . $locationValue . '%')
        // ->select('job_post.*','job_bookmark.seeker_id','job_bookmark.poster_id','job_bookmark.bookmark_status')
        // ->get();
        $jobDetail = JobPost::leftJoin('job_bookmark','job_post.id','=','job_bookmark.job_id')->where('job_post.id', '=', "$id")->with('category','getposter')->select('job_post.*','job_bookmark.poster_id','.job_bookmark.seeker_id','job_bookmark.job_id','job_bookmark.bookmark_status')->first()->toArray();
        // dd($jobDetail);
        $jobDetail['file'] = explode(",",$jobDetail['file']);

        $pitchedRequest = DB::select("SELECT GROUP_CONCAT(job_pitched_documents.filename) as attached_documents,job_pitched.comments,job_post.id as job_id,job_pitched.job_seeker_id,users.first_name,users.last_name,job_post.job_project_id,job_post.title,job_post.description,job_post.created_at,case when(job_awarded.id>0) then '1' else '0' END as is_awarded,
        CASE WHEN (job_awarded.job_seeker_id = job_pitched.job_seeker_id)THEN '1' ELSE '0' END AS is_assigned FROM job_post
        RIGHT JOIN job_pitched ON job_pitched.job_id = job_post.id
        LEFT JOIN users ON users.id = job_pitched.job_seeker_id
        LEFT JOIN job_awarded ON job_awarded.job_id = job_post.id
        LEFT JOIN job_pitched_documents ON job_pitched.id=job_pitched_documents.pitched_id
        WHERE job_post.job_poster_id =".$userId." and job_post.id =".$id." group by job_pitched_documents.pitched_id order by created_at LIMIT ".$offset.",".$limit."");


        $totalRequest = DB::select("SELECT job_post.id as job_id,job_pitched.job_seeker_id,users.first_name,users.last_name,job_post.job_project_id,job_post.title,job_post.description,job_post.created_at,case when(job_awarded.id>0) then '1' else '0' END as is_awarded,
        CASE WHEN (job_awarded.job_seeker_id = job_pitched.job_seeker_id)THEN '1' ELSE '0' END AS is_assigned FROM job_post
        RIGHT JOIN job_pitched ON job_pitched.job_id = job_post.id
        LEFT JOIN users ON users.id = job_pitched.job_seeker_id
        LEFT JOIN job_awarded ON job_awarded.job_id = job_post.id
        WHERE job_post.job_poster_id =".$userId." and job_post.id =".$id." GROUP BY  job_pitched.id order by created_at");

        $requestPages = ceil(sizeof($totalRequest)/$limit);

        return View::make("jobpost/jobDetail", compact('jobDetail','is_pitched','pitchedRequest','requestPages'));
      }

      public function loadJobsPitcherList(){
        extract($_POST);
        if(!isset($page)){
          $page = 0;
        }else {
          $tpage = $page;
          $page = $page - 1;
        }
        $limit = 2;
        $offset = $page * $limit;

        $userId = Auth::user()->id;

        $pitchedRequest = DB::select("SELECT GROUP_CONCAT(job_pitched_documents.filename) as attached_documents,job_pitched.comments,job_post.id as job_id,job_pitched.job_seeker_id,users.first_name,users.last_name,job_post.job_project_id,job_post.title,job_post.description,job_post.created_at,case when(job_awarded.id>0) then '1' else '0' END as is_awarded,
        CASE WHEN (job_awarded.job_seeker_id = job_pitched.job_seeker_id)THEN '1' ELSE '0' END AS is_assigned FROM job_post
        RIGHT JOIN job_pitched ON job_pitched.job_id = job_post.id
        LEFT JOIN users ON users.id = job_pitched.job_seeker_id
        LEFT JOIN job_awarded ON job_awarded.job_id = job_post.id
        LEFT JOIN job_pitched_documents ON job_pitched.id=job_pitched_documents.pitched_id
        WHERE job_post.job_poster_id =".$userId." and job_post.id = ".$job_id." order by created_at LIMIT ".$offset.",".$limit."");

        // $myJobs = DB::select("SELECT job_post.id as job_id,job_post.job_project_id,job_post.project_deadline,job_post.budget,job_post.title,job_post.created_at,job_pitched.job_seeker_id,CASE WHEN(COUNT(job_pitched.id)>=0) THEN COUNT(job_pitched.id) ELSE 0 END AS pitched_request FROM `job_post`
        // LEFT JOIN job_pitched ON job_pitched.job_id = job_post.id
        // WHERE job_poster_id = ".$userId." GROUP BY job_post.id order by job_id DESC LIMIT ".$offset.",".$limit."");

        if($pitchedRequest!=null){
          return view('jobpost/loadPosterJobsPitches',compact('pitchedRequest'));
        }else{
          return "0";
        }

      }

      /**
       * Update the specified resource in storage.
       *
       * @param  \Illuminate\Http\Request  $request
       * @param  int  $id
       * @return \Illuminate\Http\Response
       */
      public function pitch(Request $request)
      {
        $id=$request->post_id;
        $userId = Auth::user()->id;
        $add_days = 30;
        $limit = 2;
        $offset = 0;
        $objUserDetail = User::find($userId);
        $response=['data'=>[]];
        $jobDetail = JobPost::where('id', '=', "$id")->with('category','getposter')->first();
        $pitchedDoc=array();


        $pitchedRequest = DB::select("SELECT job_post.id as job_id,job_pitched.job_seeker_id,users.first_name,users.last_name,job_post.job_project_id,job_post.title,job_post.description,job_post.created_at,case when(job_awarded.id>0) then '1' else '0' END as is_awarded,
        CASE WHEN (job_awarded.job_seeker_id = job_pitched.job_seeker_id)THEN '1' ELSE '0' END AS is_assigned FROM job_post
        RIGHT JOIN job_pitched ON job_pitched.job_id = job_post.id
        LEFT JOIN users ON users.id = job_pitched.job_seeker_id
	      LEFT JOIN job_awarded ON job_awarded.job_id = job_post.id
        WHERE job_post.job_poster_id =".$userId." and job_post.id =".$id." order by created_at LIMIT ".$offset.",".$limit."");

        $totalRequest = DB::select("SELECT job_post.id as job_id,job_pitched.job_seeker_id,users.first_name,users.last_name,job_post.job_project_id,job_post.title,job_post.description,job_post.created_at,case when(job_awarded.id>0) then '1' else '0' END as is_awarded,
        CASE WHEN (job_awarded.job_seeker_id = job_pitched.job_seeker_id)THEN '1' ELSE '0' END AS is_assigned FROM job_post
        RIGHT JOIN job_pitched ON job_pitched.job_id = job_post.id
        LEFT JOIN users ON users.id = job_pitched.job_seeker_id
	      LEFT JOIN job_awarded ON job_awarded.job_id = job_post.id
        WHERE job_post.job_poster_id =".$userId." and job_post.id =".$id." order by created_at");

        $requestPages = (int)ceil(sizeof($totalRequest)/$limit);
        // first check is already pitched. Then redirect to detail page agin
        $isAlreadyPitched = JobPitched::where('job_seeker_id','=',$userId)->where('job_id','=',$id)->count();
        if($isAlreadyPitched>0){
          $response['status']="0";
          $response['message']="You have already pitched that job";
        }else{
          if($objUserDetail['free_pitch']=="1"){
            $objUserDetail['free_pitch'] = 0;
            $objUserDetail->update();

            $objJobPitched = new JobPitched;
            $obj = new PitchDocuments;
            $objJobPitched->job_seeker_id = $userId;
            $objJobPitched->job_id = $id;
            $objJobPitched->free_request = "1";
            if(isset($request->comments) && !empty($request->comments))
                  $objJobPitched->comments = $request->comments;
            $objJobPitched->save();
            if(isset($request->file_names) && !empty($request->file_names))
            {
                $files=explode(",",$request->file_names);
                foreach($files as $file){
                  $pitchedDoc=array("pitched_id"=>$objJobPitched->id,"filename"=>$file);
                }
                $obj->insert($pitchedDoc);
            }
            $info['name']=Auth::user()->first_name." ".Auth::user()->last_name;
            $info['phone']=Auth::user()->phone;
            $info['receiver_email']=$user_detail->email;
            $info['email']=Auth::user()->email;
            $info['job_title']=$request->job_title;
            Mail::send('emails._job_seeker_pitched', ['info' =>$info], function ($m) use ($info){
              $m->to($info['receiver_email'])->subject('Pitch Your Concepts Pitch Job');
            });
            $pitched = "yes";
            $response['status']="1";
            $response['message']="Pitched has be done successfully";
          }else {
            // check balance is available
              if($objUserDetail->available_pitch>0 && $objUserDetail->available_pitch!=null){
                $objUserDetail->available_pitch = ($objUserDetail->available_pitch!=null)?(($objUserDetail->available_pitch-1==0)?null:$objUserDetail->available_pitch-1):null;
                $objUserDetail->update();
                $objJobPitched = new JobPitched;
                $obj = new PitchDocuments;
                $objJobPitched->job_seeker_id = $userId;
                $objJobPitched->job_id = $id;
                if(isset($request->comments) && !empty($request->comments))
                      $objJobPitched->comments = $request->comments;
                $objJobPitched->save();
                if(isset($request->file_names) && !empty($request->file_names))
                {
                    $files=explode(",",$request->file_names);
                    foreach($files as $file){
                      $pitchedDoc=array("pitched_id"=>$objJobPitched->id,"filename"=>$file);
                    }
                    $obj->insert($pitchedDoc);
                }
                $info['name']=$user_detail->first_name." ".$user_detail->last_name;
                $info['phone']=$user_detail->phone;
                $info['receiver_email']=$user_detail->email;
                $info['email']=Auth::user()->email;
                $info['job_title']=$request->job_title;
                Mail::send('emails._job_seeker_pitched', ['info' =>$info], function ($m) use ($info){
                  $m->to($info['receiver_email'])->subject('Pitch Your Concepts Pitch Job');
                });
                $pitched = "yes";
                $response['status']="1";
                $response['message']="Pitched has be done successfully";
              }else {
                $response['status']="0";
                $response['message']="Your job pitched limit is over please purchase package to pitch more jobs";
              }
            }
        }
        // free pitch available
        return response()->json($response,200);
      }


      public function sentUpdateEmail(){
        $mailInformation=DB::select("select * from job_bookmark_email where mail_status='unsend' LIMIT 50");
        foreach($mailInformation as $info){
            Mail::send('emails._job_update', ['info' =>$info], function ($m) use ($info){
              $m->to($info->email)->subject('Pitch Your Concepts Job Updated');
            });
            BookMarkEmail::where('id',$info->id)->update(['mail_status'=>'sent']);
        }
      }

      /**
       *
       * Developer Name : Hitesh Tank
       * Date : 16-Aug-2016
       * @return \Illuminate\Http\Response
       * Description : upload files
       */
       public function uploadFiles(Request $request){

         $statusCode = 200;
         $file_names=array();
         if($request->hasFile('file')){
             // getting all of the post data
             $files = Input::file('file');
             // Making counting of uploaded images
             $file_count = count($files);
             // start count how many uploaded
             $uploadcount = 0;

             foreach($files as $file) {
               $filename = md5(microtime()).".".$file->getClientOriginalExtension();
               if($file->move(IMAGE_DIR_PATH,$filename)){
                chmod(IMAGE_DIR_PATH.$filename, 0777);
                $file_names[]=$filename;
               }
             }
             return response()->json(array('success' => true,'data'=>$file_names,'message'=>'file uploaded successfully'));
         }
       }

       public function deleteFile(Request $request){
         $statusCode=200;
         $response=['data'=>[]];
         try{
            $obj=JobPost::find($request->post_id);
            $files=$obj->file;
            $files=explode(",",$files);
            if(($key = array_search($request->file_name, $files)) !== false) {
                unset($files[$key]);
            }
            $files=implode(",",$files);
            $obj->file=$files;
            $obj->save();
            if($obj){
              $response['data']=$obj->file;
              $response['status']="1";
            }

         }catch(Exception $e){
           $statusCode=400;
           $response['message']='Bad Request';
           $response['status']="0";
         }finally{
           return response()->json($response,$statusCode);
         }
       }

       /**
        *
        * Developer Name : Hitesh Tank
        * Date : 29-Aug-2016
        * @return \Illuminate\Http\Response
        * Description : upload seeker files
        */
        public function uploadseekerFiles(Request $request){

          $statusCode = 200;
          $file_names=array();
          if($request->hasFile('file')){
              // getting all of the post data
              $files = Input::file('file');
              // Making counting of uploaded images
              $file_count = count($files);
              // start count how many uploaded
              $uploadcount = 0;

              foreach($files as $file) {
                $filename = md5(microtime()).".".$file->getClientOriginalExtension();
                if($file->move(IMAGE_SEEKER_DIR_PATH,$filename)){
                 chmod(IMAGE_SEEKER_DIR_PATH.$filename, 0777);
                 $file_names[]=$filename;
                }
              }
              return response()->json(array('success' => true,'data'=>$file_names,'message'=>'file uploaded successfully'));
          }
        }
    }
