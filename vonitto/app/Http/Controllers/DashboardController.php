<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\JobPost;
use App\Category;
use App\PageCms;
use View;
use App\User;

class DashboardController extends Controller
{
    public function index(){
      $category_count = Category::all()->count();
      $page_count = PageCms::all()->count();
      $job_count = JobPost::all()->count();
      $user_count = User::all()->count();
      //dd($category_count);
      return View::make("admin.welcome", compact('user_count','category_count','page_count','job_count'));
      //return view('admin.welcome');
    }

    public function edit($id){
      $userData = User::find($id);
      return View::make("admin.profileEdit", compact('userData'));

    }

}
