<?php
namespace App\Http\Controllers\admin;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Routing\UrlGenerator;
use App\User;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;
use DB;
use Redirect, Input, Hash;
use Response;
use Eventviva\ImageResize;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    public function index()
    {
        if (Auth::check()) {
            $id   = Auth::user()->id;
            $user = DB::table('users')->where('id', $id)->first();
            $ProjectName = \Config::get('constants.PROJECT_NAME');

            return view('admin.users.users', ['PageTitle' => 'Users | '.$ProjectName])->with('users', $user);
        } else {
            return Redirect('/');
        }
    }

    /**
     * @Action:
     * @Author: LN
     * @Created: March 16 2016
     * @Modified By:
     * @Comment:
     * @Todo:
     */
    public function loadNewUser()
    {
        if (Auth::check()) {
            $id   = Auth::user()->id;
            $user = DB::table('users')->where('id', $id)->first();
            $ProjectName = \Config::get('constants.PROJECT_NAME');

            return view('admin.users.add', ['PageTitle' => 'Add New User | '.$ProjectName])->with('users', $user);
        } else {
            return Redirect('/');
        }
    }

    /**
     * @Action:
     * @Author: LN
     * @Created: March 16 2016
     * @Modified By:
     * @Comment:
     * @Todo:
     */
    public function addUser(Guard $auth, Request $request)
    {
        // GET ALL THE INPUT DATA , $_GET,$_POST,$_FILES.
        $Input = Input::all();
        //dd($Input); die;

        // VALIDATION RULES
        $rules = array(
            'email' => 'required|email|max:254|unique:users',
            'password' => 'required|min:6|confirmed',
            'password_confirmation' => 'required|min:6',
            'first_name' => 'required|max:254',
            'last_name' => 'required|max:254',
            'file' => 'required|image|max:3000',
        );

        // PASS THE INPUT AND RULES INTO THE VALIDATOR
        $validator = Validator::make($request->all(), $rules);
        // CHECK GIVEN DATA IS VALID OR NOT
        if ($validator->fails()) {
            // Get the error messages from the validator
            return $validator->messages();
        } else {
            include base_path().'/vendor/eventviva/php-image-resize/src/ImageResize.php';

            if ($request->hasFile('image')) {
                $File = $request->file('image');
                $Input = array('image' => $File);
                $imageName = hash('ripemd160', time()).".".$File->getClientOriginalExtension();

                $Day = date('d');
                $Month = date('m');
                $Year = date('Y');

                $request->file('file')->move(public_path() .'/upload/userimage/'.$Year.'/'.$Month.'/'.$Day.'/',$imageName);
                chmod(public_path() .'/upload/userimage/'.$Year.'/'.$Month.'/'.$Day, 0777);
                if (!file_exists(public_path().'/upload/userimage/'.$Year.'/'.$Month.'/'.$Day.'/thumbnail')) {
                    mkdir(public_path().'/upload/userimage/'.$Year.'/'.$Month.'/'.$Day.'/thumbnail', 0777, true);
                }
                $image = new ImageResize(url().'/public/upload/userimage/'.$Year.'/'.$Month.'/'.$Day.'/'.$imageName);
                $image->scale(50);
                $image->save(public_path().'/upload/userimage/'.$Year.'/'.$Month.'/'.$Day.'/thumbnail/'.$imageName);

                $ImagePath = $Year.'/'.$Month.'/'.$Day.'/'.$imageName;
            }

            $CurrentData = date(\Config::get('constants.DATE_YYYY_MM_DD_H_M_S_24'));

            $Users = new User();
            $Users->email = $request->email;
            $Users->password = Hash::make($request->password);
            $Users->first_name = $request->first_name;
            $Users->last_name = $request->last_name;
            $Users->role = 'sAdmin';
            $Users->status = '1';
            $Users->image = $ImagePath;
            $Users->save();
        }
    }

    /**
     * @Action:
     * @Author: LN
     * @Created: March 16 2016
     * @Modified By:
     * @Comment:
     * @Todo:
     */
    public function getDataTable()
    {
        $Users    = new User();
        return $userList = $Users->GetUsers($_REQUEST);
    }

    /**
     * @Action:
     * @Author: LN
     * @Created: March 16 2016
     * @Modified By:
     * @Comment:
     * @Todo:
     */
    public function changeStatus()
    {
        if (ctype_digit($_POST['user_id']) && ctype_digit($_POST['status'])) {
            $Users    = new User();
            $status = $Users->changeStatus($_POST['user_id'], $_POST['status']);
            echo json_encode(array(
                "status" => TRUE
            ));
        }
    }

    /**
     * @Action:
     * @Author: LN
     * @Created: March 16 2016
     * @Modified By:
     * @Comment:
     * @Todo:
     */
    public function getUserData($id)
    {
        $Users    = new User();
        $UserData = $Users->getUserData($id);
        $ProjectName = \Config::get('constants.PROJECT_NAME');

        if (!empty($UserData)) {
            $PageTitle = 'Edit User | '.$ProjectName;
        } else {
            $PageTitle = 'Oops! Page not found | '.$ProjectName;
        }

        return view('admin.users.edit', ['PageTitle' => $PageTitle], compact('UserData'));
    }

    /**
     * @Action:
     * @Author: LN
     * @Created: March 16 2016
     * @Modified By:
     * @Comment:
     * @Todo:
     */
    public function viewUserData($id)
    {
        $Users    = new User();
        $UserData = $Users->getUserData($id);
        $ProjectName = \Config::get('constants.PROJECT_NAME');

        if (!empty($UserData)) {
            $PageTitle = 'User Profile | '.$ProjectName;
        } else {
            $PageTitle = 'Oops! Page not found | '.$ProjectName;
        }

        return view('admin.users.view', ['PageTitle' => $PageTitle], compact('UserData'));
    }

    /**
     * @Action:
     * @Author: LN
     * @Created: March 16 2016
     * @Modified By:
     * @Comment:
     * @Todo:
     */
    public function updateGeneralData(Guard $auth, Request $request)
    {
        $rules = array(
            //'ID' => 'required|email|max:254|unique:user',
            'password' => 'min:6|confirmed',
            'password_confirmation' => 'min:6',
            'first_name' => 'required|max:254',
            'last_name' => 'required|max:254',
        );

        // Validate against the inputs from our form
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            // Get the error messages from the validator
            return $validator->messages();
        } else {
            $obj = User::find($request->id);
            $Password = $request->password;
            $obj->email = $request->email;
            $obj->first_name = $request->first_name;
            if(isset($Password) && !empty($Password)) {
                $obj->password = Hash::make($Password);
            }
            $obj->last_name = $request->last_name;
            $obj->save();
        }
    }

    /**
     * @Action:
     * @Author: LN
     * @Created: March 16 2016
     * @Modified By:
     * @Comment:
     * @Todo:
     */
    public function updateMediaData(Guard $auth, Request $request)
    {
        // GET ALL THE INPUT DATA , $_GET,$_POST,$_FILES.
        $Input = $request->all();

        // VALIDATION RULES
        $rules = array(
            'image' => 'required|image|mimes:jpeg,jpg,png,bmp,gif,svg',
        );

        // PASS THE INPUT AND RULES INTO THE VALIDATOR
        $validator = Validator::make($request->all(), $rules);
        // CHECK GIVEN DATA IS VALID OR NOT
        if ($validator->fails()) {
            // Get the error messages from the validator
            return $validator->messages();
        } else {
            include base_path().'/vendor/eventviva/php-image-resize/src/ImageResize.php';

            if ($request->hasFile('image')) {
                $File = $request->file('image');
                $Input = array('image' => $File);
                $imageName = hash('ripemd160', time()).".".$File->getClientOriginalExtension();

                $Day = date('d');
                $Month = date('m');
                $Year = date('Y');

                $request->file('image')->move(public_path() .'/upload/userimage/'.$Year.'/'.$Month.'/'.$Day.'/',$imageName);
                chmod(public_path() .'/upload/userimage/'.$Year.'/'.$Month.'/'.$Day, 0777);
                if (!file_exists(public_path().'/upload/userimage/'.$Year.'/'.$Month.'/'.$Day.'/thumbnail')) {
                    mkdir(public_path().'/upload/userimage/'.$Year.'/'.$Month.'/'.$Day.'/thumbnail', 0777, true);
                }
                $image = new ImageResize(url("/").'/public/upload/userimage/'.$Year.'/'.$Month.'/'.$Day.'/'.$imageName);
                $image->scale(50);
                $image->save(public_path().'/upload/userimage/'.$Year.'/'.$Month.'/'.$Day.'/thumbnail/'.$imageName);

                $ImagePath = $Year.'/'.$Month.'/'.$Day.'/'.$imageName;

                $obj = User::find($request->id);
                $obj->image = $ImagePath;
                $obj->save();
            }
        }
    }

    /**
     * @Action:
     * @Author: LN
     * @Created: March 16 2016
     * @Modified By:
     * @Comment:
     * @Todo:
     */
    public function listData()
    {
        $users    = new User();
        $userList = $users->getUsers();
        $ProjectName = \Config::get('constants.PROJECT_NAME');

        return view('pages.admin.users.userListing', ['PageTitle' => 'Users | '.$ProjectName], compact('userList'));
    }

    /**
     * @Action:
     * @Author: LN
     * @Created: March 16 2016
     * @Modified By:
     * @Comment:
     * @Todo:
     */
    public function deleteRecord()
    {
        if (ctype_digit($_POST['user_id'])) {
            $Users    = new User();
            $status = $Users->deleteUser($_POST['user_id']);
            echo json_encode(array(
                "status" => TRUE
            ));
        }
    }

    /**
     * @Action:
     * @Author: LN
     * @Created: March 16 2016
     * @Modified By:
     * @Comment:
     * @Todo:
     */
    public function getUserView()
    {
        $html = "";
        if (isset($_POST['user_id']) && !empty($_POST['user_id']) && !empty($_POST['action'])) {
            $users        = new User();
            $partyModal   = new Parties();
            $blogModal    = new Blogs();
            $userData     = $users->viewUser($_POST['user_id']);
            $totalBlogs   = $blogModal->getTotalUserBlog($userData[0]->user_id);
            $totalParties = $partyModal->getTotalUserParties($userData[0]->user_id);

            $url = "";
            if (empty($userData[0]->user_image) && !isset($userData[0]->user_image)) {
                if ($userData[0]->gender == 'Male') {
                    $url = asset('resources/assets/admin/dist/img/user-male.png');
                } else {
                    $url = asset('resources/assets/admin/dist/img/user-female.png');
                }
            } else {
                if (in_array("http", parse_url($userData[0]->user_image)) || in_array("https", parse_url($userData[0]->user_image))) {
                    $url = $userData[0]->user_image;
                } else {

                    $url = url() . '/public/uploads/user_picture/' . $userData[0]->user_image;
                }
            }

            // User Profile
            $html .= '<div class="row">';
            $html .= '<div class="col-md-3">';
            $html .= '<div class="box box-primary">';
            $html .= '<div class="box-body box-profile">';
            $html .= '<img alt="User profile picture" src="' . $url . '" class="profile-user-img img-responsive img-circle">';
            $html .= '<h3 class="profile-username text-center">' . $userData[0]->firstname . ' ' . $userData[0]->lastname . '</h3>';
            $userType = "";
            if (!empty($userData[0]->facebook_id)) {
                if (!empty($userData[0]->username)) {
                    $userType = "Facebook User(" . $userData[0]->username . ")";
                } else {
                    $userType = "Facebook User";
                }
            } else if (!empty($userData[0]->twitter_id)) {
                if (!empty($userData[0]->username)) {
                    $userType = "Twitter User(" . $userData[0]->username . ")";
                } else {
                    $userType = "Twitter User";
                }
            } else {
                $userType = "Normal User(" . $userData[0]->username . ")";
            }

            $html .= '<p class="text-muted text-center">' . $userType . '</p>';
            $html .= '<ul class="list-group list-group-unbordered">';
            $html .= '<li class="list-group-item">';
            $html .= '<b>Total Blog</b> <a class="pull-right">' . $totalBlogs . '</a>';
            $html .= '</li>';
            $html .= '<li class="list-group-item">';
            $html .= '<b>Total Party</b> <a class="pull-right">' . $totalParties . '</a>';
            $html .= '</li>';
            $html .= '</ul>';
            $html .= '</div>';
            $html .= '</div>';
            $html .= '</div>';
            //User Profile

            //User body
            $html .= '<div class="col-md-9">';
            $html .= '<div class="box box-primary">';
            $html .= '<div class="box-header with-border">';
            $html .= '<h3 class="box-title">About ' . $userData[0]->firstname . ' ' . $userData[0]->lastname . '</h3>';
            $html .= '</div>';
            $html .= '<div class="box-body">';
            $html .= '<strong><i class="fa fa-fw fa-envelope-o margin-r-5"></i> Email</strong>';
            $html .= '<p class="text-muted">';
            if (!empty($userData[0]->email))
                $html .= '<a href="mailto:' . $userData[0]->email . '">';
            $html .= $userData[0]->email . '</a>';
            $html .= '</p><hr>';
            $html .= '<strong><i class="fa fa-fw fa-intersex margin-r-5"></i> Gender</strong>';
            if (!empty($userData[0]->gender))
                $html .= '<p class="text-muted">' . $userData[0]->gender . '</p>';

            $html .= '<hr>';
            $html .= '<strong><i class="fa  fa-fw fa-map-marker margin-r-5"></i> Address</strong>';

            if (!empty($userData[0]->address))
                $html .= '<p class="text-muted">' . $userData[0]->address . ' ,' . $userData[0]->city . ' ' . $userData[0]->zipcode . ' ,' . $userData[0]->state . ' ,' . $userData[0]->country . '</p>';

            $html .= "<hr>";
            $html .= '<strong><i class="fa fa-fw fa-phone margin-r-5"></i> Phone Number</strong>';
            if (!empty($userData[0]->mobile_number))
                $html .= '<p class="text-muted">' . $userData[0]->mobile_number . '</p>';
            $html .= '<hr>';
            $html .= '<strong><i class="fa fa-fw fa-battery-4 margin-r-5"></i> Status</strong>';

            if ($userData[0]->status == 'Active')
                $html .= '<p><span class="label label-success">Active</span></p></div>';
            else
                $html .= '<p><span class="label label-danger">Inactive</span></p></div>';

            echo $html;
        }
    }
}
