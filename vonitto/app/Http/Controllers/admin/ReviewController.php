<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Exception;
use App\SeekerReviewRating;
use Input;
use Validator;
use Auth;
use DB;
class ReviewController extends Controller
{

  public function index()
  {
      $ProjectName = \Config::get('constants.PROJECT_NAME');
      return view('admin.review.reviews',['PageTitle' => 'Review & Rating | '.$ProjectName]);
  }
  /**
   * @Author: Hitesh Tank
   * @Created: Aug-10-2016
   * @Modified By:
   * @Comment:
   * @Todo: get All review and rating
   */

  public function getAll(Request $request){
      $RequestData = $request;
    $Columns     = array(
        // Datatable column index  => Database column name
        0 => 'job.title',
        1 => 'seeker.first_name',
        2 => 'poster.first_name',
        3 => 'review.rating'

    );

    $TotalData     = DB::select("SELECT count(*) FROM job_review_rating as review LEFT JOIN
    job_post as job ON review.job_post_id=job.id JOIN users as seeker ON
    review.seeker_id=seeker.id LEFT JOIN users as poster ON
    review.poster_id=poster.id");
    $TotalFiltered = $TotalData; // When there is no search parameter then total number rows = total number filtered rows.

    $sql = "SELECT review.rating,review.review,review.seeker_id,review.id,review.job_post_id,
           review.poster_id,job.title,seeker.first_name as
           seeker_first_name,seeker.last_name as seeker_last_name,
           poster.first_name as poster_first_name,poster.last_name as
           poster_last_name FROM job_review_rating as review LEFT JOIN
           job_post as job ON review.job_post_id=job.id JOIN users as seeker ON
           review.seeker_id=seeker.id LEFT JOIN users as poster ON
           review.poster_id=poster.id ";

    if (!empty($RequestData['search']['value'])) {
        // If there is a search parameter, $RequestData['search']['value'] contains search parameter
        $sql .= " WHERE (seeker.last_name LIKE '" . $RequestData['search']['value'] . "%' ";
        $sql .= " OR poster.first_name LIKE '" . $RequestData['search']['value'] . "%' ";
        $sql .= " OR poster.last_name LIKE '" . $RequestData['search']['value'] . "%' ";
        $sql .= " OR job.title LIKE '" . $RequestData['search']['value'] . "%' ";
        $sql .= " OR review.rating LIKE '" . $RequestData['search']['value'] . "%' ";
        $sql .= " OR seeker.first_name LIKE '" . $RequestData['search']['value'] . "%') ";
    }

    $TempData      = DB::select($sql);
    $TotalFiltered = count($TempData);
    $sql .= " ORDER BY " . $Columns[$RequestData['order'][0]['column']] . "   " . $RequestData['order'][0]['dir'] . "  LIMIT " . $RequestData['start'] . " ," . $RequestData['length'] . "   ";

    $GetData = DB::select($sql);
    $data    = array();
    foreach ($GetData as $row) { // Preparing an array

        // $Edit = '<td style="padding: 2px 2px;"><a title="Edit" style="width:90px;"  href="'.url("/").'/admin/pagecms/'.$row->id.'/edit" data-page-id="'.$row->id.'"> <i class="fa fa-edit" style="color:#000;"></i></a></td>';
        //
        // $Delete = '<td style="padding: 2px 2px;"><a title="Delete" style="width:90px;" class="deleteCourseUnitBtn" href="javascript:void(0);" data-page-id="'.$row->id.'"> <i class="fa fa-trash-o" style="color:#000;"></i></a></td>';

          $NestedData   = array();
          $NestedData[] = (isset($row->title) && !empty($row->title)) ? stripslashes($row->title) : '-';
          $NestedData[] = $row->seeker_first_name.' '.$row->seeker_last_name;
          $NestedData[] = $row->poster_first_name.' '.$row->poster_last_name;
          $NestedData[] = $row->rating;
          $NestedData[] = '<td style="padding: 2px 2px;"><a title="View" data-review="'.stripslashes($row->review).'" data-job="'.stripslashes($row->title).'" data-rating="'.$row->rating.'" data-poster="'.$row->poster_first_name.' '.$row->poster_last_name.'" data-seeker="'.$row->seeker_first_name.' '.$row->seeker_last_name.'" class="viewDetail" style="width:90px;"  href="javascript:;"> <i class="fa fa-eye" style="color:#000;"></i></a></td> &nbsp; <td style="padding: 2px 2px;"><a title="Delete" style="width:90px;" class="delete" href="javascript:void(0);" data-page-id="'.$row->id.'"> <i class="fa fa-trash-o" style="color:#000;"></i></a></td>';
          $data[] = $NestedData;

      }

      $JsonData = array(
          "draw" => intval($RequestData['draw']), // For every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
          "recordsTotal" => intval($TotalData), // Total number of records
          "recordsFiltered" => intval($TotalFiltered), // Total number of records after searching, if there is no searching then totalFiltered = totalData
          "data" => $data // Total data array
      );
    echo json_encode($JsonData); // Send data as json format
  }

  /**
   * Remove the specified resource from storage.
   * Developer Name : Hitesh tank
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    try{
        $response = [ 'data' => [] ];
        $statusCode = 200;
        $query = SeekerReviewRating::find($id);

        if($query)
        {
          if($query->delete())
          {
            $response['status'] = "1";
            $response['message'] = "Removed Successfull";
          }
        } else {
          $response['status'] = "0";
          $response['message'] = "No Review found";
        }

    } catch (Exception $e){
        $response['status'] = 0;
        $response['message'] = "Bad Request Error";
    } finally{
        return response()->json($response, $statusCode);
    }
  }
}
