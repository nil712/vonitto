<?php

namespace App\Http\Controllers\admin;
define('IMAGE_DIR_POSTER_PATH',base_path()."/resources/assets/upload/jobposter");
define('IMAGE_DIR_SEEKER_PATH',base_path()."/resources/assets/upload/jobseeker");

use Illuminate\Http\Request;
use App\UserDetail;
use App\User;
use Input;
use App\Http\Requests;
use Redirect,Hash;
use Validator;
use App\Http\Controllers\Controller;

class UserDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

      $ProjectName = \Config::get('constants.PROJECT_NAME');
      return view('admin.user_detail.userdetail',['PageTitle' => 'User Detail | '.$ProjectName]);  //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $ProjectName = \Config::get('constants.PROJECT_NAME');
      $user_model=new User();
      $course_model=new UserDetail();
      return view('admin.user_detail.addUserDetail',['PageTitle' => 'Add User Detail| '.$ProjectName]);  //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        include base_path().'/vendor/eventviva/php-image-resize/src/ImageResize.php';

      $userdetail_model=new UserDetail();
      $statusCode=200;
      $response=['data'=>[]];

        $rules = array(
            'email'  => 'required',
          );

          $validator = Validator::make($request->all(), $rules);

          if ($validator->fails()) {
              // get the error messages from the validator
              $messages = $validator->messages();
              $response['status']='0';
              $response['error']=$messages;
          }else{
              if(isset($request->company_name) && !empty($request->company_name)){
                     $company_name=$request->company_name;
              }else{
                   $company_name="";
              }

              if(isset($request->company_link) && !empty($request->company_link)){
                     $company_link=$request->company_link;
              }else{
                   $company_link="";
              }
              if(isset($request->register_as) && !empty($request->register_as)){
                     $register_as =$request->register_as;
              }else{
                     $register_as = "";
              }
              $userdetail_model->role=$request->job_type;
              $userdetail_model->password=bcrypt($request->password);
              $userdetail_model->first_name=$request->first_name;
              $userdetail_model->last_name=strip_tags($request->last_name);
              $userdetail_model->email = $request->email;
              $userdetail_model->register_as = $register_as;
              $userdetail_model->phone = $request->phone;
              $userdetail_model->city = $request->city;
              $userdetail_model->state = $request->state;
              $userdetail_model->country = $request->country;
              $userdetail_model->street_address1 = $request->address1;
              $userdetail_model->street_address2 = $request->address2;
              $userdetail_model->company_name = $company_name;
              $userdetail_model->zip = $request->postcode;
              $userdetail_model->company_link = $company_link;


              if ($request->hasFile('image')) {
                $file = $request->file('image');
                $filename = time().rand(1,100).".".$file->getClientOriginalExtension();
                if($request->job_type=="JOB_POSTER"){
                    if($file->move(IMAGE_DIR_POSTER_PATH, $filename))
                    {
                      $data['image'] = $filename;
                      $userdetail_model->image = $filename;
                    }
                }else{
                    if($file->move(IMAGE_DIR_SEEKER_PATH, $filename))
                    {
                      $data['image'] = $filename;
                      $userdetail_model->image = $filename;
                    }
                }
            }

              if($userdetail_model->save()){
                $response['status']=1;
                $response['message']='User Detail has been added successfully';
              }else{
                $response['status']=0;
                $response['error']='Opps, something went wrong, please try again..';
              }
          }
          return response()->json($response,$statusCode);
    }

	    /**
     * @Action:
     * @Author: LN
     * @Created: March 16 2016
     * @Modified By:
     * @Comment:
     * @Todo:
     */
    public function viewUserData($id)
    {
        $Users    = new UserDetail();
        $UserData = $Users->getUserData($id);

        $ProjectName = \Config::get('constants.PROJECT_NAME');

        if (!empty($UserData)) {
            $PageTitle = 'User Profile | '.$ProjectName;
        } else {
            $PageTitle = 'Oops! Page not found | '.$ProjectName;
        }

        return view('admin.user_detail.view', ['PageTitle' => $PageTitle], compact('UserData'));
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      if(isset($id) && !empty($id)){
        $EditUserData=User::find($id);
        $ProjectName = \Config::get('constants.PROJECT_NAME');;
        return view('admin.user_detail.editUserDetail',compact('EditUserData'),['PageTitle' => 'Edit User Detail| '.$ProjectName]);
      }else{
        return Redirect('admin/userdetail');
      }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function update(Request $request, $id)
     {

         include base_path().'/vendor/eventviva/php-image-resize/src/ImageResize.php';

       $statusCode=200;
       $response=['data'=>[]];

         $rules = array(
             'email'  => 'required'
           );

          $validator = Validator::make($request->all(), $rules);

           if ($validator->fails()) {
               // get the error messages from the validator
               $messages = $validator->messages();
               $response['status']='0';
               $response['error']=$messages;
           }else{
               $userdetail_model=UserDetail::find($id);

               if(isset($request->company_name) && !empty($request->company_name)){
                      $company_name=$request->company_name;
               }else{
                    $company_name="";
               }

               if(isset($request->company_link) && !empty($request->company_link)){
                      $company_link=$request->company_link;
               }else{
                    $company_link="";
               }
               if(isset($request->register_as) && !empty($request->register_as)){
                      $register_as =$request->register_as;
               }else{
                      $register_as = "";
               }

               $userdetail_model->password=bcrypt($request->password);
               $userdetail_model->first_name=$request->first_name;
               $userdetail_model->last_name=strip_tags($request->last_name);
               $userdetail_model->email = $request->email;
               $userdetail_model->register_as = $register_as;
               $userdetail_model->phone = $request->phone;
               $userdetail_model->city = $request->city;
               $userdetail_model->state = $request->state;
               $userdetail_model->country = $request->country;
               $userdetail_model->street_address1 = $request->address1;
               $userdetail_model->street_address2 = $request->address2;
               $userdetail_model->company_name = $company_name;
               $userdetail_model->company_link = $company_link;
                $userdetail_model->zip = $request->postcode;


               if ($request->hasFile('image')) {
                 $file = $request->file('image');
                 $filename = time().rand(1,100).".".$file->getClientOriginalExtension();
                 if($request->job_type=="JOB_POSTER"){
                     if($file->move(IMAGE_DIR_POSTER_PATH, $filename))
                     {
                       $data['image'] = $filename;
                       $userdetail_model->image = $filename;
                     }
                 }else{
                     if($file->move(IMAGE_DIR_SEEKER_PATH, $filename))
                     {
                       $data['image'] = $filename;
                       $userdetail_model->image = $filename;
                     }
                 }
             }

               if($userdetail_model->save()){
                 $response['status']=1;
                 $response['message']='User Detail has been updated successfully';
               }else{
                 $response['status']=0;
                 $response['error']='Opps, somethcourseuniting went wrong, please try again..';
               }
           }
           return response()->json($response,$statusCode);
     }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      try{
          $response = [ 'data' => [] ];
          $statusCode = 200;
          $query = UserDetail::find($id);

          if($query)
          {
            if($query->delete())
            {
              $response['status'] = "1";
              $response['message'] = "Removed Successfull";
            }
          } else {
            $response['status'] = "0";
            $response['message'] = "No Course Unit Found";
          }

      } catch (Exception $e){
          $statusCode = 400;  // Bad Request Error Code
          $response['status'] = 0;
          $response['message'] = "Bad Request Error";
      } finally{
          return response()->json($response, $statusCode);
      }
    }

    /**
     * @Author: kadia
     * @Created: March 14 2016
     * @Modified By:
     * @Comment:
     * @Todo: Get Course Unit Data
     */

    public function userDetailListing(Request $request){
      $userdetail_model=new UserDetail();
      return $userdetail_model->getListing($_REQUEST);
    }

    /**
     * @Author: Kadia
     * @Created: March 14 2016
     * @Modified By:
     * @Comment:
     * @Todo: Get Course Unit Data
     */

    public function GetState(Request $request){
      $location=new Location();
      $response = $location->GetState($id);
       return response()->json($response);
    }

}
