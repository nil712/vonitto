<?php

namespace App\Http\Controllers\admin;

define('IMAGE_DIR_PATH',base_path()."/public/upload/page_cms/");

use Illuminate\Http\Request;
use App\PageCms;
use App\Http\Requests;
use Redirect,Input,Hash;
use Validator;
use App\Http\Controllers\Controller;

class PageCmsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

      $ProjectName = \Config::get('constants.PROJECT_NAME');
      return view('admin.pagecms.pagecms',['PageTitle' => 'CMS Pages | '.$ProjectName]);  //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $ProjectName = \Config::get('constants.PROJECT_NAME');
      $pagecms_model=new PageCms();
      return view('admin.pagecms.addPageCms',['PageTitle' => 'Add CMS Page| '.$ProjectName]);  //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      include base_path().'/vendor/eventviva/php-image-resize/src/ImageResize.php';

      $pagecms_model=new PageCms();
      $statusCode=200;
      $response=['data'=>[]];

        $rules = array(
            'page_name'  => 'required',
          );

          $validator = Validator::make($request->all(), $rules);

          if ($validator->fails()) {
              // get the error messages from the validator
              $messages = $validator->messages();
              $response['status']='0';
              $response['error']=$messages;
          }else{
              $pagecms_model->page_name = $request->page_name;
              $pagecms_model->page_text = $request->page_text;

              if ($request->hasFile('image')) {

                  $file = $request->file('image');
                  $filename = time().rand(1,100).".".$file->getClientOriginalExtension();
                  if($file->move(IMAGE_DIR_PATH, $filename))
                  {
                    $data['image'] = $filename;
                    $pagecms_model->image = $filename;
                  }
              }

              if($pagecms_model->save()){
                $obj=PageCms::find($pagecms_model->id);
                $obj->slug=str_slug($pagecms_model->page_name,'-');
                $obj->save();
                $response['status']=1;
                $response['message']='Cms Page has been added successfully';
              }else{
                $response['status']=0;
                $response['error']='Opps, something went wrong, please try again..';
              }
          }
          return response()->json($response,$statusCode);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      if(isset($id) && !empty($id)){
        $EditPageCmsData=PageCms::find($id);
        $ProjectName = \Config::get('constants.PROJECT_NAME');
        return view('admin.pagecms.editPageCms',compact('EditPageCmsData'),['PageTitle' => 'Edit CMS Page| '.$ProjectName]);
      }else{
        return Redirect('admin/pagecms');
      }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function update(Request $request, $id)
     {
           include base_path().'/vendor/eventviva/php-image-resize/src/ImageResize.php';
           $statusCode=200;
           $response=['data'=>[]];

           $rules = array(
             'page_name'  => 'required',
           );

           $validator = Validator::make($request->all(), $rules);

           if ($validator->fails()) {
               // get the error messages from the validator
               $messages = $validator->messages();
               $response['status']='0';
               $response['error']=$messages;
           }else{
               $pagecms_model=PageCms::find($id);
               $pagecms_model->page_name = $request->page_name;
               $pagecms_model->page_text = $request->page_text;

               if ($request->hasFile('image')) {
                 $file = $request->file('image');
                 $filename = time().rand(1,100).".".$file->getClientOriginalExtension();
                 if($file->move(IMAGE_DIR_PATH, $filename))
                 {
                   $data['image'] = $filename;
                   $pagecms_model->image = $filename;
                 }
               }

               $pagecms_model->slug=str_slug($request->page_name,'-');

               if($pagecms_model->save()){

                 $response['status']=1;
                 $response['message']='CMS Page has been updated successfully';
               }else{
                 $response['status']=0;
                 $response['error']='Opps, somethcourseuniting went wrong, please try again..';
               }
           }
           return response()->json($response,$statusCode);
     }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      try{
          $response = [ 'data' => [] ];
          $statusCode = 200;
          $query = PageCms::find($id);

          if($query)
          {
            if($query->delete())
            {
              $response['status'] = "1";
              $response['message'] = "Removed Successfull";
            }
          } else {
            $response['status'] = "0";
            $response['message'] = "No Course Unit Found";
          }

      } catch (Exception $e){
          $statusCode = 400;  // Bad Request Error Code
          $response['status'] = 0;
          $response['message'] = "Bad Request Error";
      } finally{
          return response()->json($response, $statusCode);
      }
    }

    /**
     * @Author: kadia
     * @Created: March 14 2016
     * @Modified By:
     * @Comment:
     * @Todo: Get Course Unit Data
     */

    public function pagecmsListing(Request $request){
      $pagecms_model=new PageCms();
      return $pagecms_model->getListing($_REQUEST);
    }

}
