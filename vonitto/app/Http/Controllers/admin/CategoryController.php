<?php

namespace App\Http\Controllers\admin;


use Illuminate\Http\Request;
use App\Category;
use App\Http\Requests;
use Redirect,Input,Hash;
use Validator;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

      $ProjectName = \Config::get('constants.PROJECT_NAME');
      return view('admin.category.category',['PageTitle' => 'Category | '.$ProjectName]);  //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $ProjectName = \Config::get('constants.PROJECT_NAME');
      $category_model=new Category();
      return view('admin.category.addCategory',['PageTitle' => 'Add Category| '.$ProjectName]);  //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $category_model=new Category();
      $statusCode=200;
      $response=['data'=>[]];

        $rules = array(
            'name'  => 'required',
          );

          $validator = Validator::make($request->all(), $rules);

          if ($validator->fails()) {
              // get the error messages from the validator
              $messages = $validator->messages();
              $response['status']='0';
              $response['error']=$messages;
          }else{
              $category_model->name = $request->name;
              
              if($category_model->save()){
                $response['status']=1;
                $response['message']='Category has been added successfully';
              }else{
                $response['status']=0;
                $response['error']='Opps, something went wrong, please try again..';
              }
          }
          return response()->json($response,$statusCode);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      if(isset($id) && !empty($id)){
        $EditCategoryData=Category::find($id);
        $ProjectName = \Config::get('constants.PROJECT_NAME');
        return view('admin.category.editCategory',compact('EditCategoryData'),['PageTitle' => 'Edit Category| '.$ProjectName]);
      }else{
        return Redirect('admin/category');
      }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function update(Request $request, $id)
     {
           $statusCode=200;
           $response=['data'=>[]];

           $rules = array(
             'name'  => 'required',
           );

           $validator = Validator::make($request->all(), $rules);

           if ($validator->fails()) {
               // get the error messages from the validator
               $messages = $validator->messages();
               $response['status']='0';
               $response['error']=$messages;
           }else{
               $category_model=Category::find($id);
               $category_model->name = $request->name;

               if($category_model->save()){
                 $response['status']=1;
                 $response['message']='Category has been updated successfully';
               }else{
                 $response['status']=0;
                 $response['error']='Opps, somethcourseuniting went wrong, please try again..';
               }
           }
           return response()->json($response,$statusCode);
     }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      try{
          $response = [ 'data' => [] ];
          $statusCode = 200;
          $query = Category::find($id);

          if($query)
          {
            if($query->delete())
            {
              $response['status'] = "1";
              $response['message'] = "Removed Successfully";
            }
          } else {
            $response['status'] = "0";
            $response['message'] = "No Course Unit Found";
          }

      } catch (Exception $e){
          $statusCode = 400;  // Bad Request Error Code
          $response['status'] = 0;
          $response['message'] = "Bad Request Error";
      } finally{
          return response()->json($response, $statusCode);
      }
    }

    /**
     * @Author: kadia
     * @Created: March 14 2016
     * @Modified By:
     * @Comment:
     * @Todo: Get Course Unit Data
     */

    public function categoryListing(Request $request){
      $category_model=new Category();
      return $category_model->getListing($_REQUEST);
    }

}
