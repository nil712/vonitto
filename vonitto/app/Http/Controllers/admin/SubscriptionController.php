<?php

namespace App\Http\Controllers\admin;


use Illuminate\Http\Request;
use App\Subscription;
use App\Http\Requests;
use Redirect,Input,Hash;
use Validator;
use App\Http\Controllers\Controller;

class SubscriptionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

      $ProjectName = \Config::get('constants.PROJECT_NAME');
      return view('admin.subscription.subscription',['PageTitle' => 'Subscription | '.$ProjectName]);  //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $ProjectName = \Config::get('constants.PROJECT_NAME');
      $subscription_model=new Subscription();
      return view('admin.subscription.addSubscription',['PageTitle' => 'Add Subscription| '.$ProjectName]);  //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $subscription_model=new Subscription();
      $statusCode=200;
      $response=['data'=>[]];

        $rules = array(
            'subscription_title'  => 'required',
            'subscription_type'  => 'required',
            'subscription_amount'  => 'required',
            'subscription_period'  => 'required',
          );

          $validator = Validator::make($request->all(), $rules);

          if ($validator->fails()) {
              // get the error messages from the validator
              $messages = $validator->messages();
              $response['status']='0';
              $response['error']=$messages;
          }else{
              $subscription_model->subscription_title = $request->subscription_title;
              $subscription_model->subscription_desc = $request->subscription_desc;
              $subscription_model->subscription_type = $request->subscription_type;
              $subscription_model->subscription_amount = $request->subscription_amount;
              $subscription_model->subscription_period = $request->subscription_period;
              $subscription_model->subscription_applicable_for = $request->subscription_applicable_for;
              $subscription_model->free_for_month = $request->free_for_month;
              $subscription_model->additional_for_month = $request->additional_for_month;

              if($subscription_model->save()){
                $response['status']=1;
                $response['message']='Subscription has been added successfully';
              }else{
                $response['status']=0;
                $response['error']='Opps, something went wrong, please try again..';
              }
          }
          return response()->json($response,$statusCode);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      if(isset($id) && !empty($id)){
        $EditSubscriptionData=Subscription::find($id);
        $ProjectName = \Config::get('constants.PROJECT_NAME');
        return view('admin.subscription.editSubscription',compact('EditSubscriptionData'),['PageTitle' => 'Edit Subscription| '.$ProjectName]);
      }else{
        return Redirect('admin/subscription');
      }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function update(Request $request, $id)
     {
           $statusCode=200;
           $response=['data'=>[]];

           $rules = array(
             'subscription_title'  => 'required',
             'subscription_type'  => 'required',
             'subscription_amount'  => 'required',
             'subscription_period'  => 'required',
           );

           $validator = Validator::make($request->all(), $rules);

           if ($validator->fails()) {
               // get the error messages from the validator
               $messages = $validator->messages();
               $response['status']='0';
               $response['error']=$messages;
           }else{
               $subscription_model=Subscription::find($id);
               $subscription_model->subscription_title = $request->subscription_title;
               $subscription_model->subscription_desc = $request->subscription_desc;
               $subscription_model->subscription_type = $request->subscription_type;
               $subscription_model->subscription_amount = $request->subscription_amount;
               $subscription_model->subscription_period = $request->subscription_period;
               $subscription_model->subscription_applicable_for = $request->subscription_applicable_for;
               $subscription_model->free_for_month = $request->free_for_month;
               $subscription_model->additional_for_month = $request->additional_for_month;

               if($subscription_model->save()){
                 $response['status']=1;
                 $response['message']='Subscription has been updated successfully';
               }else{
                 $response['status']=0;
                 $response['error']='Opps, somethcourseuniting went wrong, please try again..';
               }
           }
           return response()->json($response,$statusCode);
     }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      try{
          $response = [ 'data' => [] ];
          $statusCode = 200;
          $query = Subscription::find($id);

          if($query)
          {
            if($query->delete())
            {
              $response['status'] = "1";
              $response['message'] = "Removed Successfully";
            }
          } else {
            $response['status'] = "0";
            $response['message'] = "No Course Unit Found";
          }

      } catch (Exception $e){
          $statusCode = 400;  // Bad Request Error Code
          $response['status'] = 0;
          $response['message'] = "Bad Request Error";
      } finally{
          return response()->json($response, $statusCode);
      }
    }

    /**
     * @Author: kadia
     * @Created: March 14 2016
     * @Modified By:
     * @Comment:
     * @Todo: Get Subscription Unit Data
     */

    public function subscriptionListing(Request $request){
      $subscription_model=new Subscription();
      return $subscription_model->getListing($_REQUEST);
    }

}
