<?php

namespace App\Http\Controllers\admin;

define('IMAGE_DIR_PATH',base_path()."/resources/assets/upload/jobposter/");

use Illuminate\Http\Request;
use App\JobPost;
use App\Category;
//Hitesh Tank
use Exception;
use DB;
//Hitesh Tank
use App\Http\Requests;
use Redirect,Input,Hash;
use Validator;
use App\Http\Controllers\Controller;

class JobController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

      $ProjectName = \Config::get('constants.PROJECT_NAME');
      return view('admin.job.job',['PageTitle' => 'Job | '.$ProjectName]);  //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $ProjectName = \Config::get('constants.PROJECT_NAME');
      $job_model=new JobPost();
      $category_model=new Category();
      $GetCategoryList = $category_model->GetCategoryList();
      return view('admin.job.addJob',['PageTitle' => 'Add Job| '.$ProjectName],compact('GetCategoryList'));  //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      include base_path().'/vendor/eventviva/php-image-resize/src/ImageResize.php';

      $jobpost_model=new JobPost();
      $statusCode=200;
      $response=['data'=>[]];

        $rules = array(
            'title'  => 'required',
            'description'  => 'required',
            'category_id'  => 'required',
            'how_long'  => 'required',
          );

          $validator = Validator::make($request->all(), $rules);

          if ($validator->fails()) {
              // get the error messages from the validator
              $messages = $validator->messages();
              $response['status']='0';
              $response['error']=$messages;
          }else{
              $jobpost_model->title = $request->title;
              $jobpost_model->description = $request->description;
              $jobpost_model->what_you_want = $request->what_you_want;
              $jobpost_model->project_deadline = $request->project_deadline;
              $jobpost_model->category_id = $request->category_id;
              $jobpost_model->location_city = $request->location_city;
              $jobpost_model->location_country = $request->location_country;
              $jobpost_model->budget = $request->budget;
              $jobpost_model->skills = $request->skills;
              $jobpost_model->how_long = $request->how_long;

              // Multiple Museum Image Uplod Code
              if ($request->hasFile('file')) {
                 $fileImage = $request->file('file');
                 $StoreName = array();
                 foreach ($fileImage as $files) {
                     $filename = time().rand(1,100).".".$files->getClientOriginalExtension();
                     $StoreName[] = $filename;
                     if($files->move(IMAGE_DIR_PATH, $filename))
                     {
                       $data['file'] = $filename;
                     }
                 }
                 $jobpost_model->file = serialize($StoreName);
              }

              if($jobpost_model->save()){
                $response['status']=1;
                $response['message']='Job has been added successfully';
              }else{
                $response['status']=0;
                $response['error']='Opps, something went wrong, please try again..';
              }
          }
          return response()->json($response,$statusCode);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      if(isset($id) && !empty($id)){
        $EditJobData=JobPost::find($id);
        $ProjectName = \Config::get('constants.PROJECT_NAME');
        $category_model=new Category();
        $GetCategoryList = $category_model->GetCategoryList();
        return view('admin.job.editJob',compact('EditJobData','GetCategoryList'),['PageTitle' => 'Edit Job| '.$ProjectName]);
      }else{
        return Redirect('admin/job');
      }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function update(Request $request, $id)
     {
           include base_path().'/vendor/eventviva/php-image-resize/src/ImageResize.php';
           $statusCode=200;
           $response=['data'=>[]];

           $rules = array(
             'title'  => 'required',
             'description'  => 'required',
             'category_id'  => 'required',
             'how_long'  => 'required',
           );

           $validator = Validator::make($request->all(), $rules);

           if ($validator->fails()) {
               // get the error messages from the validator
               $messages = $validator->messages();
               $response['status']='0';
               $response['error']=$messages;
           }else{
               $jobpost_model = JobPost::find($id);
               $jobpost_model->title = $request->title;
               $jobpost_model->description = $request->description;
               $jobpost_model->what_you_want = $request->what_you_want;
               $jobpost_model->project_deadline = $request->project_deadline;
               $jobpost_model->category_id = $request->category_id;
               $jobpost_model->location_city = $request->location_city;
               $jobpost_model->location_country = $request->location_country;
               $jobpost_model->budget = $request->budget;
               $jobpost_model->skills = $request->skills;
               $jobpost_model->how_long = $request->how_long;

               // Multiple Museum Image Uplod Code
               if ($request->hasFile('file')) {
                  $fileImage = $request->file('file');
                  $StoreName = array();
                  foreach ($fileImage as $files) {
                      $filename = time().rand(1,100).".".$files->getClientOriginalExtension();
                      $StoreName[] = $filename;
                      if($files->move(IMAGE_DIR_PATH, $filename))
                      {
                        $data['file'] = $filename;
                      }
                  }
                  $jobpost_model->file = serialize($StoreName);
               }

               if($jobpost_model->save()){
                 $response['status']=1;
                 $response['message']='Job has been updated successfully';
               }else{
                 $response['status']=0;
                 $response['error']='Opps, somethcourseuniting went wrong, please try again..';
               }
           }
           return response()->json($response,$statusCode);
     }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      try{
          $response = [ 'data' => [] ];
          $statusCode = 200;
          $query = JobPost::find($id);

          if($query)
          {
            if($query->delete())
            {
              $response['status'] = "1";
              $response['message'] = "Removed Successfull";
            }
          } else {
            $response['status'] = "0";
            $response['message'] = "No Course Unit Found";
          }

      } catch (Exception $e){
          $statusCode = 400;  // Bad Request Error Code
          $response['status'] = 0;
          $response['message'] = "Bad Request Error";
      } finally{
          return response()->json($response, $statusCode);
      }
    }

    /**
     * @Author: kadia
     * @Created: March 14 2016
     * @Modified By:
     * @Comment:
     * @Todo: Get Course Unit Data
     */

    public function jobListing(Request $request){
      $jobpost_model=new JobPost();
      return $jobpost_model->getListing($_REQUEST);
    }

    /**
     * @Author: Hitesh Tank
     * @Created: Aug-10-2016
     * @Modified By:
     * @Comment:
     * @Todo: featuredFunctionality
     */

    public function featuredFunctionality(Request $request){
      try{
        $responseCode=200;
        $rules = array(
            'job_id'  => 'required',
            'is_featured'=>'required',
          );
          $validator = Validator::make($request->all(), $rules);
          if ($validator->fails()) {
              $response['status']='0';
              $response['error']=$validator->errors()->first();
          }else{
              $isAble=true;
              if($request->is_featured == "true"){
                  $count=JobPost::where('is_featured',DB::raw("'1'"))->count();

                  if($count>=3){
                    $isAble=false;
                  }
              }

              if($isAble == true){
                  $action_changed=0;
                  if($request->is_featured == "true")
                      $action_changed=1;
                  $obj=JobPost::find($request->job_id);
                  $obj->is_featured=$action_changed;
                  $obj->save();
                  $response['status']="1";
              }else{
                $response['status']="0";
                $response['error']="You can not set featured job more than three";
              }




          }
      }catch(Exception $e){
        $response['status']='0';
        $response['error']="Bad Request Error";
      }finally{
        return response()->json($response,$responseCode);
      }
    }

}
