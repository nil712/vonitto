<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\GoogleAds;
use Validator;

class GoogleAdsController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $ProjectName = \Config::get('constants.PROJECT_NAME');
        return view('admin.google_ads.google_ads', ['PageTitle' => 'Google Ads | ' . $ProjectName]);  //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $ProjectName = \Config::get('constants.PROJECT_NAME');
        $google_ads_model = new GoogleAds();
        return view('admin.google_ads.addGoogleAds', ['PageTitle' => 'Add GoogleAds| ' . $ProjectName]);  //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $google_ads_model = new GoogleAds();
        $statusCode = 200;
        $response = ['data' => []];

        $rules = array(
            'code' => 'required',
        );

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {

            $messages = $validator->messages();
            $response['status'] = '0';
            $response['error'] = $messages;
        } else {
            //echo "hi";exit;
            $google_ads_model->code = $request->code;

            if ($google_ads_model->save()) {
                $response['status'] = 1;
                $response['message'] = 'Google Ad has been added successfully';
            } else {
                $response['status'] = 0;
                $response['error'] = 'Opps, something went wrong, please try again..';
            }
        }
        return response()->json($response, $statusCode);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {

        if (isset($id) && !empty($id)) {
            $EditGoogleAdData = GoogleAds::find($id);
            $ProjectName = \Config::get('constants.PROJECT_NAME');
            return view('admin.google_ads.editGoogleAds', compact('EditGoogleAdData'), ['PageTitle' => 'Edit Google Ads| ' . $ProjectName]);
        } else {
            return Redirect('admin/google_ads');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $google_ads_model = new GoogleAds();
        $statusCode = 200;
        $response = ['data' => []];

        $rules = array(
            'code' => 'required',
        );

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            // get the error messages from the validator
            $messages = $validator->messages();
            $response['status'] = '0';
            $response['error'] = $messages;
        } else {
            $google_ads_model = GoogleAds::find($id);
            $google_ads_model->code = $request->code;

            if ($google_ads_model->save()) {
                $response['status'] = 1;
                $response['message'] = 'Code has been updated successfully';
            } else {
                $response['status'] = 0;
                $response['error'] = 'Opps, somethcourseuniting went wrong, please try again..';
            }
        }
        return response()->json($response, $statusCode);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        try{
            $response = [ 'data' => [] ];
            $statusCode = 200;
            $query = GoogleAds::find($id);

            if($query)
            {
              if($query->delete())
              {
                $response['status'] = "1";
                $response['message'] = "Removed Successfully";
              }
            } else {
              $response['status'] = "0";
              $response['message'] = "No Course Unit Found";
            }

        } catch (Exception $e){
            $statusCode = 400;  // Bad Request Error Code
            $response['status'] = 0;
            $response['message'] = "Bad Request Error";
        } finally{
            return response()->json($response, $statusCode);
        }
    }

    public function GoogleAdsListing(Request $request) {
        $google_ads_model = new GoogleAds();
        return $google_ads_model->getListing($_REQUEST);
    }

}
