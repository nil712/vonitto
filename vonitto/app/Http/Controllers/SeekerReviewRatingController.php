<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Requests;
use Exception;
use Validator;

use App\SeekerReviewRating;
use DB;
use Auth;
use Guzzle\Plugin\Backoff\ConstantBackoffStrategy;
class SeekerReviewRatingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public $review_model;

      public function __construct(){
        $this->review_model=new SeekerReviewRating();
      }

      /**
       * Store a newly created resource in storage.
       * Developer Name : Hitesh Tank
       * Date : 29 July,2016
       * @param  \Illuminate\Http\Request  $request
       * @return \Illuminate\Http\Response
       */
      public function store(Request $request)
      {
          $statusCode=200;
          $response=['data'=>[]];
          try{
            $rules = array(
              'rating' => 'required|numeric',
              'job_id'=>'required',
              'seeker_id'=>'required'
            );
              $messages = [
                 'rating.required' => 'Please select rating',
                 'job_id.required' => 'Without job, you can\'t rate to seeker',
                 'seeker_id.required' => 'Seeker should be there',
                  ];
            $validator = Validator::make(Input::all(), $rules,$messages);
            if($validator->fails()) {
              $response['status'] = "0";
              $response['message'] = $validator->errors()->first();
            }else{
                if($request->rating > 0){
                  //check poster has already review or not
                  $exist=SeekerReviewRating::where('job_post_id',$request->job_id)->where('seeker_id',$request->seeker_id)->where('poster_id',Auth::id())->count();
                  if(isset($exist) && !empty($exist) && count($exist)>0){
                    $response['status']='0';
                    $response['message']='You had already given review';
                  }else{
                    $this->review_model->job_post_id=$request->job_id;
                    $this->review_model->seeker_id=$request->seeker_id;
                    $this->review_model->poster_id=Auth::id();
                    $this->review_model->rating=$request->rating;
                    $this->review_model->review=trim(addslashes($request->review));
                    if($this->review_model->save()){
                      $response['status']='1';
                      $response['message']='Thank you for rating';
                    }else{
                      $response['status']='0';
                      $response['message']='Opps, something went wrong, please try again';
                    }
                  }
                }else{
                  $response['status']='0';
                  $response['message']='Please choose rating';
                }

            }
          }catch(Exception $e){
            $response['status']='0';
            $response['message']='Bad Request Error';
          }finally{
              return response()->json($response,$statusCode);
          }
      }
}
