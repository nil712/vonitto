<?php

namespace App\Http\Controllers;

define('IMAGE_DIR_PATH',base_path()."/resources/assets/upload/jobposter/");

use App\Http\Requests;
use Illuminate\Http\Request;
use Validator;
use App\User;
use DB;
use Session;
use Mail;

class JobposterController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('jobposter.signup');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function login()
    {
        return view('jobposter.login');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request){
          $confirmation_code = str_random(30);
          $data = array();
          $rules = array(
              'first_name' => 'required',
              'last_name' => 'required',
              'image' => 'required|mimes:jpeg,jpg,png,gif|max:10000',
              'email' => 'required|unique:users|max:100',
              'password' => 'required|confirmed|min:6',
              'password_confirmation' => 'required|min:6',
              'register_as'=> 'required'
          );

          $validator = Validator::make($request->all(), $rules);

          if(!$validator->fails()) {
            $data = $request->all();
            $data['activation_code'] = $confirmation_code;
            $data['password'] = bcrypt($data['password']);
            if ($request->hasFile('image')) {
                $file = $request->file('image');
                $filename = time().rand(1,100).".".$file->getClientOriginalExtension();
                if($file->move(IMAGE_DIR_PATH, $filename))
                {
                  $data['image'] = $filename;
                } else {
                  return redirect('jobposter/signup')->with('status', 'Error')->with('message','Image Error');
                }
            }
            if($jobposterdata = User::create($data))
            {
                $name = $jobposterdata->first_name." ".$jobposterdata->last_name;
                Session::flash('message', 'JobPoster account created successfully. Please check your mail to verify your account.');
                // send mail to registered user with verification link
                Mail::send('emails.verify',['confirmation_code'=>$confirmation_code,'name'=>$name], function($message) use ($jobposterdata,$name)
                {
                  $message->to($jobposterdata->email, $name)->subject('Vonitto Account Verification');
                });
                return redirect('jobposter/signup')->with('status', 'Success')->with('message','JobPoster account created successfully. Please check your mail to verify your account.');
            } else {
              return redirect('jobposter/signup')->with('status', 'Error')->with('message','Something Wrong.. Please try again later..!!');
            }

          } else {
            return redirect('jobposter/signup')->withInput()->withErrors($validator);
          }
    }

    /**
       * Display the specified resource.
       *
       * @param  int  $id
       * @return \Illuminate\Http\Response
       */
      public function show($id)
      {
        $jobseekerData = User::find($id);
        return view('jobposter.jobposterdetail')->with($jobseekerData);
      }

      /**
       * Show the form for editing the specified resource.
       *
       * @param  int  $id
       * @return \Illuminate\Http\Response
       */
      public function edit($id)
      {
          //
      }

      /**
       * Update the specified resource in storage.
       *
       * @param  \Illuminate\Http\Request  $request
       * @param  int  $id
       * @return \Illuminate\Http\Response
       */
      public function update(Request $request, $id)
      {
        //
      }

}
