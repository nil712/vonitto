<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use PayPal,Auth;
use App\Payment;
use App\Subscription;
use App\User;
class PagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function thanks(){
      // dd($_REQUEST);
      //payer_id, txn_id,payment_date,payment_status,payment_for = JOB_SEEKER, job_seeker_id = userid,subscription_id
      $data = $_REQUEST;

      $objPayment = new Payment;
      // that is recurring payment data here.
      if(isset($data['recurring']) && !empty($data['recurring'])){
        $objPayment['subscr_id'] = (string)$data['subscr_id'];
        $objPayment['payer_status'] = (string)$data['payer_status'];
        $objPayment['is_recurring'] = "1";

      }else {
        $objPayment['txn_id'] = (string)$data['txn_id'];
        $objPayment['payment_status'] = (string)$data['payment_status'];

      }

      $objPayment['payment_for'] = "JOB_SEEKER";
      $objPayment['payer_id'] = (string)$data['payer_id'];
      $objPayment['job_seeker_id'] = Auth::user()->id;
      $objPayment['subscription_id'] = $data['custom'];

      $objPayment->save();
      $objSubscription = Subscription::find($data['custom']);

      $objUser = User::find(Auth::user()->id);
      $objUser->available_pitch = $objSubscription->subscription_applicable_for;
      $objUser->update();

      return view('paypal/thankyou');
    }
}
