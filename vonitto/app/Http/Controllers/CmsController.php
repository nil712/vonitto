<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Exception;
use App\Http\Requests;
use App\PageCms;

class CmsController extends Controller
{
      public function cmsPage($slug){
        if(isset($slug) && !empty($slug)){
          //find data

          $model=new PageCms();
          $cmsData=$model->findData($slug);

          if(isset($cmsData) && !empty($cmsData))
          {
              return view('cms',compact('cmsData'));
          }else{
            abort(404);
          }
        }else{
            abort(404);
        }

      }
}
