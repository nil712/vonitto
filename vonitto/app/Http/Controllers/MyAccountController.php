<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Auth,Utility;
use App\User;
use App\JobPitched;
use App\Payment;
use App\JobAwarded;
//Hitesh Tank
use App\JobPost;
use Input;
use Validator;
use Exception;
//Hitesh Tank
use DB;
class MyAccountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $objUtility = new Utility;

        $userId = Auth::user()->id;
        $userDetail = User::find($userId);
        //Add image urls
        $userDetail = $objUtility->addUrls($userDetail);

        $limit = 10;
        $pitchedJobs = JobPitched::where('job_seeker_id','=',$userId)->leftJoin('job_post','job_pitched.job_id','=','job_post.id')->where(function($query){$query->where('job_post.progress_status','Assigned')->orWhere('job_post.progress_status','In-Progress')->orWhereNull('job_post.progress_status');})->limit($limit)->get();
        $packageHistory = Payment::where('job_seeker_id','=',$userId)->leftJoin('subscription','subscription.id','=','payments.subscription_id')->limit($limit)->get();

        //Modified by: Hitesh Tank Description : return awarded job which have status of in-progress and assigned
        $awardedJobs = JobAwarded::where('job_seeker_id','=',$userId)->where(function($query){$query->where('job_post.progress_status','Assigned')->orWhere('job_post.progress_status','In-Progress');})->leftJoin('job_post','job_post.id','=','job_awarded.job_id')->leftJoin('users','users.id','=','job_awarded.job_poster_id')
        ->select('job_post.title','job_post.description','job_post.job_poster_id','users.first_name','users.last_name','job_post.project_deadline','job_awarded.created_at','job_awarded.job_id','job_post.job_poster_id','job_post.progress_status')->limit($limit)->get();

        //get Completed Job Data
        $completedJob = JobAwarded::where('job_seeker_id','=',$userId)->where('job_post.progress_status','Completed')->leftJoin('job_post','job_post.id','=','job_awarded.job_id')->leftJoin('users','users.id','=','job_awarded.job_poster_id')->leftJoin('job_review_rating','job_post.id','=','job_review_rating.job_post_id')
        ->select('users.email','users.phone','job_review_rating.review','job_review_rating.rating','job_post.title','job_post.description','job_post.job_poster_id','users.first_name','users.last_name','job_post.project_deadline','job_awarded.created_at','job_awarded.job_id','job_post.job_poster_id','job_post.progress_status')->limit($limit)->get();


        $totalPackage = Payment::where('job_seeker_id','=',$userId)->count();
        $packagePages = ceil($totalPackage/$limit);

        $totalPitchedJob = JobPitched::where('job_seeker_id','=',$userId)->leftJoin('job_post','job_pitched.job_id','=','job_post.id')->where(function($query){$query->where('job_post.progress_status','Assigned')->orWhere('job_post.progress_status','In-Progress')->orWhereNull('job_post.progress_status');})->count();
        $pitchPages = ceil($totalPitchedJob/$limit);

        //Modified by: Hitesh Tank Description : return awarded job which have status of in-progress and assigned
        $totalAwardedJobs = JobAwarded::where('job_seeker_id','=',$userId)->where(function($query){$query->where('job_post.progress_status','Assigned')->orWhere('job_post.progress_status','In-Progress');})->leftJoin('job_post','job_post.id','=','job_awarded.job_id')->count();
        $awardedJobsPages = ceil($totalAwardedJobs/$limit);

        //Total Completed job pages
        $totalCompletedJob = JobAwarded::where('job_seeker_id','=',$userId)->where('job_post.progress_status','Completed')->leftJoin('job_post','job_post.id','=','job_awarded.job_id')->count();
        $completedJobsPages = ceil($totalCompletedJob/$limit);

        //My Favourite Jobs
        $markJobs=DB::table('job_post')->join('job_bookmark','job_post.id','=','job_bookmark.job_id')->where('job_bookmark.seeker_id',Auth::id())->where('job_bookmark.bookmark_status',DB::raw("'bookmark'"))->select('job_post.*','job_bookmark.bookmark_status')->orderBy('job_bookmark.created_at','DESC')->limit($limit)->get();
        $totalMarkJobs=DB::table('job_post')->join('job_bookmark','job_post.id','=','job_bookmark.job_id')->where('job_bookmark.seeker_id',Auth::id())->where('job_bookmark.bookmark_status',DB::raw("'bookmark'"))->select('job_post.*','job_bookmark.bookmark_status')->count();
        $markJobPages=ceil($totalMarkJobs/$limit);

        //return view('jobseeker/my_account',compact('userDetail','packageHistory','pitchedJobs','packagePages','pitchPages','awardedJobsPages','awardedJobs','completedJobsPages','completedJob'));
        return view('jobseeker/my_account',compact('markJobPages','markJobs','userDetail','packageHistory','pitchedJobs','packagePages','pitchPages','awardedJobsPages','awardedJobs','completedJobsPages','completedJob'));
    }
    public function loadPosterJobs($page){

      if(!isset($page)){
        $page = 0;
      }else {
        $tpage = $page;
        $page = $page - 1;
      }
      $limit = 10;
      $offset = $page * $limit;

      $userId = Auth::user()->id;

      // $pitchedRequest = DB::select("SELECT job_post.id as job_id,job_pitched.job_seeker_id,users.first_name,users.last_name,job_post.job_project_id,job_post.title,job_post.description,job_post.created_at FROM `job_post`
      // RIGHT JOIN job_pitched ON job_pitched.job_id = job_post.id
      // LEFT JOIN users ON users.id = job_pitched.job_seeker_id
      // WHERE job_poster_id = ".$userId." order by created_at LIMIT ".$offset.",".$limit."");

      $myJobs = DB::select("SELECT job_post.currency,job_post.id as job_id,job_post.job_project_id,job_post.project_deadline,job_post.budget,job_post.title,job_post.created_at,job_pitched.job_seeker_id,CASE WHEN(COUNT(job_pitched.id)>=0) THEN COUNT(job_pitched.id) ELSE 0 END AS pitched_request,job_post.progress_status FROM `job_post`
      LEFT JOIN job_pitched ON job_pitched.job_id = job_post.id
      WHERE job_poster_id = ".$userId." GROUP BY job_post.id order by job_id DESC LIMIT ".$offset.",".$limit."");
      if($myJobs!=null){
        return view('jobpost/loadPosterJobs',compact('myJobs'));
      }else{
        return "0";
      }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function loadPitchJobs($page){
      if(!isset($page)){
        $page = 0;
      }else {
        $tpage = $page;
        $page = $page - 1;
      }
      $limit = 10;
      $offset = $page * $limit;

      $userId = Auth::user()->id;
      $pitchedJobs = JobPitched::where('job_seeker_id','=',$userId)->leftJoin('job_post','job_post.id','=','job_pitched.job_id')->limit($limit)->offset($offset)->get();
      if($pitchedJobs!=null){
        return view('jobseeker/loadPitchJobs',compact('pitchedJobs'));
      }else{
        return "0";
      }
    }

    public function loadJobsPackage($page){
      if(!isset($page)){
        $page = 0;
      }else {
        $tpage = $page;
        $page = $page - 1;
      }
      $limit = 10;
      $offset = $page * $limit;

      $userId = Auth::user()->id;
      $packageHistory = Payment::where('job_seeker_id','=',$userId)->leftJoin('subscription','subscription.id','=','payments.subscription_id')->limit($limit)->offset($offset)->get();
      if($packageHistory!=null){
        return view('jobseeker/loadJobsPackage',compact('packageHistory'));
      }else{
        return "0";
      }
    }

    /**
     * @Method		  :	POST
     * @Params		  :
     * @author        :
     * @created		  :	29 July 2016
     * @Modified by	  : Hitesh Tank
     * @Comment		  : loadJobseekerAwardedJobs
     **/
    public function loadJobseekerAwardedJobs($page){

      if(!isset($page)){
        $page = 0;
      }else {
        $tpage = $page;
        $page = $page - 1;
      }
      $limit = 10;
      $offset = $page * $limit;
      $userId = Auth::user()->id;
      $awardedJobs = JobAwarded::where('job_seeker_id','=',$userId)->where(function($query){$query->where('job_post.progress_status','Assigned')->orWhere('job_post.progress_status','In-Progress');})->leftJoin('job_post','job_post.id','=','job_awarded.job_id')->leftJoin('users','users.id','=','job_awarded.job_poster_id')
      ->select('job_post.title','job_post.description','job_post.job_poster_id','users.first_name','users.last_name','job_post.project_deadline','job_awarded.created_at','job_awarded.job_id','job_post.job_poster_id','job_post.progress_status')->limit($limit)->offset($offset)->get();

      if($awardedJobs!=null){
        return view('jobseeker/loadAwardedJobs',compact('awardedJobs'));
      }else{
        return "0";
      }
    }

    /**
     * @Method		  :	POST
     * @Params		  :
     * @author        :Hitesh Tank
     * @created		  :	29 July 2016
     * @Modified by	  :
     * @Comment		  : loadAwardedCompletedJobs
     **/
    public function loadAwardedCompletedJobs($page){

      if(!isset($page)){
        $page = 0;
      }else {
        $tpage = $page;
        $page = $page - 1;
      }
      $limit = 10;
      $offset = $page * $limit;
      $userId = Auth::user()->id;
      $completedJob = JobAwarded::where('job_post.progress_status','Completed')->where('job_seeker_id','=',$userId)->leftJoin('job_post','job_post.id','=','job_awarded.job_id')->leftJoin('users','users.id','=','job_awarded.job_poster_id')->leftJoin('job_review_rating','job_post.id','=','job_review_rating.job_post_id')
      ->select('users.email','users.phone','job_review_rating.review','job_review_rating.rating','job_post.title','job_post.description','job_post.job_poster_id','users.first_name','users.last_name','job_post.project_deadline','job_awarded.created_at','job_awarded.job_id','job_post.job_poster_id','job_post.progress_status')->limit($limit)->offset($offset)->get();

      if($completedJob!=null){
        return view('jobseeker/loadAwardedCompletedJob',compact('completedJob'));
      }else{
        return "0";
      }
    }

    //Modified By :Hitesh Tank, Date: 29 July,2016, Description: get only in-progress or assigned jobs
    public function loadAwardedJobs($page){
      if(!isset($page)){
        $page = 0;
      }else {
        $tpage = $page;
        $page = $page - 1;
      }
      $limit = 10;
      $offset = $page * $limit;

      $userId = Auth::user()->id;
      $awardedJobs = DB::select("SELECT job_post.id as job_id,job_post.title,users.id as user_id,users.first_name,users.last_name,job_awarded.created_at,job_post.progress_status
      from job_awarded
      left join users on users.id = job_awarded.job_seeker_id
      left join job_post on job_post.id = job_awarded.job_id
      where (job_post.progress_status='In-Progress' OR job_post.progress_status='Assigned') AND job_awarded.job_poster_id = ".$userId." order by job_awarded.created_at LIMIT ".$offset.",".$limit."");
      if($awardedJobs!=null){
        return view('jobposter/loadAwardedJobs',compact('awardedJobs'));
      }else {
        return "0";
      }
    }


    /**
     * @Method		  :	POST
     * @Params		  :
     * @author        :Hitesh Tank
     * @created		  :	29 July 2016
     * @Modified by	  :
     * @Comment		  : loadPosterCompletedJob
     **/
    public function loadPosterCompletedJob($page){

      if(!isset($page)){
        $page = 0;
      }else {
        $tpage = $page;
        $page = $page - 1;
      }
      $limit = 10;
      $offset = $page * $limit;
      $userId = Auth::user()->id;
        $completedJob = JobAwarded::where('job_awarded.job_poster_id','=',$userId)->where('job_post.progress_status','Completed')->leftJoin('job_post','job_post.id','=','job_awarded.job_id')->leftJoin('users','users.id','=','job_awarded.job_seeker_id')->leftJoin('job_review_rating','job_post.id','=','job_review_rating.job_post_id')->limit($limit)->offset($offset)->get();

        if($completedJob!=null){
          return view('jobposter/_completedJob',compact('completedJob'));
        }else{
          return "0";
        }
    }


    /**
     * @Method		  :	POST
     * @Params		  :
     * @author        : Hitesh Tank
     * @created		  :	29 July 2016
     * @Modified by	  :
     * @Comment		  : changeJobStatus
     **/
    public function changeJobStatus(Request $request){
      $response=['data'=>[]];
      $data=$request->json()->get('data');
      $statusCode=200;
      try{
        $rules = array(
          'job_id' => 'required',
          'poster_id'=>'required',
        );
          $messages = [
             'job_id.required' => 'Unable to complete job',
             'poster_id.required' => 'Unable to complete job of poster.'
              ];
        $validator = Validator::make($data, $rules,$messages);
        if($validator->fails()) {
          $response['status'] = "0";
          $response['message'] = $validator->errors()->first();
        }else{
          $success=JobAwarded::where('job_seeker_id',Auth::id())->where('job_poster_id',$data['poster_id'])->where('job_id',$data['job_id'])->count();
          if(isset($success) && $success>0){
            $obj=JobPost::find($data['job_id']);
            $obj->progress_status=$data['status'];
            $obj->save();
            $response['status']='1';
            if(strcmp(strtolower($data['status']),'in-progress') == 0)
            {
                $response['updateto']='progress';
                $response['message']='Job has been update to in-progress';
            }else if(strcmp(strtolower($data['status']),'completed') == 0){
                $response['message']='Job has been Completed';
                $response['updateto']='complete';
            }
          }else{
            $response['status']='0';
            $response['message']='It\'s not your job';
          }
        }
      }catch(Exception $e){
        $response['status']='0';
        $response['message']='Bad Request Error';
      }finally{
        return response()->json($response,$statusCode);
      }
    }

    /**
     * @Method		  :	POST
     * @Params		  :
     * @author        : Hitesh Tank
     * @created		  :	8 Aug 2016
     * @Modified by	  :
     * @Comment		  : loadFavouriteJobs
     **/

    public function loadFavouriteJobs($page){

      if(!isset($page)){
        $page = 0;
      }else {
        $page = $page - 1;
      }
      $limit = 10;
      $offset = $page * $limit;
      $userId = Auth::user()->id;
      $markJobs=DB::table('job_post')->join('job_bookmark','job_post.id','=','job_bookmark.job_id')->where('job_bookmark.seeker_id',Auth::id())->where('job_bookmark.bookmark_status',DB::raw("'bookmark'"))->select('job_post.*','job_bookmark.bookmark_status')->orderBy('job_bookmark.created_at','DESC')->limit($limit)->offset($offset)->get();
      if($markJobs!=null){
        return view('jobseeker/_favouriteJob',compact('markJobs'));
      }else{
        return "0";
      }
    }
}
