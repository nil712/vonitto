<?php

namespace App\Http\Controllers;

define('IMAGE_DIR_PATH',base_path()."/resources/assets/upload/jobposter");

use App\Http\Requests;
use Illuminate\Http\Request;
use Validator;
use App\User;
use DB;
use Session;
use Mail;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

class JobPosterProfileController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        if(isset($id) && !empty($id)){
          $EditJobPosterData=User::find($id);
          return view('jobposter.editprofile',compact('EditJobPosterData'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request){
          $confirmation_code = str_random(30);
          $data = array();
          $rules = array(
              'first_name' => 'required',
              'last_name' => 'required',
              'image' => 'required|mimes:jpeg,jpg,png,gif|max:10000',
              'email' => 'required|unique:users|max:100',
              'password' => 'required|confirmed|min:6',
              'password_confirmation' => 'required|min:6'
          );
          $validator = Validator::make($request->all(), $rules);

          if(!$validator->fails()) {
            $data = $request->all();
            $data['activation_code'] = $confirmation_code;
            $data['password'] = bcrypt($data['password']);
            if($request->hasFile('image')) {
                $file = $request->file('image');
                $filename = time().rand(1,100).".".$file->getClientOriginalExtension();
                if($file->move(IMAGE_DIR_PATH, $filename))
                {
                  $data['image'] = $filename;
                } else {
                  return redirect('jobseeker/signup')->with('status', 'Error')->with('message','Image Error');
                }
            }

            if($jobseekerdata = User::create($data))
            {
                $name = $jobseekerdata->first_name." ".$jobseekerdata->last_name;
                Session::flash('message', 'JobSeeker account created successfully. Please check your mail to verify your account.');
                // send mail to registered user with verification link
                Mail::send('emails.verify',['confirmation_code'=>$confirmation_code,'name'=>$name], function($message) use ($jobseekerdata,$name)
                {
                  $message->to($jobseekerdata->email, $name)->subject('Vonitto Account Verification');
                });
                return redirect('jobseeker/signup')->with('status', 'Success')->with('message','JobSeeker account created successfully. Please check your mail to verify your account.');;
            } else {
              return redirect('jobseeker/signup')->with('status', 'Error')->with('message','Something Wrong.. Please try again later..!!');
            }

          } else {
            return redirect('jobseeker/signup')->withInput()->withErrors($validator);
          }
    }

    /**
       * Display the specified resource.
       *
       * @param  int  $id
       * @return \Illuminate\Http\Response
       */
      public function show($id)
      {
        $jobseekerData = User::find($id);
        return view('jobseeker.jobseekerdetail')->with($jobseekerData);
      }

      /**
       * Show the form for editing the specified resource.
       *
       * @param  int  $id
       * @return \Illuminate\Http\Response
       */
      public function edit($id)
      {
          //
      }

      /**
       * Update the specified resource in storage.
       *
       * @param  \Illuminate\Http\Request  $request
       * @param  int  $id
       * @return \Illuminate\Http\Response
       */
      public function update(Request $request, $id)
      {
        $data = array();
        $rules = array(
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email|unique:users,id,:id',
        );
        $validator = Validator::make($request->all(), $rules);

        if(!$validator->fails()) {
          $data = $request->all();
        
          if($request->hasFile('image')) {
              $file = $request->file('image');
              $filename = time().rand(1,100).".".$file->getClientOriginalExtension();
              if($file->move(IMAGE_DIR_PATH, $filename))
              {
                $data['image'] = $filename;
              } else {
                //return redirect('jobseeker.editprofile')->with('status', 'Error')->with('message','Image Error');
                return Redirect::back()->withErrors(['msg', 'Image Error']);
              }
          }

          unset($data['_token']);
          $query = User::find($id);

          if($query)
          {
            if($query->update($data))
            {
              return Redirect::back()->with('status', 'Success')->with('message','Profile updated successfully..!!');
            }
          }
          else{
            return Redirect::back()->with('status', 'Error')->with('message','Something Wrong.. Please try again later..!!');
          }
        } else {
            return Redirect::back()->with('status', 'Error')->with('message','Validation Error')->withErrors($validator);
        }
      }

}
