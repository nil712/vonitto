<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Requests;
use PayPal,Auth;
use App\Subscription;
class PaymentController extends Controller
{
    public function index(){


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $data = Input::all();
      // get package details
      $packageDetail = Subscription::find($data['package_id']);
      // dd($packageDetail);
      $token = $request->session()->token();
      //dd($token);
      // $header = $request->header('X-XSRF-TOKEN');
      // $cookie = $request->cookie('XSRF-TOKEN');
      $objPaypal = new PayPal;

      // $amount =  $_POST['test_price'];
      // $duration = $_POST['duration'];
      // $itemName = "My Product";
      // $itemNumber = $itemName . " - Premium ($amount$)";

      $objPaypal->paypal_url = 'https://www.sandbox.paypal.com/cgi-bin/webscr';// testing paypal url
      //$objPaypal->paypal_url = 'https://www.paypal.com/cgi-bin/webscr';// Live paypal url

      $objPaypal->add_field('product_name', $packageDetail->subscription_type." Monthly ( $$packageDetail->subscription_amount )");

      if(isset($_POST['recurring_checkbox']) && !empty($_POST['recurring_checkbox'])){

      	$objPaypal->add_field('cmd','_xclick-subscriptions');
      	$objPaypal->add_field('txn_type', "recurring_payment");
      	$objPaypal->add_field('desc', $packageDetail->subscription_type." Monthly( $$packageDetail->subscription_amount )");
      	$objPaypal->add_field('src', "1");
      	$objPaypal->add_field('sra', "1");
      	$objPaypal->add_field('t3', 'M');
      	$objPaypal->add_field('p3', "1");
      	$objPaypal->add_field('a3', "$packageDetail->subscription_amount");

      }else{
      	$objPaypal->add_field('amount', "$packageDetail->subscription_amount");
      }

      $objPaypal->add_field('_token',$token);
      $objPaypal->add_field('business',"nilesh.letsnurture-facilitator@gmail.com");
      $objPaypal->add_field('custom',$packageDetail->id);
      $objPaypal->add_field('item_name',$packageDetail->subscription_type);
      $objPaypal->add_field('return',url('payment/success'));
      $objPaypal->add_field('cancel_return',url('/'));
      $objPaypal->add_field('currency_code','GBP');

      $objPaypal->submit_paypal_post();

      // $objPaypal->add_field('business',Auth::user()->email);
      // $objPaypal->add_field('return',url('payment/success'));
      // $objPaypal->add_field('cancel_return',url('/'));
      // $objPaypal->add_field('currency_code','GBP');
      // $objPaypal->add_field('shipping','');
      // // $objPaypal->add_field('email',$email);//if any custom id needs to pass
      // $objPaypal->add_field('item_name',$packageDetail->subscription_type);
      // $objPaypal->add_field('amount',$packageDetail->subscription_amount);
      // $objPaypal->add_field('custom',$packageDetail->id);
      // $objPaypal->add_field('_token',$token);
      // //var_dump($objPaypal);
      // //dd("exit frm here");
      // $objPaypal->submit_paypal_post(); // submit the fields to paypal
      // //$p->dump_fields();      // for debugging, output a table of all the fields

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
