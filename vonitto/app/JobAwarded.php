<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobAwarded extends Model
{
    protected $table ="job_awarded";
    protected $fillable = ['job_seeker_id','job_id','job_poster_id'];
    public $timestamps = false;
}
