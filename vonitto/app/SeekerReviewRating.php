<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class SeekerReviewRating extends Model
{
  /**
   * The database table used by the model.
   *
   * @var string
   */
   protected $table = 'job_review_rating';
   protected $primaryKey = 'id';
   public $timestamps = false;
   /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = ['job_post_id','seeker_id','poster_id','rating','created_at','status'];

}
