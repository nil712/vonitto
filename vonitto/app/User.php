<?php
namespace App;
use DB;
use Illuminate\Foundation\Auth\User as Authenticatable;
class User extends Authenticatable
{
    protected $table = "users";
    protected $primaryKey = 'id';
    /**
     * The attributes that are mass assignable.
     * @var array
     * Updated By Hitesh Tank on 3 - Aug - 2016
     */
    protected $fillable = ['id','first_name','last_name','password','email','role','image','register_as','company_name','remember_token','created_at','updated_at','activation_code','street_address1','street_address2','city','state','zip','country','company_link','phone','experience','graduation','post_graduation','computer_skill','portfolio_image','portfolio_description','project_category_specialization'];

    /**
     * @Author: LN
     * @Created: March 16 2016
     * @Modified By:
     * @Comment:
     * @Todo:
     */
    public function findAccount($searchInputString)
    {
        return $results = DB::table($this->table)
                    ->where('id', 'LIKE', '%'.$searchInputString.'%')
                    ->orWhere('first_name', 'LIKE', "%$searchInputString%")
                    ->orWhere('last_name', 'LIKE', "%$searchInputString%")
                    ->get(array('id', 'first_name', 'last_name'));
    }

    /**
     * @Author: LN
     * @Created: March 16 2016
     * @Modified By:
     * @Comment:
     * @Todo:
     */
    public function GetUsers($Data)
    {
        $RequestData = $Data;
        $Columns     = array(
            // Datatable column index  => Database column name
            0 => 'ID',
            1 => 'FirstName',
            2 => 'LastName',
            3 => 'Email',
            4 => 'Status',
        );

        $TotalData     = DB::table($this->table)->count();
        $TotalFiltered = $TotalData; // When there is no search parameter then total number rows = total number filtered rows.

        $sql = "SELECT id, first_name, last_name, email, status FROM $this->table ";

        if (!empty($RequestData['search']['value'])) {
            // If there is a search parameter, $RequestData['search']['value'] contains search parameter
            $sql .= " WHERE (id LIKE '" . $RequestData['search']['value'] . "%' ";
            $sql .= " OR first_name LIKE '" . $RequestData['search']['value'] . "%' ";
            $sql .= " OR last_name LIKE '" . $RequestData['search']['value'] . "%' ) ";
        }


        $TempData      = DB::select($sql);
        $TotalFiltered = count($TempData);
        $sql .= " ORDER BY " . $Columns[$RequestData['order'][0]['column']] . "   " . $RequestData['order'][0]['dir'] . "  LIMIT " . $RequestData['start'] . " ," . $RequestData['length'] . "   ";

        $GetData = DB::select($sql);
        $data    = array();
        foreach ($GetData as $row) { // Preparing an array

            if ($row->Status == '1') {
                $Status = '<td style="padding: 2px 2px;"><a style="width:90px;" class="btn btn-sm btn-danger btn-flat btn-change-status" href="javascript:void(0);" data-user-id="'.$row->id.'" data-status="'.$row->status.'"> <i class="fa fa-close"></i> Inactive</a></td>';
            } else {
                $Status = '<td style="padding: 2px 2px;"><a style="width:90px;" class="btn btn-sm btn-success btn-flat btn-change-status" href="javascript:void(0);" data-user-id="'.$row->id.'" data-status="'.$row->status.'"> <i class="fa fa-check"></i> Activate</a></td>';
            }

            $Edit = '<td style="padding: 2px 2px;"><a style="width:90px;" class="btn btn-sm btn-warning btn-flat" href="'.url("/").'/admin/user/edit/'.$row->id.'" data-page-id="'.$row->id.'"> <i class="fa fa-edit"></i> Edit </a></td>';

            $View = '<td style="padding: 2px 2px;"><a style="width:90px;" class="btn btn-sm btn-primary btn-flat" href="'.url("/").'/admin/user/view/'.$row->id.'" data-page-id="'.$row->id.'"> <i class="fa fa-eye"></i> View </a></td>';

            $Delete = '<td style="padding: 2px 2px;"><a style="width:90px;" class="btn btn-sm btn-danger btn-flat btn-del-rec" href="javascript:void(0);" data-user-id="'.$row->id.'"> <i class="fa fa-trash-o"></i> Delete</a></td>';

            $NestedData   = array();
            $NestedData[] = $row->id;
            $NestedData[] = ucwords($row->first_name);
            $NestedData[] = ucwords($row->last_name);
            $NestedData[] = $row->email;
            $NestedData[] = $row->status;
            $NestedData[] = '<table>
                                <tr>
                                    '.$View.'
                                    '.$Status.'
                                </tr>
                                <tr>
                                    '.$Edit.'
                                    '.$Delete.'
                                </tr>
                            </table>';
            $data[] = $NestedData;

        }

        $JsonData = array(
            "draw" => intval($RequestData['draw']), // For every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
            "recordsTotal" => intval($TotalData), // Total number of records
            "recordsFiltered" => intval($TotalFiltered), // Total number of records after searching, if there is no searching then totalFiltered = totalData
            "data" => $data // Total data array
        );
        return json_encode($JsonData); // Send data as json format
    }

    /**
     * @Author: LN
     * @Created: March 16 2016
     * @Modified By:
     * @Comment:
     * @Todo:
     */
    public function changeStatus($UserIdx, $Status) {
        if ($Status == "0") {
            $status = "1";
        } else if($Status == "1") {
            $status = "0";
        }

        return DB::table($this->table)->where('UserIdx', $UserIdx)->update(['Status' => $status]);
    }

    /**
     * @Author: LN
     * @Created: March 16 2016
     * @Modified By:
     * @Comment:
     * @Todo:
     */
    public function getUserData($UserIdx) {
        return DB::table($this->table)->where("id", $UserIdx)->get();
    }

    /**
     * @Author: LN
     * @Created: March 16 2016
     * @Modified By:
     * @Comment:
     * @Todo:
     */
    public function deleteUser($UserIdx) {
        return DB::table($this->table)->where('UserIdx', $UserIdx)->delete();
    }

    /**
     * @Author: LN
     * @Created: March 16 2016
     * @Modified By:
     * @Comment:
     * @Todo:
     */
    public function getTotalNumberUser(){
        return DB::table($this->table)->where("UserType","Not Like","(1,2)")->count();
    }

    /**
     * @Author: LN
     * @Created: March 16 2016
     * @Modified By:
     * @Comment:
     * @Todo:
     */
    public function viewUser($user_id){
        return DB::table($this->table)->where("UserIdx",$user_id)->get();
    }

    /**
     * @Author: LN
     * @Created: March 16 2016
     * @Modified By:
     * @Comment:
     * @Todo:
     */
    public function  getActiveUser(){
      return DB::table($this->table)->where('UserType','3')->where('Status','1')->orderBy('FirstName')->get();
    }

    /**
     * @Author: LN
     * @Created: 4-Aug-2016
     * @Modified By: Hitesh Tank
     * @Comment:getAveragerating
     * @Todo:
     */
    public static function getAveragerating($userId){
        $query=DB::table('job_review_rating');
        $query->where('seeker_id',$userId);
        $query->select('rating');
        $rating=$query->get();
        $i=0;
        $Total=0;
        $Average=0;
        $onestar=0;$twostar=0;$threestar=0;$fourstar=0;$fivestar=0;

        foreach($rating  as $rate){
          $Total+=$rate->rating;
          $i++;
        }

        if($Total>0)
          $Average=round($Total/$i,2);
        else
          $Average=0;

        if($Average>0)
          $Average=round($Average*2, 0)/2;
        else
          $Average=0;
        //$Average = floor($Total*$i)/$i;

        if($Average>5)
          $Average=5;

        return $Average;

    }


    public static function getReviewsRatings($userId){
        $sql = "SELECT job_review_rating.*,users.first_name,last_name FROM job_review_rating left join users on users.id = job_review_rating.poster_id";
        $sql.= " WHERE seeker_id = '".$userId."'";
        $GetData = DB::select($sql);
        return $GetData;
    }
}
?>
