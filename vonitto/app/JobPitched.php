<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobPitched extends Model
{
    protected $table = "job_pitched";
    protected $fillable = ['job_seeker_id','job_id','date','comments'];
    public $timestamps = false;
}
