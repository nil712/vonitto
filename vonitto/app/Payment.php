<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $table = "payments";
    protected $fillable = ['payer_id','txn_id','payment_status','subscr_id','payment_date','payer_status','subscription_id','payment_for','job_seeker_id','is_recurring'];
    public $timestamps = false;
}
