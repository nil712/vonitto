<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;
use App\Http\Requests\Request;

class UserDetail extends Model
{
  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = "users";
  protected $primaryKey = "id";
  public $timestamps = false;

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = ['id','first_name','last_name','email','password','remember_token','role','image','register_as','company_name','created_at','updated_at','status','last_login','login_ip','from_device','activation_code','company_link','street_address1','street_address2','city','state','zip','country','phone'];

  /**
   * The attributes excluded from the model's JSON form.
   *
   * @var array
   */
  protected $hidden = [''];



    /**
    * @Author: LN
    * @Created: March 14 2016
    * @Modified By: Ankit Sompura
    * @Comment:
    * @Todo: return json for datatable
    */
    public function getListing($Data){
      $RequestData = $Data;
      $Columns     = array(
          // Datatable column index  => Database column name
          0 => 'Id',
          1 => 'Image',
          2 => 'first_name',
          3 => 'last_name',
          4 => 'email',
          5 => 'role'
      );

      $TotalData     = DB::table("users")->count();
      $TotalFiltered = $TotalData; // When there is no search parameter then total number rows = total number filtered rows.

      $sql = "SELECT * FROM users";

      if (!empty($RequestData['search']['value'])) {
          // If there is a search parameter, $RequestData['search']['value'] contains search parameter
          $sql .= " WHERE (id LIKE '" . $RequestData['search']['value'] . "%' ";
          $sql .= " OR first_name LIKE '" . $RequestData['search']['value'] . "%' ";
          $sql .= " OR last_name LIKE '" . $RequestData['search']['value'] . "%' ";
          $sql .= " OR role LIKE '" . $RequestData['search']['value'] . "%' ";
          $sql .= " OR email LIKE '" . $RequestData['search']['value'] . "%'";
          $sql .= " AND role NOT IN ('sAdmin')) ";
      }else{
          $sql .= " WHERE role != 'sAdmin'";
          if (!empty($RequestData['columns'][2]['search']['value'])) { // Id
              $sql .= " AND first_name LIKE '%" . $RequestData['columns'][2]['search']['value'] . "%' ";
          }
          if (!empty($RequestData['columns'][3]['search']['value'])) { // Id
              $sql .= " AND last_name LIKE '%" . $RequestData['columns'][3]['search']['value'] . "%' ";
          }
          if (!empty($RequestData['columns'][4]['search']['value'])) { // Id
              $sql .= " AND email LIKE '%" . $RequestData['columns'][4]['search']['value'] . "%' ";
          }
          if (!empty($RequestData['columns'][5]['search']['value'])) { // Id
              $sql .= " AND role = '" . $RequestData['columns'][5]['search']['value'] . "' ";
          }
      }

      $TempData      = DB::select($sql);
      $TotalFiltered = count($TempData);
      $sql .= " ORDER BY " . $Columns[$RequestData['order'][0]['column']] . "   " . $RequestData['order'][0]['dir'] . "  LIMIT " . $RequestData['start'] . " ," . $RequestData['length'] . "   ";

      $GetData = DB::select($sql);
      $data    = array();
      foreach ($GetData as $row) { // Preparing an array

          if ($row->status == '1') {
              $Status = '<td style="padding: 2px 2px;"><a style="width:90px;" class="btn btn-sm btn-danger btn-flat btn-change-status" href="javascript:void(0);" data-user-id="'.$row->id.'" data-status="'.$row->status.'"> <i class="fa fa-close"></i> Inactive</a></td>';
          } else {
              $Status = '<td style="padding: 2px 2px;"><a style="width:90px;" class="btn btn-sm btn-success btn-flat btn-change-status" href="javascript:void(0);" data-user-id="'.$row->id.'" data-status="'.$row->status.'"> <i class="fa fa-check"></i> Activate</a></td>';
          }


          $Edit = '<td style="padding: 2px 2px;"><a title="Edit" style="width:90px;" href="'.url("/").'/admin/userdetail/'.$row->id.'/edit" data-page-id="'.$row->id.'"> <i class="fa fa-edit" style="color: #000 !important;"></i></a></td>';

          $View = '<td style="padding: 2px 2px;"><a title="View" style="width:90px;" href="'.url("/").'/admin/userdetail/view/'.$row->id.'" data-page-id="'.$row->id.'"> <i class="fa fa-eye" style="color: #000 !important;"></i></a></td>';

          $Delete = '<td style="padding: 2px 2px;"><a title="Delete" style="width:90px;" class="deleteCourseUnitBtn" href="javascript:void(0);" data-page-id="'.$row->id.'"> <i class="fa fa-trash-o" style="color: #000 !important;"></i></a></td>';

          if(isset($row->image) && !empty($row->image)){
              if($row->role=="JOB_POSTER"){
                  $thumbail_url = url("/").'/resources/assets/upload/jobposter/'.$row->image;
              }else{
                 $thumbail_url = url("/").'/resources/assets/upload/jobseeker/'.$row->image;
              }
          }else{
              $thumbail_url = url("/").'/resources/assets/admin/dist/img/default.png';
          }

          $NestedData   = array();
          $NestedData[] = $row->id;
          $NestedData[] = "<img src='".$thumbail_url."' style='height:50px;width:50px;'>";
          $NestedData[] = $row->first_name;
          $NestedData[] = $row->last_name;
          //$NestedData[] = "<img src='".$thumbail_url."' style='height:50px;width:50px;'>";
          $NestedData[] = $row->email;
          $NestedData[] = $row->role;
          $NestedData[] = '<table>
                              <tr>
                                  '.$View.'
                                  '.$Edit.'
                                  '.$Delete.'
                              </tr>
                          </table>';

          $data[] = $NestedData;

      }

      $JsonData = array(
          "draw" => intval($RequestData['draw']), // For every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
          "recordsTotal" => intval($TotalData), // Total number of records
          "recordsFiltered" => intval($TotalFiltered), // Total number of records after searching, if there is no searching then totalFiltered = totalData
          "data" => $data // Total data array
      );
      return json_encode($JsonData); // Send data as json format
    }

    public function getMaxOrder(){
      return DB::table($this->table)->count();
    }

    public function getUserData($id){
          return DB::table('users')->where('id','=',$id)->get();
    }


}
