<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class PitchDocuments extends Model
{
  /* The database table used by the model.
  *
  * @var string
  */
  protected $table = 'job_pitched_documents';
  protected $primaryKey = 'id';
  public $timestamps = false;
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
   protected $fillable = ['pitched_id','filename','created_at'];
}
