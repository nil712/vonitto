<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;
use App\Http\Requests\Request;

class Subscription extends Model
{
  protected $table = "subscription";
  protected $primaryKey = 'id';
  /**
   * The attributes that are mass assignable.
   * @var array
   */
  protected $fillable = ['subscription_title','subscription_desc','subscription_type','subscription_amount','subscription_period','	subscription_applicable_for','free_for_month','	additional_for_month'];
  protected $hidden = ['created_at','updated_at'];

  /**
    * @Author: LN
    * @Created: March 14 2016
    * @Modified By: Ankit Sompura
    * @Comment:
    * @Todo: return json for datatable
    */
    public function getListing($Data){
      $RequestData = $Data;
      $Columns     = array(
          // Datatable column index  => Database column name
          0 => 'Id',
          1 => 'Subscription Title',
          2 => 'Subscription Type',
          3 => 'Subscription Amount',
          4 => 'Subscription Period',
      );

      $TotalData     = DB::table("subscription")->count();
      $TotalFiltered = $TotalData; // When there is no search parameter then total number rows = total number filtered rows.

      $sql = "SELECT * FROM subscription";

      if (!empty($RequestData['search']['value'])) {
          // If there is a search parameter, $RequestData['search']['value'] contains search parameter
          $sql .= " WHERE (id LIKE '" . $RequestData['search']['value'] . "%' ";
          $sql .= " OR subscription_title LIKE '" . $RequestData['search']['value'] . "%'";
          $sql .= " OR subscription_type LIKE '" . $RequestData['search']['value'] . "%'";
          $sql .= " OR subscription_amount LIKE '" . $RequestData['search']['value'] . "%'";
          $sql .= " OR subscription_period LIKE '" . $RequestData['search']['value'] . "%')";
      }

      $TempData      = DB::select($sql);
      $TotalFiltered = count($TempData);
      $sql .= " ORDER BY " . $Columns[$RequestData['order'][0]['column']] . "   " . $RequestData['order'][0]['dir'] . "  LIMIT " . $RequestData['start'] . " ," . $RequestData['length'] . "   ";

      $GetData = DB::select($sql);
      $data    = array();
      foreach ($GetData as $row) { // Preparing an array

          $Edit = '<td style="padding: 2px 2px;"><a style="width:90px;"  href="'.url("/").'/admin/subscription/'.$row->id.'/edit" data-page-id="'.$row->id.'"> <i class="fa fa-edit" style="color:#000;"></i></a></td>';

          $Delete = '<td style="padding: 2px 2px;"><a style="width:90px;" class="deleteCourseUnitBtn" href="javascript:void(0);" data-page-id="'.$row->id.'"> <i class="fa fa-trash-o" style="color:#000;"></i></a></td>';


          $NestedData   = array();
          $NestedData[] = $row->id;
          $NestedData[] = $row->subscription_title;
          $NestedData[] = $row->subscription_type;
          $NestedData[] = $row->subscription_amount;
          $NestedData[] = $row->subscription_period;
          $NestedData[] = '<table>
                            <tr>
                                '.$Edit.'
                                '.$Delete.'
                            </tr>

                          </table>';

          $data[] = $NestedData;

      }

      $JsonData = array(
          "draw" => intval($RequestData['draw']), // For every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
          "recordsTotal" => intval($TotalData), // Total number of records
          "recordsFiltered" => intval($TotalFiltered), // Total number of records after searching, if there is no searching then totalFiltered = totalData
          "data" => $data // Total data array
      );
      return json_encode($JsonData); // Send data as json format
    }

    public function getMaxOrder(){
      return DB::table($this->table)->count();
    }

    public function GetSubscriptionList(){
      return DB::table('subscription')->select('id', 'subscription_title')->get();
    }
}
