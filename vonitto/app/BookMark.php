<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class BookMark extends Model
{
  /**
   * The database table used by the model.
   *
   * @var string
   */
   protected $table = 'job_bookmark';
   protected $primaryKey = 'id';
   public $timestamps = false;
   /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = ['poster_id','seeker_id','job_id','bookmark_status','created_at'];


}
