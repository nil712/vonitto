<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class BookMarkEmail extends Model
{
  /**
   * The database table used by the model.
   *
   * @var string
   */
   protected $table = 'job_bookmark_email';
   protected $primaryKey = 'id';
   public $timestamps = false;
   /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = ['job_id','seeker_id','poster_id','email','content','mail_status','url'];
}
