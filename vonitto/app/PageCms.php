<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

class PageCms extends Model
{
  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = "page_cms";
  protected $primaryKey = "id";
  public $timestamps = false;

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = ['id','page_name','page_text','	image','slug','created_at','updated_at','status'];

  /**
   * The attributes excluded from the model's JSON form.
   *
   * @var array
   */
  protected $hidden = [''];



    /**
    * @Author: LN
    * @Created: March 14 2016
    * @Modified By: Ankit Sompura
    * @Comment:
    * @Todo: return json for datatable
    */
    public function getListing($Data){
      $RequestData = $Data;
      $Columns     = array(
          // Datatable column index  => Database column name
          0 => 'Id',
          1 => 'Image',
          2 => 'Page Name'
      );

      $TotalData     = DB::table("page_cms")->count();
      $TotalFiltered = $TotalData; // When there is no search parameter then total number rows = total number filtered rows.

      $sql = "SELECT * FROM page_cms ";

      if (!empty($RequestData['search']['value'])) {
          // If there is a search parameter, $RequestData['search']['value'] contains search parameter
          $sql .= " WHERE (id LIKE '" . $RequestData['search']['value'] . "%' ";
          $sql .= " OR image LIKE '" . $RequestData['search']['value'] . "%' ";
          $sql .= " OR page_name LIKE '" . $RequestData['search']['value'] . "%') ";
      }

      $TempData      = DB::select($sql);
      $TotalFiltered = count($TempData);
      $sql .= " ORDER BY " . $Columns[$RequestData['order'][0]['column']] . "   " . $RequestData['order'][0]['dir'] . "  LIMIT " . $RequestData['start'] . " ," . $RequestData['length'] . "   ";

      $GetData = DB::select($sql);
      $data    = array();
      foreach ($GetData as $row) { // Preparing an array

          $Edit = '<td style="padding: 2px 2px;"><a title="Edit" style="width:90px;"  href="'.url("/").'/admin/pagecms/'.$row->id.'/edit" data-page-id="'.$row->id.'"> <i class="fa fa-edit" style="color:#000;"></i></a></td>';

          $Delete = '<td style="padding: 2px 2px;"><a title="Delete" style="width:90px;" class="deleteCourseUnitBtn" href="javascript:void(0);" data-page-id="'.$row->id.'"> <i class="fa fa-trash-o" style="color:#000;"></i></a></td>';


		$image =  $row->image;
        if(isset($image) && !empty($image)){
            $path = url("/")."/public/upload/page_cms/".$image;

         }else{
            $path = url("/").'/resources/assets/admin/dist/img/default.png';
         }

          $thumbail_url = "<img src='".$path."' style='height:50px;width:50px;'>";


          $NestedData   = array();
          $NestedData[] = $row->id;
          $NestedData[] = $thumbail_url;
          $NestedData[] = $row->page_name;
          $NestedData[] = '<table>
                            <tr>
                                '.$Edit.'
                                '.$Delete.'
                            </tr>

                          </table>';

          $data[] = $NestedData;

      }

      $JsonData = array(
          "draw" => intval($RequestData['draw']), // For every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
          "recordsTotal" => intval($TotalData), // Total number of records
          "recordsFiltered" => intval($TotalFiltered), // Total number of records after searching, if there is no searching then totalFiltered = totalData
          "data" => $data // Total data array
      );
      return json_encode($JsonData); // Send data as json format
    }

    public function getMaxOrder(){
      return DB::table($this->table)->count();
    }

    public function getActiveCmsLink(){
       return DB::table($this->table)->where('status','1')->orderBy('page_name')->get();

   }
     public function findData($slug){
       return DB::table($this->table)->where('slug',$slug)->get();
     }
}
