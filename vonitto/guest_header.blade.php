<div class="container_menu">
   <div class="logo"><a href="{{url('/')}}"><img src="{{URL::asset('resources/assets/images/logo.png')}}" class="img_responsive_max" alt=""/></a></div>
   <a href="javascript:void(0)" class="menuBtn"> <span class="iconMenu"> <span class="bar1"></span> <span class="bar2"></span> <span class="bar3"></span> </span> </a>
   <div class="right_side_menu menu_links">
      <ul>
         <li class="active call"><a href="#"> <i class="fa fa-phone"></i>1-873-487-2987</a></li>
         {{--*/
                use App\PageCms;
              $Page = PageCms::Find('3');
        /*--}}
         <li><a href="{{url('cms/')}}/{{$Page->slug}}">How It Works</a></li>
         @if(Auth::guest())
         <!-- <li ><a href="#" data-toggle="modal" data-target="#signup_model">Sign Up</a></li> -->
         <li ><a href="{{url('/register')}}">Sign Up</a></li>
         <li ><a href="#" data-toggle="modal" data-target="#login_model">Login</a></li>
         @else
          <li ><a href="#"><span class="fa fa-user">&nbsp;</span> Hi, {{Auth::user()->first_name}} {{Auth::user()->last_name}}</a></li>
            @if(Auth::user()->user_role_id==2)
              <li ><a href="{{url('property')}}"><span class="fa fa-list">&nbsp;</span>Manage Properties</a></li>
            @else
              <li ><a href="{{url('admin/dashboard')}}"><span class="fa fa-list">&nbsp;</span>Manage Admin Panel</a></li>
            @endif
          <li ><a href="{{url('/logout')}}"><span class="fa fa-power-off">&nbsp;</span>Logout</a></li>
         @endif
      </ul>
   </div>
   <div class="clearfix"></div>
</div>
