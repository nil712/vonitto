-- phpMyAdmin SQL Dump
-- version 4.4.10
-- http://www.phpmyadmin.net
--
-- Host: 10.169.0.94
-- Generation Time: Aug 05, 2016 at 02:49 PM
-- Server version: 5.6.30
-- PHP Version: 5.3.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `letsnurt_vonitto`
--

-- --------------------------------------------------------

--
-- Table structure for table `adj_params_master`
--

CREATE TABLE IF NOT EXISTS `adj_params_master` (
  `id` bigint(10) NOT NULL,
  `field_name` varchar(254) NOT NULL,
  `field_code` varchar(254) NOT NULL,
  `field_value` varchar(254) NOT NULL,
  `related_with` enum('JOB_POSTER','JOB_SEEKER') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `job_awarded`
--

CREATE TABLE IF NOT EXISTS `job_awarded` (
  `id` int(11) NOT NULL,
  `job_seeker_id` int(11) NOT NULL,
  `job_poster_id` int(11) NOT NULL,
  `job_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `job_awarded`
--

INSERT INTO `job_awarded` (`id`, `job_seeker_id`, `job_poster_id`, `job_id`, `created_at`) VALUES
(1, 70, 59, 55, '2016-07-18 10:46:07'),
(4, 95, 98, 62, '2016-07-29 12:23:27'),
(6, 96, 97, 64, '2016-08-02 07:13:44'),
(7, 96, 97, 68, '2016-08-02 09:40:16'),
(8, 96, 97, 70, '2016-08-02 09:54:28'),
(9, 149, 150, 71, '2016-08-02 10:11:43'),
(11, 70, 150, 72, '2016-08-02 10:46:38'),
(12, 70, 59, 73, '2016-08-02 12:15:48'),
(13, 149, 150, 74, '2016-08-02 13:50:32'),
(15, 149, 150, 75, '2016-08-02 14:16:14'),
(16, 149, 150, 77, '2016-08-03 04:34:43'),
(18, 149, 150, 76, '2016-08-05 06:19:24'),
(19, 149, 150, 79, '2016-08-05 07:20:53'),
(20, 149, 150, 78, '2016-08-05 07:26:27'),
(21, 149, 150, 81, '2016-08-05 07:32:54'),
(22, 149, 150, 80, '2016-08-05 07:34:01'),
(23, 149, 150, 82, '2016-08-05 08:44:04'),
(24, 149, 150, 84, '2016-08-05 12:48:46'),
(25, 149, 150, 83, '2016-08-05 13:45:19');

-- --------------------------------------------------------

--
-- Table structure for table `job_bookmark`
--

CREATE TABLE IF NOT EXISTS `job_bookmark` (
  `id` bigint(11) NOT NULL,
  `poster_id` bigint(11) NOT NULL,
  `seeker_id` bigint(11) NOT NULL,
  `job_id` bigint(10) NOT NULL,
  `bookmark_status` enum('bookmark','unbookmark') NOT NULL DEFAULT 'bookmark',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `job_bookmark`
--

INSERT INTO `job_bookmark` (`id`, `poster_id`, `seeker_id`, `job_id`, `bookmark_status`, `created_at`) VALUES
(1, 97, 149, 69, 'bookmark', '2016-08-05 13:48:42');

-- --------------------------------------------------------

--
-- Table structure for table `job_category`
--

CREATE TABLE IF NOT EXISTS `job_category` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `job_category`
--

INSERT INTO `job_category` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Culture work', '0000-00-00 00:00:00', '2016-06-14 14:32:07'),
(2, 'Education', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'Health', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'Hospitality', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 'Industrial', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 'Office & Workplace', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 'Residential', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 'Retail', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 'Sports', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, 'Transportation', '0000-00-00 00:00:00', '2016-06-14 08:45:03');

-- --------------------------------------------------------

--
-- Table structure for table `job_default_logos`
--

CREATE TABLE IF NOT EXISTS `job_default_logos` (
  `id` int(11) NOT NULL,
  `job_category_id` int(11) NOT NULL,
  `logo` varchar(500) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `job_default_logos`
--

INSERT INTO `job_default_logos` (`id`, `job_category_id`, `logo`, `status`) VALUES
(1, 1, 'dummy-logo.jpg', '1'),
(2, 2, 'dummy-logo.jpg', '1'),
(3, 3, 'dummy-logo.jpg', '1'),
(4, 4, 'dummy-logo.jpg', '1'),
(5, 5, 'dummy-logo.jpg', '1'),
(6, 6, 'dummy-logo.jpg', '1'),
(7, 7, 'dummy-logo.jpg', '1'),
(8, 8, 'dummy-logo.jpg', '1'),
(9, 9, 'dummy-logo.jpg', '1'),
(10, 10, 'dummy-logo.jpg', '1');

-- --------------------------------------------------------

--
-- Table structure for table `job_pitched`
--

CREATE TABLE IF NOT EXISTS `job_pitched` (
  `id` int(11) NOT NULL,
  `job_seeker_id` int(11) NOT NULL,
  `job_id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `free_request` enum('0','1') NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=143 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `job_pitched`
--

INSERT INTO `job_pitched` (`id`, `job_seeker_id`, `job_id`, `date`, `free_request`) VALUES
(104, 60, 49, '2016-07-08 06:40:41', '1'),
(114, 70, 56, '2016-07-18 12:59:37', '1'),
(115, 70, 46, '2016-07-18 13:27:49', '0'),
(117, 70, 57, '2016-07-18 14:01:38', '0'),
(119, 70, 58, '2016-07-19 13:40:17', '1'),
(120, 60, 60, '2016-07-26 22:56:24', '1'),
(121, 146, 61, '2016-07-27 04:45:40', '1'),
(122, 95, 62, '2016-07-29 12:21:01', '1'),
(123, 96, 64, '2016-08-02 07:02:44', '1'),
(124, 96, 68, '2016-08-02 09:39:35', '0'),
(125, 96, 70, '2016-08-02 09:53:34', '0'),
(126, 149, 72, '2016-08-02 10:09:33', '1'),
(127, 149, 71, '2016-08-02 10:11:01', '0'),
(128, 96, 72, '2016-08-02 10:31:11', '0'),
(129, 70, 72, '2016-08-02 10:45:23', '0'),
(130, 70, 73, '2016-08-02 12:14:58', '0'),
(131, 149, 74, '2016-08-02 13:17:19', '0'),
(132, 149, 75, '2016-08-02 14:09:52', '0'),
(133, 149, 77, '2016-08-03 04:31:37', '0'),
(134, 149, 76, '2016-08-03 04:36:24', '0'),
(135, 149, 66, '2016-08-05 05:33:41', '0'),
(136, 149, 79, '2016-08-05 07:20:19', '0'),
(137, 149, 78, '2016-08-05 07:23:38', '0'),
(138, 149, 81, '2016-08-05 07:32:29', '0'),
(139, 149, 80, '2016-08-05 07:33:43', '0'),
(140, 149, 82, '2016-08-05 08:42:42', '0'),
(141, 149, 84, '2016-08-05 12:47:37', '0'),
(142, 149, 83, '2016-08-05 13:43:39', '0');

-- --------------------------------------------------------

--
-- Table structure for table `job_post`
--

CREATE TABLE IF NOT EXISTS `job_post` (
  `id` bigint(10) NOT NULL,
  `job_project_id` varchar(254) NOT NULL,
  `title` varchar(254) NOT NULL,
  `job_logo` varchar(300) NOT NULL,
  `description` text NOT NULL,
  `what_you_want` text NOT NULL,
  `what_kind_job` varchar(255) NOT NULL,
  `project_deadline` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `category_id` int(11) NOT NULL,
  `location_city` varchar(254) NOT NULL,
  `location_country` varchar(254) NOT NULL,
  `budget` varchar(254) NOT NULL,
  `file` varchar(255) NOT NULL,
  `skills` text NOT NULL,
  `how_long` varchar(255) NOT NULL,
  `is_one_time` tinyint(4) NOT NULL,
  `job_poster_id` bigint(10) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL,
  `is_active` int(10) NOT NULL,
  `progress_status` enum('Assigned','In-Progress','Completed') DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=85 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `job_post`
--

INSERT INTO `job_post` (`id`, `job_project_id`, `title`, `job_logo`, `description`, `what_you_want`, `what_kind_job`, `project_deadline`, `category_id`, `location_city`, `location_country`, `budget`, `file`, `skills`, `how_long`, `is_one_time`, `job_poster_id`, `created_at`, `updated_at`, `is_active`, `progress_status`) VALUES
(39, 'PYC_20160706_39', 'Online Shopping Web', 'dummy-logo.jpg', 'We want to create a online shopping site just like a flipkart.', 'testing', 'tesssssty', '2016-07-18 09:56:38', 6, 'Atlanta', 'UK', '$50000', 'a:1:{i:0;s:13:"146778628168.";}', 'angular, php, mysql', 'More than 6 months', 0, 59, '2016-07-06 05:24:41', '2016-07-06 06:24:41', 1, NULL),
(43, 'PYC_20160706_43', 'IOS Developer', 'dummy-logo.jpg', 'We need ios developer for a one application project', 'if you are interested then please contact us.', '', '2016-07-18 09:56:38', 6, 'Atlanta', 'UK', '$1000', 'a:1:{i:0;s:13:"146779553560.";}', 'java, database', 'More than 6 months', 0, 59, '2016-07-06 07:58:55', '2016-07-06 12:16:28', 1, 'Assigned'),
(44, 'PYC_20160706_44', 'Quality Analyst', 'dummy-logo.jpg', 'Need 1-2 year experience person', 'Good background in testing field', '', '2016-07-18 09:56:38', 5, 'Rajkot', 'Malaysia', '$400', 'a:1:{i:0;s:13:"146781169745.";}', 'GOOD BACKGROUND IN TESTING FIELD\r\nGOOD BACKGROUND IN TESTING FIELD\r\nGOOD BACKGROUND IN TESTING FIELD', 'Less than 1 week', 0, 59, '2016-07-06 12:28:17', '2016-07-06 13:28:17', 1, NULL),
(45, 'PYC_20160707_45', 'Architecture', 'dummy-logo.jpg', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '', '2016-07-18 09:56:38', 4, 'Rajkot', 'Malaysia', '$10000', 'a:1:{i:0;s:15:"14678683139.png";}', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'Less than 1 week', 0, 98, '2016-07-07 04:11:53', '2016-07-07 05:11:53', 1, NULL),
(46, 'PYC_20160707_46', 'Test Engineer', 'dummy-logo.jpg', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '', '2016-07-30 09:56:38', 7, 'Ahamedabad', 'Malaysia', '$100', '', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'Less than 1 month', 0, 98, '2016-07-07 04:39:00', '2016-07-07 05:39:00', 1, NULL),
(47, 'PYC_20160707_47', 'UI developer', 'dummy-logo.jpg', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '', '2016-07-18 09:56:38', 8, 'tester', 'Malaysia', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has s', '', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '3 to 6 months', 0, 98, '2016-07-07 04:54:38', '2016-07-27 10:33:23', 1, NULL),
(48, 'PYC_20160707_48', 'development', 'dummy-logo.jpg', 'require for developer', 'REQUIRE', '', '2016-07-18 09:56:38', 5, 'MEXO', 'Malaysia', '25252', '', 'DEVELOPMENT SKILL', '3 to 6 months', 0, 103, '2016-07-07 05:26:34', '2016-07-07 06:26:34', 1, NULL),
(49, 'PYC_20160707_49', 'PHP DEVELOPER', 'dummy-logo.jpg', 'COMPLETE PHP KNOWLEDGE', 'Require', '', '2016-07-18 09:56:38', 6, 'year', 'UK', 'sadsd', '', 'fsdffdgfg', '3 to 6 months', 0, 103, '2016-07-07 05:29:51', '2016-07-07 06:29:51', 1, NULL),
(50, 'PYC_20160707_50', 'PHP DEVELOPER', 'dummy-logo.jpg', 'COMPLETE PHP KNOWLEDGE', 'Require', '', '2016-07-18 09:56:38', 6, 'year', 'UK', 'sadsd', '', 'fdfdfgfgfg', '3 to 6 months', 0, 103, '2016-07-07 05:30:16', '2016-07-07 06:30:16', 1, NULL),
(51, 'PYC_20160707_51', 'QA', 'dummy-logo.jpg', 'sddfdf', '', '', '2016-07-18 09:56:38', 2, '', '', '', '', '', '1 to 3 months', 0, 103, '2016-07-07 05:53:36', '2016-07-07 06:53:36', 1, NULL),
(52, 'PYC_20160707_52', 'data', 'dummy-logo.jpg', 'entry', '', '', '2016-07-18 09:56:38', 8, '', '', '', '', '', 'More than 6 months', 0, 103, '2016-07-07 06:00:07', '2016-07-07 07:00:07', 1, NULL),
(53, 'PYC_20160710_53', 'Test', 'dummy-logo.jpg', 'Testing 123', 'Test', '', '2016-07-18 09:56:38', 4, 'London', 'UK', '10000', 'a:1:{i:0;s:16:"146816585127.jpg";}', 'autocad', 'Less than 1 month', 0, 94, '2016-07-10 14:50:51', '2016-07-10 15:50:51', 1, NULL),
(55, 'PYC_20160718_55', 'New Job', '14688381373.png', 'this is a testing job post here', 'hello', '', '2016-07-31 10:46:07', 1, 'Atlanta', 'UK', '$100', '', '', 'More than 6 months', 0, 59, '2016-07-18 09:35:37', '2016-08-02 11:13:56', 1, 'Completed'),
(56, 'PYC_20160718_56', 'fresh', 'dummy-logo.jpg', 'fresgher job', 'test', '', '2016-07-30 23:00:00', 2, 'Atlanta', 'UK', '', '', 'tsdfsadf', 'More than 6 months', 0, 59, '2016-07-18 10:07:49', '2016-07-18 11:07:49', 1, NULL),
(57, 'PYC_20160718_57', 'hahah', 'dummy-logo.jpg', 'ghaha', 'haha', '', '2016-07-29 23:00:00', 7, 'Atlanta', 'UK', '$100', '', 'ahahah', 'More than 6 months', 0, 59, '2016-07-18 10:10:38', '2016-07-18 11:10:38', 1, NULL),
(58, 'PYC_20160718_58', 'asdf', 'dummy-logo.jpg', 'dfasdf', '', '', '2016-07-31 03:07:00', 2, 'Atlanta', 'UK', '$100', '', 'asdfasdf', 'More than 6 months', 0, 59, '2016-07-18 10:13:24', '2016-07-18 11:13:24', 1, NULL),
(59, 'PYC_20160720_59', 'Marketing Manager', 'dummy-logo.jpg', 'researching and analysing market trends identifying target markets and how best to reach them coming up with marketing strategies planning campaigns and managing budgets organising the production of posters and brochures designing social media strategies attending trade shows, conferences and sales meetings making sure that campaigns run to deadline and are on budget monitoring and reporting on the effectiveness of strategies and campaigns managing a team of marketing executives and assistants.', 'You would usually work 9am to 5pm, Monday to Friday. Hours could be more irregular at busy times such as the run-up to a campaign launch. You may also need to attend trade fairs, exhibitions and networking events in the evening or at the weekend.\r\n\r\nYou would normally work in an office. You may also travel to meet clients and attend conferences and product launches', '', '2016-07-30 02:07:00', 7, 'test', 'Malaysia', '$200', 'a:1:{i:0;s:16:"146901013964.jpg";}', 'You would usually work 9am to 5pm, Monday to Friday. Hours could be more irregular at busy times such as the run-up to a campaign launch. You may also need to attend trade fairs, exhibitions and networking events in the evening or at the weekend.\r\n\r\nYou would normally work in an office. You may also travel to meet clients and attend conferences and product launches', '3 to 6 months', 0, 98, '2016-07-20 09:22:19', '2016-07-20 10:22:19', 1, NULL),
(60, 'PYC_20160720_60', 'test', '146901110922.jpg', 'asdfasdf', 'asdfasdf', '', '2016-07-31 03:07:00', 5, 'Atlanta', 'UK', '$100', '', 'asdfasdfa sdf asd fasdf asd fsadf', 'More than 6 months', 0, 59, '2016-07-20 09:38:29', '2016-07-27 07:28:47', 1, NULL),
(61, 'PYC_20160726_61', 'Testing July ', 'dummy-logo.jpg', 'test', 'sdfsdfsdfsdf\r\ndfsdfsdf\r\nsdfsdfsdf\r\nsdfsdfsdf\r\nsfsdfsdf\r\n', '', '2016-08-17 11:08:00', 9, 'London', 'UK', 'GBP300', '', '', '1 to 3 months', 0, 145, '2016-07-26 22:12:43', '2016-07-26 23:12:43', 1, NULL),
(62, 'PYC_20160729_62', 'Sales Engineer', 'dummy-logo.jpg', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '', '2016-09-28 04:09:00', 9, 'Ahmedabad', 'Malaysia', '$2000', '', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '1 to 3 months', 0, 98, '2016-07-29 11:00:30', '2016-07-30 10:04:03', 1, 'Completed'),
(63, 'PYC_20160801_63', 'test august', 'dummy-logo.jpg', 'sdfsdf', 'sdfsdf', '', '2016-08-24 10:08:00', 8, 'Nottingham', 'UK', 'GBP300', '', '', '1 to 3 months', 0, 94, '2016-08-01 21:42:12', '2016-08-01 22:42:12', 1, NULL),
(64, 'PYC_20160802_64', 'Code Analyst', 'dummy-logo.jpg', 'Simplify and enhance data collection performance and analysis. AMD CodeAnalyst Performance Analyzer helps software developers improve the performance of applications, drivers and system software. Well-tuned software delivers a better end-user experience through shorter response time, increased throughput and better resource utilization. AMD CodeAnalyst is a profiling tool used by developers worldwide to improve the performance of games and media-oriented applications, transaction processing, and high performance scientific/engineering computation', 'Simplify and enhance data collection performance and analysis. AMD CodeAnalyst Performance Analyzer helps software developers improve the performance of applications, drivers and system software. Well-tuned software delivers a better end-user experience through shorter response time, increased throughput and better resource utilization. AMD CodeAnalyst is a profiling tool used by developers worldwide to improve the performance of games and media-oriented applications, transaction processing, and high performance scientific/engineering computation', '', '2016-08-31 11:08:00', 4, 'Ghandhinagar', 'Malaysia', '$4000', '', 'Simplify and enhance data collection performance and analysis. AMD CodeAnalyst Performance Analyzer helps software developers improve the performance of applications, drivers and system software. Well-tuned software delivers a better end-user experience through shorter response time, increased throughput and better resource utilization. AMD CodeAnalyst is a profiling tool used by developers worldwide to improve the performance of games and media-oriented applications, transaction processing, and high performance scientific/engineering computation', '1 to 3 months', 0, 97, '2016-08-02 05:53:00', '2016-08-02 08:35:40', 1, 'Completed'),
(65, 'PYC_20160802_65', 'SEO Engineer', 'dummy-logo.jpg', 'The SEO/SMO Analyst is responsible for implementing SEO and social media strategies for clients. The SEO/SMO Analyst can quickly understand and support initiatives that will contribute to the goals and success of client campaigns.', 'The SEO/SMO Analyst is responsible for implementing SEO and social media strategies for clients. The SEO/SMO Analyst can quickly understand and support initiatives that will contribute to the goals and success of client campaigns.', '', '2016-08-25 11:08:00', 8, 'UK', 'UK', '$3000', 'a:1:{i:0;s:16:"147012090467.jpg";}', 'The SEO/SMO Analyst is responsible for implementing SEO and social media strategies for clients. The SEO/SMO Analyst can quickly understand and support initiatives that will contribute to the goals and success of client campaigns.', '1 to 3 months', 0, 97, '2016-08-02 05:55:04', '2016-08-02 06:55:04', 1, NULL),
(66, 'PYC_20160802_66', 'Test Job', 'dummy-logo.jpg', 'Test Job', 'Test Job', '', '2016-08-26 02:08:00', 3, 'test', 'Malaysia', '$2000', '', 'Test Job', 'Less than 1 month', 0, 97, '2016-08-02 08:31:27', '2016-08-02 09:31:29', 1, NULL),
(68, 'PYC_20160802_68', 'Tester', 'dummy-logo.jpg', 'Tester', 'Tester', '', '2016-08-24 02:08:00', 9, 'Tester', 'Malaysia', '400', '', 'Tester', '1 to 3 months', 0, 97, '2016-08-02 08:34:57', '2016-08-02 09:41:49', 1, 'Completed'),
(69, 'PYC_20160802_69', 'Job1', 'dummy-logo.jpg', 'Job1', 'Job1', '', '2016-08-30 02:08:00', 3, 'Job1', 'Malaysia', '566', '', 'Job1', '1 to 3 months', 0, 97, '2016-08-02 08:52:23', '2016-08-02 09:52:23', 1, NULL),
(70, 'PYC_20160802_70', 'QA', 'dummy-logo.jpg', 'QA', 'QA', '', '2016-08-26 02:08:00', 3, 'Tester', 'Malaysia', '4568746', '', 'QA', '3 to 6 months', 0, 97, '2016-08-02 08:53:01', '2016-08-02 09:56:12', 1, 'Completed'),
(71, 'PYC_20160802_71', 'My First JOB', 'dummy-logo.jpg', 'My First JOB', 'My First JOB', '', '2016-08-31 02:08:00', 4, 'My First JOB', 'Malaysia', '7000', '', 'My First JOB', 'Less than 1 week', 0, 150, '2016-08-02 09:08:11', '2016-08-02 10:12:49', 1, 'Completed'),
(72, 'PYC_20160802_72', 'My Second Job', 'dummy-logo.jpg', 'My Second Job', 'My Second Job', '', '2016-08-31 02:08:00', 6, 'My Second Job', 'Malaysia', '97898', '', 'My Second Job', '1 to 3 months', 0, 150, '2016-08-02 09:08:50', '2016-08-02 10:59:04', 1, 'Completed'),
(73, 'PYC_20160802_73', 'Manager', 'dummy-logo.jpg', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '', '2016-08-25 04:08:00', 5, 'Ahmedabad', 'Malaysia', '$7897', '', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '1 to 3 months', 0, 59, '2016-08-02 11:14:16', '2016-08-02 12:17:54', 1, 'Completed'),
(74, 'PYC_20160802_74', 'Testing JOb', 'dummy-logo.jpg', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '', '2016-08-25 05:08:00', 2, 'test', 'UK', '5666', '', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'Less than 1 month', 0, 150, '2016-08-02 12:16:18', '2016-08-02 14:05:39', 1, 'Completed'),
(75, 'PYC_20160802_75', 'QAQA', 'dummy-logo.jpg', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '', '2016-08-25 06:08:00', 3, 'Test', 'Malaysia', '56798', '', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '3 to 6 months', 0, 150, '2016-08-02 13:09:25', '2016-08-02 14:16:41', 1, 'Completed'),
(76, 'PYC_20160803_76', 'Technical Recruiter', 'dummy-logo.jpg', 'An in-house technical recruiter works to source candidates for open positions at technology companies and lends their tech background and understanding to the process. This can help them better connect with candidates and understand the job, leading to better hires. In addition to filling open positions, an in-house technical recruiter also develops long-tail relationships that may lead to hiring or further networking down the road. ', 'n the same way that a non-technical person would have an enormous learning curve coming into your company, including everything from the company’s overall technology concept to industry jargon, a general recruiter isn’t going to be as knowledgeable about the industry and what you need as a technical recruiter will. A technical recruiter will likely already speak the language and understand the industry, position and complex terminology that is associated with developers, programmers and engineers. ', '', '2016-08-24 08:08:00', 7, 'Tester LAnd', 'Malaysia', '$8000', 'a:3:{i:0;s:16:"147019843838.png";i:1;s:16:"147019843857.png";i:2;s:16:"147019843841.png";}', 'Because of these challenges, many tech companies are hiring technical recruiters to lead their hiring efforts. These specialized, skilled recruiters can make the process easier, cut down on hiring time and create long-term recruiting opportunities.', 'Less than 1 month', 0, 150, '2016-08-03 03:27:18', '2016-08-05 07:22:51', 1, 'Completed'),
(77, 'PYC_20160803_77', 'SEO Manager', 'dummy-logo.jpg', 'The SEO/SMO Manager is responsible for coordinating and implementing SEO & SMO strategies for clients. The correct person will be goal oriented, possess exceptional attention to detail, and have outstanding interpersonal skills. This is a role that is interacting with clients as well as working closely with the company team to make sure that the goals of the client are achieved', '\r\n    Perform keyword research in coordination with client business objectives to optimize existing content and uncover new opportunities\r\n    Provide SEO analysis and recommendations in coordination with elements and structure of websites and web pages\r\n    Provide recommendations and execute/manage strategies for content development in coordination with SEO goals – general and keyword specific\r\n    Help to create and support marketing content to socialize and use for social media purposes (e.g. customer videos briefs, customer case studies, blog posts, posts from analysts and customers)\r\n    Develop and implement link building campaigns\r\n    Develop, manage and execute communication/content strategies via social communities in coordination with client goals\r\n    Implement and administer search engine programs (XML sitemaps, shopping feeds, webmaster tools)\r\n    Monitor and evaluate search results and search performance across the major search channels in order to improve rankings\r\n    Research and administer social media tools in support of clients’ social media strategy\r\n    Monitor and evaluate web analytics dashboards and reports in order to develop and recommend SEO strategies\r\n    Communication to team and management on project development, timelines, and results', '', '2016-08-23 08:08:00', 4, 'MALAYSIA', 'Malaysia', '$8000', 'a:5:{i:0;s:16:"147019861921.odt";i:1;s:16:"147019861972.odt";i:2;s:15:"14701986194.odt";i:3;s:13:"147019861971.";i:4;s:13:"147019861963.";}', '\r\n    2+ years experience in Search Engine Marketing (SEM) and Search Engine Optimization (SEO)\r\n    Strong understanding of Search Engine Marketing (SEM) and Search Engine Optimization (SEO) process\r\n    Experience working with popular keyword tools (Google, WordTracker, Keyword Discovery, etc)\r\n    Experience working with CMS and building/administering content in multiple CMS environments\r\n    Knowledge of HTML/CSS and website administration\r\n    High-level proficiency in MS Excel, PowerPoint, and Word\r\n    Experience with website analysis using a variety of analytics tools including Google Analytics as well as internal reporting tools\r\n    Knowledge of PPC programs and optimizing data gathered from both organic and paid sources\r\n    BS/BA degree required', '1 to 3 months', 0, 150, '2016-08-03 03:30:20', '2016-08-03 04:38:08', 1, 'Completed'),
(78, 'PYC_20160805_78', 'HR Manager', 'dummy-logo.jpg', 'HR Manager', 'HR Manager', '', '2016-08-31 11:08:00', 8, 'Ahmedabad', 'Malaysia', '$4700', '', 'HR Manager', '1 to 3 months', 0, 150, '2016-08-05 06:14:14', '2016-08-05 07:27:21', 1, 'Completed'),
(79, 'PYC_20160805_79', 'DESINGNER', 'dummy-logo.jpg', 'Desingner', 'DESINGNER', '', '2016-08-27 11:08:00', 5, 'DESINGNER', 'Malaysia', '$4000', '', 'DESINGNER', '3 to 6 months', 0, 150, '2016-08-05 06:19:33', '2016-08-05 07:24:26', 1, 'Completed'),
(80, 'PYC_20160805_80', 'Data Maining', 'dummy-logo.jpg', 'Data Maining', 'Data Maining', '', '2016-09-30 11:09:00', 7, 'Data Maining', 'Malaysia', '$7000', '', 'Data Maining', '1 to 3 months', 0, 150, '2016-08-05 06:28:55', '2016-08-05 07:34:43', 1, 'Completed'),
(81, 'PYC_20160805_81', 'Accountant', 'dummy-logo.jpg', 'Accountant', 'Accountant', '', '2016-08-16 06:08:00', 8, 'Tester', 'Malaysia', '$9000', '', 'Accountant', '1 to 3 months', 0, 150, '2016-08-05 06:31:04', '2016-08-05 07:33:26', 1, 'Completed'),
(82, 'PYC_20160805_82', 'Mechanical Engineer', 'dummy-logo.jpg', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '', '2016-08-31 01:08:00', 5, 'test City', 'Malaysia', '$2000', 'a:2:{i:0;s:16:"147038640472.odt";i:1;s:16:"147038640455.odt";}', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '3 to 6 months', 0, 150, '2016-08-05 07:40:04', '2016-08-05 08:47:22', 1, 'Completed'),
(83, 'PYC_20160805_83', 'Sr. Java Developer', 'dummy-logo.jpg', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '', '2016-08-20 04:08:00', 3, 'Dreamcity', 'Malaysia', '$100000', '', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '3 to 6 months', 0, 150, '2016-08-05 10:38:21', '2016-08-05 13:46:11', 1, 'Completed'),
(84, 'PYC_20160805_84', 'Sr.Android Developer', 'dummy-logo.jpg', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '', '2016-08-27 05:08:00', 10, 'test City', 'Malaysia', '$40000', '', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '1 to 3 months', 0, 150, '2016-08-05 11:45:54', '2016-08-05 12:51:18', 1, 'Completed');

-- --------------------------------------------------------

--
-- Table structure for table `job_poster`
--

CREATE TABLE IF NOT EXISTS `job_poster` (
  `id` bigint(10) NOT NULL,
  `user_id` bigint(10) NOT NULL,
  `company_name` varchar(254) NOT NULL,
  `address` varchar(254) NOT NULL,
  `location_city` varchar(254) NOT NULL,
  `location_country` varchar(254) NOT NULL,
  `location_gps` varchar(254) NOT NULL,
  `url` varchar(500) NOT NULL,
  `contact1` varchar(254) NOT NULL,
  `contact2` varchar(254) NOT NULL,
  `email` varchar(254) NOT NULL,
  `ratings_points` double NOT NULL,
  `has_strength` int(10) NOT NULL,
  `is_active` int(10) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `job_poster_attachments`
--

CREATE TABLE IF NOT EXISTS `job_poster_attachments` (
  `id` bigint(10) NOT NULL,
  `job_poster_id` bigint(10) NOT NULL,
  `path` varchar(254) NOT NULL,
  `comments` varchar(254) NOT NULL,
  `user_id` bigint(10) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_active` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `job_poster_rating_comments`
--

CREATE TABLE IF NOT EXISTS `job_poster_rating_comments` (
  `id` bigint(10) NOT NULL,
  `job_poster_id` bigint(10) NOT NULL,
  `points` int(10) NOT NULL,
  `out_of` int(10) NOT NULL,
  `comments` varchar(254) NOT NULL,
  `created_by` bigint(10) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_active` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `job_poster_strength`
--

CREATE TABLE IF NOT EXISTS `job_poster_strength` (
  `id` bigint(10) NOT NULL,
  `job_poster_id` bigint(10) NOT NULL,
  `field_code` varchar(254) NOT NULL,
  `adj_params_master_id` bigint(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `job_post_attachments`
--

CREATE TABLE IF NOT EXISTS `job_post_attachments` (
  `id` bigint(10) NOT NULL,
  `job_post_id` bigint(10) NOT NULL,
  `path` varchar(254) NOT NULL,
  `comments` varchar(254) NOT NULL,
  `user_id` bigint(10) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_active` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `job_post_skills`
--

CREATE TABLE IF NOT EXISTS `job_post_skills` (
  `id` bigint(10) NOT NULL,
  `job_post_id` bigint(10) NOT NULL,
  `field_code` varchar(254) NOT NULL,
  `adj_params_master_id` bigint(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `job_review_rating`
--

CREATE TABLE IF NOT EXISTS `job_review_rating` (
  `id` int(10) NOT NULL,
  `job_post_id` bigint(10) NOT NULL,
  `seeker_id` bigint(11) NOT NULL,
  `poster_id` bigint(10) DEFAULT NULL,
  `rating` float NOT NULL,
  `review` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` enum('1','0') DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `job_review_rating`
--

INSERT INTO `job_review_rating` (`id`, `job_post_id`, `seeker_id`, `poster_id`, `rating`, `review`, `created_at`, `status`) VALUES
(25, 71, 149, 150, 3, 'test testtesttesttesttesttesttesttesttesttesttesttest', '2016-08-04 11:24:43', '1'),
(26, 72, 70, 150, 3, 'testtesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttest', '2016-08-04 11:25:57', '1'),
(27, 74, 149, 150, 3, 'testtesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttest', '2016-08-04 11:27:33', '1'),
(28, 75, 149, 150, 3, 'test test test test test test test test test test test test test test test test test test test test test tes', '2016-08-04 11:28:19', '1'),
(29, 77, 149, 150, 2, 'test test test test test test test test test test test test test test test test test test test test test test test test', '2016-08-04 11:30:58', '1'),
(30, 83, 149, 150, 4, 'Very Good Work!!!!!! Very Good Work!!!!!! Very Good Work!!!!!!', '2016-08-05 13:47:42', '1'),
(31, 84, 149, 150, 4, 'Testing Award Testing AwardTesting AwardTesting AwardTesting AwardTesting Award', '2016-08-05 13:48:35', '1');

-- --------------------------------------------------------

--
-- Table structure for table `job_seeker`
--

CREATE TABLE IF NOT EXISTS `job_seeker` (
  `id` bigint(10) NOT NULL,
  `user_id` bigint(10) NOT NULL,
  `company` varchar(254) NOT NULL,
  `location_city` varchar(254) NOT NULL,
  `location_country` varchar(254) NOT NULL,
  `year_of_experiance` varchar(100) NOT NULL,
  `location_gps` varchar(254) NOT NULL,
  `has_education` int(10) NOT NULL,
  `has_sector` int(10) NOT NULL,
  `has_specialisation` int(10) NOT NULL,
  `ratings_points` double NOT NULL,
  `is_active` int(10) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `subscription_id` bigint(10) NOT NULL,
  `subscription_is_active` int(10) NOT NULL,
  `subscription_start_date` datetime NOT NULL,
  `subscription_end_date` datetime NOT NULL,
  `no_of_bid_made` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `job_seekers_bid`
--

CREATE TABLE IF NOT EXISTS `job_seekers_bid` (
  `id` bigint(10) NOT NULL,
  `job_post_id` bigint(10) NOT NULL,
  `job_seeker_id` bigint(10) NOT NULL,
  `bid_amount` double NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_awarded` int(10) NOT NULL,
  `payment_id` bigint(10) NOT NULL,
  `subscription_id` bigint(10) NOT NULL,
  `subscription_is_active` int(10) NOT NULL,
  `subscription_start_date` datetime NOT NULL,
  `subscription_end_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `job_seekers_bid_attachments`
--

CREATE TABLE IF NOT EXISTS `job_seekers_bid_attachments` (
  `id` bigint(10) NOT NULL,
  `job_seekers_bid_id` bigint(10) NOT NULL,
  `path` varchar(254) NOT NULL,
  `comments` varchar(254) NOT NULL,
  `user_id` bigint(10) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_active` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `job_seeker_attachments`
--

CREATE TABLE IF NOT EXISTS `job_seeker_attachments` (
  `id` bigint(10) NOT NULL,
  `job_seeker_id` bigint(10) NOT NULL,
  `path` varchar(254) NOT NULL,
  `comments` varchar(254) NOT NULL,
  `created_by` bigint(10) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_active` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `job_seeker_education`
--

CREATE TABLE IF NOT EXISTS `job_seeker_education` (
  `id` bigint(10) NOT NULL,
  `job_seeker_id` bigint(10) NOT NULL,
  `field_code` varchar(254) NOT NULL,
  `adj_params_master_id` bigint(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `job_seeker_rating_comments`
--

CREATE TABLE IF NOT EXISTS `job_seeker_rating_comments` (
  `id` bigint(10) NOT NULL,
  `job_seeker_id` bigint(10) NOT NULL,
  `points` int(10) NOT NULL,
  `out_of` int(10) NOT NULL,
  `comments` varchar(254) NOT NULL,
  `created_by` bigint(10) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_active` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `job_seeker_sector`
--

CREATE TABLE IF NOT EXISTS `job_seeker_sector` (
  `id` bigint(10) NOT NULL,
  `job_seeker_id` bigint(10) NOT NULL,
  `field_code` varchar(254) NOT NULL,
  `adj_params_master_id` bigint(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `job_seeker_specialisation`
--

CREATE TABLE IF NOT EXISTS `job_seeker_specialisation` (
  `id` bigint(10) NOT NULL,
  `job_seeker_id` bigint(10) NOT NULL,
  `field_code` varchar(254) NOT NULL,
  `adj_params_master_id` bigint(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `old_job_post`
--

CREATE TABLE IF NOT EXISTS `old_job_post` (
  `id` bigint(10) NOT NULL,
  `title` varchar(254) NOT NULL,
  `description` text NOT NULL,
  `category_id` bigint(10) NOT NULL,
  `location_city` varchar(254) NOT NULL,
  `location_country` varchar(254) NOT NULL,
  `location_gps` varchar(254) NOT NULL,
  `estimation` varchar(254) NOT NULL,
  `job_poster_id` bigint(10) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_modified` datetime NOT NULL,
  `is_active` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `page_cms`
--

CREATE TABLE IF NOT EXISTS `page_cms` (
  `id` bigint(10) NOT NULL,
  `page_name` varchar(254) NOT NULL,
  `page_text` text NOT NULL,
  `image` varchar(254) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `status` enum('1','0') NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `page_cms`
--

INSERT INTO `page_cms` (`id`, `page_name`, `page_text`, `image`, `created_at`, `updated_at`, `status`) VALUES
(2, 'How It Works', '<p><strong><span style="line-height:1.6em">Job Poster</span></strong></p>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n\r\n<p><strong>Job Seeker&nbsp;</strong></p>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n\r\n<p>&nbsp;</p>\r\n', '', '2016-06-14 08:55:56', '0000-00-00 00:00:00', '1'),
(3, 'About Us', '<p><strong><span style="line-height:1.6em">Von</span></strong></p>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n\r\n<p><strong><span style="line-height:1.6em">Yvonne</span></strong></p>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n\r\n<p><strong><span style="line-height:1.6em">Why was Pitch Your Concepts formed?&nbsp;</span></strong></p>\r\n\r\n<p><span style="line-height:1.6em">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</span></p>\r\n\r\n<p><strong>How does the website work?</strong></p>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n', '', '2016-06-14 11:14:19', '0000-00-00 00:00:00', '1'),
(5, 'Quality', '<p>Step to reproduce:<br />\r\n1. Open admin panel<br />\r\n2. Login with valid credentials<br />\r\n3. Go to right side top corner of admin icon<br />\r\n&nbsp;</p>\r\n', '146599641372.jpg', '2016-06-15 12:49:33', '0000-00-00 00:00:00', '1'),
(6, 'Privacy Policy', '<p><span style="line-height:1.6em">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</span></p>\r\n', '', '2016-06-16 23:23:06', '0000-00-00 00:00:00', '1'),
(7, 'Terms and Conditions', '<p><span style="line-height:1.6em">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</span></p>\r\n', '', '2016-06-16 23:23:38', '0000-00-00 00:00:00', '1');

-- --------------------------------------------------------

--
-- Table structure for table `password_reminders`
--

CREATE TABLE IF NOT EXISTS `password_reminders` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('ravi.kadia@letsnurture.com', 'ca17b1874313444f41d4dcabe1bd205cc76eb105ea57dbb7e43f29cc7f69e7f2', '2015-09-25 05:17:36'),
('ankit.letsnurture@gmail.com', '9cb693fe3f7abf3172c842bb82cb653cf68fcccf3e24420f79b3be1bab02f3cf', '2015-09-29 03:21:02'),
('nilesh@letsnurture.com', 'd260411000242d7b42f4d7aa4483cc32338aab699e81ac3636414fad2b50b070', '2016-05-12 05:02:07'),
('peter@orphmedia.com', '836576de4980128bde343e181f19bf4de2415f06dc734b571a04f69344a9878f', '2016-06-23 04:41:08'),
('pradip.darji@letsnurture.com', '9fb80529500ecfad814b30a886ac40cdf969a1df11fc0fdf42947a7ea9ccae09', '2016-07-22 06:19:48'),
('Pradip.letsnurture@gmail.com', '48049c8c4e309fb608ac1ee9c53513942c66fc9747c63b3233ccc7d487b1e2b7', '2016-07-22 06:20:43'),
('qa@letsnurture.com', '4f784ad957510a43327f7959ae4992762e5362e549ea8e02cb40d0cf03481f8f', '2016-07-22 10:12:56');

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE IF NOT EXISTS `payments` (
  `id` bigint(10) NOT NULL,
  `payer_id` varchar(254) NOT NULL,
  `txn_id` varchar(254) NOT NULL,
  `subscr_id` varchar(255) NOT NULL,
  `payer_status` varchar(255) NOT NULL,
  `payment_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `payment_status` varchar(254) NOT NULL,
  `subscription_id` bigint(10) NOT NULL,
  `payment_for` enum('JOB_SEEKER','JOB_POST_BID') NOT NULL,
  `job_seeker_id` bigint(10) NOT NULL,
  `is_recurring` enum('0','1') NOT NULL DEFAULT '0',
  `subscr_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `txn_type` varchar(255) NOT NULL,
  `payment_type` varchar(255) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `payments`
--

INSERT INTO `payments` (`id`, `payer_id`, `txn_id`, `subscr_id`, `payer_status`, `payment_date`, `payment_status`, `subscription_id`, `payment_for`, `job_seeker_id`, `is_recurring`, `subscr_date`, `txn_type`, `payment_type`, `first_name`, `last_name`) VALUES
(21, 'QZSYUBTJ76KQS', '7CR50623SA980625P', '', '', '2016-08-02 23:00:00', 'Pending', 1, 'JOB_SEEKER', 70, '0', '0000-00-00 00:00:00', '', 'instant', 'Kalpesh', 'Joshi'),
(22, 'QZSYUBTJ76KQS', '', 'I-AE1Y3FU7GARE', 'verified', '2016-08-04 12:56:25', '', 1, 'JOB_SEEKER', 70, '1', '0000-00-00 00:00:00', 'subscr_signup', '', 'Kalpesh', 'Joshi'),
(23, '8C3VK9G5BL4RG', '2R594955PG0188247', '', '', '2022-05-11 23:00:00', 'Pending', 3, 'JOB_SEEKER', 149, '0', '0000-00-00 00:00:00', '', 'instant', 'Kalpesh', 'Joshi'),
(24, '8C3VK9G5BL4RG', '76A599216V053242D', '', '', '0000-00-00 00:00:00', 'Pending', 1, 'JOB_SEEKER', 149, '0', '0000-00-00 00:00:00', '', 'instant', 'Kalpesh', 'Joshi'),
(25, '8C3VK9G5BL4RG', '1LC93375GG992162V', '', '', '2022-10-03 23:00:00', 'Pending', 2, 'JOB_SEEKER', 149, '0', '0000-00-00 00:00:00', '', 'instant', 'Kalpesh', 'Joshi'),
(26, '8C3VK9G5BL4RG', '6U230244MM7845726', '', '', '0000-00-00 00:00:00', 'Pending', 3, 'JOB_SEEKER', 149, '0', '0000-00-00 00:00:00', '', 'instant', 'Kalpesh', 'Joshi'),
(27, '8C3VK9G5BL4RG', '0Y949550ML5383824', '', '', '0000-00-00 00:00:00', 'Pending', 3, 'JOB_SEEKER', 149, '0', '0000-00-00 00:00:00', '', 'instant', 'Kalpesh', 'Joshi'),
(28, '8C3VK9G5BL4RG', '9X366286V21439618', '', '', '0000-00-00 00:00:00', 'Pending', 1, 'JOB_SEEKER', 149, '0', '0000-00-00 00:00:00', '', 'instant', 'Kalpesh', 'Joshi'),
(29, '8C3VK9G5BL4RG', '98179145K1707310X', '', '', '0000-00-00 00:00:00', 'Pending', 3, 'JOB_SEEKER', 149, '0', '0000-00-00 00:00:00', '', 'instant', 'Kalpesh', 'Joshi'),
(30, '8C3VK9G5BL4RG', '6PX87254UA328752N', '', '', '0000-00-00 00:00:00', 'Pending', 1, 'JOB_SEEKER', 149, '0', '0000-00-00 00:00:00', '', 'instant', 'Kalpesh', 'Joshi'),
(31, '8C3VK9G5BL4RG', '6L75013535215083U', '', '', '0000-00-00 00:00:00', 'Pending', 2, 'JOB_SEEKER', 149, '0', '0000-00-00 00:00:00', '', 'instant', 'Kalpesh', 'Joshi'),
(32, '8C3VK9G5BL4RG', '37G57111KM793582W', '', '', '0000-00-00 00:00:00', 'Pending', 1, 'JOB_SEEKER', 149, '0', '0000-00-00 00:00:00', '', 'instant', 'Kalpesh', 'Joshi'),
(33, '8C3VK9G5BL4RG', '1R902757RH074563M', '', '', '0000-00-00 00:00:00', 'Pending', 3, 'JOB_SEEKER', 149, '0', '0000-00-00 00:00:00', '', 'instant', 'Kalpesh', 'Joshi'),
(34, '8C3VK9G5BL4RG', '0YM290826M436602S', '', '', '0000-00-00 00:00:00', 'Pending', 1, 'JOB_SEEKER', 70, '0', '0000-00-00 00:00:00', '', 'instant', 'Kalpesh', 'Joshi'),
(35, '8C3VK9G5BL4RG', '05M121675L940093X', '', '', '0000-00-00 00:00:00', 'Pending', 1, 'JOB_SEEKER', 70, '0', '0000-00-00 00:00:00', '', 'instant', 'Kalpesh', 'Joshi'),
(36, '8C3VK9G5BL4RG', '7EK19715EN385933D', '', '', '2016-08-05 07:25:02', 'Pending', 1, 'JOB_SEEKER', 70, '0', '0000-00-00 00:00:00', '', 'instant', 'Kalpesh', 'Joshi');

-- --------------------------------------------------------

--
-- Table structure for table `subscription`
--

CREATE TABLE IF NOT EXISTS `subscription` (
  `id` bigint(10) NOT NULL,
  `subscription_title` varchar(254) NOT NULL,
  `subscription_desc` text NOT NULL,
  `subscription_type` varchar(254) NOT NULL,
  `subscription_amount` varchar(100) NOT NULL,
  `subscription_period` varchar(254) NOT NULL,
  `subscription_applicable_for` varchar(254) NOT NULL,
  `free_for_month` int(10) NOT NULL,
  `additional_for_month` int(10) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status` enum('1','0') NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `subscription`
--

INSERT INTO `subscription` (`id`, `subscription_title`, `subscription_desc`, `subscription_type`, `subscription_amount`, `subscription_period`, `subscription_applicable_for`, `free_for_month`, `additional_for_month`, `created_at`, `updated_at`, `status`) VALUES
(1, 'Pitch Your Concept', 'Professional job seeker and the essentials to find them.', 'Pitch Your Concepts - Silver', '10', '1', '4', 1, 4, '2016-06-23 18:28:47', '2016-07-04 13:35:33', '1'),
(2, 'Pitch Your Concept', 'Professional job seeker and the essentials to find them.', 'Pitch Your Concepts - Pro', '15', '1', '7', 1, 7, '2016-06-23 19:19:47', '2016-07-04 13:35:20', '1'),
(3, 'Pitch Your Concept', 'Professional job seeker and the essentials to find them.', 'Pitch Your Concepts - Enterprise', '30', '1', '23', 1, 23, '2016-06-23 19:21:46', '2016-07-04 13:35:06', '1');

-- --------------------------------------------------------

--
-- Table structure for table `sys_defaults_params`
--

CREATE TABLE IF NOT EXISTS `sys_defaults_params` (
  `id` bigint(10) NOT NULL,
  `param_code` varchar(254) NOT NULL,
  `param_name` varchar(254) NOT NULL,
  `param_val` varchar(254) NOT NULL,
  `param_set` varchar(254) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(11) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `email` varchar(254) NOT NULL,
  `password` varchar(500) NOT NULL,
  `remember_token` varchar(100) NOT NULL,
  `role` enum('JOB_POSTER','JOB_SEEKER','sAdmin') NOT NULL,
  `image` varchar(254) NOT NULL,
  `register_as` varchar(254) NOT NULL,
  `company_name` varchar(254) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status` int(10) NOT NULL DEFAULT '0',
  `last_login` datetime NOT NULL,
  `login_ip` varchar(50) NOT NULL,
  `from_device` enum('mob','web') NOT NULL,
  `activation_code` varchar(254) NOT NULL,
  `company_link` varchar(255) NOT NULL,
  `street_address1` varchar(255) NOT NULL,
  `street_address2` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `state` varchar(255) NOT NULL,
  `zip` varchar(255) NOT NULL,
  `country` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `available_pitch` int(255) DEFAULT NULL,
  `free_pitch` int(11) NOT NULL DEFAULT '1',
  `experience` varchar(10) DEFAULT NULL,
  `graduation` varchar(254) DEFAULT NULL,
  `post_graduation` varchar(254) DEFAULT NULL,
  `computer_skill` text,
  `portfolio_image` varchar(100) DEFAULT NULL,
  `portfolio_description` text,
  `project_category_specialization` text
) ENGINE=InnoDB AUTO_INCREMENT=151 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `password`, `remember_token`, `role`, `image`, `register_as`, `company_name`, `created_at`, `updated_at`, `status`, `last_login`, `login_ip`, `from_device`, `activation_code`, `company_link`, `street_address1`, `street_address2`, `city`, `state`, `zip`, `country`, `phone`, `available_pitch`, `free_pitch`, `experience`, `graduation`, `post_graduation`, `computer_skill`, `portfolio_image`, `portfolio_description`, `project_category_specialization`) VALUES
(1, 'Admin', 'Admin', 'admin@gmail.com', '$2y$10$1eHEb1GpEdZ3yJ8L5njUt.qYjLNBOr4EVd8ZwydHblVTiwTR50uYG', 'NicFhYJ50p6LbN7VpnldtVe0FCBBEDBmbciR8FiNmGTu1CCESUrXxt0xG6ZM', 'sAdmin', '2016/06/15/7672e851d21fead4205811cc18cf39964cc80422.jpg', '', '', '2016-05-18 06:05:41', '2016-06-16 13:07:15', 1, '0000-00-00 00:00:00', '', 'mob', '', '', '205, Pavan Flat', 'Nr. K.K. Nagar Cross Road', 'Ahmedabad', 'Gujarat', '380006', 'UK', '9999998887', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(59, 'Vinay', 'Srivastav', 'vinay.letsnurture@gmail.com', '$2y$10$JKjXRk498t.fVNYyY/CP4u3.hSFXMbYU8k5xarMF8cLfoCRABYaA6', '', 'JOB_POSTER', '146460589468.jpg', 'company', 'LetsNurture', '2016-05-27 04:48:57', '2016-05-30 10:12:35', 1, '0000-00-00 00:00:00', '', 'mob', '', 'http://letsnurture.com', '205, Pavan Flat', 'Nr. K.K. Nagar Cross Road', 'Ahmedabad', 'Gujarat', '380006', 'UK', '9999998887', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(60, 'V', 'C', 'von.chua@vonitto.com', '$2y$10$k7mp65B0NEpdHxgGeJxJrOJThyxkR9HFurNjwSb1lCQvLcOv3fhNC', '0KYCV4vVoj9l4eTquznW9CAGHCzgwHMsSYqel0YnHfH1mZQKmAhTapgRFsuH', 'JOB_SEEKER', '146433202084.jpg', '', '', '2016-05-27 05:53:40', '2016-07-26 21:56:24', 1, '0000-00-00 00:00:00', '', 'mob', '', '', '205, Pavan Flat', 'Nr. K.K. Nagar Cross Road', 'Ahmedabad', 'Gujarat', '380006', 'UK', '9999998887', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(70, 'nilesh', 'panchal', 'nilesh@letsnurture.com', '$2y$10$TinTd2STYBe7VNWEXcyFF.pf3d1Xi02tP7rdR5VtgKPWJCHLiCgQG', '', 'JOB_SEEKER', '146520862630.png', '', '', '2016-06-30 23:00:00', '2016-08-04 10:40:17', 1, '0000-00-00 00:00:00', '', 'mob', '', '', '205, Pavan Flat', 'Nr. K.K. Nagar Cross Road', 'Ahmedabad', 'Gujarat', '380006', 'UK', '9999998887', 4, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(94, 'Yvonne', 'Chua', 'yvonne.chua@vonitto.com', '$2y$10$nnsYqNY3l3Z5bli.pRx./OOW/xG.KyJZGj3jTdNzvXnoorUzUZiq.', '', 'JOB_POSTER', '146758797851.jpg', 'company', 'ABCo', '2016-07-03 22:19:38', '2016-07-03 22:21:35', 1, '0000-00-00 00:00:00', '', 'mob', '', 'abco.co.uk', '205, Pavan Flat', 'Nr. K.K. Nagar Cross Road', 'Ahmedabad', 'Gujarat', '380006', 'UK', '9999998887', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(96, 'Dharmendra', 'Solanki', 'Dharmendra.letsnurture@gmail.com', '$2y$10$dFsohjtZQb8TOjHr8ccQ8uSxR8MSFgwmZWP0Dk13Ore08pRowuivq', 'kyQnyob5cOugJxgjTJoN4m2haie4oyvd3OK0XgKFK8qlscF8Bwsa8cDI5GEk', 'JOB_SEEKER', '146781331926.png', '', '', '2017-05-01 23:00:00', '2016-08-02 09:31:11', 1, '0000-00-00 00:00:00', '', 'mob', '', '', 'L-12,Anand Nagar', 'Near J.k Park', 'Ahmedabad', 'Gujarat', '382411', 'UK', '788574712', 20, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(97, 'Gaurav', 'QA', 'gauravqa.letsnurture@gmail.com', '$2y$10$jpi8.8zFMs8kuAgpxYP0d.jhusFvnOyQDKgwRBi2ZEZEYABJvmgoG', 'oFLEuu3IvOc5tCylIIOK3T3mcuAZlmb2eBQT9dzNqYPgPlConzHATE0fCMLV', 'JOB_POSTER', '146781339924.jpg', 'individual', '', '2016-07-06 12:56:39', '2016-08-02 05:49:41', 1, '0000-00-00 00:00:00', '', 'mob', '', '', '', '', '', '', '', '', '', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(113, 'era', 'dandi', 'zalasid.mca@gmail.com', '$2y$10$jsgoMXqQap9ZQaV1ZFWWD.amBQzNcCMIh8JyXDktXOOD/hkIYNZGO', 'Vj0APr4RvpHmMV9aDARBuRGIUodHIyKseNYX2sxCUx4Ora6p96YDsGmYRALT', 'JOB_POSTER', '146787598533.png', 'individual', '', '2016-07-07 06:19:45', '2016-07-08 05:37:59', 0, '0000-00-00 00:00:00', '', 'mob', 'QNhsBK1Q8WZhuqUFK8bw4Q7AdYDzzo', '', '', '', '', '', '', '', '', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(114, 'reema', 'tandel', 'tand@gmail.com', '$2y$10$aM2uqefG4h94GEQQW3xQcux4X9jmFzeZibi1O4u/muzNQN9pmggge', '', 'JOB_POSTER', '146787617782.jpg', 'individual', '', '2016-07-07 06:22:57', '2016-07-07 06:22:57', 0, '0000-00-00 00:00:00', '', 'mob', 'E8GC8BT607RJundyB4bmvitzwjZsP7', '', '', '', '', '', '', '', '', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(115, 'Riya', 'Patel', 'riya@gmail.com', '$2y$10$CE3Ug8AI.F0dSmIhL0JaIOrwR3IK9KTbqobMH6RK0xGN4RADASDza', '', 'JOB_POSTER', '146787634634.png', 'individual', '', '2016-07-07 06:25:46', '2016-07-07 06:25:46', 0, '0000-00-00 00:00:00', '', 'mob', 'LmnMQRd5qrt6jtuprqFWhThVOIJEpB', '', '', '', '', '', '', '', '', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(116, 'TesterLN', 'Tester', 't@gmail.v', '$2y$10$2/MqSzc0UTvylqHN.qscnOlOIHmiuLRPsYMBp3MtvygW9mQBBR4xK', '', 'JOB_SEEKER', '146787653391.png', '', '', '2016-07-07 06:28:53', '2016-07-07 06:28:53', 0, '0000-00-00 00:00:00', '', 'mob', '7YirjmBxD3YovWwFCXF2OEZnOzGzDW', '', '', '', '', '', '', '', '', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(117, 'Ankit', 'Sompura', 'ankit.letsnurture@gmail.com', '$2y$10$EMzaEgOPgW6Yt9Fl94WoGuPI9g2F.O6l6nEOhKjOHFQIt.6fUiq52', '', 'JOB_SEEKER', '146831425283.png', '', '', '2016-07-12 08:04:12', '2016-07-12 08:04:35', 1, '0000-00-00 00:00:00', '', 'mob', '', '', '', '', '', '', '', '', '', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(118, 'ravi', 'kadia', 'ravi.kadia@letsnurture.com', '$2y$10$0VBCpM5KcZVt5pvbewE2euT3V.AtsON6taY3iMQlBkeFMbxSkokX6', '', 'JOB_POSTER', '146855948561.jpg', 'individual', '', '2016-07-15 04:11:25', '2016-07-15 04:11:46', 1, '0000-00-00 00:00:00', '', 'mob', '', '', '', '', '', '', '', '', '', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(119, 'ravi', 'kadia', 'ravi.kadia@lesnurture.com', '$2y$10$nd2ANJRIj.kITetDMMV9reyAkGZ8C0voT0aJmBhms6sHYSQQgU3Dq', '', 'JOB_POSTER', '146855967048.png', 'individual', '', '2016-07-15 04:14:30', '2016-07-15 04:14:30', 0, '0000-00-00 00:00:00', '', 'mob', 'kAlUnUyjUcUSlPJhyC1MahEM5d0jN8', '', '', '', '', '', '', '', '', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(145, 'happy', 'always', 'polarbears8888@gmail.com', '$2y$10$3Yby1Ou2s20BCU0E9IaQdeuxQyjXzGXpRsztiZXeQyIFujE9XmrxC', '', 'JOB_POSTER', '146957445276.JPG', 'company', 'POLAR BEARS', '2016-07-26 22:07:32', '2016-07-26 22:08:30', 1, '0000-00-00 00:00:00', '', 'mob', '', '', '', '', '', '', '', '', '', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(146, 'Hitesh', 'Tank', 'hiteshtank.letsnurture@gmail.com', '$2y$10$rx9isXZostzfncZjZw7tA.QQ6svymXjilyEx2teyL5D4pNyR14azW', '', 'JOB_SEEKER', '146959454071.png', '', '', '2016-07-27 03:42:20', '2016-07-27 03:45:40', 1, '0000-00-00 00:00:00', '', 'mob', '', '', '', '', '', '', '', '', '', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(147, 'sdfsdf', 'dsfdsf', 'dsfds@gmail.com', '$2y$10$rNX.RlFGxXL6zNTazpARFOWxoKQZUv0KEwR9jAoTTe7jn207kUIJ.', '', 'JOB_SEEKER', '14696217399.png', '', '', '2016-07-27 11:15:39', '2016-07-27 11:15:39', 0, '0000-00-00 00:00:00', '', 'mob', 'udTzvMSE5dzD1smeNIeV4C3RH9FeNX', '', '', '', '', '', '', '', '', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(148, 'hiteh', 'sdfh', 'hiteshtank.lesynsad@gmail.com', '$2y$10$Sawkqm8ALnKUoga95zihy.myZg6E4dd6MWfVyYjaQiKyeMBgmkS4a', '', 'JOB_SEEKER', '146962194141.png', '', '', '2016-07-27 11:19:01', '2016-07-27 11:19:01', 0, '0000-00-00 00:00:00', '', 'mob', 'lsrpuT0mtm3ba4doTwZRV91OnlUHpK', '', '', '', '', '', '', '', '', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(149, 'Pradip', 'Darji', 'pradip.letsnurture@gmail.com', '$2y$10$ur9/KgcCBWcp0CfnlNvEJO4eBbrp/5K3e6/AExA3J9TQnZNXcYsOO', '', 'JOB_SEEKER', '147013204244.png', '', '', '2016-08-02 09:00:42', '2016-08-05 12:43:39', 1, '0000-00-00 00:00:00', '', 'mob', 'lsrpuT0mtm3ba4doTwZRV91OnlUHpK', '', 'M-13 Harivilla Flat', 'Near J.k Park', 'Ahmedabad', 'Gujarat', '382481', 'Malaysia', '9727357149', 15, 0, '2', 'BEhttp://192.168.1.3:8080/openproject/work_package', 'MEhttp://192.168.1.3:8080/openproject/work_package', 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.', '147030342452.jpg', 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.', 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.'),
(150, 'QA', 'Pradip', 'pradip.darji@letsnurture.com', '$2y$10$puCIzu/dSX.1tO5DXi.78OKUbsh21sBNJNApeXUrP6dHj3IBSLnRe', '', 'JOB_POSTER', '147013208570.png', 'individual', '', '2016-08-02 09:01:25', '2016-08-02 09:01:25', 1, '0000-00-00 00:00:00', '', 'mob', 'lsrpuT0mtm3ba4doTwZRV91OnlUHpK', '', '', '', '', '', '', '', '', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `adj_params_master`
--
ALTER TABLE `adj_params_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `job_awarded`
--
ALTER TABLE `job_awarded`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `job_bookmark`
--
ALTER TABLE `job_bookmark`
  ADD PRIMARY KEY (`id`),
  ADD KEY `poster_id` (`poster_id`),
  ADD KEY `seeker_id` (`seeker_id`),
  ADD KEY `job_id` (`job_id`);

--
-- Indexes for table `job_category`
--
ALTER TABLE `job_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `job_default_logos`
--
ALTER TABLE `job_default_logos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `job_category_id` (`job_category_id`);

--
-- Indexes for table `job_pitched`
--
ALTER TABLE `job_pitched`
  ADD PRIMARY KEY (`id`),
  ADD KEY `job_seeker_id` (`job_seeker_id`),
  ADD KEY `job_id` (`job_id`);

--
-- Indexes for table `job_post`
--
ALTER TABLE `job_post`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category_id` (`category_id`),
  ADD KEY `job_poster_id` (`job_poster_id`);

--
-- Indexes for table `job_poster`
--
ALTER TABLE `job_poster`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `job_poster_attachments`
--
ALTER TABLE `job_poster_attachments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `job_poster_id` (`job_poster_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `job_poster_rating_comments`
--
ALTER TABLE `job_poster_rating_comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `job_poster_id` (`job_poster_id`);

--
-- Indexes for table `job_poster_strength`
--
ALTER TABLE `job_poster_strength`
  ADD PRIMARY KEY (`id`),
  ADD KEY `job_poster_id` (`job_poster_id`),
  ADD KEY `adj_params_master_id` (`adj_params_master_id`);

--
-- Indexes for table `job_post_attachments`
--
ALTER TABLE `job_post_attachments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `job_post_id` (`job_post_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `job_post_skills`
--
ALTER TABLE `job_post_skills`
  ADD PRIMARY KEY (`id`),
  ADD KEY `job_post_id` (`job_post_id`),
  ADD KEY `adj_params_master_id` (`adj_params_master_id`);

--
-- Indexes for table `job_review_rating`
--
ALTER TABLE `job_review_rating`
  ADD PRIMARY KEY (`id`),
  ADD KEY `seeker_id` (`seeker_id`),
  ADD KEY `poster_id` (`poster_id`),
  ADD KEY `job_post_id` (`job_post_id`);

--
-- Indexes for table `job_seeker`
--
ALTER TABLE `job_seeker`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `user_id_2` (`user_id`),
  ADD KEY `subscription_id` (`subscription_id`);

--
-- Indexes for table `job_seekers_bid`
--
ALTER TABLE `job_seekers_bid`
  ADD PRIMARY KEY (`id`),
  ADD KEY `job_post_id` (`job_post_id`),
  ADD KEY `job_seeker_id` (`job_seeker_id`),
  ADD KEY `subscription_id` (`subscription_id`),
  ADD KEY `payment_id` (`payment_id`);

--
-- Indexes for table `job_seekers_bid_attachments`
--
ALTER TABLE `job_seekers_bid_attachments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `job_seekers_bid_id` (`job_seekers_bid_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `job_seeker_attachments`
--
ALTER TABLE `job_seeker_attachments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `job_seeker_id` (`job_seeker_id`),
  ADD KEY `created_by` (`created_by`);

--
-- Indexes for table `job_seeker_education`
--
ALTER TABLE `job_seeker_education`
  ADD PRIMARY KEY (`id`),
  ADD KEY `job_seeker_id` (`job_seeker_id`),
  ADD KEY `adj_params_master_id` (`adj_params_master_id`);

--
-- Indexes for table `job_seeker_rating_comments`
--
ALTER TABLE `job_seeker_rating_comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `job_seeker_id` (`job_seeker_id`),
  ADD KEY `created_by` (`created_by`);

--
-- Indexes for table `job_seeker_sector`
--
ALTER TABLE `job_seeker_sector`
  ADD PRIMARY KEY (`id`),
  ADD KEY `job_seeker_id` (`job_seeker_id`),
  ADD KEY `adj_params_master_id` (`adj_params_master_id`);

--
-- Indexes for table `job_seeker_specialisation`
--
ALTER TABLE `job_seeker_specialisation`
  ADD PRIMARY KEY (`id`),
  ADD KEY `job_seeker_id` (`job_seeker_id`),
  ADD KEY `adj_params_master_id` (`adj_params_master_id`);

--
-- Indexes for table `old_job_post`
--
ALTER TABLE `old_job_post`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category_id` (`category_id`),
  ADD KEY `job_poster_id` (`job_poster_id`);

--
-- Indexes for table `page_cms`
--
ALTER TABLE `page_cms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_reminders`
--
ALTER TABLE `password_reminders`
  ADD KEY `password_reminders_email_index` (`email`),
  ADD KEY `password_reminders_token_index` (`token`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `subscription_id` (`subscription_id`),
  ADD KEY `job_seeker_id` (`job_seeker_id`);

--
-- Indexes for table `subscription`
--
ALTER TABLE `subscription`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sys_defaults_params`
--
ALTER TABLE `sys_defaults_params`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `adj_params_master`
--
ALTER TABLE `adj_params_master`
  MODIFY `id` bigint(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `job_awarded`
--
ALTER TABLE `job_awarded`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `job_bookmark`
--
ALTER TABLE `job_bookmark`
  MODIFY `id` bigint(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `job_category`
--
ALTER TABLE `job_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `job_default_logos`
--
ALTER TABLE `job_default_logos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `job_pitched`
--
ALTER TABLE `job_pitched`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=143;
--
-- AUTO_INCREMENT for table `job_post`
--
ALTER TABLE `job_post`
  MODIFY `id` bigint(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=85;
--
-- AUTO_INCREMENT for table `job_poster`
--
ALTER TABLE `job_poster`
  MODIFY `id` bigint(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `job_poster_attachments`
--
ALTER TABLE `job_poster_attachments`
  MODIFY `id` bigint(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `job_poster_rating_comments`
--
ALTER TABLE `job_poster_rating_comments`
  MODIFY `id` bigint(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `job_poster_strength`
--
ALTER TABLE `job_poster_strength`
  MODIFY `id` bigint(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `job_post_attachments`
--
ALTER TABLE `job_post_attachments`
  MODIFY `id` bigint(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `job_post_skills`
--
ALTER TABLE `job_post_skills`
  MODIFY `id` bigint(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `job_review_rating`
--
ALTER TABLE `job_review_rating`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT for table `job_seeker`
--
ALTER TABLE `job_seeker`
  MODIFY `id` bigint(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `job_seekers_bid`
--
ALTER TABLE `job_seekers_bid`
  MODIFY `id` bigint(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `job_seekers_bid_attachments`
--
ALTER TABLE `job_seekers_bid_attachments`
  MODIFY `id` bigint(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `job_seeker_attachments`
--
ALTER TABLE `job_seeker_attachments`
  MODIFY `id` bigint(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `job_seeker_education`
--
ALTER TABLE `job_seeker_education`
  MODIFY `id` bigint(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `job_seeker_rating_comments`
--
ALTER TABLE `job_seeker_rating_comments`
  MODIFY `id` bigint(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `job_seeker_sector`
--
ALTER TABLE `job_seeker_sector`
  MODIFY `id` bigint(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `job_seeker_specialisation`
--
ALTER TABLE `job_seeker_specialisation`
  MODIFY `id` bigint(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `old_job_post`
--
ALTER TABLE `old_job_post`
  MODIFY `id` bigint(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `page_cms`
--
ALTER TABLE `page_cms`
  MODIFY `id` bigint(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `payments`
--
ALTER TABLE `payments`
  MODIFY `id` bigint(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT for table `subscription`
--
ALTER TABLE `subscription`
  MODIFY `id` bigint(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `sys_defaults_params`
--
ALTER TABLE `sys_defaults_params`
  MODIFY `id` bigint(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=151;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `job_bookmark`
--
ALTER TABLE `job_bookmark`
  ADD CONSTRAINT `job_bookmark_ibfk_1` FOREIGN KEY (`poster_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `job_bookmark_ibfk_2` FOREIGN KEY (`seeker_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `job_bookmark_ibfk_3` FOREIGN KEY (`job_id`) REFERENCES `job_post` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `job_default_logos`
--
ALTER TABLE `job_default_logos`
  ADD CONSTRAINT `job_default_logos_ibfk_1` FOREIGN KEY (`job_category_id`) REFERENCES `job_category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `job_poster`
--
ALTER TABLE `job_poster`
  ADD CONSTRAINT `job_poster_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `job_poster_attachments`
--
ALTER TABLE `job_poster_attachments`
  ADD CONSTRAINT `job_poster_attachments_ibfk_1` FOREIGN KEY (`job_poster_id`) REFERENCES `job_poster` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `job_poster_attachments_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `job_poster_rating_comments`
--
ALTER TABLE `job_poster_rating_comments`
  ADD CONSTRAINT `job_poster_rating_comments_ibfk_1` FOREIGN KEY (`job_poster_id`) REFERENCES `job_poster` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `job_poster_rating_comments_ibfk_2` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `job_poster_strength`
--
ALTER TABLE `job_poster_strength`
  ADD CONSTRAINT `job_poster_strength_ibfk_1` FOREIGN KEY (`job_poster_id`) REFERENCES `job_poster` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `job_poster_strength_ibfk_2` FOREIGN KEY (`adj_params_master_id`) REFERENCES `adj_params_master` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `job_post_attachments`
--
ALTER TABLE `job_post_attachments`
  ADD CONSTRAINT `job_post_attachments_ibfk_1` FOREIGN KEY (`job_post_id`) REFERENCES `old_job_post` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `job_post_attachments_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `job_post_skills`
--
ALTER TABLE `job_post_skills`
  ADD CONSTRAINT `job_post_skills_ibfk_1` FOREIGN KEY (`job_post_id`) REFERENCES `old_job_post` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `job_post_skills_ibfk_2` FOREIGN KEY (`adj_params_master_id`) REFERENCES `adj_params_master` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `job_review_rating`
--
ALTER TABLE `job_review_rating`
  ADD CONSTRAINT `job_review_rating_ibfk_1` FOREIGN KEY (`seeker_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `job_seeker`
--
ALTER TABLE `job_seeker`
  ADD CONSTRAINT `job_seeker_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `job_seeker_ibfk_2` FOREIGN KEY (`subscription_id`) REFERENCES `subscription` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `job_seekers_bid`
--
ALTER TABLE `job_seekers_bid`
  ADD CONSTRAINT `job_seekers_bid_ibfk_1` FOREIGN KEY (`job_post_id`) REFERENCES `old_job_post` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `job_seekers_bid_ibfk_2` FOREIGN KEY (`job_seeker_id`) REFERENCES `job_seeker` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `job_seekers_bid_ibfk_3` FOREIGN KEY (`subscription_id`) REFERENCES `subscription` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `job_seekers_bid_ibfk_4` FOREIGN KEY (`payment_id`) REFERENCES `payments` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `job_seekers_bid_attachments`
--
ALTER TABLE `job_seekers_bid_attachments`
  ADD CONSTRAINT `job_seekers_bid_attachments_ibfk_1` FOREIGN KEY (`job_seekers_bid_id`) REFERENCES `job_seekers_bid` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `job_seekers_bid_attachments_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `job_seeker_attachments`
--
ALTER TABLE `job_seeker_attachments`
  ADD CONSTRAINT `job_seeker_attachments_ibfk_1` FOREIGN KEY (`job_seeker_id`) REFERENCES `job_seeker` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `job_seeker_attachments_ibfk_2` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `job_seeker_education`
--
ALTER TABLE `job_seeker_education`
  ADD CONSTRAINT `job_seeker_education_ibfk_1` FOREIGN KEY (`job_seeker_id`) REFERENCES `job_seeker` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `job_seeker_education_ibfk_2` FOREIGN KEY (`adj_params_master_id`) REFERENCES `adj_params_master` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `job_seeker_rating_comments`
--
ALTER TABLE `job_seeker_rating_comments`
  ADD CONSTRAINT `job_seeker_rating_comments_ibfk_1` FOREIGN KEY (`job_seeker_id`) REFERENCES `job_seeker` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `job_seeker_rating_comments_ibfk_2` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `job_seeker_sector`
--
ALTER TABLE `job_seeker_sector`
  ADD CONSTRAINT `job_seeker_sector_ibfk_1` FOREIGN KEY (`job_seeker_id`) REFERENCES `job_seeker` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `job_seeker_sector_ibfk_2` FOREIGN KEY (`adj_params_master_id`) REFERENCES `adj_params_master` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `job_seeker_specialisation`
--
ALTER TABLE `job_seeker_specialisation`
  ADD CONSTRAINT `job_seeker_specialisation_ibfk_1` FOREIGN KEY (`job_seeker_id`) REFERENCES `job_seeker` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `job_seeker_specialisation_ibfk_2` FOREIGN KEY (`adj_params_master_id`) REFERENCES `adj_params_master` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
