-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 16, 2016 at 03:12 PM
-- Server version: 5.5.49-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.17

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `vonitto`
--

-- --------------------------------------------------------

--
-- Table structure for table `adj_params_master`
--

CREATE TABLE IF NOT EXISTS `adj_params_master` (
  `id` bigint(10) NOT NULL AUTO_INCREMENT,
  `field_name` varchar(254) NOT NULL,
  `field_code` varchar(254) NOT NULL,
  `field_value` varchar(254) NOT NULL,
  `related_with` enum('JOB_POSTER','JOB_SEEKER') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `job_awarded`
--

CREATE TABLE IF NOT EXISTS `job_awarded` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `job_seeker_id` int(11) NOT NULL,
  `job_poster_id` int(11) NOT NULL,
  `job_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=24 ;

--
-- Dumping data for table `job_awarded`
--

INSERT INTO `job_awarded` (`id`, `job_seeker_id`, `job_poster_id`, `job_id`, `created_at`) VALUES
(1, 70, 59, 55, '2016-07-18 10:46:07'),
(2, 95, 98, 47, '2016-07-28 06:27:42'),
(23, 146, 98, 45, '2016-08-11 11:42:15');

-- --------------------------------------------------------

--
-- Table structure for table `job_bookmark`
--

CREATE TABLE IF NOT EXISTS `job_bookmark` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `poster_id` bigint(11) NOT NULL,
  `seeker_id` bigint(11) NOT NULL,
  `job_id` bigint(10) NOT NULL,
  `bookmark_status` enum('bookmark','unbookmark') NOT NULL DEFAULT 'bookmark',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `poster_id` (`poster_id`),
  KEY `seeker_id` (`seeker_id`),
  KEY `job_id` (`job_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `job_bookmark`
--

INSERT INTO `job_bookmark` (`id`, `poster_id`, `seeker_id`, `job_id`, `bookmark_status`, `created_at`) VALUES
(1, 145, 95, 61, 'bookmark', '2016-08-05 06:55:03'),
(2, 145, 95, 56, 'unbookmark', '2016-08-05 06:55:03'),
(3, 59, 95, 58, 'bookmark', '2016-08-05 07:42:59'),
(4, 59, 95, 60, 'bookmark', '2016-08-05 07:45:16'),
(5, 98, 95, 59, 'bookmark', '2016-08-05 08:28:23'),
(6, 59, 95, 39, 'bookmark', '2016-08-05 09:06:48');

-- --------------------------------------------------------

--
-- Table structure for table `job_bookmark_email`
--

CREATE TABLE IF NOT EXISTS `job_bookmark_email` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `job_id` bigint(11) NOT NULL,
  `seeker_id` bigint(11) NOT NULL,
  `poster_id` int(11) NOT NULL,
  `email` varchar(50) NOT NULL,
  `url` text NOT NULL,
  `content` text NOT NULL,
  `mail_status` enum('unsend','sent') DEFAULT 'unsend',
  PRIMARY KEY (`id`),
  KEY `seeker_id` (`seeker_id`),
  KEY `job_id` (`job_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `job_category`
--

CREATE TABLE IF NOT EXISTS `job_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `job_category`
--

INSERT INTO `job_category` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Culture work', '0000-00-00 00:00:00', '2016-06-14 14:32:07'),
(2, 'Education', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'Health', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'Hospitality', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 'Industrial', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 'Office & Workplace', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 'Residential', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 'Retail', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 'Sports', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, 'Transportation', '0000-00-00 00:00:00', '2016-06-14 08:45:03');

-- --------------------------------------------------------

--
-- Table structure for table `job_default_logos`
--

CREATE TABLE IF NOT EXISTS `job_default_logos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `job_category_id` int(11) NOT NULL,
  `logo` varchar(500) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `job_category_id` (`job_category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `job_default_logos`
--

INSERT INTO `job_default_logos` (`id`, `job_category_id`, `logo`, `status`) VALUES
(1, 1, 'dummy-logo.jpg', '1'),
(2, 2, 'dummy-logo.jpg', '1'),
(3, 3, 'dummy-logo.jpg', '1'),
(4, 4, 'dummy-logo.jpg', '1'),
(5, 5, 'dummy-logo.jpg', '1'),
(6, 6, 'dummy-logo.jpg', '1'),
(7, 7, 'dummy-logo.jpg', '1'),
(8, 8, 'dummy-logo.jpg', '1'),
(9, 9, 'dummy-logo.jpg', '1'),
(10, 10, 'dummy-logo.jpg', '1');

-- --------------------------------------------------------

--
-- Table structure for table `job_pitched`
--

CREATE TABLE IF NOT EXISTS `job_pitched` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `job_seeker_id` int(11) NOT NULL,
  `job_id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `free_request` enum('0','1') NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `job_seeker_id` (`job_seeker_id`),
  KEY `job_id` (`job_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=123 ;

--
-- Dumping data for table `job_pitched`
--

INSERT INTO `job_pitched` (`id`, `job_seeker_id`, `job_id`, `date`, `free_request`) VALUES
(104, 60, 49, '2016-07-08 06:40:41', '1'),
(105, 95, 50, '2016-07-15 08:33:56', '1'),
(106, 95, 47, '2016-07-15 10:02:56', '0'),
(107, 95, 47, '2016-07-15 10:03:28', '0'),
(108, 95, 47, '2016-07-15 10:03:46', '0'),
(109, 95, 47, '2016-07-15 10:15:33', '0'),
(110, 95, 45, '2016-07-15 12:40:41', '0'),
(114, 70, 56, '2016-07-18 12:59:37', '1'),
(115, 70, 46, '2016-07-18 13:27:49', '0'),
(117, 70, 57, '2016-07-18 14:01:38', '0'),
(118, 95, 58, '2016-07-19 10:02:22', '1'),
(119, 70, 58, '2016-07-19 13:40:17', '1'),
(120, 60, 60, '2016-07-26 22:56:24', '1'),
(121, 146, 61, '2016-07-27 04:45:40', '1'),
(122, 146, 45, '2016-08-11 11:15:49', '1');

-- --------------------------------------------------------

--
-- Table structure for table `job_post`
--

CREATE TABLE IF NOT EXISTS `job_post` (
  `id` bigint(10) NOT NULL AUTO_INCREMENT,
  `is_featured` enum('1','0') NOT NULL DEFAULT '0',
  `job_project_id` varchar(254) NOT NULL,
  `title` varchar(254) NOT NULL,
  `job_logo` varchar(300) NOT NULL,
  `description` text NOT NULL,
  `what_you_want` text NOT NULL,
  `what_kind_job` varchar(255) NOT NULL,
  `project_deadline` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `category_id` int(11) NOT NULL,
  `location_city` varchar(254) NOT NULL,
  `location_country` varchar(254) NOT NULL,
  `postcode` varchar(20) DEFAULT NULL,
  `budget` varchar(254) NOT NULL,
  `currency` enum('GBP','MYR') NOT NULL DEFAULT 'GBP',
  `file` varchar(255) NOT NULL,
  `skills` text NOT NULL,
  `how_long` varchar(255) NOT NULL,
  `is_one_time` tinyint(4) NOT NULL,
  `job_poster_id` bigint(10) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL,
  `is_active` int(10) NOT NULL,
  `progress_status` enum('Assigned','In-Progress','Completed') DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `category_id` (`category_id`),
  KEY `job_poster_id` (`job_poster_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=67 ;

--
-- Dumping data for table `job_post`
--

INSERT INTO `job_post` (`id`, `is_featured`, `job_project_id`, `title`, `job_logo`, `description`, `what_you_want`, `what_kind_job`, `project_deadline`, `category_id`, `location_city`, `location_country`, `postcode`, `budget`, `currency`, `file`, `skills`, `how_long`, `is_one_time`, `job_poster_id`, `created_at`, `updated_at`, `is_active`, `progress_status`) VALUES
(39, '0', 'PYC_20160706_39', 'Online Shopping Web', 'dummy-logo.jpg', 'We want to create a online shopping site just like a flipkart.', 'testing', 'tesssssty', '2016-07-18 09:56:38', 6, 'Atlanta', 'UK', NULL, '$50000', 'GBP', 'a:1:{i:0;s:13:"146778628168.";}', 'angular, php, mysql', 'More than 6 months', 0, 59, '2016-07-06 05:24:41', '2016-07-06 06:24:41', 1, NULL),
(43, '0', 'PYC_20160706_43', 'IOS Developer', 'dummy-logo.jpg', 'We need ios developer for a one application project', 'if you are interested then please contact us.', '', '2016-07-18 09:56:38', 6, 'Atlanta', 'UK', NULL, '$1000', 'GBP', 'a:1:{i:0;s:13:"146779553560.";}', 'java, database', 'More than 6 months', 0, 59, '2016-07-06 07:58:55', '2016-07-06 12:16:28', 1, 'Assigned'),
(44, '0', 'PYC_20160706_44', 'Quality Analyst', 'dummy-logo.jpg', 'Need 1-2 year experience person', 'Good background in testing field', '', '2016-07-18 09:56:38', 5, 'Rajkot', 'Malaysia', NULL, '$400', 'GBP', 'a:1:{i:0;s:13:"146781169745.";}', 'GOOD BACKGROUND IN TESTING FIELD\r\nGOOD BACKGROUND IN TESTING FIELD\r\nGOOD BACKGROUND IN TESTING FIELD', 'Less than 1 week', 0, 59, '2016-07-06 12:28:17', '2016-07-06 13:28:17', 1, NULL),
(45, '0', 'PYC_20160707_45', 'Architecture', 'dummy-logo.jpg', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '', '2016-08-31 09:56:38', 4, 'Rajkot', 'Malaysia', NULL, '$10000', 'GBP', 'a:1:{i:0;s:15:"14678683139.png";}', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'Less than 1 week', 0, 98, '2016-07-07 04:11:53', '2016-08-11 11:42:15', 1, 'Assigned'),
(46, '0', 'PYC_20160707_46', 'Test Engineer', 'dummy-logo.jpg', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '', '2016-07-30 09:56:38', 7, 'Ahamedabad', 'Malaysia', NULL, '$100', 'GBP', '', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'Less than 1 month', 0, 98, '2016-07-07 04:39:00', '2016-07-07 05:39:00', 1, NULL),
(47, '0', 'PYC_20160707_47', 'UI developer', 'dummy-logo.jpg', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '', '2016-07-18 09:56:38', 8, 'tester', 'Malaysia', NULL, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has s', 'GBP', '', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '3 to 6 months', 0, 98, '2016-07-07 04:54:38', '2016-08-05 13:09:27', 1, 'Completed'),
(48, '0', 'PYC_20160707_48', 'development', 'dummy-logo.jpg', 'require for developer', 'REQUIRE', '', '2016-07-18 09:56:38', 5, 'MEXO', 'Malaysia', NULL, '25252', 'GBP', '', 'DEVELOPMENT SKILL', '3 to 6 months', 0, 103, '2016-07-07 05:26:34', '2016-07-07 06:26:34', 1, NULL),
(49, '0', 'PYC_20160707_49', 'PHP DEVELOPER', 'dummy-logo.jpg', 'COMPLETE PHP KNOWLEDGE', 'Require', '', '2016-07-18 09:56:38', 6, 'year', 'UK', NULL, 'sadsd', 'GBP', '', 'fsdffdgfg', '3 to 6 months', 0, 103, '2016-07-07 05:29:51', '2016-07-07 06:29:51', 1, NULL),
(50, '0', 'PYC_20160707_50', 'PHP DEVELOPER', 'dummy-logo.jpg', 'COMPLETE PHP KNOWLEDGE', 'Require', '', '2016-07-18 09:56:38', 6, 'year', 'UK', NULL, 'sadsd', 'GBP', '', 'fdfdfgfgfg', '3 to 6 months', 0, 103, '2016-07-07 05:30:16', '2016-07-07 06:30:16', 1, NULL),
(51, '1', 'PYC_20160707_51', 'QA', 'dummy-logo.jpg', 'sddfdf', '', '', '2016-07-18 09:56:38', 2, '', '', NULL, '', 'GBP', '', '', '1 to 3 months', 0, 103, '2016-07-07 05:53:36', '2016-08-10 12:44:47', 1, NULL),
(52, '0', 'PYC_20160707_52', 'data', 'dummy-logo.jpg', 'entry', '', '', '2016-07-18 09:56:38', 8, '', '', NULL, '', 'GBP', '', '', 'More than 6 months', 0, 103, '2016-07-07 06:00:07', '2016-07-07 07:00:07', 1, NULL),
(53, '0', 'PYC_20160710_53', 'Test', 'dummy-logo.jpg', 'Testing 123', 'Test', '', '2016-07-18 09:56:38', 4, 'London', 'UK', NULL, '10000', 'GBP', 'a:1:{i:0;s:16:"146816585127.jpg";}', 'autocad', 'Less than 1 month', 0, 94, '2016-07-10 14:50:51', '2016-08-10 12:44:17', 1, NULL),
(55, '1', 'PYC_20160718_55', 'New Job', '14688381373.png', 'this is a testing job post here', 'hello', '', '2016-07-31 10:46:07', 1, 'Atlanta', 'UK', NULL, '$100', 'GBP', '', '', 'More than 6 months', 0, 59, '2016-07-18 09:35:37', '2016-08-10 12:44:48', 1, 'Assigned'),
(56, '0', 'PYC_20160718_56', 'fresh', 'dummy-logo.jpg', 'fresgher job', 'test', '', '2016-07-30 23:00:00', 2, 'Atlanta', 'UK', NULL, '', 'GBP', '', 'tsdfsadf', 'More than 6 months', 0, 59, '2016-07-18 10:07:49', '2016-07-18 11:07:49', 1, NULL),
(57, '0', 'PYC_20160718_57', 'hahah', 'dummy-logo.jpg', 'ghaha', 'haha', '', '2016-07-29 23:00:00', 7, 'Atlanta', 'UK', NULL, '$100', 'GBP', '', 'ahahah', 'More than 6 months', 0, 59, '2016-07-18 10:10:38', '2016-08-10 12:44:15', 1, NULL),
(58, '0', 'PYC_20160718_58', 'asdf', 'dummy-logo.jpg', 'dfasdf', '', '', '2016-07-31 03:07:00', 2, 'Atlanta', 'UK', NULL, '$100', 'GBP', '', 'asdfasdf', 'More than 6 months', 0, 59, '2016-07-18 10:13:24', '2016-08-10 12:44:13', 1, NULL),
(59, '0', 'PYC_20160720_59', 'Marketing Manager', '147065304513.png', 'researching and analysing market trends identifying target markets and how best to reach them coming up with marketing strategies planning campaigns and managing budgets organising the production of posters and brochures designing social media strategies attending trade shows, conferences and sales meetings making sure that campaigns run to deadline and are on budget monitoring and reporting on the effectiveness of strategies and campaigns managing a team of marketing executives and assistants.', 'researching and analysing market trends identifying target markets and how best to reach them coming up with marketing strategies planning campaigns and managing budgets organising the production of posters and brochures designing social media strategies attending trade shows, conferences and sales meetings making sure that campaigns run to deadline and are on budget monitoring and reporting on the effectiveness of strategies and campaigns managing a team of marketing executives and assistants.', '', '2016-07-29 21:37:00', 7, 'test', 'Malaysia', NULL, '$200', 'GBP', 'Array', '', '', 0, 98, '2016-07-20 09:22:19', '2016-08-10 12:14:48', 1, NULL),
(60, '1', 'PYC_20160720_60', 'test', '146901110922.jpg', 'asdfasdf', 'asdfasdf', '', '2016-07-31 03:07:00', 5, 'Atlanta', 'UK', NULL, '$100', 'GBP', '', 'asdfasdfa sdf asd fasdf asd fsadf', 'More than 6 months', 0, 59, '2016-07-20 09:38:29', '2016-08-10 12:44:22', 1, NULL),
(61, '0', 'PYC_20160726_61', 'Testing July ', 'dummy-logo.jpg', 'test', 'sdfsdfsdfsdf\r\ndfsdfsdf\r\nsdfsdfsdf\r\nsdfsdfsdf\r\nsfsdfsdf\r\n', '', '2016-08-17 11:08:00', 9, 'London', 'UK', NULL, 'GBP300', 'GBP', '', '', '1 to 3 months', 0, 145, '2016-07-26 22:12:43', '2016-08-10 12:13:02', 1, NULL),
(62, '0', 'PYC_20160816_62', 'HR Managers', '147133694135.png', 'asdasd', 'asdasdasd', '', '2016-08-28 20:38:00', 3, 'asdasdad', 'UK', 'KASD 123', '12', 'MYR', '9d006876ecc5d184cc9e3a9c0420a1ba.xlsx,ec64f19587cac11ce1ad01ce417a50ff.png,37ace1411886afeccc0d14a846bc3663.xlsx', 'asdsada das ads ad asd', 'More than 6 months', 0, 98, '2016-08-16 03:12:21', '2016-08-16 08:42:21', 0, NULL),
(63, '0', 'PYC_20160816_63', 'HR Managers', '147133696066.png', 'asdasd', 'asdasdasd', '', '2016-08-28 20:38:00', 3, 'asdasdad', 'UK', 'KASD 123', '12', 'MYR', '9d006876ecc5d184cc9e3a9c0420a1ba.xlsx,ec64f19587cac11ce1ad01ce417a50ff.png,37ace1411886afeccc0d14a846bc3663.xlsx', 'asdsada das ads ad asd', 'More than 6 months', 0, 98, '2016-08-16 03:12:40', '2016-08-16 08:42:40', 0, NULL),
(64, '0', 'PYC_20160816_64', 'HR Managers', '147133718383.png', 'asdasd', 'asdasdasd', '', '2016-08-28 20:38:00', 3, 'asdasdad', 'UK', 'KASD 123', '12', 'MYR', '9d006876ecc5d184cc9e3a9c0420a1ba.xlsx,ec64f19587cac11ce1ad01ce417a50ff.png,37ace1411886afeccc0d14a846bc3663.xlsx', 'asdsada das ads ad asd', 'More than 6 months', 0, 98, '2016-08-16 03:16:23', '2016-08-16 08:46:23', 0, NULL),
(65, '0', 'PYC_20160816_65', 'dsfdsf', 'dummy-logo.jpg', 'sdfdsfdsf', 'sdfsdfsdf', '', '2016-08-28 20:38:00', 1, 'dsfsdf', 'UK', '1232332', '233', 'GBP', 'e7204f9848dbad586c4051f57c95d8dd.xlsx', '', '3 to 6 months', 0, 98, '2016-08-16 03:18:03', '2016-08-16 08:48:03', 0, NULL),
(66, '0', 'PYC_20160816_66', 'Test Job', '147133761482.png', 'dsfdsfdsf', 'dsfdsfdsf', '', '2016-08-29 20:38:00', 2, 'asdsad', 'UK', '123213', '566', 'GBP', '4d8a3c50ddb094bb27998ada4353c1f0.xlsx', 'sada asd asd asdadsads ', '3 to 6 months', 0, 98, '2016-08-16 03:23:34', '2016-08-16 08:53:34', 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `job_poster`
--

CREATE TABLE IF NOT EXISTS `job_poster` (
  `id` bigint(10) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(10) NOT NULL,
  `company_name` varchar(254) NOT NULL,
  `address` varchar(254) NOT NULL,
  `location_city` varchar(254) NOT NULL,
  `location_country` varchar(254) NOT NULL,
  `location_gps` varchar(254) NOT NULL,
  `url` varchar(500) NOT NULL,
  `contact1` varchar(254) NOT NULL,
  `contact2` varchar(254) NOT NULL,
  `email` varchar(254) NOT NULL,
  `ratings_points` double NOT NULL,
  `has_strength` int(10) NOT NULL,
  `is_active` int(10) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `job_poster_attachments`
--

CREATE TABLE IF NOT EXISTS `job_poster_attachments` (
  `id` bigint(10) NOT NULL AUTO_INCREMENT,
  `job_poster_id` bigint(10) NOT NULL,
  `path` varchar(254) NOT NULL,
  `comments` varchar(254) NOT NULL,
  `user_id` bigint(10) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_active` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `job_poster_id` (`job_poster_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `job_poster_rating_comments`
--

CREATE TABLE IF NOT EXISTS `job_poster_rating_comments` (
  `id` bigint(10) NOT NULL AUTO_INCREMENT,
  `job_poster_id` bigint(10) NOT NULL,
  `points` int(10) NOT NULL,
  `out_of` int(10) NOT NULL,
  `comments` varchar(254) NOT NULL,
  `created_by` bigint(10) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_active` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `created_by` (`created_by`),
  KEY `job_poster_id` (`job_poster_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `job_poster_strength`
--

CREATE TABLE IF NOT EXISTS `job_poster_strength` (
  `id` bigint(10) NOT NULL AUTO_INCREMENT,
  `job_poster_id` bigint(10) NOT NULL,
  `field_code` varchar(254) NOT NULL,
  `adj_params_master_id` bigint(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `job_poster_id` (`job_poster_id`),
  KEY `adj_params_master_id` (`adj_params_master_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `job_post_attachments`
--

CREATE TABLE IF NOT EXISTS `job_post_attachments` (
  `id` bigint(10) NOT NULL AUTO_INCREMENT,
  `job_post_id` bigint(10) NOT NULL,
  `path` varchar(254) NOT NULL,
  `comments` varchar(254) NOT NULL,
  `user_id` bigint(10) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_active` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `job_post_id` (`job_post_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `job_post_skills`
--

CREATE TABLE IF NOT EXISTS `job_post_skills` (
  `id` bigint(10) NOT NULL AUTO_INCREMENT,
  `job_post_id` bigint(10) NOT NULL,
  `field_code` varchar(254) NOT NULL,
  `adj_params_master_id` bigint(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `job_post_id` (`job_post_id`),
  KEY `adj_params_master_id` (`adj_params_master_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `job_review_rating`
--

CREATE TABLE IF NOT EXISTS `job_review_rating` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `job_post_id` bigint(10) NOT NULL,
  `seeker_id` bigint(11) NOT NULL,
  `poster_id` bigint(10) DEFAULT NULL,
  `rating` float NOT NULL,
  `review` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` enum('1','0') DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `seeker_id` (`seeker_id`),
  KEY `poster_id` (`poster_id`),
  KEY `job_post_id` (`job_post_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `job_review_rating`
--

INSERT INTO `job_review_rating` (`id`, `job_post_id`, `seeker_id`, `poster_id`, `rating`, `review`, `created_at`, `status`) VALUES
(11, 47, 95, 98, 4.5, 'sahdghjasg dhjadghjasdghjas dghjadghja dghja dghjadg hjadg hjasdghjasdghjasd', '2016-08-01 09:44:52', '1');

-- --------------------------------------------------------

--
-- Table structure for table `job_seeker`
--

CREATE TABLE IF NOT EXISTS `job_seeker` (
  `id` bigint(10) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(10) NOT NULL,
  `company` varchar(254) NOT NULL,
  `location_city` varchar(254) NOT NULL,
  `location_country` varchar(254) NOT NULL,
  `year_of_experiance` varchar(100) NOT NULL,
  `location_gps` varchar(254) NOT NULL,
  `has_education` int(10) NOT NULL,
  `has_sector` int(10) NOT NULL,
  `has_specialisation` int(10) NOT NULL,
  `ratings_points` double NOT NULL,
  `is_active` int(10) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `subscription_id` bigint(10) NOT NULL,
  `subscription_is_active` int(10) NOT NULL,
  `subscription_start_date` datetime NOT NULL,
  `subscription_end_date` datetime NOT NULL,
  `no_of_bid_made` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `user_id_2` (`user_id`),
  KEY `subscription_id` (`subscription_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `job_seekers_bid`
--

CREATE TABLE IF NOT EXISTS `job_seekers_bid` (
  `id` bigint(10) NOT NULL AUTO_INCREMENT,
  `job_post_id` bigint(10) NOT NULL,
  `job_seeker_id` bigint(10) NOT NULL,
  `bid_amount` double NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_awarded` int(10) NOT NULL,
  `payment_id` bigint(10) NOT NULL,
  `subscription_id` bigint(10) NOT NULL,
  `subscription_is_active` int(10) NOT NULL,
  `subscription_start_date` datetime NOT NULL,
  `subscription_end_date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `job_post_id` (`job_post_id`),
  KEY `job_seeker_id` (`job_seeker_id`),
  KEY `subscription_id` (`subscription_id`),
  KEY `payment_id` (`payment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `job_seekers_bid_attachments`
--

CREATE TABLE IF NOT EXISTS `job_seekers_bid_attachments` (
  `id` bigint(10) NOT NULL AUTO_INCREMENT,
  `job_seekers_bid_id` bigint(10) NOT NULL,
  `path` varchar(254) NOT NULL,
  `comments` varchar(254) NOT NULL,
  `user_id` bigint(10) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_active` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `job_seekers_bid_id` (`job_seekers_bid_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `job_seeker_attachments`
--

CREATE TABLE IF NOT EXISTS `job_seeker_attachments` (
  `id` bigint(10) NOT NULL AUTO_INCREMENT,
  `job_seeker_id` bigint(10) NOT NULL,
  `path` varchar(254) NOT NULL,
  `comments` varchar(254) NOT NULL,
  `created_by` bigint(10) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_active` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `job_seeker_id` (`job_seeker_id`),
  KEY `created_by` (`created_by`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `job_seeker_education`
--

CREATE TABLE IF NOT EXISTS `job_seeker_education` (
  `id` bigint(10) NOT NULL AUTO_INCREMENT,
  `job_seeker_id` bigint(10) NOT NULL,
  `field_code` varchar(254) NOT NULL,
  `adj_params_master_id` bigint(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `job_seeker_id` (`job_seeker_id`),
  KEY `adj_params_master_id` (`adj_params_master_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `job_seeker_rating_comments`
--

CREATE TABLE IF NOT EXISTS `job_seeker_rating_comments` (
  `id` bigint(10) NOT NULL AUTO_INCREMENT,
  `job_seeker_id` bigint(10) NOT NULL,
  `points` int(10) NOT NULL,
  `out_of` int(10) NOT NULL,
  `comments` varchar(254) NOT NULL,
  `created_by` bigint(10) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_active` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `job_seeker_id` (`job_seeker_id`),
  KEY `created_by` (`created_by`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `job_seeker_sector`
--

CREATE TABLE IF NOT EXISTS `job_seeker_sector` (
  `id` bigint(10) NOT NULL AUTO_INCREMENT,
  `job_seeker_id` bigint(10) NOT NULL,
  `field_code` varchar(254) NOT NULL,
  `adj_params_master_id` bigint(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `job_seeker_id` (`job_seeker_id`),
  KEY `adj_params_master_id` (`adj_params_master_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `job_seeker_specialisation`
--

CREATE TABLE IF NOT EXISTS `job_seeker_specialisation` (
  `id` bigint(10) NOT NULL AUTO_INCREMENT,
  `job_seeker_id` bigint(10) NOT NULL,
  `field_code` varchar(254) NOT NULL,
  `adj_params_master_id` bigint(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `job_seeker_id` (`job_seeker_id`),
  KEY `adj_params_master_id` (`adj_params_master_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `old_job_post`
--

CREATE TABLE IF NOT EXISTS `old_job_post` (
  `id` bigint(10) NOT NULL AUTO_INCREMENT,
  `title` varchar(254) NOT NULL,
  `description` text NOT NULL,
  `category_id` bigint(10) NOT NULL,
  `location_city` varchar(254) NOT NULL,
  `location_country` varchar(254) NOT NULL,
  `location_gps` varchar(254) NOT NULL,
  `estimation` varchar(254) NOT NULL,
  `job_poster_id` bigint(10) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_modified` datetime NOT NULL,
  `is_active` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `category_id` (`category_id`),
  KEY `job_poster_id` (`job_poster_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `page_cms`
--

CREATE TABLE IF NOT EXISTS `page_cms` (
  `id` bigint(10) NOT NULL AUTO_INCREMENT,
  `page_name` varchar(254) NOT NULL,
  `page_text` text NOT NULL,
  `image` varchar(254) NOT NULL,
  `slug` varchar(254) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `status` enum('1','0') NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `page_cms`
--

INSERT INTO `page_cms` (`id`, `page_name`, `page_text`, `image`, `slug`, `created_at`, `updated_at`, `status`) VALUES
(2, 'How It Works', '<p><strong>Job Poster</strong></p>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n\r\n<p><strong>Job Seeker&nbsp;</strong></p>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n\r\n<p>&nbsp;</p>\r\n', '', 'how-it-works', '2016-06-14 08:55:56', '0000-00-00 00:00:00', '1'),
(3, 'About Us', '<p><strong>Von</strong></p>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n\r\n<p><strong>Yvonne</strong></p>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n\r\n<p><strong>Why was Pitch Your Concepts formed?&nbsp;</strong></p>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n\r\n<p><strong>How does the website work?</strong></p>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n', '', 'about-us', '2016-06-14 11:14:19', '0000-00-00 00:00:00', '1'),
(5, 'Quality', '<p>Step to reproduce:<br />\r\n1. Open admin panel<br />\r\n2. Login with valid credentials<br />\r\n3. Go to right side top corner of admin icon<br />\r\n&nbsp;</p>\r\n', '146599641372.jpg', 'quality', '2016-06-15 12:49:33', '0000-00-00 00:00:00', '1'),
(6, 'Privacy Policy', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n', '', 'privacy-policy', '2016-06-16 23:23:06', '0000-00-00 00:00:00', '1'),
(7, 'Terms and Conditions', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n', '', 'terms-and-conditions', '2016-06-16 23:23:38', '0000-00-00 00:00:00', '1'),
(9, 'sdfsdfsdfsd fsd', '<p>f sdf sdfsd fdsf</p>\r\n', '', 'sdfsdfsdfsd-fsd', '2016-08-09 10:03:34', '0000-00-00 00:00:00', '1');

-- --------------------------------------------------------

--
-- Table structure for table `password_reminders`
--

CREATE TABLE IF NOT EXISTS `password_reminders` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  KEY `password_reminders_email_index` (`email`),
  KEY `password_reminders_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('ravi.kadia@letsnurture.com', 'ca17b1874313444f41d4dcabe1bd205cc76eb105ea57dbb7e43f29cc7f69e7f2', '2015-09-25 05:17:36'),
('ankit.letsnurture@gmail.com', '9cb693fe3f7abf3172c842bb82cb653cf68fcccf3e24420f79b3be1bab02f3cf', '2015-09-29 03:21:02'),
('nilesh@letsnurture.com', 'd260411000242d7b42f4d7aa4483cc32338aab699e81ac3636414fad2b50b070', '2016-05-12 05:02:07'),
('peter@orphmedia.com', '836576de4980128bde343e181f19bf4de2415f06dc734b571a04f69344a9878f', '2016-06-23 04:41:08'),
('pradip.darji@letsnurture.com', '9fb80529500ecfad814b30a886ac40cdf969a1df11fc0fdf42947a7ea9ccae09', '2016-07-22 06:19:48'),
('Pradip.letsnurture@gmail.com', '48049c8c4e309fb608ac1ee9c53513942c66fc9747c63b3233ccc7d487b1e2b7', '2016-07-22 06:20:43'),
('qa@letsnurture.com', '4f784ad957510a43327f7959ae4992762e5362e549ea8e02cb40d0cf03481f8f', '2016-07-22 10:12:56');

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE IF NOT EXISTS `payments` (
  `id` bigint(10) NOT NULL AUTO_INCREMENT,
  `payer_id` varchar(254) NOT NULL,
  `txn_id` varchar(254) NOT NULL,
  `subscr_id` varchar(255) NOT NULL,
  `payer_status` varchar(255) NOT NULL,
  `payment_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `payment_status` varchar(254) NOT NULL,
  `subscription_id` bigint(10) NOT NULL,
  `payment_for` enum('JOB_SEEKER','JOB_POST_BID') NOT NULL,
  `job_seeker_id` bigint(10) NOT NULL,
  `is_recurring` enum('0','1') NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `subscription_id` (`subscription_id`),
  KEY `job_seeker_id` (`job_seeker_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `payments`
--

INSERT INTO `payments` (`id`, `payer_id`, `txn_id`, `subscr_id`, `payer_status`, `payment_date`, `payment_status`, `subscription_id`, `payment_for`, `job_seeker_id`, `is_recurring`) VALUES
(11, '8C3VK9G5BL4RG', '3G742996X5926942N', '', '', '2016-07-14 23:00:00', 'Pending', 1, 'JOB_SEEKER', 95, '0'),
(12, '8C3VK9G5BL4RG', '63B285008H4489613', '', '', '2016-07-17 23:00:00', 'Pending', 3, 'JOB_SEEKER', 70, '0'),
(13, 'QZSYUBTJ76KQS', '89E80302P6470861U', '', '', '2016-07-17 23:00:00', 'Pending', 1, 'JOB_SEEKER', 70, '0'),
(14, 'QZSYUBTJ76KQS', '', 'I-2HJ9UX3SDFTP', 'verified', '2016-07-19 13:44:39', '', 2, 'JOB_SEEKER', 70, '1');

-- --------------------------------------------------------

--
-- Table structure for table `subscription`
--

CREATE TABLE IF NOT EXISTS `subscription` (
  `id` bigint(10) NOT NULL AUTO_INCREMENT,
  `subscription_title` varchar(254) NOT NULL,
  `subscription_desc` text NOT NULL,
  `subscription_type` varchar(254) NOT NULL,
  `subscription_amount` varchar(100) NOT NULL,
  `subscription_period` varchar(254) NOT NULL,
  `subscription_applicable_for` varchar(254) NOT NULL,
  `free_for_month` int(10) NOT NULL,
  `additional_for_month` int(10) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status` enum('1','0') NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `subscription`
--

INSERT INTO `subscription` (`id`, `subscription_title`, `subscription_desc`, `subscription_type`, `subscription_amount`, `subscription_period`, `subscription_applicable_for`, `free_for_month`, `additional_for_month`, `created_at`, `updated_at`, `status`) VALUES
(1, 'Pitch Your Concept', 'Professional job seeker and the essentials to find them.', 'Pitch Your Concepts - Silver', '10', '1', '4', 1, 4, '2016-06-23 18:28:47', '2016-07-04 13:35:33', '1'),
(2, 'Pitch Your Concept', 'Professional job seeker and the essentials to find them.', 'Pitch Your Concepts - Pro', '15', '1', '7', 1, 7, '2016-06-23 19:19:47', '2016-07-04 13:35:20', '1'),
(3, 'Pitch Your Concept', 'Professional job seeker and the essentials to find them.', 'Pitch Your Concepts - Enterprise', '30', '1', '23', 1, 23, '2016-06-23 19:21:46', '2016-07-04 13:35:06', '1');

-- --------------------------------------------------------

--
-- Table structure for table `sys_defaults_params`
--

CREATE TABLE IF NOT EXISTS `sys_defaults_params` (
  `id` bigint(10) NOT NULL AUTO_INCREMENT,
  `param_code` varchar(254) NOT NULL,
  `param_name` varchar(254) NOT NULL,
  `param_val` varchar(254) NOT NULL,
  `param_set` varchar(254) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `email` varchar(254) NOT NULL,
  `password` varchar(500) NOT NULL,
  `remember_token` varchar(100) NOT NULL,
  `role` enum('JOB_POSTER','JOB_SEEKER','sAdmin') NOT NULL,
  `image` varchar(254) NOT NULL,
  `register_as` varchar(254) NOT NULL,
  `company_name` varchar(254) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status` int(10) NOT NULL DEFAULT '0',
  `last_login` datetime NOT NULL,
  `login_ip` varchar(50) NOT NULL,
  `from_device` enum('mob','web') NOT NULL,
  `activation_code` varchar(254) NOT NULL,
  `company_link` varchar(255) NOT NULL,
  `street_address1` varchar(255) NOT NULL,
  `street_address2` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `state` varchar(255) NOT NULL,
  `zip` varchar(255) NOT NULL,
  `country` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `available_pitch` int(255) DEFAULT NULL,
  `free_pitch` int(11) NOT NULL DEFAULT '1',
  `experience` varchar(10) DEFAULT NULL,
  `graduation` varchar(254) DEFAULT NULL,
  `post_graduation` varchar(254) DEFAULT NULL,
  `computer_skill` text,
  `portfolio_image` varchar(100) DEFAULT NULL,
  `portfolio_description` text,
  `project_category_specialization` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=147 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `password`, `remember_token`, `role`, `image`, `register_as`, `company_name`, `created_at`, `updated_at`, `status`, `last_login`, `login_ip`, `from_device`, `activation_code`, `company_link`, `street_address1`, `street_address2`, `city`, `state`, `zip`, `country`, `phone`, `available_pitch`, `free_pitch`, `experience`, `graduation`, `post_graduation`, `computer_skill`, `portfolio_image`, `portfolio_description`, `project_category_specialization`) VALUES
(1, 'Admin', 'Admin', 'admin@gmail.com', '$2y$10$1eHEb1GpEdZ3yJ8L5njUt.qYjLNBOr4EVd8ZwydHblVTiwTR50uYG', 'NicFhYJ50p6LbN7VpnldtVe0FCBBEDBmbciR8FiNmGTu1CCESUrXxt0xG6ZM', 'sAdmin', '2016/06/15/7672e851d21fead4205811cc18cf39964cc80422.jpg', '', '', '2016-05-18 06:05:41', '2016-06-16 13:07:15', 1, '0000-00-00 00:00:00', '', 'mob', '', '', '205, Pavan Flat', 'Nr. K.K. Nagar Cross Road', 'Ahmedabad', 'Gujarat', '380006', 'UK', '9999998887', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(59, 'Vinay', 'Srivastav', 'vinay.letsnurture@gmail.com', '$2y$10$JKjXRk498t.fVNYyY/CP4u3.hSFXMbYU8k5xarMF8cLfoCRABYaA6', '', 'JOB_POSTER', '146460589468.jpg', 'company', 'LetsNurture', '2016-05-27 04:48:57', '2016-05-30 10:12:35', 1, '0000-00-00 00:00:00', '', 'mob', '', 'http://letsnurture.com', '205, Pavan Flat', 'Nr. K.K. Nagar Cross Road', 'Ahmedabad', 'Gujarat', '380006', 'UK', '9999998887', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(60, 'V', 'C', 'von.chua@vonitto.com', '$2y$10$k7mp65B0NEpdHxgGeJxJrOJThyxkR9HFurNjwSb1lCQvLcOv3fhNC', '0KYCV4vVoj9l4eTquznW9CAGHCzgwHMsSYqel0YnHfH1mZQKmAhTapgRFsuH', 'JOB_SEEKER', '146433202084.jpg', '', '', '2016-05-27 05:53:40', '2016-07-26 21:56:24', 1, '0000-00-00 00:00:00', '', 'mob', '', '', '205, Pavan Flat', 'Nr. K.K. Nagar Cross Road', 'Ahmedabad', 'Gujarat', '380006', 'UK', '9999998887', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(70, 'nilesh', 'panchal', 'nilesh@letsnurture.com', '$2y$10$TinTd2STYBe7VNWEXcyFF.pf3d1Xi02tP7rdR5VtgKPWJCHLiCgQG', '', 'JOB_SEEKER', '146520862630.png', '', '', '2016-06-30 23:00:00', '2016-07-19 12:44:39', 1, '0000-00-00 00:00:00', '', 'mob', '', '', '205, Pavan Flat', 'Nr. K.K. Nagar Cross Road', 'Ahmedabad', 'Gujarat', '380006', 'UK', '9999998887', 7, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(94, 'Yvonne', 'Chua', 'yvonne.chua@vonitto.com', '$2y$10$nnsYqNY3l3Z5bli.pRx./OOW/xG.KyJZGj3jTdNzvXnoorUzUZiq.', '', 'JOB_POSTER', '146758797851.jpg', 'company', 'ABCo', '2016-07-03 22:19:38', '2016-07-03 22:21:35', 1, '0000-00-00 00:00:00', '', 'mob', '', 'abco.co.uk', '205, Pavan Flat', 'Nr. K.K. Nagar Cross Road', 'Ahmedabad', 'Gujarat', '380006', 'UK', '9999998887', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(95, 'Pradip', 'Darji', 'Pradip.letsnurture@gmail.com', '$2y$10$D4DWw90WNk4uHZW9KrPuSuuELl5FzYo36280zIdYalvIdt/IlUyzC', '', 'JOB_SEEKER', '1467812967100.jpg', '', '', '2017-07-30 23:00:00', '2016-08-04 01:46:49', 1, '0000-00-00 00:00:00', '', 'mob', '', '', 'M-13 Harivilla Flat ', 'Near J.k Park', 'Ahmedabad', 'Gujarat', '382481', 'Malaysia', '9727357148', NULL, 0, '12', 'B.C.A', 'M.C.A', 'Java,C++,Php,Javascript', '147022458937.png', 'asd asdas dasd asd asdas dasd asd', 'asdas das dasd asdas dasd'),
(96, 'Dharmendra', 'Solanki', 'Dharmendra.letsnurture@gmail.com', '$2y$10$EMscEI.6Y6/Hphu664hUCO.k/qI85/B.QHS95Ry5PX00CVzy8DOue', 'HyLbPAL1WkeqZoDTYTnMixmKdlTl90wZstuBIJQvzgZQVFCpffxFPNK3uiG9', 'JOB_SEEKER', '146781331926.png', '', '', '2017-05-01 23:00:00', '2016-07-08 04:27:03', 1, '0000-00-00 00:00:00', '', 'mob', '', '', 'L-12,Anand Nagar', 'Near J.k Park', 'Ahmedabad', 'Gujarat', '382411', 'UK', '788574712', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(97, 'Gaurav', 'QA', 'gauravqa.letsnurture@gmail.com', '$2y$10$nATgqyqEZlyF6zO6frPdhOy0SJ.KRemiBm9mm3dp/hT2CaQPRfynu', '51HAyEbNUkdRMxBMegNDC8eSmzoVgdlPpD1VxaTVAIyRswfIXiqaxkbhalLr', 'JOB_POSTER', '146781339924.jpg', 'individual', '', '2016-07-06 12:56:39', '2016-07-08 04:02:45', 1, '0000-00-00 00:00:00', '', 'mob', '', '', '', '', '', '', '', '', '', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(98, 'Pradip', 'Tailor', 'pradip.darji@letsnurture.com', '$2y$10$qQxOvqnEqi0xLczEXb95reD8ojgGpD0.YWak5RyJCAxDWGZWpxTbW', 'vPIvJehAE1uLDonYp6h9Vq9vnsnKiauwq4qDMrwCJFYcWf7nBPS2yrjebnVQ', 'JOB_POSTER', '146781345786.jpg', 'individual', '', '2016-07-06 12:57:37', '2016-07-20 07:54:02', 1, '0000-00-00 00:00:00', '', 'mob', '', '', '', '', '', '', '', '', '', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(113, 'era', 'dandi', 'zalasid.mca@gmail.com', '$2y$10$jsgoMXqQap9ZQaV1ZFWWD.amBQzNcCMIh8JyXDktXOOD/hkIYNZGO', 'Vj0APr4RvpHmMV9aDARBuRGIUodHIyKseNYX2sxCUx4Ora6p96YDsGmYRALT', 'JOB_POSTER', '146787598533.png', 'individual', '', '2016-07-07 06:19:45', '2016-07-08 05:37:59', 0, '0000-00-00 00:00:00', '', 'mob', 'QNhsBK1Q8WZhuqUFK8bw4Q7AdYDzzo', '', '', '', '', '', '', '', '', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(114, 'reema', 'tandel', 'tand@gmail.com', '$2y$10$aM2uqefG4h94GEQQW3xQcux4X9jmFzeZibi1O4u/muzNQN9pmggge', '', 'JOB_POSTER', '146787617782.jpg', 'individual', '', '2016-07-07 06:22:57', '2016-07-07 06:22:57', 0, '0000-00-00 00:00:00', '', 'mob', 'E8GC8BT607RJundyB4bmvitzwjZsP7', '', '', '', '', '', '', '', '', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(115, 'Riya', 'Patel', 'riya@gmail.com', '$2y$10$CE3Ug8AI.F0dSmIhL0JaIOrwR3IK9KTbqobMH6RK0xGN4RADASDza', '', 'JOB_POSTER', '146787634634.png', 'individual', '', '2016-07-07 06:25:46', '2016-07-07 06:25:46', 0, '0000-00-00 00:00:00', '', 'mob', 'LmnMQRd5qrt6jtuprqFWhThVOIJEpB', '', '', '', '', '', '', '', '', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(116, 'TesterLN', 'Tester', 't@gmail.v', '$2y$10$2/MqSzc0UTvylqHN.qscnOlOIHmiuLRPsYMBp3MtvygW9mQBBR4xK', '', 'JOB_SEEKER', '146787653391.png', '', '', '2016-07-07 06:28:53', '2016-07-07 06:28:53', 0, '0000-00-00 00:00:00', '', 'mob', '7YirjmBxD3YovWwFCXF2OEZnOzGzDW', '', '', '', '', '', '', '', '', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(117, 'Ankit', 'Sompura', 'ankit.letsnurture@gmail.com', '$2y$10$EMzaEgOPgW6Yt9Fl94WoGuPI9g2F.O6l6nEOhKjOHFQIt.6fUiq52', '', 'JOB_SEEKER', '146831425283.png', '', '', '2016-07-12 08:04:12', '2016-07-12 08:04:35', 1, '0000-00-00 00:00:00', '', 'mob', '', '', '', '', '', '', '', '', '', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(118, 'ravi', 'kadia', 'ravi.kadia@letsnurture.com', '$2y$10$0VBCpM5KcZVt5pvbewE2euT3V.AtsON6taY3iMQlBkeFMbxSkokX6', '', 'JOB_POSTER', '146855948561.jpg', 'individual', '', '2016-07-15 04:11:25', '2016-07-15 04:11:46', 1, '0000-00-00 00:00:00', '', 'mob', '', '', '', '', '', '', '', '', '', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(119, 'ravi', 'kadia', 'ravi.kadia@lesnurture.com', '$2y$10$nd2ANJRIj.kITetDMMV9reyAkGZ8C0voT0aJmBhms6sHYSQQgU3Dq', '', 'JOB_POSTER', '146855967048.png', 'individual', '', '2016-07-15 04:14:30', '2016-07-15 04:14:30', 0, '0000-00-00 00:00:00', '', 'mob', 'kAlUnUyjUcUSlPJhyC1MahEM5d0jN8', '', '', '', '', '', '', '', '', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(144, 'Pradip', 'Darji', 'qa@letsnurture.com', '$2y$10$FXnBggJW6Vo6O76.3uur4eveaOYx.I5UiOwCf0oVUNDIFCqP4wtGS', 'z01jSLyVFptOva0ZsewYRCSopOR2Nctj44HQUQoCRUWGuMbfmGhchJbUQjd2', 'JOB_SEEKER', '146918249725.png', '', '', '2016-07-22 09:14:57', '2016-07-22 09:23:55', 1, '0000-00-00 00:00:00', '', 'mob', '', '', 'M-13,Harivilla', 'Near J.: Park Gota Road Chandlodia', 'Ahmedabad', 'Gujarat', '382481', 'Malaysia', '9727357148', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(145, 'happy', 'always', 'polarbears8888@gmail.com', '$2y$10$3Yby1Ou2s20BCU0E9IaQdeuxQyjXzGXpRsztiZXeQyIFujE9XmrxC', '', 'JOB_POSTER', '146957445276.JPG', 'company', 'POLAR BEARS', '2016-07-26 22:07:32', '2016-07-26 22:08:30', 1, '0000-00-00 00:00:00', '', 'mob', '', '', '', '', '', '', '', '', '', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(146, 'Hitesh', 'Tank', 'hiteshtank.letsnurture@gmail.com', '$2y$10$D4DWw90WNk4uHZW9KrPuSuuELl5FzYo36280zIdYalvIdt/IlUyzC', '', 'JOB_SEEKER', '146959454071.png', '', '', '2016-07-27 03:42:20', '2016-08-11 05:45:49', 1, '0000-00-00 00:00:00', '', 'mob', '', '', '', '', '', '', '', '', '', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `job_bookmark`
--
ALTER TABLE `job_bookmark`
  ADD CONSTRAINT `job_bookmark_ibfk_1` FOREIGN KEY (`poster_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `job_bookmark_ibfk_2` FOREIGN KEY (`seeker_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `job_bookmark_ibfk_3` FOREIGN KEY (`job_id`) REFERENCES `job_post` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `job_bookmark_email`
--
ALTER TABLE `job_bookmark_email`
  ADD CONSTRAINT `job_bookmark_email_ibfk_1` FOREIGN KEY (`job_id`) REFERENCES `job_post` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `job_bookmark_email_ibfk_2` FOREIGN KEY (`seeker_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `job_default_logos`
--
ALTER TABLE `job_default_logos`
  ADD CONSTRAINT `job_default_logos_ibfk_1` FOREIGN KEY (`job_category_id`) REFERENCES `job_category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `job_poster`
--
ALTER TABLE `job_poster`
  ADD CONSTRAINT `job_poster_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `job_poster_attachments`
--
ALTER TABLE `job_poster_attachments`
  ADD CONSTRAINT `job_poster_attachments_ibfk_1` FOREIGN KEY (`job_poster_id`) REFERENCES `job_poster` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `job_poster_attachments_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `job_poster_rating_comments`
--
ALTER TABLE `job_poster_rating_comments`
  ADD CONSTRAINT `job_poster_rating_comments_ibfk_1` FOREIGN KEY (`job_poster_id`) REFERENCES `job_poster` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `job_poster_rating_comments_ibfk_2` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `job_poster_strength`
--
ALTER TABLE `job_poster_strength`
  ADD CONSTRAINT `job_poster_strength_ibfk_1` FOREIGN KEY (`job_poster_id`) REFERENCES `job_poster` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `job_poster_strength_ibfk_2` FOREIGN KEY (`adj_params_master_id`) REFERENCES `adj_params_master` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `job_post_attachments`
--
ALTER TABLE `job_post_attachments`
  ADD CONSTRAINT `job_post_attachments_ibfk_1` FOREIGN KEY (`job_post_id`) REFERENCES `old_job_post` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `job_post_attachments_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `job_post_skills`
--
ALTER TABLE `job_post_skills`
  ADD CONSTRAINT `job_post_skills_ibfk_1` FOREIGN KEY (`job_post_id`) REFERENCES `old_job_post` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `job_post_skills_ibfk_2` FOREIGN KEY (`adj_params_master_id`) REFERENCES `adj_params_master` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `job_review_rating`
--
ALTER TABLE `job_review_rating`
  ADD CONSTRAINT `job_review_rating_ibfk_1` FOREIGN KEY (`seeker_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `job_seeker`
--
ALTER TABLE `job_seeker`
  ADD CONSTRAINT `job_seeker_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `job_seeker_ibfk_2` FOREIGN KEY (`subscription_id`) REFERENCES `subscription` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `job_seekers_bid`
--
ALTER TABLE `job_seekers_bid`
  ADD CONSTRAINT `job_seekers_bid_ibfk_1` FOREIGN KEY (`job_post_id`) REFERENCES `old_job_post` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `job_seekers_bid_ibfk_2` FOREIGN KEY (`job_seeker_id`) REFERENCES `job_seeker` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `job_seekers_bid_ibfk_3` FOREIGN KEY (`subscription_id`) REFERENCES `subscription` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `job_seekers_bid_ibfk_4` FOREIGN KEY (`payment_id`) REFERENCES `payments` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `job_seekers_bid_attachments`
--
ALTER TABLE `job_seekers_bid_attachments`
  ADD CONSTRAINT `job_seekers_bid_attachments_ibfk_1` FOREIGN KEY (`job_seekers_bid_id`) REFERENCES `job_seekers_bid` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `job_seekers_bid_attachments_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `job_seeker_attachments`
--
ALTER TABLE `job_seeker_attachments`
  ADD CONSTRAINT `job_seeker_attachments_ibfk_1` FOREIGN KEY (`job_seeker_id`) REFERENCES `job_seeker` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `job_seeker_attachments_ibfk_2` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `job_seeker_education`
--
ALTER TABLE `job_seeker_education`
  ADD CONSTRAINT `job_seeker_education_ibfk_1` FOREIGN KEY (`job_seeker_id`) REFERENCES `job_seeker` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `job_seeker_education_ibfk_2` FOREIGN KEY (`adj_params_master_id`) REFERENCES `adj_params_master` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `job_seeker_rating_comments`
--
ALTER TABLE `job_seeker_rating_comments`
  ADD CONSTRAINT `job_seeker_rating_comments_ibfk_1` FOREIGN KEY (`job_seeker_id`) REFERENCES `job_seeker` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `job_seeker_rating_comments_ibfk_2` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `job_seeker_sector`
--
ALTER TABLE `job_seeker_sector`
  ADD CONSTRAINT `job_seeker_sector_ibfk_1` FOREIGN KEY (`job_seeker_id`) REFERENCES `job_seeker` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `job_seeker_sector_ibfk_2` FOREIGN KEY (`adj_params_master_id`) REFERENCES `adj_params_master` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `job_seeker_specialisation`
--
ALTER TABLE `job_seeker_specialisation`
  ADD CONSTRAINT `job_seeker_specialisation_ibfk_1` FOREIGN KEY (`job_seeker_id`) REFERENCES `job_seeker` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `job_seeker_specialisation_ibfk_2` FOREIGN KEY (`adj_params_master_id`) REFERENCES `adj_params_master` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
