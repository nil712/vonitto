-- phpMyAdmin SQL Dump
-- version 4.4.10
-- http://www.phpmyadmin.net
--
-- Host: 10.169.0.94
-- Generation Time: Jun 06, 2016 at 08:12 AM
-- Server version: 5.6.30
-- PHP Version: 5.3.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `letsnurt_vonitto`
--

-- --------------------------------------------------------

--
-- Table structure for table `adj_params_master`
--

CREATE TABLE IF NOT EXISTS `adj_params_master` (
  `id` bigint(10) NOT NULL,
  `field_name` varchar(254) NOT NULL,
  `field_code` varchar(254) NOT NULL,
  `field_value` varchar(254) NOT NULL,
  `related_with` enum('JOB_POSTER','JOB_SEEKER') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `job_category`
--

CREATE TABLE IF NOT EXISTS `job_category` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `job_category`
--

INSERT INTO `job_category` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Culture', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'Education', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'Health', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'Hospitality', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 'Industrial', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 'Office & Workplace', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 'Residential', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 'Retail', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 'Sports', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, 'Transportation', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `job_post`
--

CREATE TABLE IF NOT EXISTS `job_post` (
  `id` bigint(10) NOT NULL,
  `title` varchar(254) NOT NULL,
  `description` text NOT NULL,
  `what_you_want` text NOT NULL,
  `what_kind_job` varchar(255) NOT NULL,
  `category_id` int(11) NOT NULL,
  `location_city` varchar(254) NOT NULL,
  `location_country` varchar(254) NOT NULL,
  `budget` varchar(254) NOT NULL,
  `file` varchar(255) NOT NULL,
  `skills` text NOT NULL,
  `how_long` varchar(255) NOT NULL,
  `is_one_time` tinyint(4) NOT NULL,
  `job_poster_id` bigint(10) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL,
  `is_active` int(10) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `job_post`
--

INSERT INTO `job_post` (`id`, `title`, `description`, `what_you_want`, `what_kind_job`, `category_id`, `location_city`, `location_country`, `budget`, `file`, `skills`, `how_long`, `is_one_time`, `job_poster_id`, `created_at`, `updated_at`, `is_active`) VALUES
(3, 'garden landscape', 'design landscape', 'awesome design', 'designing outer', 7, 'London', 'UK', '$5000', '146502205051.txt', 'master in designing, master in Archicad', '1 to 3 months', 1, 4, '2016-06-04 00:04:10', '2016-06-04 06:34:10', 1),
(4, 'Terrace design', 'design penthouse terrace ', 'terrace swimming pool, swing other designs', 'ongoing', 7, 'Kuala Lumpur', 'Malaysia', '$10000', '146502280264.png', ' master in 3D and awesome design', 'More than 6 months', 0, 4, '2016-06-04 00:16:42', '2016-06-04 06:46:42', 1),
(16, 'Bed room design', 'design awesome bedroom', 'design extremely exclusive room', 'interior planning', 4, 'London', 'UK', '$5000', '', 'architect licence', '1 to 3 months', 1, 67, '2016-06-04 09:40:39', '2016-06-04 10:40:39', 1),
(17, 'Church design', 'design a church', 'design whole church including landscape', 'planning and design', 1, 'ipoh', 'Malaysia', '$2500000', '', 'good planning and design skills', 'More than 6 months', 0, 67, '2016-06-04 09:44:03', '2016-06-04 10:44:03', 1);

-- --------------------------------------------------------

--
-- Table structure for table `job_poster`
--

CREATE TABLE IF NOT EXISTS `job_poster` (
  `id` bigint(10) NOT NULL,
  `user_id` bigint(10) NOT NULL,
  `company_name` varchar(254) NOT NULL,
  `address` varchar(254) NOT NULL,
  `location_city` varchar(254) NOT NULL,
  `location_country` varchar(254) NOT NULL,
  `location_gps` varchar(254) NOT NULL,
  `url` varchar(500) NOT NULL,
  `contact1` varchar(254) NOT NULL,
  `contact2` varchar(254) NOT NULL,
  `email` varchar(254) NOT NULL,
  `ratings_points` double NOT NULL,
  `has_strength` int(10) NOT NULL,
  `is_active` int(10) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `job_poster_attachments`
--

CREATE TABLE IF NOT EXISTS `job_poster_attachments` (
  `id` bigint(10) NOT NULL,
  `job_poster_id` bigint(10) NOT NULL,
  `path` varchar(254) NOT NULL,
  `comments` varchar(254) NOT NULL,
  `user_id` bigint(10) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_active` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `job_poster_rating_comments`
--

CREATE TABLE IF NOT EXISTS `job_poster_rating_comments` (
  `id` bigint(10) NOT NULL,
  `job_poster_id` bigint(10) NOT NULL,
  `points` int(10) NOT NULL,
  `out_of` int(10) NOT NULL,
  `comments` varchar(254) NOT NULL,
  `created_by` bigint(10) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_active` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `job_poster_strength`
--

CREATE TABLE IF NOT EXISTS `job_poster_strength` (
  `id` bigint(10) NOT NULL,
  `job_poster_id` bigint(10) NOT NULL,
  `field_code` varchar(254) NOT NULL,
  `adj_params_master_id` bigint(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `job_post_attachments`
--

CREATE TABLE IF NOT EXISTS `job_post_attachments` (
  `id` bigint(10) NOT NULL,
  `job_post_id` bigint(10) NOT NULL,
  `path` varchar(254) NOT NULL,
  `comments` varchar(254) NOT NULL,
  `user_id` bigint(10) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_active` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `job_post_skills`
--

CREATE TABLE IF NOT EXISTS `job_post_skills` (
  `id` bigint(10) NOT NULL,
  `job_post_id` bigint(10) NOT NULL,
  `field_code` varchar(254) NOT NULL,
  `adj_params_master_id` bigint(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `job_seeker`
--

CREATE TABLE IF NOT EXISTS `job_seeker` (
  `id` bigint(10) NOT NULL,
  `user_id` bigint(10) NOT NULL,
  `company` varchar(254) NOT NULL,
  `location_city` varchar(254) NOT NULL,
  `location_country` varchar(254) NOT NULL,
  `year_of_experiance` varchar(100) NOT NULL,
  `location_gps` varchar(254) NOT NULL,
  `has_education` int(10) NOT NULL,
  `has_sector` int(10) NOT NULL,
  `has_specialisation` int(10) NOT NULL,
  `ratings_points` double NOT NULL,
  `is_active` int(10) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `subscription_id` bigint(10) NOT NULL,
  `subscription_is_active` int(10) NOT NULL,
  `subscription_start_date` datetime NOT NULL,
  `subscription_end_date` datetime NOT NULL,
  `no_of_bid_made` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `job_seekers_bid`
--

CREATE TABLE IF NOT EXISTS `job_seekers_bid` (
  `id` bigint(10) NOT NULL,
  `job_post_id` bigint(10) NOT NULL,
  `job_seeker_id` bigint(10) NOT NULL,
  `bid_amount` double NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_awarded` int(10) NOT NULL,
  `payment_id` bigint(10) NOT NULL,
  `subscription_id` bigint(10) NOT NULL,
  `subscription_is_active` int(10) NOT NULL,
  `subscription_start_date` datetime NOT NULL,
  `subscription_end_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `job_seekers_bid_attachments`
--

CREATE TABLE IF NOT EXISTS `job_seekers_bid_attachments` (
  `id` bigint(10) NOT NULL,
  `job_seekers_bid_id` bigint(10) NOT NULL,
  `path` varchar(254) NOT NULL,
  `comments` varchar(254) NOT NULL,
  `user_id` bigint(10) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_active` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `job_seeker_attachments`
--

CREATE TABLE IF NOT EXISTS `job_seeker_attachments` (
  `id` bigint(10) NOT NULL,
  `job_seeker_id` bigint(10) NOT NULL,
  `path` varchar(254) NOT NULL,
  `comments` varchar(254) NOT NULL,
  `created_by` bigint(10) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_active` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `job_seeker_education`
--

CREATE TABLE IF NOT EXISTS `job_seeker_education` (
  `id` bigint(10) NOT NULL,
  `job_seeker_id` bigint(10) NOT NULL,
  `field_code` varchar(254) NOT NULL,
  `adj_params_master_id` bigint(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `job_seeker_rating_comments`
--

CREATE TABLE IF NOT EXISTS `job_seeker_rating_comments` (
  `id` bigint(10) NOT NULL,
  `job_seeker_id` bigint(10) NOT NULL,
  `points` int(10) NOT NULL,
  `out_of` int(10) NOT NULL,
  `comments` varchar(254) NOT NULL,
  `created_by` bigint(10) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_active` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `job_seeker_sector`
--

CREATE TABLE IF NOT EXISTS `job_seeker_sector` (
  `id` bigint(10) NOT NULL,
  `job_seeker_id` bigint(10) NOT NULL,
  `field_code` varchar(254) NOT NULL,
  `adj_params_master_id` bigint(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `job_seeker_specialisation`
--

CREATE TABLE IF NOT EXISTS `job_seeker_specialisation` (
  `id` bigint(10) NOT NULL,
  `job_seeker_id` bigint(10) NOT NULL,
  `field_code` varchar(254) NOT NULL,
  `adj_params_master_id` bigint(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `old_job_post`
--

CREATE TABLE IF NOT EXISTS `old_job_post` (
  `id` bigint(10) NOT NULL,
  `title` varchar(254) NOT NULL,
  `description` text NOT NULL,
  `category_id` bigint(10) NOT NULL,
  `location_city` varchar(254) NOT NULL,
  `location_country` varchar(254) NOT NULL,
  `location_gps` varchar(254) NOT NULL,
  `estimation` varchar(254) NOT NULL,
  `job_poster_id` bigint(10) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_modified` datetime NOT NULL,
  `is_active` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE IF NOT EXISTS `payments` (
  `id` bigint(10) NOT NULL,
  `payment_id` varchar(254) NOT NULL,
  `transaction_id` varchar(254) NOT NULL,
  `payment_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `payment_status` varchar(254) NOT NULL,
  `subscription_id` bigint(10) NOT NULL,
  `payment_for` enum('JOB_SEEKER','JOB_POST_BID') NOT NULL,
  `job_seeker_id` bigint(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `subscription`
--

CREATE TABLE IF NOT EXISTS `subscription` (
  `id` bigint(10) NOT NULL,
  `subscription_title` varchar(254) NOT NULL,
  `subscription_desc` text NOT NULL,
  `subscription_type` varchar(254) NOT NULL,
  `subscription_amount` varchar(100) NOT NULL,
  `subscription_period` varchar(254) NOT NULL,
  `subscription_applicable_for` varchar(254) NOT NULL,
  `free_for_month` int(10) NOT NULL,
  `additional_for_month` int(10) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status` enum('1','0') NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sys_defaults_params`
--

CREATE TABLE IF NOT EXISTS `sys_defaults_params` (
  `id` bigint(10) NOT NULL,
  `param_code` varchar(254) NOT NULL,
  `param_name` varchar(254) NOT NULL,
  `param_val` varchar(254) NOT NULL,
  `param_set` varchar(254) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(11) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `email` varchar(254) NOT NULL,
  `password` varchar(500) NOT NULL,
  `remember_token` varchar(100) NOT NULL,
  `role` enum('JOB_POSTER','JOB_SEEKER','sAdmin') NOT NULL,
  `image` varchar(254) NOT NULL,
  `register_as` varchar(254) NOT NULL,
  `company_name` varchar(254) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status` int(10) NOT NULL DEFAULT '0',
  `last_login` datetime NOT NULL,
  `login_ip` varchar(50) NOT NULL,
  `from_device` enum('mob','web') NOT NULL,
  `activation_code` varchar(254) NOT NULL,
  `company_link` varchar(255) NOT NULL,
  `street_address1` varchar(255) NOT NULL,
  `street_address2` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `state` varchar(255) NOT NULL,
  `zip` varchar(255) NOT NULL,
  `country` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=70 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `password`, `remember_token`, `role`, `image`, `register_as`, `company_name`, `created_at`, `updated_at`, `status`, `last_login`, `login_ip`, `from_device`, `activation_code`, `company_link`, `street_address1`, `street_address2`, `city`, `state`, `zip`, `country`, `phone`) VALUES
(1, 'admin', 'admin', 'admin@gmail.com', '$2y$10$E30dyggYFZI6qD73aQbKze/qz8BtXl7mY/GMjM92Sp/C7mTKK3p9y', 'NicFhYJ50p6LbN7VpnldtVe0FCBBEDBmbciR8FiNmGTu1CCESUrXxt0xG6ZM', 'sAdmin', '', '', '', '2016-05-18 06:05:41', '2016-05-19 03:28:55', 1, '0000-00-00 00:00:00', '', 'mob', '', '', '', '', '', '', '', '', ''),
(59, 'Vinay', 'Srivastav', 'vinay.letsnurture@gmail.com', '$2y$10$/YSR.BoS4aaZhu/HLbMmrOyd1SwRQxnhc7F6L/ZG.gMCea1LYgMHC', '', 'JOB_POSTER', '146460589468.jpg', 'company', 'LetsNurture', '2016-05-27 04:48:57', '2016-05-30 10:12:35', 1, '0000-00-00 00:00:00', '', 'mob', '', 'http://letsnurture.com', '205, Pavan Flat', 'Nr. K.K. Nagar Cross Road', 'Ahmedabad', 'Gujarat', '380006', 'Malaysia', '9723719797'),
(60, 'V', 'C', 'von.chua@vonitto.com', '$2y$10$6NmABwWypc49gIQYr5dsieawTlOYpTaFr5Cxf8rCNdNKt3GieQeWS', '', 'JOB_SEEKER', '146433202084.jpg', '', '', '2016-05-27 05:53:40', '2016-05-27 05:56:08', 1, '0000-00-00 00:00:00', '', 'mob', '', '', '', '', '', '', '', '', ''),
(61, 'Y', 'C', 'yvonne.chua@vonitto.com', '$2y$10$WYHIxwS5xFIoZDUJtu.y2OHTrCCHkvc0k/kH2H2Oza5SfjhypL3QC', '', 'JOB_POSTER', '146433210983.jpg', 'individual', '', '2016-05-27 05:55:09', '2016-06-02 22:27:15', 1, '0000-00-00 00:00:00', '', 'mob', '', '', '1 ', 'Road', 'London', '', '1AB 2CD', 'UK', '12345678'),
(62, 'pratima', 'patel', 'pratima.letsnurture@gmail.com', '$2y$10$nq.PX2RbYNShBe8iTeMD.ubKQ.Un14i9egK9a/YdXmHStIwo5liYC', '', 'JOB_SEEKER', '146433314513.jpeg', '', '', '2016-05-27 06:12:25', '2016-06-02 08:53:54', 1, '0000-00-00 00:00:00', '', 'mob', '', '', '', '', '', '', '', '', '878978979878'),
(63, 'Dharmendra', 'Solanki', 'dharmendarsinh.solanki@letsnurture.com', '$2y$10$VWZLQGcsnvAtCPpaHlVds.Xwr1.qL9KUbdf8WdTHFYVArZH1e/Dz6', '', 'JOB_SEEKER', '146433922683.jpg', '', '', '2016-05-27 07:53:46', '2016-05-27 08:42:42', 1, '0000-00-00 00:00:00', '', 'mob', '', '', '', '', '', '', '', '', ''),
(64, 'Pradip', 'Darji', 'pradip.darji@letsnurture.com', '$2y$10$uDpOoF3Fv1Ku7WrAdRAAoOeSIxXivIkMtKN7W65HniPGEkYRkBIAO', '', 'JOB_SEEKER', '14643422786.png', '', '', '2016-05-27 08:44:38', '2016-05-27 08:44:38', 0, '0000-00-00 00:00:00', '', 'mob', 'LyVKVlcbpWRgRyzGvj5vL8mhaKmpqL', '', '', '', '', '', '', '', ''),
(65, 'Pradip', 'Darji', 'pradip.letsnurture@gmail.com', '$2y$10$/H/m2q.Y/jAxdD9iruB9z.Ib3Bpj6Kv1c2OESIxqsI2J1C8engVqS', '', 'JOB_SEEKER', '14643431207.png', '', '', '2016-05-27 08:58:40', '2016-05-27 08:59:23', 1, '0000-00-00 00:00:00', '', 'mob', '', '', '', '', '', '', '', '', ''),
(66, 'pratima', 'patel', 'pratima.patel@letsnurture.com', '$2y$10$SG1P1xR3dPQtm4u9ih0Pa.eEbSzc.Fbn8SHCyHymGVyl5Ewstg17O', '', 'JOB_POSTER', '146434444392.jpeg', 'individual', '', '2016-05-27 09:20:43', '2016-05-27 09:31:09', 1, '0000-00-00 00:00:00', '', 'mob', '', '', '', '', '', '', '', '', ''),
(67, 'Bhavesh', 'Chunch', 'bhavesh.letsnurture@gmail.com', '$2y$10$DAft0DdZ.t6JLmKec6hT1emW0BXb.hpalo/k4gxkEo6Gcz0bLg7z6', '', 'JOB_POSTER', '146478784491.jpg', 'company', 'LetsNurture', '2016-06-01 12:30:44', '2016-06-01 12:34:00', 1, '0000-00-00 00:00:00', '', 'mob', '', '', '', '', '', '', '', '', ''),
(68, 'Tusar', 'Darji', 'tusar@gmail.com', '$2y$10$by2pvvHhdiHZ.9X8cTKbpetGTnEjxrLKZTi1g1SC0v0NJoYXoqwFu', '', 'JOB_SEEKER', '146486040610.png', '', '', '2016-06-02 08:40:06', '2016-06-02 08:40:06', 0, '0000-00-00 00:00:00', '', 'mob', 'NaiwMkSbxEcqgO0aO4ytzPKhwt6hny', '', '', '', '', '', '', '', ''),
(69, 'Dharmendra', 'Patel', 'dharmendra.letsnurture@gmail.com', '$2y$10$Xg4BRiK/aTDPRRyosyTNcOW8htZRETz5KObP/CSSommPncpRU/RKa', '', 'JOB_POSTER', '146486134694.png', 'company', 'Tester', '2016-06-02 08:55:46', '2016-06-02 08:56:47', 1, '0000-00-00 00:00:00', '', 'mob', '', '', '', '', '', '', '', '', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `adj_params_master`
--
ALTER TABLE `adj_params_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `job_category`
--
ALTER TABLE `job_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `job_post`
--
ALTER TABLE `job_post`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category_id` (`category_id`),
  ADD KEY `job_poster_id` (`job_poster_id`);

--
-- Indexes for table `job_poster`
--
ALTER TABLE `job_poster`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `job_poster_attachments`
--
ALTER TABLE `job_poster_attachments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `job_poster_id` (`job_poster_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `job_poster_rating_comments`
--
ALTER TABLE `job_poster_rating_comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `job_poster_id` (`job_poster_id`);

--
-- Indexes for table `job_poster_strength`
--
ALTER TABLE `job_poster_strength`
  ADD PRIMARY KEY (`id`),
  ADD KEY `job_poster_id` (`job_poster_id`),
  ADD KEY `adj_params_master_id` (`adj_params_master_id`);

--
-- Indexes for table `job_post_attachments`
--
ALTER TABLE `job_post_attachments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `job_post_id` (`job_post_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `job_post_skills`
--
ALTER TABLE `job_post_skills`
  ADD PRIMARY KEY (`id`),
  ADD KEY `job_post_id` (`job_post_id`),
  ADD KEY `adj_params_master_id` (`adj_params_master_id`);

--
-- Indexes for table `job_seeker`
--
ALTER TABLE `job_seeker`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `user_id_2` (`user_id`),
  ADD KEY `subscription_id` (`subscription_id`);

--
-- Indexes for table `job_seekers_bid`
--
ALTER TABLE `job_seekers_bid`
  ADD PRIMARY KEY (`id`),
  ADD KEY `job_post_id` (`job_post_id`),
  ADD KEY `job_seeker_id` (`job_seeker_id`),
  ADD KEY `subscription_id` (`subscription_id`),
  ADD KEY `payment_id` (`payment_id`);

--
-- Indexes for table `job_seekers_bid_attachments`
--
ALTER TABLE `job_seekers_bid_attachments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `job_seekers_bid_id` (`job_seekers_bid_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `job_seeker_attachments`
--
ALTER TABLE `job_seeker_attachments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `job_seeker_id` (`job_seeker_id`),
  ADD KEY `created_by` (`created_by`);

--
-- Indexes for table `job_seeker_education`
--
ALTER TABLE `job_seeker_education`
  ADD PRIMARY KEY (`id`),
  ADD KEY `job_seeker_id` (`job_seeker_id`),
  ADD KEY `adj_params_master_id` (`adj_params_master_id`);

--
-- Indexes for table `job_seeker_rating_comments`
--
ALTER TABLE `job_seeker_rating_comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `job_seeker_id` (`job_seeker_id`),
  ADD KEY `created_by` (`created_by`);

--
-- Indexes for table `job_seeker_sector`
--
ALTER TABLE `job_seeker_sector`
  ADD PRIMARY KEY (`id`),
  ADD KEY `job_seeker_id` (`job_seeker_id`),
  ADD KEY `adj_params_master_id` (`adj_params_master_id`);

--
-- Indexes for table `job_seeker_specialisation`
--
ALTER TABLE `job_seeker_specialisation`
  ADD PRIMARY KEY (`id`),
  ADD KEY `job_seeker_id` (`job_seeker_id`),
  ADD KEY `adj_params_master_id` (`adj_params_master_id`);

--
-- Indexes for table `old_job_post`
--
ALTER TABLE `old_job_post`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category_id` (`category_id`),
  ADD KEY `job_poster_id` (`job_poster_id`);

--
-- Indexes for table `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `subscription_id` (`subscription_id`),
  ADD KEY `job_seeker_id` (`job_seeker_id`);

--
-- Indexes for table `subscription`
--
ALTER TABLE `subscription`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sys_defaults_params`
--
ALTER TABLE `sys_defaults_params`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `adj_params_master`
--
ALTER TABLE `adj_params_master`
  MODIFY `id` bigint(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `job_category`
--
ALTER TABLE `job_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `job_post`
--
ALTER TABLE `job_post`
  MODIFY `id` bigint(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `job_poster`
--
ALTER TABLE `job_poster`
  MODIFY `id` bigint(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `job_poster_attachments`
--
ALTER TABLE `job_poster_attachments`
  MODIFY `id` bigint(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `job_poster_rating_comments`
--
ALTER TABLE `job_poster_rating_comments`
  MODIFY `id` bigint(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `job_poster_strength`
--
ALTER TABLE `job_poster_strength`
  MODIFY `id` bigint(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `job_post_attachments`
--
ALTER TABLE `job_post_attachments`
  MODIFY `id` bigint(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `job_post_skills`
--
ALTER TABLE `job_post_skills`
  MODIFY `id` bigint(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `job_seeker`
--
ALTER TABLE `job_seeker`
  MODIFY `id` bigint(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `job_seekers_bid`
--
ALTER TABLE `job_seekers_bid`
  MODIFY `id` bigint(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `job_seekers_bid_attachments`
--
ALTER TABLE `job_seekers_bid_attachments`
  MODIFY `id` bigint(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `job_seeker_attachments`
--
ALTER TABLE `job_seeker_attachments`
  MODIFY `id` bigint(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `job_seeker_education`
--
ALTER TABLE `job_seeker_education`
  MODIFY `id` bigint(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `job_seeker_rating_comments`
--
ALTER TABLE `job_seeker_rating_comments`
  MODIFY `id` bigint(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `job_seeker_sector`
--
ALTER TABLE `job_seeker_sector`
  MODIFY `id` bigint(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `job_seeker_specialisation`
--
ALTER TABLE `job_seeker_specialisation`
  MODIFY `id` bigint(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `old_job_post`
--
ALTER TABLE `old_job_post`
  MODIFY `id` bigint(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `payments`
--
ALTER TABLE `payments`
  MODIFY `id` bigint(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `subscription`
--
ALTER TABLE `subscription`
  MODIFY `id` bigint(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sys_defaults_params`
--
ALTER TABLE `sys_defaults_params`
  MODIFY `id` bigint(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=70;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `job_poster`
--
ALTER TABLE `job_poster`
  ADD CONSTRAINT `job_poster_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `job_poster_attachments`
--
ALTER TABLE `job_poster_attachments`
  ADD CONSTRAINT `job_poster_attachments_ibfk_1` FOREIGN KEY (`job_poster_id`) REFERENCES `job_poster` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `job_poster_attachments_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `job_poster_rating_comments`
--
ALTER TABLE `job_poster_rating_comments`
  ADD CONSTRAINT `job_poster_rating_comments_ibfk_1` FOREIGN KEY (`job_poster_id`) REFERENCES `job_poster` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `job_poster_rating_comments_ibfk_2` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `job_poster_strength`
--
ALTER TABLE `job_poster_strength`
  ADD CONSTRAINT `job_poster_strength_ibfk_1` FOREIGN KEY (`job_poster_id`) REFERENCES `job_poster` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `job_poster_strength_ibfk_2` FOREIGN KEY (`adj_params_master_id`) REFERENCES `adj_params_master` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `job_post_attachments`
--
ALTER TABLE `job_post_attachments`
  ADD CONSTRAINT `job_post_attachments_ibfk_1` FOREIGN KEY (`job_post_id`) REFERENCES `old_job_post` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `job_post_attachments_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `job_post_skills`
--
ALTER TABLE `job_post_skills`
  ADD CONSTRAINT `job_post_skills_ibfk_1` FOREIGN KEY (`job_post_id`) REFERENCES `old_job_post` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `job_post_skills_ibfk_2` FOREIGN KEY (`adj_params_master_id`) REFERENCES `adj_params_master` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `job_seeker`
--
ALTER TABLE `job_seeker`
  ADD CONSTRAINT `job_seeker_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `job_seeker_ibfk_2` FOREIGN KEY (`subscription_id`) REFERENCES `subscription` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `job_seekers_bid`
--
ALTER TABLE `job_seekers_bid`
  ADD CONSTRAINT `job_seekers_bid_ibfk_1` FOREIGN KEY (`job_post_id`) REFERENCES `old_job_post` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `job_seekers_bid_ibfk_2` FOREIGN KEY (`job_seeker_id`) REFERENCES `job_seeker` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `job_seekers_bid_ibfk_3` FOREIGN KEY (`subscription_id`) REFERENCES `subscription` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `job_seekers_bid_ibfk_4` FOREIGN KEY (`payment_id`) REFERENCES `payments` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `job_seekers_bid_attachments`
--
ALTER TABLE `job_seekers_bid_attachments`
  ADD CONSTRAINT `job_seekers_bid_attachments_ibfk_1` FOREIGN KEY (`job_seekers_bid_id`) REFERENCES `job_seekers_bid` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `job_seekers_bid_attachments_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `job_seeker_attachments`
--
ALTER TABLE `job_seeker_attachments`
  ADD CONSTRAINT `job_seeker_attachments_ibfk_1` FOREIGN KEY (`job_seeker_id`) REFERENCES `job_seeker` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `job_seeker_attachments_ibfk_2` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `job_seeker_education`
--
ALTER TABLE `job_seeker_education`
  ADD CONSTRAINT `job_seeker_education_ibfk_1` FOREIGN KEY (`job_seeker_id`) REFERENCES `job_seeker` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `job_seeker_education_ibfk_2` FOREIGN KEY (`adj_params_master_id`) REFERENCES `adj_params_master` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `job_seeker_rating_comments`
--
ALTER TABLE `job_seeker_rating_comments`
  ADD CONSTRAINT `job_seeker_rating_comments_ibfk_1` FOREIGN KEY (`job_seeker_id`) REFERENCES `job_seeker` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `job_seeker_rating_comments_ibfk_2` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `job_seeker_sector`
--
ALTER TABLE `job_seeker_sector`
  ADD CONSTRAINT `job_seeker_sector_ibfk_1` FOREIGN KEY (`job_seeker_id`) REFERENCES `job_seeker` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `job_seeker_sector_ibfk_2` FOREIGN KEY (`adj_params_master_id`) REFERENCES `adj_params_master` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `job_seeker_specialisation`
--
ALTER TABLE `job_seeker_specialisation`
  ADD CONSTRAINT `job_seeker_specialisation_ibfk_1` FOREIGN KEY (`job_seeker_id`) REFERENCES `job_seeker` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `job_seeker_specialisation_ibfk_2` FOREIGN KEY (`adj_params_master_id`) REFERENCES `adj_params_master` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `payments`
--
ALTER TABLE `payments`
  ADD CONSTRAINT `payments_ibfk_1` FOREIGN KEY (`subscription_id`) REFERENCES `subscription` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `payments_ibfk_2` FOREIGN KEY (`job_seeker_id`) REFERENCES `job_seeker` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
