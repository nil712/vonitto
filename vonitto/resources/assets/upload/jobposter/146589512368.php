<?php

namespace App\Helpers;
use App\AES128;

class CommonHelper{

  public function AesDecrypt($array)
  {

    $inputKey = AESKey;
    $blockSize = EncryptionBlockSize;

      if (is_array($array)) {
          foreach ($array as $key => $value) {
              if (is_array($value)) {
                  $array[$key] = $this->AesDecrypt($value);
              }else{
                  $aes = new AES128();
                  $array[$key] = $aes->decrypt($value);
              }
              if ($value instanceof stdClass) {
                  $array[$key] = $this->AesDecrypt((array)$value);
              }
          }
      }
      if ($array instanceof stdClass) {
          return $this->AesDecrypt((array)$array);
      }
      return $array;
  }

    public function AesEncrypt($array)
    {

      $inputKey = AESKey;
      $blockSize = EncryptionBlockSize;

        if (is_array($array)) {
            foreach ($array as $key => $value) {
                if (is_array($value)) {
                    $array[$key] = $this->AesEncrypt($value);
                }else{
                    $aes = new AES128();
                    $array[$key] = $aes->encrypt($value);
                }
                if ($value instanceof stdClass) {
                    $array[$key] = $this->AesEncrypt((array)$value);
                }
            }
        }
        if ($array instanceof stdClass) {
            return $this->AesEncrypt((array)$array);
        }
        return $array;
    }



    public function AesEncryptNew($array)
    {

      $inputKey = AESKey;
      $blockSize = EncryptionBlockSize;

        if (is_array($array)) {
            foreach ($array as $key => $value) {
                if (is_array($value)) {
                    $array[$key] = $this->AesEncryptNew($value);
                }else{
                  if (is_object($value)) {
                    //print_r($value);
                    $array[$key] = $this->AesEncryptNew((array)$value);
                  } else {
                    //$array[$key] = $value;
                    $aes = new AES128();
                    $array[$key] = $aes->encrypt($value);
                  }
                    //$aes = new AES128();
                    //$array[$key] = $aes->encrypt($value);

                }
            }
        }
        return $array;
    }



}
