-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 27, 2016 at 05:51 PM
-- Server version: 5.5.49-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `museum`
--

-- --------------------------------------------------------

--
-- Table structure for table `beacon`
--

CREATE TABLE IF NOT EXISTS `beacon` (
  `id` bigint(10) NOT NULL AUTO_INCREMENT,
  `udid` varchar(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `major` varchar(11) NOT NULL,
  `minor` varchar(11) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` enum('1','0') NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `beacon`
--

INSERT INTO `beacon` (`id`, `udid`, `name`, `major`, `minor`, `updated_at`, `created_at`, `status`) VALUES
(1, 'B9407F30-F5F8-466E-AFF9-25556B57FE6D', 'Ice', '59180', '56681', '0000-00-00 00:00:00', '2016-05-27 08:45:41', '1'),
(2, 'B9407F30-F5F8-466E-AFF9-25556B57FE6D', 'MINT', '17797', '13316', '0000-00-00 00:00:00', '2016-05-27 08:46:55', '1'),
(3, 'B9407F30-F5F8-466E-AFF9-25556B57FE6D', 'Cafe Mint', '45925', '33015', '0000-00-00 00:00:00', '2016-05-27 08:47:44', '1');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
