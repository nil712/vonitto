/*
* Developer Name : Hitesh Tank
* Date : 28,July 2016
* Description : Update Post with validation action
*/





//Add Review & Rating
$(document).on("click","#editPostBtn",function(event){
  var $submitButton=$(this);
  if($("#formJobPost").length){
    jQuery.validator.addMethod("budget",function(value, element) {
      var isValidMoney = /^\d{0,15}(\.\d{0,2})?$/.test(value);
      return this.optional(element) || isValidMoney;
      },
      "Please enter proper budget"
      );
      $("#formJobPost").validate({
              focusInvalid: false, // do not focus the last invalid input
              errorElement: 'p',
              errorClass: 'error_message', // default input error message class
              invalidHandler: function(form, validator) {
                  var errors = validator.numberOfInvalids();
                if (errors) {
                   validator.errorList[0].element.focus();
                   $('html, body').animate({
                   scrollTop: $(validator.errorList[0].element).offset().top-150
                }, "fast");
                  }
               } ,
              ignore: [],
                  rules: {
                    "title":{
                      required:true
                    },
                    "description":{
                      required:true
                    },
                    "category_id":{
                      required:true
                    },
                    "how_long":{
                      required:true
                    },
                    "budget":{
                      budget:true,
                      required:true,
                    }
                  },
                  messages: {
                    "budget":{
                        required:"Budget should not be empty",
                    },
                    "title":{
                      required:"Title should not be empty"
                    },
                    "description":{
                      required:"Description should not be empty"
                    },
                    "category_id":{
                      required:"Please choose category"
                    },
                    "how_long":{
                      required:"Please choose how long"
                    }
                  },
                  errorPlacement: function(error, element) {
                      if(element.attr("name") == "txtBoardContent"){
                          error.insertAfter("#cke_editor1");
                      }else if(element.attr("name") == "content_du"){
                          error.insertAfter("#cke_content_du");
                      }else{
                          error.insertAfter(element);
                      }
                  },
                  highlight: function (element) { // hightlight error inputs
                      $(element)
                          .closest('.form-group').addClass('has-error'); // set error class to the control group
                  },
                  success: function (label) {
                      label.closest('.form-group').removeClass('has-error');
                      label.remove();
                  },
                  submitHandler: function(form) {
                      var formData = new FormData($("#formJobPost")[0]);
                      var dataString = formData;

                      $.ajax({
                          type: "POST",
                          cache:false,
                          contentType: false,
                          processData: false,
                          headers: {'X-CSRF-TOKEN': $("#token").val()},
                          url:$("#formJobPost").attr("action"),
                          data:dataString,
                          beforeSend:function(){
                           $submitButton.attr('disabled',true);
                           $submitButton.val('Processing');
                          },
                          success: function (response){
                             $submitButton.val('Update');
                             $submitButton.removeAttr('disabled');
                            $("html, body").animate({ scrollTop: 0 }, "slow");
                            if(response.status == 1){
                              $(".messageBox").show();
                              $(".messageBox").html('<div class="alert alert-success" id="successMessageBox"><button type="button" class="close" data-dismiss="alert">×</button>'+response.message+'</div></div>');
                            }else{
                              $(".messageBox").show();
                              $(".messageBox").html('<div class="alert alert-error" id="successMessageBox"><button type="button" class="close" data-dismiss="alert">×</button>'+response.message+'</div></div>');
                            }
                          }
                      });

                  }
          });
      }
});
