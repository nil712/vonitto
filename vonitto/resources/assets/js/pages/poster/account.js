/*
* Developer Name : Hitesh Tank
* Date : 28,July 2016
* Description : review & rating purpose
*/



$(document).on("click","#ajax-complete .reviewRating",function(){
  console.log($(this).attr("data-review"));
  var $rateYo;
  var $model=$("#reviewModel").modal("show");
  if($(this).attr("data-rating") != '' || $(this).attr("data-rating") != 0){
    var rating=$(this).attr("data-rating");
    $rateYo=$("#rateYo").rateYo({halfStar:true,ratedFill: "#222638"});
    $("#rateYo").rateYo("option", "readOnly", true);
    $("#rateYo").rateYo("option", "rating",rating);
    $model.find("#submitReview").hide();
    $model.find("#review").text($(this).attr("data-review"));
    $model.find("#review").attr('disabled','disabled');
  }else{

    $model.find("#frmReviewRating")[0].reset();
    $rateYo=$("#rateYo").rateYo({rating: 0,halfStar:true,ratedFill: "#222638"})
    .on("rateyo.change", function (e, data) {
      var rating = data.rating;
      $("#rating").val(rating);
    });
    $("#rateYo").rateYo("option", "readOnly", false);
    $("#rateYo").rateYo("option", "rating", 0);
    $model.find("#seeker_id").val($(this).data('seeker'));
    $model.find("#job_id").val($(this).data('job'));
    $model.find("#rating").removeAttr('disabled','disabled');
    $model.find("#review").removeAttr('disabled','disabled');
    $model.find("#submitReview").show();
  }
});

//Add Review & Rating
$(document).on("click",".submitReview",function(event){
  $submitButton=$(this);
  if($("#reviewModel .frmReviewRating").length){

      $("#reviewModel .frmReviewRating").validate({
              focusInvalid: false, // do not focus the last invalid input
              errorElement: 'span',
              errorClass: 'error_message', // default input error message class
              invalidHandler: function(form, validator) {
                  var errors = validator.numberOfInvalids();
                if (errors) {
                   validator.errorList[0].element.focus();
                   $('html, body').animate({
                   scrollTop: $(validator.errorList[0].element).offset().top-150
                }, "fast");
                  }
               } ,
              ignore: [],
                  rules: {
                      "review":{
                        minlength:20,
                        maxlength:200,
                      },
                  },
                  messages: {
                    "review":{
                      minlength:"Review must have more than {0} characters",
                      maxlength:"Review must have less than {0} characters",
                    }
                  },
                  errorPlacement: function(error, element) {
                      if(element.attr("name") == "txtBoardContent"){
                          error.insertAfter("#cke_editor1");
                      }else if(element.attr("name") == "content_du"){
                          error.insertAfter("#cke_content_du");
                      }else{
                          error.insertAfter(element);
                      }
                  },
                  highlight: function (element) { // hightlight error inputs
                      $(element)
                          .closest('.form-group').addClass('has-error'); // set error class to the control group
                  },
                  success: function (label) {
                      label.closest('.form-group').removeClass('has-error');
                      label.remove();
                  },
                  submitHandler: function(form) {
                      var dataString=$("#reviewModel .frmReviewRating").serialize();
                      $.ajax({
                          type: "POST",
                          cache:false,
                          headers: {'X-CSRF-TOKEN': $("#token").val()},
                          url:BASE_URL+'jobposter/job-seeker/review',
                          data:dataString,
                          beforeSend:function(){
                            $submitButton.addClass('disabled');
                            $submitButton.text('Processing');
                          },
                          success: function (response){
                              $submitButton.text('Submit');
                              $submitButton.removeClass('disabled');
                            if(response.status == 1){
                              $(".messageBox").show();
                              $(".messageBox").html('<div class="alert alert-success" id="successMessageBox"><button type="button" class="close" data-dismiss="alert">×</button>'+response.message+'</div></div>');
                              setTimeout(function(){$('#reviewModel').modal("hide");location.reload(true);}, 3000);

                            }else{
                              $(".messageBox").show();
                              $(".messageBox").html('<div class="alert alert-error" id="successMessageBox"><button type="button" class="close" data-dismiss="alert">×</button>'+response.message+'</div></div>');
                            }
                          }
                      });

                  }
          });
      }
});
