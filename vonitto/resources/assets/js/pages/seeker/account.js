/*
* Developer Name : Hitesh Tank
* Date : 28,July 2016
* Description : action implement on in-progress,compelet Job
*/

$(document).on("change",".ajax-awarded .job-status",function(event){
  var $currentObj=$(this);
  if($.trim($(this).val()).toLowerCase() !== 'assigned'){
        var $action="false";
        var option=false;
        if($.trim($(this).val()).toLowerCase() == 'in-progress'){
              option=confirm('Are you sure to change of status to In-Progress');
        }else{
              option=confirm('Your Job is Completed');
          var $action="true";
        }

        if(option){
            var job_id=$(this).find(':selected').attr('data-project');
            var poster_id=$(this).find(':selected').attr('data-posterid');
            var dataString={"job_id":job_id,"poster_id":poster_id,'status':$(this).val()};
            $.ajax({
              type: "POST",
              headers: {'X-CSRF-TOKEN': $("#token").val()},
              url: BASE_URL+"jobseeker/myaccount/job-status-update",
              cache: false,
              data:JSON.stringify({ data: dataString }),
              beforeSend: function () {

              },
             success: function(data) {
               if(data.status == 1){
                 if(data.updateto =='complete'){
                    $currentObj.closest('tr').slideUp('100');
                    setTimeout(function(){location.reload();},2000);
                 }
                 $(".messageBox").show();
                 $(".messageBox").html('<div class="alert alert-success" id="successMessageBox"><button type="button" class="close" data-dismiss="alert">×</button>'+data.message+'</div></div>')
               }else{
                 $(".messageBox").show();
                 $(".messageBox").html('<div class="alert alert-error" id="successMessageBox"><button type="button" class="close" data-dismiss="alert">×</button>'+data.message+'</div></div>')
               }
             }
           });
        }

  }
});
