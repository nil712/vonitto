$(document).ready(function(){

	$("#agree").on("click",function(){

		if (!$(this).is(':checked')) {
        	$("#register").attr('disabled','disabled');
    }else{
				  $("#register").removeAttr('disabled');
		}

	});

	//open and close mobile menu//
	$( ".js-primary-menu-toggle" ).on( "click", function() {
		$(".site-primary-navigation ").addClass("site-primary-navigation--open");
		$(".js-primary-menu-toggle").addClass("primary-menu-toggle--opened");
	});

	$( ".primary-menu-toggle--close" ).on( "click", function() {
		$(".site-primary-navigation ").removeClass("site-primary-navigation--open");
		$(".js-primary-menu-toggle").removeClass("primary-menu-toggle--opened");
	});

})
