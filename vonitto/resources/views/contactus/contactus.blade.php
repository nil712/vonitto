<!DOCTYPE html>
<html lang="en-US">
    <head>
        <!-- include header files  -->
        @include('layouts.header')
        <link rel="stylesheet" media="all" type="text/css" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
        <link rel="stylesheet" media="all" type="text/css" href="{{URL::asset('resources/assets/css/jquery-ui-timepicker-addon.css')}}" />
    </head>
    <body class="fixed-header jobpost">
        <div id="page" class="hfeed site inner_padding">
            <header id="masthead" class="site-header" role="banner">
                @include('layouts.header_menu')
            </header><!-- #masthead -->
            <div id="main" class="site-main">
                <header class="page-header">
                    <h2 class="page-title">Contact Us</h2>
                </header>
                
                <div class="messageBox" style="display:none;"></div>
                <div id="primary" class="content-area container" role="main">
                    <article id="post-1673" class="post-1673 page type-page status-publish hentry">
                        <div class="entry-content">
                            <div class="registration-form woocommerce register_dv loginsignupmain jobpostlogin">
                                @if(session('status') && session('status') == 'Success')
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">×</button>
                                        {{ session('message') }}
                                    </div>
                                @endif
                                <form  class="register" id="formJobPost" enctype="multipart/form-data" role="form" method="POST" action="{{url('contact_us')}}" >
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
                                    <div class="form-row form-row-wide">
                                        <input type="text" class="input-text" name="name" id="name" value="{{ old('name') }}"  placeholder="NAME"/>
                                        <div class="clearfix"></div>
                                        @if ($errors->has('name'))<p class="error_message">{!!$errors->first('name')!!}</p>@endif
                                    </div>
                                    <div class="form-row form-row-wide">
                                        <input type="text" class="input-text" name="phone" id="name" value="{{ old('phone') }}"  placeholder="PHONE"/>
                                        <div class="clearfix"></div>
                                        @if ($errors->has('phone'))<p class="error_message">{!!$errors->first('phone')!!}</p>@endif
                                    </div>
                                    <div class="form-row form-row-wide">
                                        <input type="text" class="input-text" name="email" id="email" value="{{ old('email') }}"  placeholder="EMAIL"/>
                                        <div class="clearfix"></div>
                                        @if ($errors->has('email'))<p class="error_message">{!!$errors->first('email')!!}</p>@endif
                                    </div>
                                    <div class="form-row form-row-wide">
                                        <textarea style="resize:none;border-radius:0px;" rows="5"  class="" name="description" id="description" value="{{ old('description') }}"  placeholder="MESSAGE"></textarea>
                                        <div class="clearfix"></div>
                                        @if ($errors->has('description'))<p class="error_message">{!!$errors->first('description')!!}</p>@endif
                                    </div>
                                    <div class="form-row" >
                                        <input type="hidden" name="file_names" id="file_names" class="file_names" />
                                        <input type="submit" class="button" id="submitData" name="register" value="Submit" />
                                    </div>
                                </form>
                            </div>
                        </div>
                    </article><!-- #post -->
                </div><!-- #primary -->
            </div><!-- #main -->
            <div class="footer-cta">
                <div class="container">
                    <h2>Got a question?</h2>
                    <p>We're here to help. Check out our FAQs, send us an email or call us at 1 (800) 555-5555</p>
                    <p><a href="#" class="button button--type-action">Contact Us</a></p>
                </div>
            </div>
            <footer id="colophon" class="site-footer" role="contentinfo">
                @include('layouts.footer')
            </footer><!-- #colophon -->
        </div><!-- #page -->
    </body>


<!--<script type='text/javascript' src="{{URL::asset('resources/assets/js/vonitto.minfcbd.js')}}"></script>-->
    <script type='text/javascript' src="{{URL::asset('resources/assets/js/jquery.validate.js')}}"></script>
    <script type="text/javascript" src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
    <script type="text/javascript" src="http://code.jquery.com/ui/1.10.3/jquery-ui.min.js"></script>
    <script src="{{URL::asset('resources/assets/js/jQuery-validation-plugin/jquery.validate.js')}}"></script>
</html>
