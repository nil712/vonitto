<!DOCTYPE html>
<html lang="en-US">
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
   <head>
      <!-- include header files  -->
      @include('layouts.header')
   </head>
   <body class="fixed-header">
   <div id="page" class="hfeed site inner_padding">
     <header id="masthead" class="site-header" role="banner">
       @include('layouts.header_menu')
     </header>
     <!-- #masthead -->

     <div id="main" class="site-main">
       <div id="primary" role="main">
         <section id="vonitto_widget_feature_callout-3" class="widget widget--home widget--home--no-margin jobify_widget_feature_callout widget--home-feature-callout">
           <div class="feature-callout text-center image-cover" style="background-color: #ffffff;">

             <div class="feature-callout-cover feature-callout-cover--overlay-none" style="background-image:url(../vonitto/resources/assets/images/home-header-people-hero.jpg); ?>; background-position: center center">
               <div class="container">
                 <div class="row">
                   <div class="col-xs-12  ">
                     <div class="callout-feature-content" style="color:#ffffff">
                       <h2 class="callout-feature-title" style="color:#ffffff">Project Pitch</h2>
                       <p>Your platform to seeking a local architect or designer for your next project.</p>
                       <p><a href="" class="button button--type-action">get started now</a></p>
                     </div>
                   </div>
                 </div>
               </div>
             </div>
           </div>
         </section>
         <section id="vonitto_widget_jobs_spotlight-2" class="widget widget--home vonitto_widget_jobs_spotlight widget--home-job-spotlights">
           <div class="container">
             <h3 class="widget-title widget-title--home">Featured Jobs</h3>
             <div class="row job-spotlights" data-columns>

               @if(isset($jobPostFeatured) && !empty($jobPostFeatured) && count($jobPostFeatured)>0)
                  @foreach($jobPostFeatured as $featured)
                    <div class="job-spotlight">
                      <div class="job-spotlight__featured-image">
                        {{--*/$imageUrl="";/*--}}
                        @if(isset($featured->job_logo) && !empty($featured->job_logo))
                            {{--*/ $header=URL::asset('resources/assets/upload/joblogo/'.$featured->job_logo); /*--}}
                             {{--*/ $file_headers = @get_headers($header); /*--}}
                            @if($file_headers[0] == 'HTTP/1.0 404 Not Found')
                              {{--*/ $imageUrl=URL::asset('resources/assets/images/airbnb-spotlight.jpg'); /*--}}
                            @else
                              {{--*/ $imageUrl=$header; /*--}}
                            @endif
                        @else
                            {{--*/ $imageUrl=URL::asset('resources/assets/images/airbnb-spotlight.jpg'); /*--}}
                        @endif
                      <img width="720" height="350" src="{{$imageUrl}}" class="attachment-content-job-featured size-content-job-featured" alt="" />  </div>
                      <div class="job-spotlight__content">
                        <p><a href="javascript:;" rel="bookmark" class="job-spotlight__title">{{stripslashes($featured->title)}}</a></p>
                        <div class="job-spotlight__actions"> <span class="job_listing-location">@if(isset($featured->location_country) && !empty($featured->location_country)) {{$featured->location_country}} @else - @endif</span></div>
                        <p>@if(isset($featured->description) && !empty($featured->description))
                                @if(strlen($featured->description)>150)
                                      {{substr(stripslashes($featured->description),0,145).'...'}}
                                @else
                                {{stripslashes($featured->description)}}
                                @endif
                          @else - @endif</p>
                      </div>
                    </div>
                  @endforeach
              @else
                <h3>There is no feature job</h3>
               @endif
             </div>
           </div>
         </section>


         <section id="vonitto_widget_jobs_spotlight-2" class="widget widget--home vonitto_widget_jobs_spotlight widget--home-job-spotlights">
           <div class="container">
             <h3 class="widget-title widget-title--home">Featured Architects and Designers</h3>
             <div class="row job-spotlights" data-columns>
               <div class="job-spotlight">
                 <div class="job-spotlight__featured-image">
                 <img width="720" height="350" src="{{URL::asset('resources/assets/images/airbnb-spotlight.jpg')}}" class="attachment-content-job-featured size-content-job-featured" alt="" />  </div>
                 <div class="job-spotlight__content">
                   <p><a href="job/senior-ux-designer/index.html" rel="bookmark" class="job-spotlight__title">Senior UX Designer</a></p>
                   <div class="job-spotlight__actions"> <span class="job_listing-location">New York</span> <span class="job-type full-time">Full Time</span> </div>
                   <p>Company Description Company is a 2016 Iowa City-born start-up that develops consectetuer adipiscing elit. Phasellus hendrerit. Pellentesque aliquet nibh nec</p>
                 </div>
               </div>
               <div class="job-spotlight">
                 <div class="job-spotlight__featured-image"> <img width="720" height="350" src="{{URL::asset('resources/assets/images/airbnb-spotlight.jpg')}}" class="attachment-content-job-featured size-content-job-featured" alt="" /> </div>
                 <div class="job-spotlight__content">
                   <p><a href="job/art-director/index.html" rel="bookmark" class="job-spotlight__title">Art Director</a></p>
                   <div class="job-spotlight__actions"> <span class="job_listing-location">Toronto</span> <span class="job-type part-time">Part Time</span> </div>
                   <p>Company Description Company is a 2016 Iowa City-born start-up that develops consectetuer adipiscing elit. Phasellus hendrerit. Pellentesque aliquet nibh nec</p>
                 </div>
               </div>
               <div class="job-spotlight">
                 <div class="job-spotlight__featured-image"> <img width="720" height="350" src="{{URL::asset('resources/assets/images/airbnb-spotlight.jpg')}}" class="attachment-content-job-featured size-content-job-featured" alt="" /></div>
                 <div class="job-spotlight__content">
                   <p><a href="job/surround/index.html" rel="bookmark" class="job-spotlight__title">Web Designer</a></p>
                   <div class="job-spotlight__actions"> <span class="job_listing-location">New York</span> <span class="job-type full-time">Full Time</span> </div>
                   <p>Company Description Company is a 2016 Iowa City-born start-up that develops consectetuer adipiscing elit. Phasellus hendrerit. Pellentesque aliquet nibh nec</p>
                 </div>
               </div>
             </div>
           </div>
         </section>

        <!--how-it-works-->
        <section id="vonitto_widget_jobs_spotlight-3" class="widget howitworks widget--home vonitto_widget_jobs_spotlight widget--home-job-spotlights">
           <div class="container">
             <h3 class="widget-title widget-title--home">How it works - Job Poster</h3>
                <div class="col-lg-4 col-md-4 col-sm-4">
                    <img src="{{URL::asset('resources/assets/images/howit-1.png')}}" alt="" />
                    <div class="clearfix"></div>
                    <span>Post a job now</span>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4">
                    <img src="{{URL::asset('resources/assets/images/howit-2.png')}}" alt="" />
                    <div class="clearfix"></div>
                    <span>Receive pitches from architects or designers</span>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4">
                    <img src="{{URL::asset('resources/assets/images/howit-3.png')}}" alt="" />
                    <div class="clearfix"></div>
                    <span>Award the job and make direct contact with an architect or a designer who satisfy your requirements </span>
                </div>
           </div>
         </section>
        <section id="vonitto_widget_jobs_spotlight-5" class="widget howitworks widget--home vonitto_widget_jobs_spotlight widget--home-job-spotlights">
           <div class="container">
             <h3 class="widget-title widget-title--home">How it works - Job Seeker</h3>
                <div class="col-lg-3 col-md-3 col-sm-6">
                    <img src="{{URL::asset('resources/assets/images/howit-1.png')}}" alt="" />
                    <div class="clearfix"></div>
                    <span>Browse jobs available</span>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6">
                    <img src="{{URL::asset('resources/assets/images/howit-4.png')}}" alt="" />
                    <div class="clearfix"></div>
                    <span>Pitch for a job matching your skills</span>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6">
                    <img src="{{URL::asset('resources/assets/images/howit-5.png')}}" alt="" />
                    <div class="clearfix"></div>
                    <span>Interested in more work? pay a <a href="#">small fee</a> to widen your potential search for a new project. </span>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6">
                    <img src="{{URL::asset('resources/assets/images/howit-6.png')}}" alt="" />
                    <div class="clearfix"></div>
                    <span>Get Hired </span>
                </div>
           </div>
         </section>
         <section id="vonitto_widget_jobs_spotlight-6" class="widget howitworks widget--home vonitto_widget_jobs_spotlight widget--home-job-spotlights">
           <div class="container">
             <h3 class="widget-title widget-title--home">Find the membership that works for you</h3>
             <h4 class="howtxt">Free for one pitch a month</h4>
                <div class="col-lg-4 col-md-4 col-sm-6">
                	<!--whitebox-->
                    <div class="whitebox">
                    	<div class="cntent">
                        	<h2>GBP 10 Membership Fee/month for a total of 5 pitches</h2>
                        </div>
                    </div>
                    <!--whitebox-->
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6">
                	<!--whitebox-->
                    <div class="whitebox">
                    	<div class="cntent">
                        	<h2>GBP 15 Membership Fee/month for a total of 8 pitches</h2>
                        </div>
                    </div>
                    <!--whitebox-->
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6">
                	<!--whitebox-->
                    <div class="whitebox">
                    	<div class="cntent">
                        	<h2>GBP 30 Membership Fee/month for a total of 24 pitches</h2>
                        </div>
                    </div>
                    <!--whitebox-->
                </div>
                <p class="para">For more pitches per month or for Company Rates, please contact us directly at <a href="mailto:info@pitchyourconcepts.com"> info@pitchyourconcepts.com </a> </p>
                <div class="clearfix"></div>
                <a href="{{url('/register')}}" class="register">Register Now</a>
                <div class="clearfix"></div>
                <div id="map_canvas" style="clear: both;display: block;height: 450px;width: 100%;"></iframe>
           </div>
         </section>
		<!--how-it-works-->



         <section id="vonitto_widget_stats-3" class="widget widget--home vonitto_widget_stats widget--home-stats">
           <div class="container">
             <h3 class="widget-title widget-title--home">Pitch Your Concepts Site Stats</h3>
             <p class="widget-description widget-description--home">Here we list our site stats and how many people we've helped find a job and companies have
               found recruits</p>
             <ul class="job-stats row showing-4">
               <li class="job-stat col-md-3 col-sm-6 col-xs-12"> <strong>22</strong> Jobs Posted </li>
               <li class="job-stat col-md-3 col-sm-6 col-xs-12"> <strong>0</strong> Jobs Filled </li>
               <li class="job-stat col-md-3 col-sm-6 col-xs-12"> <strong>22</strong> Companies </li>
               <li class="job-stat col-md-3 col-sm-6 col-xs-12"> <strong>92</strong> Members </li>
             </ul>
           </div>
         </section>
         <section id="vonitto_widget_feature_callout-12" class="widget widget--home widget--home--no-margin vonitto_widget_feature_callout widget--home-feature-callout">
           <div class="feature-callout text-center image-cover" style="background-color: #ffffff;">

             <div class="feature-callout-cover feature-callout-cover--overlay-full" style="background-image:url(../vonitto/resources/assets/images/hero-home-header.jpg); ?>; background-position: center center">
               <div class="container">
                 <div class="row">
                   <div class="col-xs-12  ">
                     <div class="callout-feature-content" style="color:#ffffff">
                       <h2 class="callout-feature-title" style="color:#ffffff">Got a question?</h2>
                       <p>We&#8217;re here to help. Check out our FAQs, send us an email or call us at <span>1 (800) 555-5555</span></p>
                       <p><a href="{{url('/contact_us')}}" class="button button--type-action">Contact Us</a></p>
                     </div>
                   </div>
                 </div>
               </div>
             </div>
           </div>
         </section>
       </div>
       <!-- #primary -->

     </div>
     <!-- #main -->
      <!-- main content -->
      @yield('content')
      <footer class="footer_Sec" >
        @include('layouts.footer')
      </footer>
      <script type="text/javascript" src="{{URL::asset('resources/assets/js/googlemaps.js')}}" ></script>

      <script>
        /* code for showing map on home page*/

        $(document).ready(function () {
          var map;
          var elevator;
          var myOptions = {
              zoom: 1,
              center: new google.maps.LatLng(0, 0),
              mapTypeId: 'terrain'
          };
          map = new google.maps.Map($('#map_canvas')[0], myOptions);
          var addresses =[
            <?php
            foreach($mapJobs as $job){
              $city = $job['location_city'];
              $country = $job['location_country'];
              echo "'".$city.",".$country."',";
            }
            ?>
          ];

          var titles =[
            <?php
            foreach($mapJobs as $job){
              $city = $job['location_city'];
              $country = $job['location_country'];
              echo "'".$city.",".$country."',";
            }
            ?>
          ];


          for (var x = 0; x < addresses.length; x++) {
              $.getJSON('http://maps.googleapis.com/maps/api/geocode/json?address='+addresses[x]+'&sensor=false', null, function (data) {
                  var p = data.results[0].geometry.location;

                  if(p.lat != "" && p.lng != ""){

                  var latlng = new google.maps.LatLng(p.lat, p.lng);
                  var marker = new google.maps.Marker({
                      position: latlng,
                      map: map
                  });

                  var infowindow = new google.maps.InfoWindow(
                      { content: '<b>'+data.results[0].formatted_address+'</b>',
                        size: new google.maps.Size(150,50)
                      });
                      google.maps.event.addListener(marker, 'click', function() {
                          infowindow.open(map,marker);
                      });
                  }
              });
          }

        });


      </script>

</body>
</html>
