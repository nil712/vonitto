<!DOCTYPE html>
<html>
    <head>
        <title>Be right back.</title>
	<link rel="icon" type="icon/ico" href="{{URL::asset('resources/assets/images/favicon.ico')}}" />
        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
	<link href="{{ URL::asset('resources/assets/css/app.css') }}" rel="stylesheet">
	<link  href="{{URL::asset('resources/assets/css/style.css')}}" />
        <style>
            .four_class{
		  width: 100%;
  		  text-align: center;
	  	  padding-top: 56px;
	  	  font-size: 119px;
		  color: gray;
	    }
            .title {
                  font-size: 72px;
		  margin-bottom: 40px;
		  width: 100%;
		  color: lightcoral;
		  text-align: center;
            }
	    .navnew {
		  background: #283A54;
		  padding: 10px 0;
		  border: none;
		  border-radius: 0;
	     }
	     .back_button{
	          width: 50%;
		  text-align: center;
		  height: 35px;
		  background-color: #283A54;
		  font-size: 18px;
		  float: none;
		  color: aliceblue;
		  display: inline-block;
	     }
	     button{
            	  background-color: #283A54;
  		  border: none;
		  padding-top: 6px;
		  width: 100%;
	     }
	     .content{
		  text-align: center;
	     }
        </style>
    </head>
    <body>
	<nav class="navnew">
	<div class="container-fluid">
		<div class="row">
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav">
				    <a class="logo logonew" title="Vacation Rental" href="{{ url('/') }}">
						<img height="56" alt="Vacation Rental" src="{{URL::asset('resources/assets/images/logo.png')}}">
					</a>
				</ul>
			</div>
		</div>
	</div>
	</nav>
	<div class="container">
		 <div class="content">
			<div class="four_class">404</div>
		        <div class="title">Page Not Found.</div>
			<div class="back_button"><a href="{{ url('/') }}"><button name="back">Back To Homepage</button></a></div>
		 </div>
	</div>

    </body>
</html>
