@extends('admin.layouts.AdminMaster')
@section('content')

<section class="content-header">
    <h1> Edit User Detail: {{$EditPageCmsData->username}} </h1>
    <ol class="breadcrumb">
        <li><a href="{{ url('admin/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{ url('admin/pagecms') }}"></i>CMS Pages</a></li>
        <li class="active">Edit CMS Pages: {{$EditPageCmsData->page_name}} </li>
    </ol>
</section>

<?php

    if(isset($EditPageCmsData->page_name) && !empty($EditPageCmsData->page_name)){
        $Pagename = $EditPageCmsData->page_name;
    }else{
        $Pagename = "";
    }
    if(isset($EditPageCmsData->page_text) && !empty($EditPageCmsData->page_text)){
        $Pagedescription = $EditPageCmsData->page_text;
    }else{
        $Pagedescription = "";
    }

?>

<section class="content">
    <div class="row">
        <div class="col-md-12 box2">
            <div id="messageAlert" style="display:none;"></div>
            <div class="box box-primary">
                <div id="ajax-loader" class="overlay none">
                    <i class="fa fa-refresh fa-spin"></i>
                </div>

                <form role="form" name="updateCMSPage" id="updateCMSPage" action="{{url('admin/pagecms',array($EditPageCmsData->id))}}" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="_method" value="PUT" />
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    <div class="box-body">

                        <div class="form-group">
                            <label for="title">Page Name</label>
                            <input type="text" name="page_name" id="page_name" value="{{$Pagename}}" class="form-control" maxlength="254">
                        </div>

                        <div class="form-group">
                            <label for="content">Page Description</label>
                            <textarea name="page_text" id="content" class="form-control" rows="3">{{$Pagedescription}}</textarea>
                        </div>

                        <div class="form-group">
                            <label for="title">Image</label>
                            <input type="file" id="image" name="image" placeholder="" value="{{ old('image') }}" accept="image/*">
                        </div>



                    <div class="box-footer">
                        <input type="hidden" value="{{$EditPageCmsData->id}}" name="id" />
                        <button id="updateCMSPageBtn" type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>


<script src="{{URL::asset('resources/assets/admin/plugins/jQuery-validation-plugin/jquery.validate.js')}}"></script>
<script src="{{URL::asset('resources/assets/admin/plugins/jQuery-validation-plugin/additional-methods.js')}}"></script>
<script>
$(document.body).on('click', '#updateCMSPageBtn', function() {
    $.validator.addMethod('filesize', function(value, element, param) {
        return this.optional(element) || (element.files[0].size <= param)
    }, "File must be JPG, JPEG or PNG, less than 2 MB.");

    if ($("#updateCMSPage").length) {
        // Email custom regex
        $("#updateCMSPage").validate({
            onkeyup: false,
            onfocusout: false,
            errorElement: 'span',
            errorClass: 'has-error',
            ignore: [],
            rules: {
                "page_name": {
                    required: true,
                }
            },
            messages: {
                "page_name": {
                    required: "Page Name should not be empty.",
                }
            },
            errorPlacement: function(error, element) {
                error.insertAfter(element);
            },
            submitHandler: function(form) {
                var formData = new FormData($("#updateCMSPage")[0]);
                var dataString = formData;
                $("#updateCMSPageBtn").attr("disabled", true);
                $.ajax({
                    type: "POST",
                    cache: false,
                    contentType: false,
                    processData: false,
                    url: $("#updateCMSPage").attr("action"),
                    data: dataString,
                    before: function() {
                        $("#ajax-loader").show();
                    },
                    success: function(response) {
                        $("#ajax-loader").hide();
                        $("#messageAlert").show();

                        if (response.status == 1) {
                            $("#messageAlert").html('<div class="alert alert-success alert-dismissible"><h4><i class="icon fa fa-check"></i> Success!</h4>' + response.message + '</div>');
                            $("html, body").animate({
                                scrollTop: 0
                            }, "slow");

                            window.setTimeout(function() {
                                location.href = "<?php echo url("/")?>/admin/pagecms"
                            }, 3000);
                            $("#updateCMSPageBtn").attr("disabled", false);
                        } else {
                            $("#messageAlert").html('<div class="alert alert-danger alert-dismissible"><h4><i class="icon fa fa-ban"></i> Error!</h4>' + response.error + '</div>');
                            $("html, body").animate({
                                scrollTop: 0
                            }, "slow");

                        }
                    }
                });
                //  return false;
            }
        });
    }
});
</script>

@stop
