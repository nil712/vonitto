@extends('admin.layouts.AdminMaster')
@section('content')
<link rel="stylesheet" media="all" type="text/css" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
<link rel="stylesheet" media="all" type="text/css" href="{{URL::asset('resources/assets/css/jquery-ui-timepicker-addon.css')}}" />
<section class="content-header">
    <h1> Add Job</h1>
    <ol class="breadcrumb">
        <li><a href="{{ url('admin/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{ url('admin/pagecms') }}"></i>Job</a></li>
        <li class="active">Add Job</li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-md-12 box2">
            <div id="messageAlert" style="display:none;"></div>
            <div class="box box-primary">
                <div id="ajax-loader" class="overlay none">
                    <i class="fa fa-refresh fa-spin"></i>
                </div>

                <form role="form" name="addJobForm" id="addJobForm" action="" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    <div class="box-body">

                        <div class="form-group">
                            <label for="title">Job Title</label>
                            <input type="text" name="title" id="title" value="{{ old('title') }}" class="form-control" maxlength="254">
                        </div>

                        <div class="form-group">
                            <label for="content">Job Description</label>
                            <textarea name="description" class="form-control" rows="3"></textarea>
                        </div>

                        <div class="form-group">
                            <label for="content">Tell us what you want to get done</label>
                            <textarea name="what_you_want" class="form-control" rows="3"></textarea>
                        </div>

                        <div class="form-group">
                            <label for="Type">Category</label>
                            <select class="form-control state" name="category_id">
                                <?php for($i=0;$i<count($GetCategoryList);$i++) { ?>
                                    <option value=<?php echo  $GetCategoryList[$i]->id ?>>{{$GetCategoryList[$i]->name}}</option>
                                 <?php } ?>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="title">Project Deadline</label>
                            <input type="text" name="project_deadline" id="project_deadline" value="{{ old('project_deadline') }}" class="form-control" maxlength="254">
                        </div>

                        <div class="form-group">
                            <label for="title">Attach File</label>
                            <input type="file" id="file" name="file[]" placeholder="" value="" accept="image/*" multiple="multiple">
                        </div>

                        <div class="form-group">
                            <label for="title">Estimated Job Budget</label>
                            <input type="text" name="budget" id="budget" value="{{ old('budget') }}" class="form-control" maxlength="254">
                        </div>

                        <div class="form-group">
                            <label for="title">What skills should the job seeker have?</label>
                            <input type="text" name="skills" id="skills" value="{{ old('skills') }}" class="form-control" maxlength="254">
                        </div>

                        <div class="form-group">
                            <label for="Type">How long do you expect this job to last?</label>
                            <select class="form-control state" name="how_long" id="how_long">
                                <option value=""> Select duration </option>
                                <option value="More than 6 months"> More than 6 months </option>
                                <option value="3 to 6 months"> 3 to 6 months </option>
                                <option value="1 to 3 months"> 1 to 3 months </option>
                                <option value="Less than 1 month"> Less than 1 month </option>
                                <option value="Less than 1 week"> Less than 1 week </option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="Type">Country</label>
                            <select class="form-control state" name="location_country" id="location_country">
                              <option value=""> Select Country </option>
                              <option value="UK"> UK </option>
                              <option value="Malaysia"> Malaysia </option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="title">City</label>
                            <input type="text" name="location_city" id="location_city" value="{{ old('location_city') }}" class="form-control" maxlength="254">
                        </div>

                        <div class="box-body" style="display: block;">
                            <div class="row unitsContainer"></div>
                        </div>

                        <div class="box-footer">
                            <button id="addJobBtn" type="submit" class="btn btn-primary">Submit</button>
                        </div>
                </form>
            </div>
        </div>
    </div>
</section>

<script src="{{URL::asset('resources/assets/admin/plugins/jQuery-validation-plugin/jquery.validate.js')}}"></script>
<script src="{{URL::asset('resources/assets/admin/plugins/jQuery-validation-plugin/additional-methods.js')}}"></script>
<script>
$(document.body).on('click', '#addJobBtn', function() {


    if ($("#addJobForm").length) {
        // Email custom regex

        $.validator.addMethod('filesize', function(value, element, param) {
            return this.optional(element) || (element.files[0].size <= param)
        }, "File must be JPG, JPEG or PNG, less than 2 MB.");

        $("#addJobForm").validate({
            onkeyup: false,
            onfocusout: false,
            errorElement: 'span',
            errorClass: 'has-error',
            ignore: [],
            rules: {
                "title": {
                    required: true,
                },
                "description": {
                    required: true,
                },
                "category_id": {
                    required: true,
                },
                "how_long": {
                    required: true,
                }
            },
            messages: {
                "title": {
                  required: "Job Title should not be empty.",
                },
                "description": {
                    required: "Job Description should not be empty.",
                },
                "category_id": {
                    required: "Please select category name.",
                },
                "how_long": {
                    required: "Please select how long.",
                }

            },
            errorPlacement: function(error, element) {
                 error.insertAfter(element);
                },
            submitHandler: function(form) {
                var formData = new FormData($("#addJobForm")[0]);
                var dataString = formData;
                $("#addJobBtn").attr("disabled", true);
                $.ajax({
                    type: "POST",
                    cache: false,
                    contentType: false,
                    processData: false,
                    url: "<?php echo url("/")?>/admin/job",
                    data: dataString,
                    before: function() {
                        $("#ajax-loader").show();
                    },
                    success: function(response) {

                        $("#ajax-loader").hide();
                        $("#messageAlert").show();
                        if (response.status == 1) {
                            $('#addJobForm')[0].reset();
                            $("#messageAlert").html('<div class="alert alert-success alert-dismissible"><h4><i class="icon fa fa-check"></i> Success!</h4>' + response.message + '</div>');
                            $("html, body").animate({
                                scrollTop: 0
                            }, "slow");

                            window.setTimeout(function() {
                                location.href = "<?php echo url("/")?>/admin/job"
                            }, 3000);
                            $("#addJobBtn").attr("disabled", false);
                            return false;
                        } else {
                            $("#messageAlert").html('<div class="alert alert-danger alert-dismissible"><h4><i class="icon fa fa-ban"></i> Error!</h4>' + response.error + '</div>');
                            $("html, body").animate({
                                scrollTop: 0
                            }, "slow");
                        }
                    }
                });
                //  return false;
            }
        });
    }
});
</script>
<script type="text/javascript" src="http://code.jquery.com/ui/1.10.3/jquery-ui.min.js"></script>
<script type="text/javascript" src="{{URL::asset('resources/assets/js/jquery-ui-sliderAccess.js')}}"></script>
<script type='text/javascript' src="{{URL::asset('resources/assets/js/jquery-ui-timepicker-addon.js')}}"></script>
<script type="text/javascript">
$(function(){
  $('#project_deadline').bind("cut copy paste",function(e) {
          e.preventDefault();
  });
  $('#project_deadline').datetimepicker({
		minDate: 0,
	});
});
</script>
@stop
