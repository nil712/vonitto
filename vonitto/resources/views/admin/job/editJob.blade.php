@extends('admin.layouts.AdminMaster')
@section('content')
<link rel="stylesheet" media="all" type="text/css" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
<link rel="stylesheet" media="all" type="text/css" href="{{URL::asset('resources/assets/css/jquery-ui-timepicker-addon.css')}}" />
<section class="content-header">
    <h1> Edit Job: {{$EditJobData->title}} </h1>
    <ol class="breadcrumb">
        <li><a href="{{ url('admin/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{ url('admin/job') }}"></i>Job</a></li>
        <li class="active">Edit Job: {{$EditJobData->title}} </li>
    </ol>
</section>

<?php

    if(isset($EditJobData->title) && !empty($EditJobData->title)){
        $Jobtitle = $EditJobData->title;
    }else{
        $Jobtitle = "";
    }
    if(isset($EditJobData->description) && !empty($EditJobData->description)){
        $Jobdescription = $EditJobData->description;
    }else{
        $Jobdescription = "";
    }
    if(isset($EditJobData->what_you_want) && !empty($EditJobData->what_you_want)){
        $Jobwhatyouwant = $EditJobData->what_you_want;
    }else{
        $Jobwhatyouwant = "";
    }
    if(isset($EditJobData->project_deadline) && !empty($EditJobData->project_deadline)){
        $Projectdeadline = $EditJobData->project_deadline;
    }else{
        $Projectdeadline = "";
    }
    if(isset($EditJobData->skills) && !empty($EditJobData->skills)){
        $Skills = $EditJobData->skills;
    }else{
        $Skills = "";
    }
    if(isset($EditJobData->budget) && !empty($EditJobData->budget)){
        $Budget = $EditJobData->budget;
    }else{
        $Budget = "";
    }
    if(isset($EditJobData->how_long) && !empty($EditJobData->how_long)){
        $Howlong = $EditJobData->how_long;
    }else{
        $Howlong = "";
    }
    if(isset($EditJobData->location_country) && !empty($EditJobData->location_country)){
        $Locationcountry = $EditJobData->location_country;
    }else{
        $Locationcountry = "";
    }
    if(isset($EditJobData->location_city) && !empty($EditJobData->location_city)){
        $Locationcity = $EditJobData->location_city;
    }else{
        $Locationcity = "";
    }

?>

<section class="content">
    <div class="row">
        <div class="col-md-12 box2">
            <div id="messageAlert" style="display:none;"></div>
            <div class="box box-primary">
                <div id="ajax-loader" class="overlay none">
                    <i class="fa fa-refresh fa-spin"></i>
                </div>

                <form role="form" name="updateJob" id="updateJob" action="{{url('admin/job',array($EditJobData->id))}}" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="_method" value="PUT" />
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    <div class="box-body">

                        <div class="form-group">
                          <label for="title">Job Title</label>
                          <input type="text" name="title" id="title" value="{{$Jobtitle}}" class="form-control" maxlength="254">
                        </div>

                        <div class="form-group">
                          <label for="content">Job Description</label>
                          <textarea name="description" class="form-control" rows="3">{{$Jobdescription}}</textarea>
                        </div>

                        <div class="form-group">
                            <label for="content">Tell us what you want to get done</label>
                            <textarea name="what_you_want" class="form-control" rows="3">{{$Jobwhatyouwant}}</textarea>
                        </div>

                        <div class="form-group">
                            <label for="Type">Category</label>
                            <select class="form-control state" name="category_id">
                                <?php for($i=0;$i<count($GetCategoryList);$i++) { ?>
                                    <option value=<?php echo  $GetCategoryList[$i]->id ?> <?php if($GetCategoryList[$i]->id==$EditJobData->category_id){ ?> selected="selected" <?php } ?> >{{$GetCategoryList[$i]->name}}</option>
                                 <?php } ?>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="title">Project Deadline</label>
                            <input type="text" name="project_deadline" id="project_deadline" value="{{$Projectdeadline}}" class="form-control" maxlength="254">
                        </div>

                        <div class="form-group">
                            <label for="title">Attach File</label>
                            <input type="file" id="file" name="file[]" placeholder="" value="" accept="image/*" multiple="multiple">
                        </div>

                        <div class="form-group">
                            <label for="title">Estimated Job Budget</label>
                            <input type="text" name="budget" id="budget" value="{{$Budget}}" class="form-control" maxlength="254">
                        </div>

                        <div class="form-group">
                            <label for="title">What skills should the job seeker have?</label>
                            <input type="text" name="skills" id="skills" value="{{$Skills}}" class="form-control" maxlength="254">
                        </div>

                        <div class="form-group">
                            <label for="Type">How long do you expect this job to last?</label>
                            <select class="form-control state" name="how_long" id="how_long">
                                <option value=""> Select duration </option>
                                <option value="More than 6 months" @if($Howlong == "More than 6 months") selected="selected" @endif > More than 6 months </option>
                                <option value="3 to 6 months" @if($Howlong == "3 to 6 months") selected="selected" @endif > 3 to 6 months </option>
                                <option value="1 to 3 months" @if($Howlong == "1 to 3 months") selected="selected" @endif > 1 to 3 months </option>
                                <option value="Less than 1 month" @if($Howlong == "Less than 1 month") selected="selected" @endif > Less than 1 month </option>
                                <option value="Less than 1 week" @if($Howlong == "Less than 1 week") selected="selected" @endif > Less than 1 week </option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="Type">Country</label>
                            <select class="form-control state" name="location_country" id="location_country">
                              <option value=""> Select Country </option>
                              <option value="UK" @if($Locationcountry == "UK") selected="selected" @endif > UK </option>
                              <option value="Malaysia" @if($Locationcountry == "Malaysia") selected="selected" @endif > Malaysia </option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="title">City</label>
                            <input type="text" name="location_city" id="location_city" value="{{$Locationcity}}" class="form-control" maxlength="254">
                        </div>


                    <div class="box-footer">
                        <input type="hidden" value="{{$EditJobData->id}}" name="id" />
                        <button id="updateJobBtn" type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>


<script src="{{URL::asset('resources/assets/admin/plugins/jQuery-validation-plugin/jquery.validate.js')}}"></script>
<script src="{{URL::asset('resources/assets/admin/plugins/jQuery-validation-plugin/additional-methods.js')}}"></script>
<script>
$(document.body).on('click', '#updateJobBtn', function() {
    $.validator.addMethod('filesize', function(value, element, param) {
        return this.optional(element) || (element.files[0].size <= param)
    }, "File must be JPG, JPEG or PNG, less than 2 MB.");

    if ($("#updateJob").length) {
        // Email custom regex
        $("#updateJob").validate({
            onkeyup: false,
            onfocusout: false,
            errorElement: 'span',
            errorClass: 'has-error',
            ignore: [],
            rules: {
                "title": {
                    required: true,
                },
                "description": {
                    required: true,
                },
                "category_id": {
                    required: true,
                },
                "how_long": {
                    required: true,
                }
            },
            messages: {
                "title": {
                  required: "Job Title should not be empty.",
                },
                "description": {
                    required: "Job Description should not be empty.",
                },
                "category_id": {
                    required: "Please select category name.",
                },
                "how_long": {
                    required: "Please select how long.",
                }
            },
            errorPlacement: function(error, element) {
                error.insertAfter(element);
            },
            submitHandler: function(form) {
                var formData = new FormData($("#updateJob")[0]);
                var dataString = formData;
                $("#updateJobBtn").attr("disabled", true);
                $.ajax({
                    type: "POST",
                    cache: false,
                    contentType: false,
                    processData: false,
                    url: $("#updateJob").attr("action"),
                    data: dataString,
                    before: function() {
                        $("#ajax-loader").show();
                    },
                    success: function(response) {
                        $("#ajax-loader").hide();
                        $("#messageAlert").show();

                        if (response.status == 1) {
                            $("#messageAlert").html('<div class="alert alert-success alert-dismissible"><h4><i class="icon fa fa-check"></i> Success!</h4>' + response.message + '</div>');
                            $("html, body").animate({
                                scrollTop: 0
                            }, "slow");

                            window.setTimeout(function() {
                                location.href = "<?php echo url("/")?>/admin/job"
                            }, 3000);
                            $("#updateJobBtn").attr("disabled", false);
                        } else {
                            $("#messageAlert").html('<div class="alert alert-danger alert-dismissible"><h4><i class="icon fa fa-ban"></i> Error!</h4>' + response.error + '</div>');
                            $("html, body").animate({
                                scrollTop: 0
                            }, "slow");

                        }
                    }
                });
                //  return false;
            }
        });
    }
});
</script>
<script type="text/javascript" src="http://code.jquery.com/ui/1.10.3/jquery-ui.min.js"></script>
<script type="text/javascript" src="{{URL::asset('resources/assets/js/jquery-ui-sliderAccess.js')}}"></script>
<script type='text/javascript' src="{{URL::asset('resources/assets/js/jquery-ui-timepicker-addon.js')}}"></script>
<script type="text/javascript">
$(function(){
  $('#project_deadline').bind("cut copy paste",function(e) {
          e.preventDefault();
  });
  $('#project_deadline').datetimepicker({
		minDate: 0,
	});
});
</script>
@stop
