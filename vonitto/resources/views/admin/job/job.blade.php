@include('admin.layouts.AdminHeader')
@include('admin.layouts.AdminLeftSidebar')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Job</h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('admin/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Job</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content pages">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                         <div style="float:right;"><h3 class="box-title"><a href="{{ url('admin/job/create') }}"><button class="btn btn-block btn-primary">Add Job</button></a></h3></div>
                    </div>
                    <div class="box-body">
                        <table id="datatable" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th style="width:5%;">Id</th>
                                    <th style="width:30%;">Category Name</th>
                                    <th style="width:35%;">Job Title</th>
                                    <th style="width:35%;">Job Description</th>
                                    <th style="width:35%;">Featured</th>
                                    <th style="width:10%;">Action</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                            <tfoot>
                                <tr>
                                    <th>Id</th>
                                    <th>Category Name</th>
                                    <th>Job Title</th>
                                    <th>Job Description</th>
                                    <th>Featured</th>
                                    <th>Action</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<!-- DataTables -->
<script src="{{ URL::asset('resources/assets/admin/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::asset('resources/assets/admin/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>

<script type="text/javascript">
var save_method; // For save method string
var dataTable;

$(document).ready(function() {

    dataTable = $('#datatable').DataTable({
        "processing": true,
        "order": [
            [0, "desc"],
            //[3, "desc"]
        ],
        "oLanguage": {
            "sProcessing": "Loading records..."
        },
        "serverSide": true,
        //"scrollX": true,
        "columns": [
            null,
            null,
            {
                "orderable": true,
                "searchable": false
            },
            {
                "orderable": false,
                "searchable": false
            },
            {
                  "orderable": false,
                "searchable": false
            },
            {
                "orderable": false,
                "searchable": false
            },
        ],
        // Load data for the table's content from an Ajax source
        "ajax": {
            url: '<?php echo Request::url()?>/datatable',
            type: "get",
            "data": function(d) {

            },
            error: function() {

            }
        }
    });

    function reload_table() {
        dataTable.ajax.reload(null, false); // Reload datatable ajax
    }

    $(document.body).on('click', '.deleteCourseUnitBtn', function() {
        $.ajaxSetup({
              headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              }
        });
        var result = confirm("Are you sure you want to delete this?");
        if (result) {
            var $id = $(this).attr("data-page-id");
            $.ajax({
                url: "<?php echo Request::url()?>/" + $id,
                type: "DELETE",
                data: "id=" + $id,
                success: function(data) {
                    if (data.status == 1) {
                        reload_table();
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    alert('Error adding / update data');
                }
            });
        }
    });

    $(document.body).on('change', '#datatable .isFeatured', function() {
        $currentObj=$(this);
        var checked="";
        if($(this).is(':checked'))
            checked="true";
        else
            checked="false";

        $.ajaxSetup({
              headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              }
        });
        var $job_id = $(this).data('id')
        $.ajax({
            url: "<?php echo Request::url()?>/featured",
            type: "post",
            data: {job_id:$job_id,is_featured:checked},
            success: function(data) {
                if(data.status == 0){
                  $currentObj.removeAttr("checked");
                  alert(data.error);
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                alert('Error adding / update data');
            }
        });

    });


});
</script>

@include('admin.layouts.AdminFooter')
