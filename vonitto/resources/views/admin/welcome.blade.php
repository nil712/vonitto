@include('admin.layouts.AdminHeader')
@include('admin.layouts.AdminLeftSidebar')
<style>
.selected{
  border-color: grey;
}
.bg-vistor, .callout.callout-info, .alert-info, .label-info, .modal-info .modal-body {
    background-color: #f39c12  !important;
    margin: -14px;
    border-radius: 50%;
}

.content-wrapper {
    min-height: 940px !important;
}
.time_mgmt{
	float: right;
	margin: -68px 13px;
}
.select_area{
    width: 20%;
    margin: 12px;
}
.month_graph{
  margin-right:120px;
}
.row {
     margin-right: 0px !important;
     margin-left: 0px !important;
}
</style>
 <div class="wrapper">
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
      <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Dashboard
            <small>Control panel</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
          </ol>
        </section>
    <!-- Main content -->
        <section class="content">
          <!-- Small boxes (Stat box) -->
          <div class="row">
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-aqua">
                <div class="inner">
		  <h3>{{$user_count}}</h3>
                  <p>Manage Users</p>
                </div>
                <div class="icon">
                  <i class="ion ion-ios-people-outline"></i>
                </div>
                <a href="{{ url('admin/userdetail') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div><!-- ./col -->

	    <!-- Small boxes (Stat box) -->
          <div class="row">
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-green">
                <div class="inner">
		  <h3>{{$category_count}}</h3>
                  <p>Job Category</p>
                </div>
                <div class="icon">
                  <i class="ion ion-ios-football"></i>
                </div>
                <a href="{{ url('admin/category') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div><!-- ./col -->

            <!-- Small boxes (Stat box) -->
           <div class="row">
             <div class="col-lg-3 col-xs-6">
               <!-- small box -->
               <div class="small-box bg-yellow">
                 <div class="inner">
 		              <h3>{{$job_count}}</h3>
                   <p>Manage Job</p>
                 </div>
                 <div class="icon">
                   <i class="ion ion-ios-bookmarks"></i>
                 </div>
                 <a href="{{ url('admin/job') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
               </div>
             </div><!-- ./col -->

            <!-- Small boxes (Stat box) -->
           <div class="row">
             <div class="col-lg-3 col-xs-6">
               <!-- small box -->
               <div class="small-box bg-red">
                 <div class="inner">
       <h3>{{$page_count}}</h3>
                   <p>CMS</p>
                 </div>
                 <div class="icon">
                   <i class="ion ion-ios-copy"></i>
                 </div>
                 <a href="{{ url('admin/pagecms') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
               </div>
             </div><!-- ./col -->

             <!-- Small boxes (Stat box) -->
            <div class="row">
              <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-blue">
                  <div class="inner">
                    <h3>{{$page_count}}</h3>
                    <p>Subscription</p>
                  </div>
                  <div class="icon">
                    <i class="ion ion-ios-drag"></i>
                  </div>
                  <a href="{{ url('admin/subscription') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
              </div><!-- ./col -->


	</section>
</div><!-- /.content-wrapper -->

</div>
<!-- jQuery 2.1.4 -->
<script src="{{ URL::asset('resources/assets/admin/plugins/jQuery/jQuery-2.1.4.min.js') }}"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
@include('admin.layouts.AdminFooter')
