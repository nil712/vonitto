@include('admin.layouts.AdminLoginHeader')

<div class="login-box">
    <div class="login-logo">
            <img src="{{ URL::asset('resources/assets/images/logo.png') }}" height="70px" width="320px" alt="Pitch Your Concepts">
    </div><!-- /.login-logo -->
    <div class="login-box-body">
        <p class="login-box-msg">Sign in to start your session</p>

        @if(Request::session()->has('flash_message'))
            <div class="alert alert-success">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>Completed!</strong> {{ Request::session()->pull('flash_message') }}
            </div>
        @elseif(Request::session()->has('flash_message_error'))
            <div class="alert alert-error">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>Oops!!</strong> {{ Request::session()->pull('flash_message_error') }}
          </div>
        @elseif(count($errors) > 0)
            <div class="alert alert-error">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>Oops!!</strong>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <form class="form-vertical" action="{{ URL::to('admin/auth/login') }}" method="post">
            <div class="form-group has-feedback">
                <input type="email" name="email" id="inputEmail" class="form-control" placeholder="Email" value="{{ old('email') }}" />
                <span class="glyphicon glyphicon-user form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <input type="password" name="password" id="inputPassword" class="form-control" placeholder="Password" />
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <div class="row">
                <div class="col-xs-8">
                    <?php /*<div class="checkbox icheck">
                        <label>
                            <input type="checkbox"> Remember Me
                        </label>
                    </div>*/ ?>
                </div><!-- /.col -->
                <div class="col-xs-4">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <button type="submit" class="btn btn-primary btn-block btn-flat">Login</button>
                </div><!-- /.col -->
            </div>
        </form>

    </div><!-- /.login-box-body -->
</div><!-- /.login-box -->

@include('admin.layouts.AdminLoginFooter')
