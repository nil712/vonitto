@include('admin.layouts.AdminHeader')
@include('admin.layouts.AdminLeftSidebar')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>User Detail</h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('admin/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">UserDetail</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content pages">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                           <div style="float:right;"><h3 class="box-title"><a href="{{ url('admin/userdetail/create') }}"><button class="btn btn-block btn-primary">Add user detail</button></a></h3></div>
                    </div>
                    <div class="box-body">
                        <table id="datatable" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th style="width:5%;">Id</th>
                                    <th style="width:30%;">Image</th>
                                    <th style="width:30%;">Firstname</th>
                                    <th style="width:35%;">Lastname</th>
                                    <th style="width:10%;">Email</th>
                                    <th style="width:10%;">Role</th>
                                    <th style="width:10%;">Action</th>
                                </tr>
                            </thead>
                            <thead>
                                <tr class="dataTables_filter">
                                    <th style="width:100px;"></th>
                                    <th style="width:100px;"></th>
                                    <th>
                                        <input type="text" data-column="2" class="form-control searc-sm search-inp" style="width:100%;" aria-controls="table">
                                    </th>
                                    <th>
                                        <input type="text" data-column="3" class="form-control searc-sm search-inp" style="width:100%;" aria-controls="table">
                                    </th>
                                    <th>
                                        <input type="text" data-column="4" class="form-control searc-sm search-inp" style="width:100%;" aria-controls="table">
                                    </th>
                                    <th>
                                        <select class="form-control searc-sm search-inp" data-column="5">
                                            <option value="">Select Role</option>
                                            <option value="JOB_POSTER">JOB_POSTER</option>
                                            <option value="JOB_SEEKER">JOB_SEEKER</option>
                                        </select>
                                    </th>
                                </tr>
                            </thead>

                            <tbody></tbody>
                            <tfoot>
                                <tr>
                                    <th>Id</th>
                                    <th>Image</th>
                                    <th>Fisrtname</th>
                                    <th>Lastname</th>
                                    <th>Email</th>
                                    <th>Role</th>
                                    <th>Action</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<!-- DataTables -->
<script src="{{ URL::asset('resources/assets/admin/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::asset('resources/assets/admin/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>

<script type="text/javascript">
var save_method; // For save method string
var dataTable;

$(document).ready(function() {
$.ajaxSetup({     headers: {         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')     } });

    dataTable = $('#datatable').DataTable({
        "processing": true,
        "order": [
            [0, "desc"],
            //[3, "desc"]
        ],
        "oLanguage": {
            "sProcessing": "Loading records..."
        },
        "serverSide": true,
        //"scrollX": true,
        "columns": [
            null,
            {
                // "orderable": false,
                // "searchable": false
            },
            null,
            null, {
                // "orderable": false,
                // "searchable": false
            },
            null,
             {
                "orderable": false,
                "searchable": false
            },
        ],
        // Load data for the table's content from an Ajax source
        "ajax": {
            url: '<?php echo Request::url()?>/datatable',
            type: "get",
            "data": function(d) {

            },
            error: function() {

            }
        }
    });
    $(document).ready(function(){
        $('.search-inp').on( 'keyup click', function () {   // For text boxes
            var i = $(this).attr('data-column');  // Getting column index
            var v = $(this).val();  // Getting search input value
            dataTable.columns(i).search(v).draw();
        });
        $('.search-inp').on( 'change', function () {   // For select box
            var i = $(this).attr('data-column');
            var v = $(this).val();
            dataTable.columns(i).search(v).draw();
        });
    });


    function reload_table() {
        dataTable.ajax.reload(null, false); // Reload datatable ajax
    }

    $(document.body).on('click', '.deleteCourseUnitBtn', function() {

        var result = confirm("Are you sure you want to delete this?");
        if (result) {
            var $id = $(this).attr("data-page-id");
            $.ajax({
                url: "<?php echo Request::url()?>/" + $id,
                type: "DELETE",
                data: "id=" + $id,
                success: function(data) {
                    if (data.status == 1) {
                        reload_table();
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    alert('Error adding / update data');
                }
            });
        }
    });
});
</script>

@include('admin.layouts.AdminFooter')
