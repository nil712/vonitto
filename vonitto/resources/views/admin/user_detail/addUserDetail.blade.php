@extends('admin.layouts.AdminMaster')

@section('content')

<?php


?>

<section class="content-header">
    <h1> Add User Detail</h1>
    <ol class="breadcrumb">
        <li><a href="{{ url('admin/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{ url('admin/userdetail') }}"></i> User Detail</a></li>
        <li class="active">Add User Detail</li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-md-12 box2">
            <div id="messageAlert" style="display:none;"></div>
            <div class="box box-primary">
                <div id="ajax-loader" class="overlay none">
                    <i class="fa fa-refresh fa-spin"></i>
                </div>

                <form role="form" name="addUserDetailForm" id="addUserDetailForm" action="" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    <div class="box-body">
                        <!-- <div class="form-group">
                            <label for="title">Username</label>
                            <input type="text" name="username" id="username" value="{{ old('username') }}" class="form-control" maxlength="254">
                        </div> -->

                        <div class="form-group">
                            <label>Select Type</label>
                            <select class="form-control job_type" name="job_type">
                                <option value="">Select Type</option>
                              <option value="JOB_POSTER">JOB_POSTER</option>
                              <option value="JOB_SEEKER">JOB_SEEKER</option>
                           </select>
                        </div>

                        <div class="form-group">
                            <label for="title">Firstname</label>
                            <input type="text" name="first_name" id="first_name" value="{{ old('first_name') }}" class="form-control" maxlength="254">
                        </div>

                        <div class="form-group">
                            <label for="content">Lastname</label>
                            <input type="text" name="last_name" id="last_name" value="{{ old('last_name') }}" class="form-control" maxlength="254">
                        </div>

                        <div class="form-group">
                            <label for="content">Email</label>
                            <input type="text" name="email" id="email" value="{{ old('email') }}" class="form-control" maxlength="254">
                        </div>

                        <div class="form-group">
                            <label for="content">Password</label>
                            <input type="password" name="password" id="password" value="{{ old('password') }}" class="form-control" maxlength="254">
                        </div>

                        <div class="form-group">
                            <label for="title">Image</label>
                            <input type="file" id="image" name="image" placeholder="" value="" accept="image/*">
                        </div>

                        <div class="form-group">
                            <label for="content">Phone Number</label>
                            <input type="text" name="phone" id="phone" value="{{ old('phone') }}" class="form-control" maxlength="254">
                        </div>

                        <div class="form-group">
                            <label for="content">Street Address1</label>
                            <input type="text" name="address1" id="address1" value="{{ old('address1') }}" class="form-control" maxlength="254">
                        </div>

                        <div class="form-group">
                            <label for="content">Street Address2</label>
                            <input type="text" name="address2" id="address2" value="{{ old('address2') }}" class="form-control" maxlength="254">
                        </div>

                        <div class="form-group">
                            <label for="content">City</label>
                            <input type="text" name="city" id="city" value="{{ old('city') }}" class="form-control" maxlength="254">
                        </div>

                        <div class="form-group">
                            <label for="content">State</label>
                            <input type="text" name="state" id="state" value="{{ old('state') }}" class="form-control" maxlength="254">
                        </div>

                        <div class="form-group">
                            <label for="content">Postcode</label>
                            <input type="text" name="postcode" id="postcode" value="{{ old('postcode') }}" class="form-control" maxlength="254">
                        </div>

                        <div class="form-group">
                            <label>Country</label>
                            <select class="form-control country" name="country">
                                <option value="">Select Country</option>
                              <option value="UK">UK</option>
                              <option value="Malaysia">Malaysia</option>
                           </select>
                        </div>

                        <div class="app_cls"></div>
                        <div class="app_reg"></div>
                        <div class="app_link"></div>


                    <div class="box-body" style="display: block;">
                        <div class="row unitsContainer"></div>
                    </div>

                    <div class="box-footer">
                        <button id="addUserDetailBtn" type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

<script src="{{URL::asset('resources/assets/admin/plugins/jQuery-validation-plugin/jquery.validate.js')}}"></script>
<script src="{{URL::asset('resources/assets/admin/plugins/jQuery-validation-plugin/additional-methods.js')}}"></script>
<script>

$(document).on("change",".job_type", function() {
    $(".app_cls").html("");
    $(".app_reg").html("");
    $(".app_link").html("");

    var role=$(this).val();
    if(role=="JOB_POSTER"){
        $(".app_cls").html('<div class="form-group"><label>Registered As</label><select class="form-control register_as" name="register_as"><option value="">Registered As</option><option value="individual">Individual</option><option value="company">Company</option></select></div>');
        }
    });

    $(document).on("change",".register_as", function() {
        $(".app_reg").html("");
        var reg_as=$(this).val();
        if(reg_as=="company"){
            $(".app_reg").html('<div class="form-group"><label>Company Name</label><input type="text" name="company_name" id="company_name" class="form-control" maxlength="254"></div>');
            $(".app_link").html('<div class="form-group"><label>Company Link</label><input type="text" placeholder="http://www.xyz.com" name="company_link" id="company_link" class="form-control" maxlength="254"></div>');
        }
    });

$(document.body).on('click', '#addUserDetailBtn', function() {


    if ($("#addUserDetailForm").length) {
        // Email custom regex

        $.validator.addMethod('filesize', function(value, element, param) {
            return this.optional(element) || (element.files[0].size <= param)
        }, "File must be JPG, JPEG or PNG, less than 2 MB.");

        jQuery.validator.addMethod("lettersonly", function(value, element) {
            return this.optional(element) || /^[a-z]+$/i.test(value);
        }, "This field accept Letters only");

        $("#addUserDetailForm").validate({
            onkeyup: false,
            onfocusout: false,
            errorElement: 'span',
            errorClass: 'has-error',
            ignore: [],
            rules: {
                "job_type":{
                  required: {
                      depends:function(){
                          $(this).val($.trim($(this).val()));
                          return true;
                      }
                  },
                },
                "first_name": {
                  required: {
                      depends:function(){
                          $(this).val($.trim($(this).val()));
                          return true;
                      }
                  },
                    lettersonly: true,
                },
                "last_name": {
                  required: {
                      depends:function(){
                          $(this).val($.trim($(this).val()));
                          return true;
                      }
                  },
                    lettersonly: true,
                },
                "email": {
                  required: {
                      depends:function(){
                          $(this).val($.trim($(this).val()));
                          return true;
                      }
                  },
                    email: true,
                },
                "password": {
                  required: {
                      depends:function(){
                          $(this).val($.trim($(this).val()));
                          return true;
                      }
                  },
                },
                "registre_as": {
                  required: {
                      depends:function(){
                          $(this).val($.trim($(this).val()));
                          return true;
                      }
                  },
                },
                "company_name": {
                  required: {
                      depends:function(){
                          $(this).val($.trim($(this).val()));
                          return true;
                      }
                  },
                },
                "profile_image":{
                   accept: "image/jpg,image/jpeg,image/png",
               },
               "phone":{
                  number: true,
               },
               "company_link":{
                   url: true,
               }
            },
            messages: {
                "job_type": {
                    required: "Please select role",
                },
                "first_name": {
                    required: "Please pass firstname",
                },
                "last_name": {
                    required: "Please pass lastname",
                },
                "email": {
                    required: "Please pass email",
                    email: "Please enter valid email address",
                },
                "password": {
                    required: "Please pass password",
                },
                "registre_as": {
                    required: "Please select register type",
                },
                "company_name": {
                    required: "Please pass company name",
                },
                "profile_image":{
                   accept: "Select valid image",
               },
               "phone":{
                  number: "Please enter number only",
               },
               "company_link":{
                   url: "Please enter valid url",
               }
            },
            errorPlacement: function(error, element) {
                 error.insertAfter(element);
                },
            submitHandler: function(form) {
                var formData = new FormData($("#addUserDetailForm")[0]);
                var dataString = formData;
                $("#addUserDetailBtn").attr("disabled", true);
                $.ajax({
                    type: "POST",
                    cache: false,
                    contentType: false,
                    processData: false,
                    url: "<?php echo url("/")?>/admin/userdetail",
                    data: dataString,
                    before: function() {
                        $("#ajax-loader").show();
                    },
                    success: function(response) {

                        $("#ajax-loader").hide();
                        $("#messageAlert").show();
                        if (response.status == 1) {
                            $('#addUserDetailForm')[0].reset();
                            $("#messageAlert").html('<div class="alert alert-success alert-dismissible"><h4><i class="icon fa fa-check"></i> Success!</h4>' + response.message + '</div>');
                            $("html, body").animate({
                                scrollTop: 0
                            }, "slow");

                            window.setTimeout(function() {
                                location.href = "<?php echo url("/")?>/admin/userdetail"
                            }, 3000);
                            $("#addUserDetailBtn").attr("disabled", false);
                            return false;
                        } else {
                            $("#messageAlert").html('<div class="alert alert-danger alert-dismissible"><h4><i class="icon fa fa-ban"></i> Error!</h4>' + response.error + '</div>');
                            $("html, body").animate({
                                scrollTop: 0
                            }, "slow");
                        }
                    }
                });
                //  return false;
            }
        });
    }
});
</script>

@stop
