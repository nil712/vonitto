@extends('admin.layouts.AdminMaster')
@section('content')

<?php
    if (isset($UserData[0]->email) && !empty($UserData[0]->email)) {
        $Email = $UserData[0]->email;
    } else {
        $Email = '';
    }

    if (isset($UserData[0]->first_name) && !empty($UserData[0]->first_name)) {
        $Firstname = $UserData[0]->first_name;
    } else {
        $Firstname = '';
    }

    if (isset($UserData[0]->last_name) && !empty($UserData[0]->last_name)) {
        $Lastname = $UserData[0]->last_name;
    } else {
        $Lastname = '';
    }

    if (isset($UserData[0]->role) && !empty($UserData[0]->role)) {
        $Role = $UserData[0]->role;
    } else {
        $Role = '';
    }

    if (isset($UserData[0]->register_as) && !empty($UserData[0]->register_as)) {
        $RegisterAs = $UserData[0]->register_as;
    } else {
        $RegisterAs = '';
    }

    if (isset($UserData[0]->company_name) && !empty($UserData[0]->company_name)) {
        $CompanyName = $UserData[0]->company_name;
    } else {
        $CompanyName = '';
    }

    if (isset($UserData[0]->company_link) && !empty($UserData[0]->company_link)) {
        $CompanyLink = $UserData[0]->company_link;
    } else {
        $CompanyLink = '';
    }

    if (isset($UserData[0]->street_address1) && !empty($UserData[0]->street_address1)) {
        $Address1 = $UserData[0]->street_address1;
    } else {
        $Address1 = '';
    }

    if (isset($UserData[0]->street_address2) && !empty($UserData[0]->street_address2)) {
        $Address2 = $UserData[0]->street_address2;
    } else {
        $Address2 = '';
    }

    if (isset($UserData[0]->city) && !empty($UserData[0]->city)) {
        $City = $UserData[0]->city;
    } else {
        $City = '';
    }

    if (isset($UserData[0]->state) && !empty($UserData[0]->state)) {
        $State = $UserData[0]->state;
    } else {
        $State = '';
    }

    if (isset($UserData[0]->image) && !empty($UserData[0]->image)) {
        if($UserData[0]->role=="JOB_POSTER"){
            $PhotoFilePathName = url("/").'/resources/assets/upload/jobposter/'.$UserData[0]->image;
        }else{
           $PhotoFilePathName = url("/").'/resources/assets/upload/jobseeker/'.$UserData[0]->image;
        }
    } else {
        $PhotoFilePathName = url("/").'/public/upload/users_image/default.png';
    }

    if (isset($UserData[0]->country) && !empty($UserData[0]->country)) {
        $Country = $UserData[0]->country;
    } else {
        $Country = '';
    }

    if (isset($UserData[0]->zip) && !empty($UserData[0]->zip)) {
        $Zipcode = $UserData[0]->zip;
    } else {
        $Zipcode = '';
    }

    if (isset($UserData[0]->phone) && !empty($UserData[0]->phone)) {
        $Phone = $UserData[0]->phone;
    } else {
        $Phone = '';
    }


?>

@if(!empty($UserData))
    <section class="content-header">
        <h1>{{ $Firstname }} Profile</h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('admin/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ url('admin/userdetail') }}"> Users</a></li>
            <li class="active">{{ $Firstname }} Profile</li>
        </ol>
    </section>

    <section class="content admin-profile-page">
        <div class="row box2">
            <div class="col-md-3">
                <div class="box box-primary">
                    <div class="box-body box-profile">
                        <img class="profile-user-img img-responsive img-circle" src="{{ $PhotoFilePathName }}" alt="">
                        <h3 class="profile-username text-center">

                        </h3>
                        <p class="text-muted text-center"></p>
                    </div>
                </div>
            </div>

            <div class="col-md-9">
                <div id="ValidationMsg"></div>
                <div class="nav-tabs-custom">
                    <div class="box box-primary">
                        <form action="#">
                            <div class="box-body">

                                <div class="form-group">
                                    <label>Firstname</label>
                                    <input type="text" value="{{ $Firstname }}" class="form-control" readonly="readonly" disabled="disabled">
                                </div>
                                <div class="form-group">
                                    <label>Lastname</label>
                                    <input type="text" value="{{ $Lastname }}" class="form-control" readonly="readonly" disabled="disabled">
                                </div>

                                <div class="form-group">
                                    <label>Email</label>
                                    <input type="text" value="{{ $Email }}" class="form-control" readonly="readonly" disabled="disabled">
                                </div>

                                <div class="form-group">
                                    <label>User Type</label>
                                    <input type="text" value="{{ $Role }}" class="form-control" readonly="readonly" disabled="disabled">
                                </div>

                                <div class="form-group">
                                    <label>Street Address1</label>
                                    <textarea name="address1" id="address" class="form-control" rows="3" readonly="readonly" disabled="disabled">{{$Address1}}</textarea>
                                </div>

                                <div class="form-group">
                                    <label>Street Address2</label>
                                    <textarea name="address2" id="address2" class="form-control" rows="3" readonly="readonly" disabled="disabled">{{$Address2}}</textarea>
                                </div>

                                <div class="form-group">
                                    <label>City</label>
                                    <input type="text" value="{{ $City }}" class="form-control" readonly="readonly" disabled="disabled">
                                </div>

                                <div class="form-group">
                                    <label>State</label>
                                    <input type="text" value="{{ $State }}" class="form-control" readonly="readonly" disabled="disabled">
                                </div>

                                <div class="form-group">
                                    <label>Country</label>
                                    <input type="text" value="{{ $Country }}" class="form-control" readonly="readonly" disabled="disabled">
                                </div>

                                <div class="form-group">
                                    <label>Phone</label>
                                    <input type="text" value="{{ $Phone }}" class="form-control" readonly="readonly" disabled="disabled">
                                </div>

                                <div class="form-group">
                                    <label>Postcode</label>
                                    <input type="text" value="{{ $Zipcode }}" class="form-control" readonly="readonly" disabled="disabled">
                                </div>

                                <div class="form-group">
                                    <label>Registered As</label>
                                    <input type="text" value="{{ $RegisterAs }}" class="form-control" readonly="readonly" disabled="disabled">
                                </div>

                                @if($RegisterAs=="company")
                                <div class="form-group">
                                    <label>Company Name</label>
                                    <input type="text" value="{{ $CompanyName }}" class="form-control" readonly="readonly" disabled="disabled">
                                </div>


                                <div class="form-group">
                                    <label>Company link</label>
                                    <input type="text" value="{{ $CompanyLink }}" class="form-control" readonly="readonly" disabled="disabled">
                                </div>
                                @endif
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@else
    <section class="content-header">
        <ol class="breadcrumb">
            <li><a href="{{ url('admin/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Oops! Page not found</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="error-page">
            <h2 class="headline text-yellow"> 404</h2>
            <div class="error-content">
                <h3><i class="fa fa-warning text-yellow"></i> Oops! Page not found.</h3>
                <p> The page you are looking for has been moved or doesn't exist anymore. If you typed the URL directly, please make sure the spelling is correct.</p>

                <a href="{{ url('admin/dashboard') }}" class="btn btn-info btn-block">GO BACK TO DASHBOARD</a>
            </div>
        </div>
    </section>
@endif

@stop
