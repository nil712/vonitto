@extends('admin.layouts.AdminMaster')
@section('content')

<section class="content-header">
    <h1> Edit User Detail: {{$EditUserData->username}} </h1>
    <ol class="breadcrumb">
        <li><a href="{{ url('admin/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{ url('admin/userdetail') }}"></i>User Detail</a></li>
        <li class="active">Edit User Detail: {{$EditUserData->first_name}} </li>
    </ol>
</section>

<?php
    if(isset($EditUserData->first_name) && !empty($EditUserData->first_name)){
        $Firstname = $EditUserData->first_name;
    }else{
        $Firstname = "";
    }

    if(isset($EditUserData->last_name) && !empty($EditUserData->last_name)){
        $Lastname = $EditUserData->last_name;
    }else{
        $Lastname = "";
    }

    if(isset($EditUserData->email) && !empty($EditUserData->email)){
        $Email = $EditUserData->email;
    }else{
        $Email = "";
    }

    if(isset($EditUserData->street_address1) && !empty($EditUserData->street_address1)){
        $Address1 = $EditUserData->street_address1;
    }else{
        $Address1 = "";
    }

    if(isset($EditUserData->street_address2) && !empty($EditUserData->street_address2)){
        $Address2 = $EditUserData->street_address2;
    }else{
        $Address2 = "";
    }

    if(isset($EditUserData->phone) && !empty($EditUserData->phone)){
        $Phone = $EditUserData->phone;
    }else{
        $Phone = "";
    }
    if(isset($EditUserData->city) && !empty($EditUserData->city)){
        $City = $EditUserData->city;
    }else{
        $City = "";
    }

    if(isset($EditUserData->state) && !empty($EditUserData->state)){
        $State = $EditUserData->state;
    }else{
        $State = "";
    }

    if(isset($EditUserData->country) && !empty($EditUserData->country)){
        $Country = $EditUserData->country;
    }else{
        $Country = "";
    }

    if(isset($EditUserData->zip) && !empty($EditUserData->zip)){
        $Zipcode = $EditUserData->zip;
    }else{
        $Zipcode = "";
    }

    if(isset($EditUserData->role) && !empty($EditUserData->role)){
        $JobType = $EditUserData->role;
    }else{
        $JobType = "";
    }

    if(isset($EditUserData->company_name) && !empty($EditUserData->company_name)){
        $CompanyName = $EditUserData->company_name;
    }else{
        $CompanyName = "";
    }

    if(isset($EditUserData->register_as) && !empty($EditUserData->register_as)){
        $RegistreAs = $EditUserData->register_as;
    }else{
        $RegistreAs = "";
    }


    if(isset($EditUserData->company_link) && !empty($EditUserData->company_link)){
        $CompanyLink = $EditUserData->company_link;
    }else{
        $CompanyLink = "";
    }
?>

<section class="content">
    <div class="row">
        <div class="col-md-12 box2">
            <div id="messageAlert" style="display:none;"></div>
            <div class="box box-primary">
                <div id="ajax-loader" class="overlay none">
                    <i class="fa fa-refresh fa-spin"></i>
                </div>

                <form role="form" name="updateUserDetail" id="updateUserDetail" action="{{url('admin/userdetail',array($EditUserData->id))}}" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="_method" value="PUT" />
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    <div class="box-body">

                        <div class="form-group">
                            <label>Select Type</label>
                            <select class="form-control job_type" name="job_type" disabled="true">
                                <option value="">Select Type</option>
                              <option value="JOB_POSTER" @if($JobType == "JOB_POSTER") selected="selected" @endif>JOB_POSTER</option>
                              <option value="JOB_SEEKER" @if($JobType == "JOB_SEEKER") selected="selected" @endif>JOB_SEEKER</option>
                           </select>
                        </div>

                        <div class="form-group">
                            <label for="title">Firstname</label>
                            <input type="text" name="first_name" id="first_name" value="{{ $Firstname }}" class="form-control" maxlength="254">
                        </div>

                        <div class="form-group">
                            <label for="content">Lastname</label>
                            <input type="text" name="last_name" id="last_name" value="{{ $Lastname }}" class="form-control" maxlength="254">
                        </div>

                        <div class="form-group">
                            <label for="content">Email</label>
                            <input type="text" name="email" id="email" value="{{ $Email }}" class="form-control" maxlength="254">
                        </div>

                        <div class="form-group">
                            <label for="title">Image</label>
                            <input type="file" id="image" name="image" placeholder="" value="" accept="image/*">
                        </div>

                        <div class="form-group">
                            <label for="content">Phone Number</label>
                            <input type="text" name="phone" id="phone" value="{{ $Phone }}" class="form-control" maxlength="254">
                        </div>

                        <div class="form-group">
                            <label for="content">Street Address1</label>
                            <textarea name="address1" id="address" class="form-control" rows="3">{{$Address1}}</textarea>
                        </div>

                        <div class="form-group">
                            <label for="content">Street Address2</label>
                            <textarea name="address2" id="address" class="form-control" rows="3">{{$Address2}}</textarea>
                        </div>

                        <div class="form-group">
                            <label for="content">City</label>
                            <input type="text" name="city" id="city" value="{{$City}}" class="form-control" maxlength="254">
                        </div>

                        <div class="form-group">
                            <label for="content">State</label>
                            <input type="text" name="state" id="state" value="{{ $State }}" class="form-control" maxlength="254">
                        </div>

                        <div class="form-group">
                            <label for="content">Postcode</label>
                            <input type="text" name="postcode" id="postcode" value="{{ $Zipcode }}" class="form-control" maxlength="254">
                        </div>

                        <div class="form-group">
                            <label>Country</label>
                            <select class="form-control country" name="country">
                                <option value="">Select Country</option>
                              <option value="UK" @if($Country == "UK") selected="selected" @endif>UK</option>
                              <option value="Malaysia" @if($Country == "Malaysia") selected="selected" @endif>Malaysia</option>
                           </select>
                        </div>

                        <?php
                        if($JobType=="JOB_POSTER"){
                        ?>
                                <div class="form-group">
                                    <label>Registered As</label>
                                    <select class="form-control register_as" name="register_as">
                                        <option value="">Registered As</option>
                                        <option value="individual" @if($RegistreAs == "individual") selected="selected" @endif>Individual</option>
                                        <option value="company" @if($RegistreAs == "company") selected="selected" @endif>Company</option>
                                    </select>
                                </div>
                        <?php if($RegistreAs=="company"){ ?>
                            <div class="form-group cmp_name">
                                <label>Company Name</label>
                                <input type="text" name="company_name" value="{{$CompanyName}}" id="company_name" class="form-control" maxlength="254">
                            </div>

                            <div class="form-group cmp_link">
                                <label>Company Link</label>
                                <input type="text" placeholder="http://www.xyz.com" value="{{$CompanyLink}}" name="company_link" id="company_link" class="form-control" maxlength="254">
                            </div>
                        <?php } } ?>
                        <div class="app_cls"></div>
                        <div class="app_reg"></div>
                        <div class="app_link"></div>

                    <div class="box-footer">
                        <input type="hidden" value="{{$EditUserData->UnitIdx}}" name="courseUnitId" />
                        <button id="updateUserDetailBtn" type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>


<script src="{{URL::asset('resources/assets/admin/plugins/jQuery-validation-plugin/jquery.validate.js')}}"></script>
<script src="{{URL::asset('resources/assets/admin/plugins/jQuery-validation-plugin/additional-methods.js')}}"></script>
<script>


    $(document).on("change",".register_as", function() {
        $(".app_reg").html("");
        var reg_as=$(this).val();
        if(reg_as=="company"){
            var compname = $('#company_name').val();
            $(".app_reg").html('<div class="form-group cmp_name"><label>Company Name</label><input type="text" name="company_name" value="'+ compname +'" id="company_name" class="form-control" maxlength="254"></div>');
            $(".app_link").html('<div class="form-group cmp_link"><label>Company Link</label><input type="text" placeholder="http://www.xyz.com" name="company_link" id="company_link" class="form-control" maxlength="254"></div>');
        }else if(reg_as=="individual"){
            $(".cmp_name").hide();
            $(".cmp_link").hide();
        }

    });

$(document.body).on('click', '#updateUserDetailBtn', function() {
    $.validator.addMethod('filesize', function(value, element, param) {
        return this.optional(element) || (element.files[0].size <= param)
    }, "File must be JPG, JPEG or PNG, less than 2 MB.");

    if ($("#updateUserDetail").length) {
        // Email custom regex
        $("#updateUserDetail").validate({
            onkeyup: false,
            onfocusout: false,
            errorElement: 'span',
            errorClass: 'has-error',
            ignore: [],
            rules: {
                "job_type":{
                  required: {
                      depends:function(){
                          $(this).val($.trim($(this).val()));
                          return true;
                      }
                  },
                },
                "first_name": {
                  required: {
                      depends:function(){
                          $(this).val($.trim($(this).val()));
                          return true;
                      }
                  },
                    lettersonly: true,
                },
                "last_name": {
                  required: {
                      depends:function(){
                          $(this).val($.trim($(this).val()));
                          return true;
                      }
                  },
                    lettersonly: true,
                },
                "email": {
                  required: {
                      depends:function(){
                          $(this).val($.trim($(this).val()));
                          return true;
                      }
                  },
                    email: true,
                },
                "password": {
                  required: {
                      depends:function(){
                          $(this).val($.trim($(this).val()));
                          return true;
                      }
                  },
                },
                "registre_as": {
                  required: {
                      depends:function(){
                          $(this).val($.trim($(this).val()));
                          return true;
                      }
                  },
                },
                "company_name": {
                  required: {
                      depends:function(){
                          $(this).val($.trim($(this).val()));
                          return true;
                      }
                  },
                },
                "profile_image":{
                   accept: "image/jpg,image/jpeg,image/png",
               },
               "phone":{
                  number: true,
               },
               "company_link":{
                   url: true,
               }
            },
            messages: {
                "job_type": {
                    required: "Please select role",
                },
                "first_name": {
                    required: "Please pass firstname",
                },
                "last_name": {
                    required: "Please pass lastname",
                },
                "email": {
                    required: "Please pass email",
                    email: "Please enter valid email address",
                },
                "password": {
                    required: "Please pass password",
                },
                "registre_as": {
                    required: "Please select register type",
                },
                "company_name": {
                    required: "Please pass company name",
                },
                "profile_image":{
                   accept: "Select valid image",
               },
               "phone":{
                  number: "Please enter number only",
               },
               "company_link":{
                   url: "Please enter valid url",
               }
            },
            errorPlacement: function(error, element) {
                if (element.attr("name") == "address") {
                    error.insertAfter("#cke_content");
                } else if (element.attr("name") == "content_du") {
                    error.insertAfter("#cke_content_du");
                } else {
                    error.insertAfter(element);
                }
            },
            submitHandler: function(form) {
                var formData = new FormData($("#updateUserDetail")[0]);
                var dataString = formData;
                $("#updateUserDetailBtn").attr("disabled", true);
                $.ajax({
                    type: "POST",
                    cache: false,
                    contentType: false,
                    processData: false,
                    url: $("#updateUserDetail").attr("action"),
                    data: dataString,
                    before: function() {
                        $("#ajax-loader").show();
                    },
                    success: function(response) {
                        $("#ajax-loader").hide();
                        $("#messageAlert").show();

                        if (response.status == 1) {
                            $("#messageAlert").html('<div class="alert alert-success alert-dismissible"><h4><i class="icon fa fa-check"></i> Success!</h4>' + response.message + '</div>');
                            $("html, body").animate({
                                scrollTop: 0
                            }, "slow");

                            window.setTimeout(function() {
                                location.href = "<?php echo url("/")?>/admin/userdetail"
                            }, 3000);
                            $("#updateUserDetailBtn").attr("disabled", false);
                        } else {
                            $("#messageAlert").html('<div class="alert alert-danger alert-dismissible"><h4><i class="icon fa fa-ban"></i> Error!</h4>' + response.error + '</div>');
                            $("html, body").animate({
                                scrollTop: 0
                            }, "slow");

                        }
                    }
                });
                //  return false;
            }
        });
    }
});
</script>

@stop
