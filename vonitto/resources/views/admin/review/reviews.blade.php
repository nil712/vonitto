@include('admin.layouts.AdminHeader')
@include('admin.layouts.AdminLeftSidebar')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Review & Rating</h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('admin/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Review & Rating</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content pages">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-body">
                        <table id="reviewTable" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th style="width:20%;">Job Title</th>
                                    <th style="width:20%;">Seeker</th>
                                    <th style="width:20%;">Poster</th>
                                    <th style="width:20%;">Rating</th>
                                    <th style="width:20%;">Action</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<div id="viewDetailModal" class="modal fade model" tabindex="-1" role="dialog" >
<div class="modal-dialog">
  <div class="modal-content">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Review & Rating Detail</h4>
      </div>
      <div class="modal-body">
        <div class="row">
        <div class="col-md-12">
          <div class="box box-solid">
            <div class="box-header with-border">
              <h3 class="box-title"></h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <dl>
                <dt>Rating</dt>
                <dd class="rating"></dd>
                <dt>Poster Name</dt>
                <dd class="poster"></dd>
                <dt>Review</dt>
                <dd class="review"></dd>
              </dl>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- ./col -->

        <!-- ./col -->
      </div>
      </div>
  </div>
</div>
</div>

<!-- DataTables -->
<script src="{{ URL::asset('resources/assets/admin/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::asset('resources/assets/admin/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>

<script type="text/javascript">
var save_method; // For save method string
var dataTable;

$(document).ready(function() {

    dataTable = $('#reviewTable').DataTable({
        "processing": true,
        "order": [
            [0, "desc"],
            //[3, "desc"]
        ],
        "oLanguage": {
            "sProcessing": "Loading records..."
        },
        "serverSide": true,
        //"scrollX": true,
        "columns": [
            null,
            null,
            null,
            null,
            {
                "orderable": false,
                "searchable": false
            },
        ],
        // Load data for the table's content from an Ajax source
        "ajax": {
            url: '<?php echo Request::url()?>/datatable',
            type: "get",
            "data": function(d) {

            },
            error: function() {

            }
        }
    });

    function reload_table() {
        dataTable.ajax.reload(null, false); // Reload datatable ajax
    }

    $(document.body).on('click', '#reviewTable .delete', function() {
        $.ajaxSetup({
           headers: {
                     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
            });
        var result = confirm("Are you sure you want to delete this?");
        if (result) {
            var $id = $(this).attr("data-page-id");
            $.ajax({
                url: "<?php echo Request::url()?>/" + $id,
                type: "DELETE",
                data: "id=" + $id,
                success: function(data) {
                    if (data.status == 1) {
                        reload_table();
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    alert('Error delete data');
                }
            });
        }
    });
});

$(document).on("click","#reviewTable .viewDetail",function(){
  var $model=$("#viewDetailModal");
  $model.modal("show");
  $model.find('.box-title').text($(this).attr("data-seeker"));
  $model.find('.rating').text($(this).attr("data-rating"));
  $model.find('.poster').text($(this).attr("data-poster"));
  $model.find('.review').text($(this).attr("data-review"));
});
</script>

@include('admin.layouts.AdminFooter')
