@extends('admin.layouts.AdminMaster')
@section('content')
<section class="content-header">
    <h1> Edit Profile</h1>
</section>

<section class="content">
    <div class="row">
        <div class="col-md-12 box2">
            <div id="messageAlert" style="display:none;"></div>
            <div class="box box-primary">
                <div id="ajax-loader" class="overlay none">
                    <i class="fa fa-refresh fa-spin"></i>
                </div>

                <form role="form" name="addCategoryForm" id="addCategoryForm" action="" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    <div class="box-body">

                        <div class="form-group">
                            <label for="title">Category Name</label>
                            <input type="text" name="name" id="name" value="{{ old('name') }}" class="form-control" maxlength="254">
                        </div>

                        <div class="box-footer">
                            <button id="addCategoryBtn" type="submit" class="btn btn-primary">Submit</button>
                        </div>
                </form>
            </div>
        </div>
    </div>
</section>

<script src="{{URL::asset('resources/assets/admin/plugins/jQuery-validation-plugin/jquery.validate.js')}}"></script>
<script src="{{URL::asset('resources/assets/admin/plugins/jQuery-validation-plugin/additional-methods.js')}}"></script>
<script>
$(document.body).on('click', '#addCategoryBtn', function() {


    if ($("#addCategoryForm").length) {
        // Email custom regex

        $("#addCategoryForm").validate({
            onkeyup: false,
            onfocusout: false,
            errorElement: 'span',
            errorClass: 'has-error',
            ignore: [],
            rules: {
                "name": {
                  required: {
                      depends:function(){
                          $(this).val($.trim($(this).val()));
                          return true;
                      }
                  },

                }
            },
            messages: {
                "name": {
                    required: "Category Name should not be empty.",
                }
            },
            errorPlacement: function(error, element) {
                 error.insertAfter(element);
                },
            submitHandler: function(form) {
                var formData = new FormData($("#addCategoryForm")[0]);
                var dataString = formData;

                $.ajax({
                    type: "POST",
                    cache: false,
                    contentType: false,
                    processData: false,
                    url: "<?php echo url("/")?>/admin/category",
                    data: dataString,
                    before: function() {
                        $("#ajax-loader").show();
                    },
                    success: function(response) {

                        $("#ajax-loader").hide();
                        $("#messageAlert").show();
                        if (response.status == 1) {
                            $('#addCategoryForm')[0].reset();
                            $("#messageAlert").html('<div class="alert alert-success alert-dismissible"><h4><i class="icon fa fa-check"></i> Success!</h4>' + response.message + '</div>');
                            $("html, body").animate({
                                scrollTop: 0
                            }, "slow");

                            window.setTimeout(function() {
                                location.href = "<?php echo url("/")?>/admin/category"
                            }, 3000);
                            return false;
                        } else {
                            $("#messageAlert").html('<div class="alert alert-danger alert-dismissible"><h4><i class="icon fa fa-ban"></i> Error!</h4>' + response.error + '</div>');
                            $("html, body").animate({
                                scrollTop: 0
                            }, "slow");
                        }
                    }
                });
                //  return false;
            }
        });
    }
});
</script>

@stop
