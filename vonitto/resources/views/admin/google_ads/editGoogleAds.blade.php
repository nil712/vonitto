@extends('admin.layouts.AdminMaster')
@section('content')

<section class="content-header">
    <h1> Edit Google Ad: {{$EditGoogleAdData->code}} </h1>
    <ol class="breadcrumb">
        <li><a href="{{ url('admin/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{ url('admin/google_ads') }}"></i>Google Ads</a></li>
        <li class="active">Edit Google Ad: {{$EditGoogleAdData->code}} </li>
    </ol>
</section>

<?php
if (isset($EditGoogleAdData->code) && !empty($EditGoogleAdData->code)) {
    $code = $EditGoogleAdData->code;
} else {
    $code = "";
}
?>

<section class="content">
    <div class="row">
        <div class="col-md-12 box2">
            <div id="messageAlert" style="display:none;"></div>
            <div class="box box-primary">
                <div id="ajax-loader" class="overlay none">
                    <i class="fa fa-refresh fa-spin"></i>
                </div>

                <form role="form" name="updateGoogleAd" id="updateGoogleAd" action="{{url('admin/google_ads',array($EditGoogleAdData->id))}}" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="_method" value="PUT" />
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    <div class="box-body">

                        <div class="form-group">
                            <label for="title">Code</label>
                            <input type="text" name="code" id="code" value="{{$code}}" class="form-control" maxlength="254">
                        </div>

                        <div class="box-footer">
                            <input type="hidden" value="{{$EditGoogleAdData->id}}" name="id" />
                            <button id="updateGoogleAdBtn" type="submit" class="btn btn-primary">Submit</button>
                        </div>
                </form>
            </div>
        </div>
    </div>
</section>


<script src="{{URL::asset('resources/assets/admin/plugins/jQuery-validation-plugin/jquery.validate.js')}}"></script>
<script src="{{URL::asset('resources/assets/admin/plugins/jQuery-validation-plugin/additional-methods.js')}}"></script>
<script>
$(document.body).on('click', '#updateGoogleAdBtn', function () {

    if ($("#updateGoogleAd").length) {
        // Email custom regex
        $("#updateGoogleAd").validate({
            onkeyup: false,
            onfocusout: false,
            errorElement: 'span',
            errorClass: 'has-error',
            ignore: [],
            rules: {
                "code": {
                    required: {
                        depends: function () {
                            $(this).val($.trim($(this).val()));
                            return true;
                        }
                    },
                }
            },
            messages: {
                "code": {
                    required: "Code should not be empty.",
                }
            },
            errorPlacement: function (error, element) {
                error.insertAfter(element);
            },
            submitHandler: function (form) {
                var formData = new FormData($("#updateGoogleAd")[0]);
                var dataString = formData;
                $("#updateGoogleAdBtn").attr("disabled", true);
                $.ajax({
                    type: "POST",
                    cache: false,
                    contentType: false,
                    processData: false,
                    url: $("#updateGoogleAd").attr("action"),
                    data: dataString,
                    before: function () {
                        $("#ajax-loader").show();
                    },
                    success: function (response) {
                        console.log(response.status);
                        $("#ajax-loader").hide();
                        $("#messageAlert").show();

                        if (response.status == 1) {
                            $("#messageAlert").html('<div class="alert alert-success alert-dismissible"><h4><i class="icon fa fa-check"></i> Success!</h4>' + response.message + '</div>');
                            $("html, body").animate({
                                scrollTop: 0
                            }, "slow");

                            window.setTimeout(function () {
                                location.href = "<?php echo url("/") ?>/admin/google_ads"
                            }, 3000);
                            $("#updateGoogleAdBtn").attr("disabled", false);
                        } else {
                            $("#messageAlert").html('<div class="alert alert-danger alert-dismissible"><h4><i class="icon fa fa-ban"></i> Error!</h4>' + response.error + '</div>');
                            $("html, body").animate({
                                scrollTop: 0
                            }, "slow");

                        }
                    }
                });
                //  return false;
            }
        });
    }
});
</script>

@stop
