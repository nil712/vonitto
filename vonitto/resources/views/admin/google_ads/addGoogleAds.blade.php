@extends('admin.layouts.AdminMaster')
@section('content')
<section class="content-header">
    <h1> Add Google Ads</h1>
    <ol class="breadcrumb">
        <li><a href="{{ url('admin/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{ url('admin/google_ads') }}"></i>Google Ads</a></li>
        <li class="active">Add Job Google Ads</li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-md-12 box2">
            <div id="messageAlert" style="display:none;"></div>
            <div class="box box-primary">
                <div id="ajax-loader" class="overlay none">
                    <i class="fa fa-refresh fa-spin"></i>
                </div>

                <form role="form" name="addGoogleAdsForm" id="addGoogleAdsForm" action="" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    <div class="box-body">

                        <div class="form-group">
                            <label for="title">Code</label>
                            <input type="text" name="code" id="code" value="{{ old('code') }}" class="form-control" maxlength="254">
                        </div>

                        <div class="box-footer">
                            <button id="addGoogleAdsBtn" type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </div>
                </form>
            
        </div>
    </div>
</section>

<script src="{{URL::asset('resources/assets/admin/plugins/jQuery-validation-plugin/jquery.validate.js')}}"></script>
<script src="{{URL::asset('resources/assets/admin/plugins/jQuery-validation-plugin/additional-methods.js')}}"></script>
<script>
$(document.body).on('click', '#addGoogleAdsBtn', function() {


    if ($("#addGoogleAdsForm").length) {
        // Email custom regex
        
        $("#addGoogleAdsForm").validate({
            onkeyup: false,
            onfocusout: false,
            errorElement: 'span',
            errorClass: 'has-error',
            ignore: [],
            rules: {
                "code": {
                  required: {
                      depends:function(){
                          $(this).val($.trim($(this).val()));
                          return true;
                      }
                  },

                }
            },
            messages: {
                "code": {
                    required: "Code should not be empty.",
                }
            },
            errorPlacement: function(error, element) {
                 error.insertAfter(element);
                },
            submitHandler: function(form) {
                var formData = new FormData($("#addGoogleAdsForm")[0]);
                var dataString = formData;
                $("#addGoogleAdsBtn").attr("disabled", true);
                $.ajax({
                    type: "POST",
                    cache: false,
                    contentType: false,
                    processData: false,
                    url: "<?php echo url("/")?>/admin/google_ads",
                    data: dataString,
                    before: function() {
                        $("#ajax-loader").show();
                    },
                    success: function(response) {
                        
                        $("#ajax-loader").hide();
                        $("#messageAlert").show();
                        if (response.status == 1) {
                            $('#addGoogleAdsForm')[0].reset();
                            $("#messageAlert").html('<div class="alert alert-success alert-dismissible"><h4><i class="icon fa fa-check"></i> Success!</h4>' + response.message + '</div>');
                            $("html, body").animate({
                                scrollTop: 0
                            }, "slow");

                            window.setTimeout(function() {
                                location.href = "<?php echo url("/")?>/admin/google_ads"
                            }, 3000);
                            $("#addGoogleAdsBtn").attr("disabled", false);
                            return false;
                        } else {
                            $("#messageAlert").html('<div class="alert alert-danger alert-dismissible"><h4><i class="icon fa fa-ban"></i> Error!</h4>' + response.error + '</div>');
                            $("html, body").animate({
                                scrollTop: 0
                            }, "slow");
                        }
                    }
                });
                //  return false;
            }
        });
    }
});
</script>

@stop
