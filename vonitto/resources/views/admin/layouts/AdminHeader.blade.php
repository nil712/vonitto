<?php
header("Expires: Tue, 01 Jan 2000 00:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
?>
<!DOCTYPE html>
<html>
    <head>
        <?php
            $ProjectName = \Config::get('constants.PROJECT_NAME');
            if (!isset($PageTitle) && empty($PageTitle)) {
                $PageTitle = "";
            }
        ?>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="csrf-token" content="{{ csrf_token() }}" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>{{ $PageTitle }}</title>        
        <!-- Favicon -->
        <link rel='shortcut icon' type='image/x-icon' href="{{URL::asset('resources/assets/images/favicon.ico')}}" />
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.5 -->
        <link rel="stylesheet" href="{{ URL::asset('resources/assets/admin/bootstrap/css/bootstrap.min.css') }}">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
        <!-- Select2 -->
        <link rel="stylesheet" href="{{ URL::asset('resources/assets/admin/plugins/select2/select2.min.css') }}">
        <!-- jvectormap -->
        <link rel="stylesheet" href="{{ URL::asset('resources/assets/admin/plugins/jvectormap/jquery-jvectormap-1.2.2.css') }}">
        <!-- Theme style -->
        <link rel="stylesheet" href="{{ URL::asset('resources/assets/admin/dist/css/AdminLTE.min.css') }}">
        <!-- Admin jsor css -->
        <link rel="stylesheet" href="{{ URL::asset('resources/assets/admin/css/jssor.css') }}">

        <!-- AdminLTE Skins. Choose a skin from the css/skins folder instead of downloading all of them to reduce the load. -->
        <link rel="stylesheet" href="{{ URL::asset('resources/assets/admin/dist/css/skins/_all-skins.min.css') }}">
        <!-- DataTables -->
	    <link rel="stylesheet" href="{{ URL::asset('resources/assets/admin/plugins/datatables/dataTables.bootstrap.css') }}">
        <!-- Daterange picker -->
	    <link rel="stylesheet" href="{{ URL::asset('resources/assets/admin/plugins/daterangepicker/daterangepicker-bs3.css') }}">
        <!-- Tag Input -->
        <link rel="stylesheet" href="{{ URL::asset('resources/assets/admin/plugins/InputFields/src/jquery.tagsinput.css') }}">

        <link rel="stylesheet" href="{{ URL::asset('resources/assets/admin/plugins/timepicker/bootstrap-timepicker.min.css') }}">

        <!-- Morris chart -->
        <link rel="stylesheet" href="{{ URL::asset('resources/assets/admin/plugins/morris/morris.css') }}">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- jQuery 2.1.4 -->
        <script src="{{ URL::asset('resources/assets/admin/plugins/jQuery/jQuery-2.1.4.min.js') }}"></script>

        <script> var BASE_URL ='<?php echo Request::url(); ?>/admin/'; </script>
    </head>
    <body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">
            <header class="main-header">
                <!-- Logo -->
                <a href="{{ url('admin/dashboard') }}" class="logo">
                    <!-- mini logo for sidebar mini 50x50 pixels -->
                    <span class="logo-mini"><b>PYC</b></span>
                    <!-- logo for regular state and mobile devices -->
                    <span class="logo-lg"><b>{{ ucfirst($ProjectName) }}</b></span>
                </a>

                <!-- Header Navbar: style can be found in header.less -->
                <nav class="navbar navbar-static-top" role="navigation">
                    <?php
                        $image =  Auth::user()->image;
                        if(isset($image) && !empty($image)){
                            $path = url("/")."/public/upload/userimage/".$image;
                            $PhotoFilePathName = $path;
                         }else{
                            $PhotoFilePathName = url("/").'/resources/assets/admin/dist/img/default.png';
                         }
                    ?>
                    <!-- Sidebar toggle button-->
                    <a href="javascript:void(0);" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                        <span class="sr-only">Toggle navigation</span>
                    </a>
                    <!-- Navbar Right Menu -->
                    <div class="navbar-custom-menu">
                        <ul class="nav navbar-nav">
                            <!-- User Account: style can be found in dropdown.less -->
                            <li class="dropdown user user-menu">
                                <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">
                                    <img src="{{ $PhotoFilePathName }}" class="user-image" alt="{{ Auth::user()->first_name }} {{ Auth::user()->last_name }}">
                                    <span class="hidden-xs">{{ ucfirst(Auth::user()->first_name) }}</span>
                                </a>
                                <ul class="dropdown-menu">
                                    <!-- User image -->
                                    <li class="user-header">
                                        <img src="{{ $PhotoFilePathName }}" class="img-circle" alt="{{ Auth::user()->first_name }} {{ Auth::user()->last_name }}">
                                        <p>
                                            {{ ucfirst(Auth::user()->first_name) }}
                                            <?php
                                                $RegisteredAt = Auth::user()->RegisteredAt;
                                                $MemberSince = new DateTime($RegisteredAt);
                                            ?>
                                            <small>Member since {{ $MemberSince->format('M. Y') }}</small>
                                        </p>
                                    </li>
                                    <!-- Menu Footer-->
                                    <li class="user-footer">
                                        <div class="pull-left">
                                            <a href="{{ url('/') }}/admin/user/edit/{{ Auth::user()->id }}" class="btn btn-default btn-flat">Edit Profile</a>
                                        </div>
                                        <div class="pull-right">
                                            <a href="{{ url('/') }}/logout" class="btn btn-default btn-flat">Sign out</a>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </nav>
            </header>
