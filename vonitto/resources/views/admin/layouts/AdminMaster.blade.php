<!DOCTYPE html>
<html>
<head>
    <!-- header inc-->
    @include('admin.layouts.AdminHeader')
    <!-- header inc-->
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    <!-- left sidebar -->
    <aside class="main-sidebar">
        @include('admin.layouts.AdminLeftSidebar')
    </aside>
    <!-- left sidebar -->
    <div class="content-wrapper">
        <!-- Dynamic Content should be here-->
        @yield('content')
        <!-- Dynamic Content should be here-->
    </div>
</div>

<!-- footer inc -->
@include('admin.layouts.AdminFooter')
<!-- footer inc -->
</body>
</html>
