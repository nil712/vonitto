<footer class="main-footer">
    <?php
        $ProjectName = \Config::get('constants.PROJECT_EDITO');
    ?>
    <strong>Copyright &copy; <?php echo date("Y"); ?> <a href="<?php echo Request::url(); ?>">{{ $ProjectName }}</a>.</strong> All rights reserved.
</footer>

</div><!-- ./wrapper -->

<!-- Bootstrap 3.3.5 -->
<script src="{{ URL::asset('resources/assets/admin/bootstrap/js/bootstrap.min.js') }}"></script>
<!-- FastClick -->
<script src="{{ URL::asset('resources/assets/admin/plugins/fastclick/fastclick.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ URL::asset('resources/assets/admin/dist/js/app.min.js') }}"></script>
<!-- Sparkline -->
<script src="{{ URL::asset('resources/assets/admin/plugins/sparkline/jquery.sparkline.min.js') }}"></script>
<!-- jvectormap -->
<script src="{{ URL::asset('resources/assets/admin/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
<script src="{{ URL::asset('resources/assets/admin/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
<!-- SlimScroll 1.3.0 -->
<script src="{{ URL::asset('resources/assets/admin/plugins/slimScroll/jquery.slimscroll.min.js') }}"></script>
<!-- ChartJS 1.0.1 -->
<script src="{{ URL::asset('resources/assets/admin/plugins/chartjs/Chart.min.js') }}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{ URL::asset('resources/assets/admin/dist/js/ln.js') }}"></script>
<script src="{{ URL::asset('resources/assets/admin/js/jssor.slider.min.js') }}"></script>
<!-- Tag Input -->
<script src="{{ URL::asset('resources/assets/admin/plugins/InputFields/src/jquery.tagsinput.js') }}"></script>

<!-- Select2 -->
<script src="{{ URL::asset('resources/assets/admin/plugins/select2/select2.full.min.js') }}"></script>

<!-- Daterange picker -->
<script src="{{ URL::asset('resources/assets/admin/plugins/daterangepicker/moment.min.js') }}"></script>
<script src="{{ URL::asset('resources/assets/admin/plugins/daterangepicker/daterangepicker.js') }}"></script>

<script src="{{ URL::asset('resources/assets/admin/plugins/timepicker/bootstrap-timepicker.min.js') }}"></script>
<!-- Sparkline -->
<script src="{{ URL::asset('resources/assets/admin/plugins/sparkline/jquery.sparkline.min.js') }}"></script>



<!-- Bootstrap WYSIHTML5 -->
<script src="{{ URL::asset('resources/assets/admin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}"></script>

<!-- jvectormap -->
<script src="{{ URL::asset('resources/assets/admin/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
<script src="{{ URL::asset('resources/assets/admin/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>

<!-- CKEditor -->
<script src="{{ URL::asset('resources/assets/admin/plugins/ckeditor/ckeditor.js') }}"></script>

<script>
    $(function () {
        if ("0" != $("body").find(".select2").length) {
            $(".select2").select2();
        }

        if ("0" != $("body").find("#content").length) {
            CKEDITOR.replace('content');
        }

        //Timepicker
        $(".timepicker").timepicker({
           showInputs: false
        });

        if ("0" != $("body").find("#Birthday").length) {
            // Date range picker
            $('#Birthday').daterangepicker({
                singleDatePicker: true,
                showWeekNumbers: false,
                showDropdowns: true,
                timePicker: false,
                timePickerIncrement: 30,
                format: 'YYYY-MM-DD'
            });
        }

    });
</script>

</body>
</html>
