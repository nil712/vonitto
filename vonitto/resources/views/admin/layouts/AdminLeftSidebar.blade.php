<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <?php
    $CurrentPage = Route::getFacadeRoot()->current()->uri();

    $image = Auth::user()->image;
    if (isset($image) && !empty($image)) {
        $path = url("/") . "/public/upload/userimage/" . $image;
        $PhotoFilePathName = $path;
    } else {
        $PhotoFilePathName = url("/") . '/resources/assets/admin/dist/img/default.png';
    }
    ?>
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ $PhotoFilePathName }}" class="img-circle" alt="{{ Auth::user()->first_name }} {{ Auth::user()->last_name }}">
            </div>
            <div class="pull-left info">
                <p>{{ ucfirst(Auth::user()->first_name) }}</p>
                <!-- <a href="javascript:void(0);"><i class="fa fa-circle text-success"></i> Online</a> -->
            </div>
        </div>

        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>

            <li class="treeview <?php if ($CurrentPage == 'admin/userdetail' || $CurrentPage == 'admin/userdetail/create') {
        echo 'active';
    } ?>">
                <a href="javascript:void(0);">
                    <i class="fa fa-briefcase"></i>
                    <i class="fa fa-angle-left pull-right"></i>
                    <span>User</span>
                </a>
                <ul class="treeview-menu">
                    <!-- <li class="<?php echo ($CurrentPage == 'admin/userdetail/create') ? 'active' : ''; ?>"><a href="{{ url('admin/userdetail/create') }}"><i class="fa fa-circle-o"></i> Add New User</a></li> -->
                    <li class="<?php echo ($CurrentPage == 'admin/userdetail') ? 'active' : ''; ?>"><a href="{{ url('admin/userdetail') }}"><i class="fa fa-circle-o"></i> All Users</a></li>
                </ul>
            </li>

            <li class="treeview <?php if ($CurrentPage == 'admin/category' || $CurrentPage == 'admin/category/create') {
        echo 'active';
    } ?>">
                <a href="javascript:void(0);">
                    <i class="fa fa-futbol-o"></i>
                    <i class="fa fa-angle-left pull-right"></i>
                    <span>Job Category</span>
                </a>
                <ul class="treeview-menu">
                    <li class="<?php echo ($CurrentPage == 'admin/category') ? 'active' : ''; ?>"><a href="{{ url('admin/category') }}"><i class="fa fa-circle-o"></i> All Job Category</a></li>
                </ul>
            </li>

            <li class="treeview <?php if ($CurrentPage == 'admin/job' || $CurrentPage == 'admin/job/create') {
        echo 'active';
    } ?>">
                <a href="javascript:void(0);">
                    <i class="fa fa-file"></i>
                    <i class="fa fa-angle-left pull-right"></i>
                    <span>Manage Jobs</span>
                </a>
                <ul class="treeview-menu">
                    <li class="<?php echo ($CurrentPage == 'admin/job') ? 'active' : ''; ?>"><a href="{{ url('admin/job') }}"><i class="fa fa-circle-o"></i> All Job Listing</a></li>
                </ul>
            </li>

            <li class="treeview <?php if ($CurrentPage == 'admin/pagecms' || $CurrentPage == 'admin/pagecms/create') {
        echo 'active';
    } ?>">
                <a href="javascript:void(0);">
                    <i class="fa fa-file-powerpoint-o"></i>
                    <i class="fa fa-angle-left pull-right"></i>
                    <span>CMS</span>
                </a>
                <ul class="treeview-menu">
                    <li class="<?php echo ($CurrentPage == 'admin/pagecms') ? 'active' : ''; ?>"><a href="{{ url('admin/pagecms') }}"><i class="fa fa-circle-o"></i> All CMS Pages</a></li>
                </ul>
            </li>

            <li class="treeview <?php if ($CurrentPage == 'admin/subscription' || $CurrentPage == 'admin/subscription/create') {
        echo 'active';
    } ?>">
                <a href="javascript:void(0);">
                    <i class="fa fa-server"></i>
                    <i class="fa fa-angle-left pull-right"></i>
                    <span>Subscription</span>
                </a>
                <ul class="treeview-menu">
                    <li class="<?php echo ($CurrentPage == 'admin/subscription') ? 'active' : ''; ?>"><a href="{{ url('admin/subscription') }}"><i class="fa fa-circle-o"></i> All Subscription</a></li>
                </ul>
            </li>
            <li class="treeview @if($CurrentPage == 'admin/reviews') active @endif">
                <a href="javascript:void(0);">
                    <i class="fa fa-fw fa-wechat"></i>
                    <span>Review & Rating</span>
                </a>
                <ul class="treeview-menu">
                    <li class="<?php echo ($CurrentPage == 'admin/reviews') ? 'active' : ''; ?>"><a href="{{ url('admin/review') }}"><i class="fa fa-circle-o"></i> Reviews</a></li>
                </ul>
            </li>

            <li class="treeview <?php if ($CurrentPage == 'admin/google_ads' || $CurrentPage == 'admin/google_ads/create') {
        echo 'active';
    } ?>">
                <a href="javascript:void(0);">
                    <i class="fa fa-file"></i>
                    <i class="fa fa-angle-left pull-right"></i>
                    <span>Google Ads</span>
                </a>
                <ul class="treeview-menu">
                    <li class="<?php echo ($CurrentPage == 'admin/google_ads') ? 'active' : ''; ?>"><a href="{{ url('admin/google_ads') }}"><i class="fa fa-circle-o"></i> All Google Ads</a></li>
                </ul>
            </li>

        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
