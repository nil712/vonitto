@extends('admin.layouts.AdminMaster')
@section('content')
<section class="content-header">
    <h1> Add Subscription</h1>
    <ol class="breadcrumb">
        <li><a href="{{ url('admin/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{ url('admin/category') }}"></i>Subscription</a></li>
        <li class="active">Add Subscription</li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-md-12 box2">
            <div id="messageAlert" style="display:none;"></div>
            <div class="box box-primary">
                <div id="ajax-loader" class="overlay none">
                    <i class="fa fa-refresh fa-spin"></i>
                </div>

                <form role="form" name="addSubscriptionForm" id="addSubscriptionForm" action="" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    <div class="box-body">

                        <div class="form-group">
                            <label for="title">Subscription Title</label>
                            <input type="text" name="subscription_title" id="subscription_title" value="{{ old('subscription_title') }}" class="form-control" maxlength="254">
                        </div>

                        <div class="form-group">
                            <label for="content">Subscription Description</label>
                            <textarea name="subscription_desc" class="form-control" rows="3"></textarea>
                        </div>

                        <div class="form-group">
                            <label for="title">Subscription Type</label>
                            <input type="text" name="subscription_type" id="subscription_type" value="{{ old('subscription_type') }}" class="form-control" maxlength="254">
                        </div>

                        <div class="form-group">
                            <label for="title">Subscription Amount</label>
                            <input type="text" name="subscription_amount" id="subscription_amount" value="{{ old('subscription_amount') }}" class="form-control" maxlength="254">
                        </div>

                        <div class="form-group">
                            <label for="title">Subscription Period</label>
                            <input type="text" name="subscription_period" id="subscription_period" value="{{ old('subscription_period') }}" class="form-control" maxlength="254">
                        </div>

                        <div class="form-group">
                            <label for="title">Subscription Application For</label>
                            <input type="text" name="subscription_applicable_for" id="subscription_applicable_for" value="{{ old('subscription_applicable_for') }}" class="form-control" maxlength="254">
                        </div>

                        <div class="form-group">
                            <label for="title">Subscription Free For Month</label>
                            <input type="text" name="free_for_month" id="free_for_month" value="{{ old('free_for_month') }}" class="form-control" maxlength="254">
                        </div>

                        <div class="form-group">
                            <label for="title">Subscription Additional For Month</label>
                            <input type="text" name="additional_for_month" id="additional_for_month" value="{{ old('additional_for_month') }}" class="form-control" maxlength="254">
                        </div>

                        <div class="box-footer">
                            <button id="addSubscriptionBtn" type="submit" class="btn btn-primary">Submit</button>
                        </div>
                </form>
            </div>
        </div>
    </div>
</section>

<script src="{{URL::asset('resources/assets/admin/plugins/jQuery-validation-plugin/jquery.validate.js')}}"></script>
<script src="{{URL::asset('resources/assets/admin/plugins/jQuery-validation-plugin/additional-methods.js')}}"></script>
<script>
$(document.body).on('click', '#addSubscriptionBtn', function() {


    if ($("#addSubscriptionForm").length) {
        // Email custom regex

        $("#addSubscriptionForm").validate({
            onkeyup: false,
            onfocusout: false,
            errorElement: 'span',
            errorClass: 'has-error',
            ignore: [],
            rules: {
                "subscription_title": {
                    required: true,
                },
                "subscription_desc": {
                    required: true,
                },
                "subscription_type": {
                    required: true,
                },
                "subscription_amount": {
                    required: true,
                },
                "subscription_period": {
                    required: true,
                }
            },
            messages: {
                "subscription_title": {
                    required: "Subscription title should not be empty.",
                },
                "subscription_desc": {
                    required: "Subscription description should not be empty.",
                },
                "subscription_type": {
                    required: "Subscription type should not be empty.",
                },
                "subscription_amount": {
                    required: "Subscription amount should not be empty.",
                },
                "subscription_period": {
                    required: "Subscription period should not be empty.",
                }
            },
            errorPlacement: function(error, element) {
                 error.insertAfter(element);
                },
            submitHandler: function(form) {
                var formData = new FormData($("#addSubscriptionForm")[0]);
                var dataString = formData;

                $.ajax({
                    type: "POST",
                    cache: false,
                    contentType: false,
                    processData: false,
                    url: "<?php echo url("/")?>/admin/subscription",
                    data: dataString,
                    before: function() {
                        $("#ajax-loader").show();
                    },
                    success: function(response) {

                        $("#ajax-loader").hide();
                        $("#messageAlert").show();
                        if (response.status == 1) {
                            $('#addSubscriptionForm')[0].reset();
                            $("#messageAlert").html('<div class="alert alert-success alert-dismissible"><h4><i class="icon fa fa-check"></i> Success!</h4>' + response.message + '</div>');
                            $("html, body").animate({
                                scrollTop: 0
                            }, "slow");

                            window.setTimeout(function() {
                                location.href = "<?php echo url("/")?>/admin/subscription"
                            }, 3000);
                            return false;
                        } else {
                            $("#messageAlert").html('<div class="alert alert-danger alert-dismissible"><h4><i class="icon fa fa-ban"></i> Error!</h4>' + response.error + '</div>');
                            $("html, body").animate({
                                scrollTop: 0
                            }, "slow");
                        }
                    }
                });
                //  return false;
            }
        });
    }
});
</script>

@stop
