@include('admin.layouts.AdminHeader')
@include('admin.layouts.AdminLeftSidebar')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Subscription</h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('admin/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Subscription</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content pages">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                         <div style="float:right;"><h3 class="box-title"><a href="{{ url('admin/subscription/create') }}"><button class="btn btn-block btn-primary">Add Subscription</button></a></h3></div>
                    </div>
                    <div class="box-body">
                        <table id="datatable" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th style="width:5%;">Id</th>
                                    <th style="width:25%;">Subscription Title</th>
                                    <th style="width:20%;">Subscription Type</th>
                                    <th style="width:20%;">Subscription Amount</th>
                                    <th style="width:20%;">Subscription Period</th>
                                    <th style="width:10%;">Action</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                            <tfoot>
                                <tr>
                                    <th>Id</th>
                                    <th>Subscription Title</th>
                                    <th>Subscription Type</th>
                                    <th>Subscription Amount</th>
                                    <th>Subscription Period</th>
                                    <th>Action</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<!-- DataTables -->
<script src="{{ URL::asset('resources/assets/admin/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::asset('resources/assets/admin/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>

<script type="text/javascript">
var save_method; // For save method string
var dataTable;

$(document).ready(function() {

    dataTable = $('#datatable').DataTable({
        "processing": true,
        "order": [
            [0, "desc"],
            //[3, "desc"]
        ],
        "oLanguage": {
            "sProcessing": "Loading records..."
        },
        "serverSide": true,
        //"scrollX": true,
        "columns": [
            null,
            null,
            {
                "orderable": false,
                "searchable": false
            },
            {
                "orderable": false,
                "searchable": false
            },
            {
                "orderable": false,
                "searchable": false
            },
            {
                "orderable": false,
                "searchable": false
            },
        ],
        // Load data for the table's content from an Ajax source
        "ajax": {
            url: '<?php echo Request::url()?>/datatable',
            type: "get",
            "data": function(d) {

            },
            error: function() {

            }
        }
    });

    function reload_table() {
        dataTable.ajax.reload(null, false); // Reload datatable ajax
    }

    $(document.body).on('click', '.deleteCourseUnitBtn', function() {
        $.ajaxSetup({
           headers: {
                     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
        });
        var result = confirm("Are you sure you want to delete this?");
        if (result) {
            var $id = $(this).attr("data-page-id");
            $.ajax({
                url: "<?php echo Request::url()?>/" + $id,
                type: "DELETE",
                data: "id=" + $id,
                success: function(data) {
                    if (data.status == 1) {
                        reload_table();
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    alert('Error adding / update data');
                }
            });
        }
    });
});
</script>

@include('admin.layouts.AdminFooter')
