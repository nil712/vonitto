@extends('layouts.admin.AdminMaster')
@section('content')

<section class="content-header">
    <h1> Add New User </h1>
    <ol class="breadcrumb">
        <li><a href="{{ url('admin/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{ url('admin/users') }}"> Users</a></li>
        <li class="active">Add New User</li>
    </ol>
</section>

<section class="content admin-add-user">
    <div class="row box2">
        <div id="ajax-loader" class="overlay none">
            <i class="fa fa-refresh fa-spin"></i>
        </div>
        <div class="col-md-12">
            <div id="messageAlert" style="display:none;"></div>
            <div class="box box-primary">
                <form name="FrmAddUser" id="FrmAddUser" class="" method="post" enctype="multipart/form-data" action="{{ url('admin/user/add') }}">
                    <div class="box-body">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">

                        <div class="form-group">
                            <label for="ID">Email</label>
                            <input type="text" name="ID" id="ID" value="" class="form-control">
                            <span class="has-error id-error"></span>
                        </div>

                        <div class="form-group">
                            <label for="password">Password</label>
                            <input type="password" name="password" id="password" value="" class="form-control">
                            <span class="has-error password-error"></span>
                        </div>

                        <div class="form-group">
                            <label for="password_confirmation">Password Confirmation</label>
                            <input type="password" name="password_confirmation" id="password_confirmation" value="" class="form-control">
                            <span class="has-error confirmation-password-error"></span>
                        </div>

                        <div class="form-group">
                            <label for="FirstName">First Name</label>
                            <input type="text" name="FirstName" id="FirstName" value="" class="form-control">
                            <span class="has-error firstname-error"></span>
                        </div>

                        <div class="form-group">
                            <label for="LastName">Last Name</label>
                            <input type="text" name="LastName" id="LastName" value="" class="form-control">
                            <span class="has-error lastname-error"></span>
                        </div>

                        <div class="form-group">
                            <label for="Nickname">Nickname</label>
                            <input type="text" name="Nickname" id="Nickname" value="" class="form-control">
                            <span class="has-error nickname-error"></span>
                        </div>

                        <div class="form-group">
                            <label for="Birthday">Birthday</label>
                            <input type="text" name="Birthday" id="Birthday" value="" class="form-control" readonly="readonly">
                            <span class="has-error birthday-error"></span>
                        </div>

                        <div class="form-group">
                            <label for="Gender">Gender</label>
                            <select name="Gender" id="Gender" class="form-control">
                                <option value="" selected="selected">Select</option>
                                <option value="1">Male</option>
                                <option value="0">Female</option>
                            </select>
                            <span class="has-error gender-error"></span>
                        </div>

                        <div class="form-group">
                            <label for="file">Profile Picture</label>
                            <input type="file" id="file" name="file" accept="image/*">
                            <p class="has-error profile-img-error"></p>
                        </div>

                        <div class="form-group">
                            <label for="Profile">Profile</label>
                            <textarea name="Profile" id="Profile" class="form-control" rows="5" cols="160"></textarea>
                            <span class="has-error profile-error"></span>
                        </div>

                        <div class="form-group">
                            <label for="Gender">User Type</label>
                            <select name="Type" id="Type" class="form-control">
                                <option value="" selected="selected">User Type</option>
                                <option value="1">Administrator</option>
                                <option value="3">User</option>
                            </select>
                            <span class="has-error user-type-error"></span>
                        </div>

                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

@stop
