@extends('layouts.admin.AdminMaster')
@section('content')

<?php
    if (isset($UserData[0]->ID) && !empty($UserData[0]->ID)) {
        $Email = $UserData[0]->ID;
    } else {
        $Email = '';
    }

    if (isset($UserData[0]->FirstName) && !empty($UserData[0]->FirstName)) {
        $FirstName = $UserData[0]->FirstName;
    } else {
        $FirstName = '';
    }

    if (isset($UserData[0]->LastName) && !empty($UserData[0]->LastName)) {
        $LastName = $UserData[0]->LastName;
    } else {
        $LastName = '';
    }

    if (isset($UserData[0]->Nickname) && !empty($UserData[0]->Nickname)) {
        $Nickname = $UserData[0]->Nickname;
    } else {
        $Nickname = '';
    }

    if (isset($UserData[0]->DateOfBirth) && !empty($UserData[0]->DateOfBirth)) {
        $DateOfBirth = $UserData[0]->DateOfBirth;
    } else {
        $DateOfBirth = '';
    }

    if (isset($UserData[0]->Gender)) {
        $Gender = $UserData[0]->Gender;

        if ($Gender == "1") {
            $Gender = "Male";
        } else {
            $Gender = "Female";
        }
    } else {
        $Gender = '';
    }

    if (isset($UserData[0]->PhotoFilePathName) && !empty($UserData[0]->PhotoFilePathName)) {
        $PhotoFilePathName = url().'/public/upload/userimage/'.$UserData[0]->PhotoFilePathName;
    } else {
        $PhotoFilePathName = url().'/resources/assets/admin/dist/img/default.png';
    }

    if (isset($UserData[0]->Language) && !empty($UserData[0]->Language)) {
        $Language = $UserData[0]->Language;
    } else {
        $Language = '';
    }

    if (isset($UserData[0]->Profile) && !empty($UserData[0]->Profile)) {
        $Profile = $UserData[0]->Profile;
    } else {
        $Profile = '';
    }

    if (isset($UserData[0]->RegisteredAt) && !empty($UserData[0]->RegisteredAt)) {
        $RegisteredAt = $UserData[0]->RegisteredAt;
    } else {
        $RegisteredAt = '';
    }

    if (isset($UserData[0]->LastLoggedInAt) && !empty($UserData[0]->LastLoggedInAt)) {
        $LastLoggedInAt = $UserData[0]->LastLoggedInAt;
    } else {
        $LastLoggedInAt = '';
    }

    if (isset($UserData[0]->NumLogins) && !empty($UserData[0]->NumLogins)) {
        $NumLogins = $UserData[0]->NumLogins;
    } else {
        $NumLogins = '0';
    }

    if (isset($UserData[0]->UserType) && !empty($UserData[0]->UserType)) {
        $UserType = $UserData[0]->UserType;
        if ($UserType == "1") {
            $UserRole = 'Administrator';
        } else {
            $UserRole = 'User';
        }
    } else {
        $UserType = '';
        $UserRole = '';
    }
?>

@if(!empty($UserData))
    <section class="content-header">
        <h1>{{ $Nickname }} Profile</h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('admin/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ url('admin/users') }}"> Users</a></li>
            <li class="active">{{ $Nickname }} Profile</li>
        </ol>
    </section>

    <section class="content admin-profile-page">
        <div class="row box2">
            <div class="col-md-3">
                <div class="box box-primary">
                    <div class="box-body box-profile">
                        <img class="profile-user-img img-responsive img-circle" src="{{ $PhotoFilePathName }}" alt="{{ $FirstName }} {{ $LastName }}">
                        <h3 class="profile-username text-center">
                            {{ $FirstName }} {{ $LastName }}
                        </h3>
                        <p class="text-muted text-center"></p>
                    </div>
                </div>
            </div>

            <div class="col-md-9">
                <div id="ValidationMsg"></div>
                <div class="nav-tabs-custom">
                    <div class="box box-primary">
                        <form action="#">
                            <div class="box-body">
                                <div class="form-group">
                                    <label>Name</label>
                                    <input type="text" value="{{ $FirstName }} {{ $LastName }}" class="form-control" readonly="readonly" disabled="disabled">
                                </div>

                                <div class="form-group">
                                    <label>Nickname</label>
                                    <input type="text" value="{{ $Nickname }}" class="form-control" readonly="readonly" disabled="disabled">
                                </div>

                                <div class="form-group">
                                    <label>Email</label>
                                    <input type="text" value="{{ $Email }}" class="form-control" readonly="readonly" disabled="disabled">
                                </div>

                                <div class="form-group">
                                    <label>Gender</label>
                                    <input type="text" value="{{ $Gender }}" class="form-control" readonly="readonly" disabled="disabled">
                                </div>

                                <div class="form-group">
                                    <label>Birthday</label>
                                    <input type="text" value="{{ $DateOfBirth }}" class="form-control" readonly="readonly" disabled="disabled">
                                </div>

                                <div class="form-group">
                                    <label>Profile</label>
                                    <textarea rows="5" cols="90" class="form-control" readonly="readonly" disabled="disabled">{{ $Profile }}</textarea>
                                </div>

                                <div class="form-group">
                                    <label>Registered At</label>
                                    <input type="text" value="{{ $RegisteredAt }}" class="form-control" readonly="readonly" disabled="disabled">
                                </div>

                                <div class="form-group">
                                    <label>Last Logged In At</label>
                                    <input type="text" value="{{ $LastLoggedInAt }}" class="form-control" readonly="readonly" disabled="disabled">
                                </div>

                                <div class="form-group">
                                    <label>Number of Logins</label>
                                    <input type="text" value="{{ $NumLogins }}" class="form-control" readonly="readonly" disabled="disabled">
                                </div>

                                <div class="form-group">
                                    <label>User Type</label>
                                    <input type="text" value="{{ $UserRole }}" class="form-control" readonly="readonly" disabled="disabled">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@else
    <section class="content-header">
        <ol class="breadcrumb">
            <li><a href="{{ url('admin/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Oops! Page not found</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="error-page">
            <h2 class="headline text-yellow"> 404</h2>
            <div class="error-content">
                <h3><i class="fa fa-warning text-yellow"></i> Oops! Page not found.</h3>
                <p> The page you are looking for has been moved or doesn't exist anymore. If you typed the URL directly, please make sure the spelling is correct.</p>

                <a href="{{ url('admin/dashboard') }}" class="btn btn-info btn-block">GO BACK TO DASHBOARD</a>
            </div>
        </div>
    </section>
@endif

@stop
