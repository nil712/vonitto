@extends('layouts.admin.adminMaster')
@section('content')
@section('title','add User')

<section class="content-header">
  <h1>
    <h3 class="box-title"><i class="fa fa-list"></i> Add User</h3>
  </h1>
  <ol class="breadcrumb">

    <li><a href="{{ url('admin/dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard </a></li>
    <li><a href="{{ url('admin/userlisting') }}"> Users</a></li>
    <li class="active">Add User</li>
  </ol>
</section>

<section class="content">
  <div class="row">
    <div class="col-md-12">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Personal Information</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form role="form" name="addUserData" action="{{ url('admin/user/addUser') }}" method="post">
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="box-body" style="display: block;">
                  <div class="row">
                      <div class="col-md-6">

                        <div class="form-group @if($errors->has('txtFirstName')) has-error @endif">
                          <label for="txtFirstName">First Name</label>
                          <input type="text" id="txtFirstName" name="txtFirstName" value="{{ old('txtFirstName') }}" placeholder="Enter First Name"  class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group @if($errors->has('txtLastName')) has-error @endif">
                          <label for="txtLastName">Last Name</label>
                          <input type="text" name="txtLastName" placeholder="Enter Last Name" value="{{ old('txtLastName') }}" id="txtLastName" class="form-control">
                        </div>
                      </div>
                  </div>
                  <div class="row">
                      <div class="col-md-6">
                        <div class="form-group @if($errors->has('email')) has-error @endif">
                          <label for="email">Email</label>
                          <input type="email" name="email" placeholder="Enter Email Address" value="{{ old('email') }}" id="" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group @if($errors->has('username')) has-error @endif">
                          <label for="txtUsername">Username</label>
                          <input type="text" name="username" placeholder="Enter Username"   value="{{ old('username') }}" class="form-control">
                        </div>
                      </div>
                  </div>
                  <div class="row">
                      <div class="col-md-6">
                        <div class="form-group @if($errors->has('password')) has-error @endif">
                          <label for="exampleInputEmail1">Password</label>
                          <input type="password" name="password" placeholder="Enter Password"  value="{{ old('password') }}" id="" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group  @if($errors->has('txtConfirmPassword')) has-error @endif">
                          <label for="exampleInputEmail1">Confirm Password</label>
                          <input type="password" name="txtConfirmPassword"   value="{{ old('txtConfirmPassword') }}" placeholder="Enter Confirm Password" id="" class="form-control">
                        </div>
                      </div>

                  </div>
                  <div class="row">
                      <div class="col-md-6">
                        <div class="form-group @if($errors->has('gender')) has-error @endif">
                          <label for="exampleInputEmail1">Gender</label>
                          <div class="input-group">
                            <label>
                              <input type="radio" value="Male" name="gender" class="minimal" checked>
                              Male
                            </label>
                            &nbsp;&nbsp;&nbsp;
                            <label>
                              <input type="radio" value="Female" name="gender" class="minimal">
                              Female
                            </label>
                          </div>
                    </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group @if($errors->has('txtDate')) has-error @endif">
                          <label for="exampleInputEmail1">Date of Birth</label>
                          <div class="input-group ">
                            <div class="input-group-addon">
                              <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" name="txtDate" id="datepicker"   value="{{ old('txtDate') }}" class="form-control datemask">
                          </div>

                        </div>
                      </div>
                  </div>
                </div>
                <div class="box-header with-border">
                  <h3 class="box-title">Address</h3>
                </div>
                <div class="box-body" style="display: block;">
                  <div class="row">
                      <div class="col-md-6">
                        <div class="form-group @if($errors->has('txtAddress')) has-error @endif">
                          <label for="exampleInputEmail1">Address</label>
                          <input type="text" name="txtAddress" placeholder="Enter Address"  value="{{ old('txtAddress') }}" id="" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group @if($errors->has('txtCity')) has-error @endif">
                          <label for="exampleInputEmail1">City</label>
                          <input type="text" name="txtCity" placeholder="Enter City" id=""   value="{{ old('txtCity') }}" class="form-control">
                        </div>
                      </div>
                  </div>
                  <div class="row">
                      <div class="col-md-6">
                        <div class="form-group @if($errors->has('txtState')) has-error @endif">
                          <label for="exampleInputEmail1">State</label>
                          <input type="text" name="txtState" placeholder="Enter State" id=""  value="{{ old('txtState') }}" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group @if($errors->has('txtCountry')) has-error @endif">
                          <label for="exampleInputEmail1">Country</label>
                          <input type="text" name="txtCountry" placeholder="Enter Country" id=""  value="{{ old('txtCountry') }}" class="form-control">
                        </div>
                      </div>
                  </div>
                  <div class="row">
                      <div class="col-md-6">
                        <div class="form-group @if($errors->has('txtZipcode')) has-error @endif">
                          <label for="exampleInputEmail1">Zipcode</label>
                          <input type="text" name="txtZipcode" placeholder="Enter Zipcode" id=""  value="{{ old('txtZipcode') }}" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group @if($errors->has('txtPhone')) has-error @endif">
                          <label for="exampleInputEmail1">Phone</label>
                          <input type="text" name="txtPhone" placeholder="Enter Phone" id=""   value="{{ old('txtPhone') }}" class="form-control">
                        </div>
                      </div>
                  </div>
                </div>

                <div class="box-header with-border">
                  <h3 class="box-title">Other Info</h3>
                </div>
                <div class="box-body" style="display: block;">
                  <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label for="exampleInputEmail1">Active</label>
                          <select class="form-control" name="txtStatus">
                                <option>Active</option>
                                <option>Inactive</option>
                          </select>
                        </div>
                      </div>

                  </div>
                </div>


              <div class="box-footer">

                <button class="btn btn-primary" type="submit">Submit</button>
              </div>
              </form>
              </div>
              <!-- /.box -->
    </div>
    <!-- /.box -->
  </div>
</section>
<link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.0/themes/ui-lightness/jquery-ui.css" rel="stylesheet" type="text/css" />
<script src="{{URL::asset('resources/assets/admin/dist/js/datepicker.js')}}"></script>
<script>
    	// date picker
		$(function() {
			$( "#datepicker" ).datepicker({
				   changeMonth: true,
			     changeYear: true,
			     dateFormat: 'yy-mm-dd',
			 	   maxDate: new Date(),
				   onSelect: function(dateText, inst) {
  				$(this).val("");
  				$(this).val(dateText)
				}
				});
			});
</script>
@stop
