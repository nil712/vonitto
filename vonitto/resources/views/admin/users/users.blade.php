@include('layouts.admin.AdminHeader')
@include('layouts.admin.AdminLeftSidebar')

<div class="content-wrapper users">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Users</h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('admin/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Pages</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content pages">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Users Table</h3>
                    </div>
                    <div class="box-body">
                        <table id="datatable" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th style="width:5%;">Id</th>
                                    <th style="width:10%;">Email</th>
                                    <th style="width:10%;">First Name</th>
                                    <th style="width:10%;">Last Name</th>
                                    <th style="width:10%;">Nickname</th>
                                    <th style="width:10%;">Birthday</th>
                                    <th style="width:10%;">Gender</th>
                                    <th style="width:10%;">RegisteredAt</th>
                                    <th style="width:10%;">Action</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                            <tfoot>
                                <tr>
                                    <th>Id</th>
                                    <th>Email</th>
                                    <th>First Name</th>
                                    <th>Last Name</th>
                                    <th>Nickname</th>
                                    <th>Birthday</th>
                                    <th>Gender</th>
                                    <th>RegisteredAt</th>
                                    <th>Action</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<!-- DataTables -->
<script src="{{ URL::asset('resources/assets/admin/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::asset('resources/assets/admin/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>

<script type="text/javascript">
var save_method; // For save method string
var dataTable;

$(document).ready(function() {
    dataTable = $('#datatable').DataTable({
        "processing": true,
        "order": [
            [0, "desc"]
        ],
        "oLanguage": {
            "sProcessing": "Loading records..."
        },
        "serverSide": true,
        //"scrollX": true,
        "columns": [
            null,
            null, null, null, null, null, null, {
                // "orderable": false,
                // "searchable": false
            }, {
                "orderable": false,
                "searchable": false
            },
        ],
        // Load data for the table's content from an Ajax source
        "ajax": {
            url: '<?php echo url()?>/admin/users/datatable',
            type: "post",
            "data": function(d) {

            },
            error: function() {

            }
        }
    });

    function reload_table() {
        dataTable.ajax.reload(null, false); // Reload datatable ajax
    }

    $(document.body).on('click', '.users .btn-change-status', function() {
        $('#ajax-loader').show();
        var user_id = $(this).attr("data-user-id");
        var status = $(this).attr("data-status");

        $.ajax({
            url: "<?php echo url()?>/admin/users/changeStatus",
            type: "POST",
            data: "user_id=" + user_id + '&status=' + status,
            success: function(data) {
                // If success reload ajax table
                $('#ajax-loader').hide();
                reload_table();
            },
            error: function(jqXHR, textStatus, errorThrown) {
                alert('Error adding / update data');
            }
        });
    });

    $(document.body).on('click', '.users .btn-del-rec', function() {
        $('#ajax-loader').show();
        var user_id = $(this).attr("data-user-id");

        if (confirm('Are you sure you want to delete this?')) {
            $.ajax({
                url: "<?php echo url()?>/admin/user/deleteUser",
                type: "POST",
                data: "user_id=" + user_id,
                success: function(data) {
                    // If success reload ajax table
                    $('#ajax-loader').hide();
                    reload_table();
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    alert('Error adding / update data');
                }
            });
        }
    });
});
</script>

@include('layouts.admin.AdminFooter')
