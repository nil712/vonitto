@extends('admin.layouts.AdminMaster')
@section('content')

<?php
    if (isset($UserData[0]->id) && !empty($UserData[0]->id)) {
        $UserIdx = $UserData[0]->id;
    } else {
        $UserIdx = '';
    }

    if (isset($UserData[0]->email) && !empty($UserData[0]->email)) {
        $Email = $UserData[0]->email;
    } else {
        $Email = '';
    }

    if (isset($UserData[0]->image) && !empty($UserData[0]->image)) {
        $PhotoFilePathName = url("/").'/public/upload/userimage/'.$UserData[0]->image;
    } else {
        $PhotoFilePathName = url("/").'/resources/assets/admin/dist/img/default.png';
    }

    if (isset($UserData[0]->first_name) && !empty($UserData[0]->first_name)) {
        $FirstName = $UserData[0]->first_name;
    } else {
        $FirstName = '';
    }

    if (isset($UserData[0]->last_name) && !empty($UserData[0]->last_name)) {
        $LastName = $UserData[0]->last_name;
    } else {
        $LastName = '';
    }
?>

@if(!empty($UserData))
    <section class="content-header">
        <h1> Edit Profile: {{ $FirstName }} </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('admin/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ url('admin/users') }}"> Users</a></li>
            <li class="active">Edit Profile: {{ $FirstName }} </li>
        </ol>
    </section>

    <section class="content admin-profile-page">
        <div class="row box2">
            <div id="ajax-loader" class="overlay none">
                <i class="fa fa-refresh fa-spin"></i>
            </div>
            <div class="col-md-3">
                <div class="box box-primary">
                    <div class="box-body box-profile">
                        <img class="profile-user-img img-responsive img-circle" src="{{ $PhotoFilePathName }}" alt="{{ $FirstName }} {{ $LastName }}">
                        <h3 class="profile-username text-center">
                            {{ $FirstName }} {{ $LastName }}
                        </h3>
                        <p class="text-muted text-center"></p>
                    </div>
                </div>
            </div>
            <div class="col-md-9">
                <div id="ValidationMsg"></div>
                <div id="messageAlert" style="display:none;"></div>
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#general" data-toggle="tab">General</a></li>
                        <li><a href="#media" data-toggle="tab">Media</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="general">
                            <form name="FrmUpdateGeneralData" id="FrmUpdateGeneralData" class="form-horizontal" method="post" action="{{ url('admin/user/general/update') }}">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                <div class="form-group">
                                    <label for="ID" class="col-sm-3 control-label">Email</label>
                                    <div class="col-sm-9">
                                        <input type="text" name="email" id="email" value="{{ $Email }}" class="form-control" readonly="readonly" >
                                        <span class="has-error id-error"></span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="Password" class="col-sm-3 control-label">Password</label>
                                    <div class="col-sm-9">
                                        <input type="password" name="password" id="password" value="" class="form-control">
                                        <span class="has-error password-error"></span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="PasswordConfirmation" class="col-sm-3 control-label">Password Confirmation</label>
                                    <div class="col-sm-9">
                                        <input type="password" name="password_confirmation" id="password_confirmation" value="" class="form-control">
                                        <span class="has-error confirmation-password-error"></span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="first_name" class="col-sm-3 control-label">Firstname</label>
                                    <div class="col-sm-9">
                                        <input type="text" name="first_name" id="first_name" value="{{ $FirstName }}" class="form-control">
                                        <span class="has-error firstname-error"></span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="last_name" class="col-sm-3 control-label">Lastname</label>
                                    <div class="col-sm-9">
                                        <input type="text" name="last_name" id="last_name" value="{{ $LastName }}" class="form-control">
                                        <span class="has-error lastname-error"></span>
                                    </div>
                                </div>

                                <div class="box-footer">
                                    <input type="hidden" name="id" id="id" value="{{ $UserIdx }}" />
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </form>
                        </div>

                        <div class="tab-pane" id="media">
                            <form name="FrmUpdateMediaData" id="FrmUpdateMediaData" class="form-horizontal" method="post" enctype="multipart/form-data" action="{{ url('admin/user/media/update') }}">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Profile Picture</label>
                                    <input type="file" name="image" accept="image/*">
                                    <p class="has-error image-error"></p>
                                </div>

                                <div class="box-footer">
                                    <input type="hidden" name="id" id="id" value="{{ $UserIdx }}" />
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@else
    <section class="content-header">
        <ol class="breadcrumb">
            <li><a href="{{ url('admin/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Oops! Page not found</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="error-page">
            <h2 class="headline text-yellow"> 404</h2>
            <div class="error-content">
                <h3><i class="fa fa-warning text-yellow"></i> Oops! Page not found.</h3>
                <p> The page you are looking for has been moved or doesn't exist anymore. If you typed the URL directly, please make sure the spelling is correct.</p>

                <a href="{{ url('admin/dashboard') }}" class="btn btn-info btn-block">GO BACK TO DASHBOARD</a>
            </div>
        </div>
    </section>
@endif

@stop
