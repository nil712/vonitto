<!DOCTYPE html>

<html lang="en-US">
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
   <head>
      <!-- include header files  -->
      @include('layouts.header')
   </head>
   <body class="fixed-header">
   <div id="page" class="hfeed site inner_padding">
     <header id="masthead" class="site-header" role="banner">
       @include('layouts.inner.header_inner_menu')
     </header>
     <!-- #masthead -->
   </div>

   @if(Auth::user()->role == 'JOB_SEEKER')
   <?php
        $subscription = App\Subscription::all()->toArray();
   ?>
   <section id="vonitto_widget_jobs_spotlight-6" class="widget howitworks widget--home vonitto_widget_jobs_spotlight widget--home-job-spotlights">
     <div class="container">
       <h3 class="widget-title widget-title--home">Find the membership that works for you</h3>
       <h4 class="howtxt">Free for one pitch a month</h4>
          @foreach($subscription as $subscriptionData)
          <div class="col-lg-4 col-md-4 col-sm-6">
            <!--whitebox-->
              <div class="whitebox">
                  <div class="cntent">
                    <form name="frmPackage{{$subscriptionData['id']}}" method="post" action="{{url('package/payment')}}">
                      <h2>GBP {{$subscriptionData['subscription_amount']}} Membership Fee/month for {{$subscriptionData['free_for_month']}} free + additional {{$subscriptionData['subscription_applicable_for']}} pitches</h2>
                      <!-- <a href="{{url('payment')}}" class="application_button button">Buy Now</a> -->
                      <input type="checkbox" name="recurring_checkbox"><span>Recurring Payment</span>
                      <input type="hidden" name="_token" id="csrf-token" value="{{ csrf_token() }}" />
                      <input type="hidden" name="package_id" value="{{$subscriptionData['id']}}">
                      <input type="submit" name="btnSubmit{{$subscriptionData['id']}}" value="PITCH NOW">
                    </form>
                  </div>

              </div>
              <!--whitebox-->
          </div>
          @endforeach
          <p class="para">For more pitches per month or for Company Rates, please contact us directly at <a href="mailto:info@pitchyourconcepts.com"> info@pitchyourconcepts.com </a> </p>
     </div>
   </section>
   @endif
   @if(Auth::user()->role == 'JOB_POSTER')
   <div id="main" class="site-main">
      <header class="page-header">
          <h2 class="page-title">My Account</h2>
      </header>
      <section id="vonitto_widget_jobs_spotlight-6" class="widget howitworks widget--home vonitto_widget_jobs_spotlight widget--home-job-spotlights">
      <div class="container">
        <!--tool_tab-->
         <div class="tool_tab">
            <ul id="myTab" class="nav nav-tabs">
               <li class="active"><a href='#tab1' data-toggle="tab">My Profile</a></li>
               <li><a href='#tab2' data-toggle="tab">My Jobs</a></li>
               <li><a href='#tab3' data-toggle="tab">Jobs Awarded</a></li>
                <li><a href='#tab4' data-toggle="tab">Jobs Completed</a></li>
                <li><a href='#tab5' data-toggle="tab">Settings</a></li>
             </ul>

            <div class="tab-content" id="myTabContent">
              <div id='tab1' class="tab-pane fade in active">
                  <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                      <div class="leftSide">
                      <img src="{{$userDetail['image']}}">
                      </div>
                  </div>
                  <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 pull-right">
                    <div class="rightSide">
                        <h4>{{$userDetail['first_name']}} {{$userDetail['last_name']}}</h4>
                        <ul>
                        <li>
                          <label class="lbl">Email</label>
                          <span>{{$userDetail['email']}}</span>
                        </li>
                        <li>
                          <label class="lbl">Account Start Date</label>
                          <span>{{date('d-M-Y',strtotime($userDetail['created_at']))}}</span>
                        </li>
                        <!-- <li>
                          <label class="lbl">Remaining Pitch Request</label>
                          <span>{{($userDetail['available_pitch']!=NULL)?$userDetail['available_pitch']:0}}</span>
                        </li> -->
                        </ul>
                    </div>
                </div>
              </div>
              <div id='tab2' class="tab-pane fade">
                 <div class="jobsPackages">
                     @if(isset($myJobs) && !empty($myJobs))
                         <div class='tablediv'>
                           <table>
                           	<thead>
                             <tr>
                               <th>No.</th>
                               <th>Title</th>
                               <th>Deadline</th>
                               <th>Budget</th>
                               <th>Pitch Request</th>
                               <th>Progress Status</th>
                               <th>Date</th>
                               <th>Action</th>
                             </tr>
                             </thead>
                             {{--*/$i=1/*--}}
                             <tbody>
                             @foreach($myJobs as $job)
                               <tr>
                                 <td data-name="No.">{{$i++}}</td>
                                 <td data-name="Title"><a href="{{url('/job/detail/'.$job->job_id)}}">{{$job->title}}</a></td>
                                 <td data-name="Deadline">{{$job->project_deadline}}</td>
                                 <td data-name="Bidget">{{$job->budget}} {{$job->currency}}</td>
                                 <td data-name="Pitch Request">{{$job->pitched_request}}</td>
                                 <td data-name="Progress Status">{{$job->progress_status}}</td>
                                 <td data-name="Date">{{$job->created_at}}</td>
                                 <td data-name="Date"><a href="{{url('jobposter/jobpost',array($job->job_id))}}/edit"><img src="{{URL::asset('resources/assets/images/edit.png')}}" /></a></td>
                               </tr>
                             @endforeach
                              </tbody>
                           </table>
                         </div>
                       @else
                     <p>No record found</p>
                   @endif
    	           </div>
               <div id="page-selection"></div>
              </div>
              <div id='tab3' class="tab-pane fade">
               <div class="awardedJobs">
                 @if(isset($awardedJobs) && !empty($awardedJobs))
                     <div class='tablediv'>
                        <table>
                       <thead>
                       <tr>
                         <th>No.</th>
                         <th>Job Title</th>
                         <th>Seeker Name</th>
                         <th>Job Progress Status</th>
                         <th>Date</th>
                       </tr>
                       </thead>
                       {{--*/$i=1/*--}}
                       <tbody>


                       @foreach($awardedJobs as $job)

                         <tr>
                           <td data-name="No.">{{$i++}}</td>
                           <td data-name="Job Title"><a href="{{url('/job/detail/'.$job->job_id)}}">{{$job->title}}</a></td>
                           <td data-name="Seeker Name"><a href="{{url('user/profile/'.$job->user_id)}}">{{$job->first_name}} {{$job->last_name}}</a></td>
                           <td data-name="Job Progress Status">{{$job->progress_status}}</td>
                           <td data-name="Date">{{date('d-M-Y',strtotime($job->created_at))}}</td>
                         </tr>

                       @endforeach
                       </tbody>
                     </table>
                    </div>
                   @else
                   <p>No record found</p>
                 @endif
               </div>
               <div id="page-selection2"></div>
              </div>
              <div id='tab4' class="tab-pane fade">
               <div class="completedJob">
                 @if(isset($completedJob) && !empty($completedJob) && count($completedJob)>0)
                     <div class='tablediv'>
                        <table id="ajax-complete">
                       <thead>
                       <tr>
                         <th>No.</th>
                         <th>Job Title</th>
                         <th>Seeker Name</th>
                         <th>Email</th>
                         <th>Mobile</th>
                         <th>Rating</th>
                       </tr>
                       </thead>
                       {{--*/$i=1/*--}}
                       <tbody>


                       @foreach($completedJob as $job)

                         <tr>
                           <td data-name="No.">{{$i++}}</td>
                           <td data-name="Job Title"><a href="{{url('/job/detail/'.$job->job_id)}}">{{$job->title}}</a></td>
                           <td data-name="Seeker Name"><a href="{{url('user/profile',array($job->job_seeker_id))}}">{{$job->first_name}} {{$job->last_name}}</a></td>
                           <td data-name="Job Progress Status">{{$job->email}}</td>
                           <td data-name="Job Progress Status">{{$job->phone}}</td>
                           <td data-name="Date"><a href="javascript:;" class="reviewRating" data-job="{{$job->job_id}}" data-rating="{{$job->rating}}" data-review="{{stripslashes($job->review)}}" data-seeker="{{$job->job_seeker_id}}" >@if(isset($job->rating) && !empty($job->rating)) {{$job->rating}}  @else 0 @endif star </a></td>
                         </tr>

                       @endforeach
                       </tbody>
                     </table>
                    </div>
                   @else
                   <p>No record found</p>
                 @endif
               </div>
               <div id="page-selection4"></div>
              </div>

              <div id='tab5' class="tab-pane fade">


                <div class="selection JobSetting">
                  <h3>I’d like to be contacted when a job poster had submitted a pitch.</h3>
                  <ul>
                    <li>
                        <input type="radio" name="notificationPreference" id="notificationPreference1" value="Immediately">
                        <label for="notificationPreference1"> Immediately </label>
                    </li>

                    <li>
                        <input type="radio" name="notificationPreference" id="notificationPreference2" value="Daily">
                        <label for="notificationPreference2"> Daily </label>
                    </li>

                    <li>
                        <input type="radio" name="notificationPreference" id="notificationPreference3" value="Weekly">
                        <label for="notificationPreference3"> Weekly </label>
                    </li>
                  </ul>
                </div>

                <div class="selection JobSetting">
                  <ul>
                    <li>
                        <input type="checkbox" name="canContact1" id="canContact1" value="1">
                        <label for="canContact1"> I’d like to be contacted on offers and promotions from pitchyourconcepts.com </label>
                    </li>

                    <li>
                      <input type="checkbox" name="canContact2" id="canContact2" value="1">
                      <label for="canContact2"> I’d like to be contacted on offers and promotions from affiliated companies by pitchyourconcepts.com </label>
                    </li>

                  </ul>
                </div>


               <div id="page-selection5"></div>
              </div>

            </div>
        </div>
      </div>
    </section>
  </div>
 @endif
 <div id="reviewModel" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Review & Rating</h4>
      </div>
      <form  class="frmReviewRating" name="frmReviewRating" id="frmReviewRating"  role="form" method="POST" >
        <div class="messageBox" style="display:none;"></div>

        <div class="modal-body">
          <p class="form-row form-row-wide">
              <label for="comment">Select Rating</label>
              <div id="rateYo" class="rateyo"></div>
         </p>
         <p class="comment-form-comment">
           <label for="comment">Your Review</label>
           <textarea style="resize:none;" aria-required="true" rows="8" cols="45" name="review" id="review" name="review"></textarea>
         </p>


        </div>
        <div class="modal-footer">
          <input type="hidden"  id="rating" class="rating" name="rating" />
          <input type="hidden"  id="job_id" class="job_id" name="job_id" />
          <input type="hidden"  id="seeker_id" class="seeker_id" name="seeker_id" />
          <button type="submit" class="btn btn-primary submitReview" id="submitReview" value="Submit">Submit</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
     </form>
    </div>

  </div>
</div>
 <input type="hidden" value="{{csrf_token()}}" id="token" name="_token">
  <!--
 <link href="{{URL::asset('resources/assets/css/pages/seeker_my_account.css')}}" rel="stylesheet" type="text/css"/>
 -->
 <link href="{{URL::asset('resources/assets/css/bootstrap.css')}}" rel="stylesheet" type="text/css"/>
 <script src="{{URL::asset('resources/assets/js/jquery.js')}}"></script>
 <script src="{{URL::asset('resources/assets/js/bootpag.min.js')}}"></script>
 <script src="{{URL::asset('resources/assets/js/bootstrap.min.js')}}"></script>
 <script src="{{URL::asset('resources/assets/js/bootbox.min.js')}}"></script>
 <script src="{{URL::asset('resources/assets/js/pages/seeker_my_account.js')}}"></script>
 <script src="{{URL::asset('resources/assets/js/jQuery-validation-plugin/jquery.validate.js')}}"></script>
 <script src="{{URL::asset('resources/assets/js/jQuery-validation-plugin/additional-methods.js')}}"></script>
 <script type="text/javascript" src="{{URL::asset('resources/assets/js/rated/src/jquery.rateyo.js')}}"></script>
 <script src="{{URL::asset('resources/assets/js/pages/poster/account.js')}}"></script>
 <script src="{{URL::asset('resources/assets/js/pages/bootstrap.min.js')}}"></script>.

  <!-- Bootstrap TabCollapse-->
 <script type="text/javascript" src="{{URL::asset('resources/assets/js/pages/bootstrap-tabcollapse.js')}}"></script>
 <script type="text/javascript">
 	 $('#myTab').tabCollapse();
 </script>
  <!-- Bootstrap TabCollapse-->
 <script>
 $(document).ready(function(){

   $('#page-selection').bootpag({
     total: {{$jobsPages}}
   }).on("page", function(event,num){
       var token = $("#token").val();
       $.ajax({
         type: "GET",
         headers: {'X-CSRF-TOKEN': token},
         url: "{{url('myaccount/posterJobs')}}"+"/"+num,
         cache: false,
         beforeSend: function () {

         },
        success: function(data) {
         if(data!="0"){
           $(".jobsPackages").html("");
           $(".jobsPackages").html(data);
         }
        }
      });
   });

   $('#page-selection1').bootpag({
     total: {{$requestPages}}
   }).on("page", function(event,num){
     var token = $("#token").val();
     $.ajax({
       type: "GET",
       headers: {'X-CSRF-TOKEN': token},
       url: "{{url('myaccount/loadPosterPitchJobs')}}"+"/"+num,
       cache: false,
       beforeSend: function () {

       },
      success: function(data) {
       if(data!="0"){
         $(".pitchedJobs").html("");
         $(".pitchedJobs").html(data);
       }
      }
    });
   });

   $('#page-selection2').bootpag({
     total: {{$awardedJobsPages}}
   }).on("page", function(event,num){
     var token = $("#token").val();
     $.ajax({
       type: "GET",
       headers: {'X-CSRF-TOKEN': token},
       url: "{{url('myaccount/loadAwardedJobs')}}"+"/"+num,
       cache: false,
       beforeSend: function () {
       },
      success: function(data) {
       if(data!="0"){
         $(".awardedJobs").html("");
         $(".awardedJobs").html(data);
       }
      }
    });
   });

   //Developer Name: Hitesh Tank, Date: 29 July, 2016
   $('#page-selection4').bootpag({
     total: {{$completedJobsPages}}
   }).on("page", function(event,num){
     var token = $("#token").val();
     $.ajax({
       type: "GET",
       headers: {'X-CSRF-TOKEN': token},
       url: "{{url('myaccount/load-completed-job')}}"+"/"+num,
       cache: false,
       beforeSend: function () {
       },
      success: function(data) {
       if(data!="0"){
         $(".completedJob").html("");
         $(".completedJob").html(data);
       }
      }
    });
   });


   $(document).on('click','.btnAward',function(){
     var obj = $(this);
     bootbox.confirm("Are you sure?",function(option){
       if(option){
         var job_id = $(obj).data("job");
         var user_id = $(obj).data("user");
        //  console.log(job_id+" "+user_id);
         var token = $("#token").val();
         var dataString = "job_id="+job_id+"&job_seeker_id="+user_id;
        $.ajax({
          type: "POST",
          headers: {'X-CSRF-TOKEN': token},
          url: "{{url('jobposter/assignJob')}}",
          data:dataString,
          cache: false,
          beforeSend: function () {

          },
         success: function(data) {
          if(data=="1"){
            $(obj).val("Awarded");
            $(obj).addClass("green");
            $(".btnAward[data-job='"+job_id+"']").removeClass("btnAward");
            $(".btnAward[data-job='"+job_id+"']").addClass("btnAssigned");
            $(".btnAward[data-job='"+job_id+"']").val("Awarded");
          }
         }
       });
       }
     });
   });
});

</script>
</body>
</html>
