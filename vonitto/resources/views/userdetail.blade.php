<!DOCTYPE html>
<html lang="en-US">
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <head>
        <!-- include header files  -->
        @include('layouts.header')
        <style>
            .rating-section {
                margin-bottom: 30px;
                margin-top: -15px;
                padding-left: 161px;
            }
        </style>
    </head>

    <body class="fixed-header">
        <div id="page" class="hfeed site inner_padding">
            <header id="masthead" class="site-header" role="banner">
                @include('layouts.inner.header_inner_menu')
            </header>
            <div class="container">
                <section id="vonitto_widget_jobs_spotlight-6" class="widget howitworks widget--home vonitto_widget_jobs_spotlight widget--home-job-spotlights">
                    <div class="userDetail">
                        <div class="topDiv">
                            <div class="imageSection">
                                @if($selectedUserDetail['image']!="")
                                <img src="{{URL::asset('resources/assets/upload/'.$selectedUserDetail['image'])}}">
                                @else
                                <img src="{{URL::asset('resources/assets/images/signup_ic_1.png')}}">
                                @endif
                            </div>
                            <div class="contentSection">
                                <span>{{$selectedUserDetail['first_name']}} {{$selectedUserDetail['last_name']}}</span>
                                <span><a href="{{$selectedUserDetail['company_link']}}">{{$selectedUserDetail['company_name']}}</a></span>
                            </div>
                        </div>
                        <div class="bottomDiv">
                            <ul>
                                @if(Auth::user()->role != 'JOB_SEEKER')
                                <li class="rating-section">
                                    <div id="rateYo" class="rateyo"></div>
                                </li>
                                @endif

                                @if(isset($selectedUserDetail['email']) && !empty($selectedUserDetail['email']))
                                <li>
                                    <label>
                                        Email
                                    </label>
                                    <p>
                                        <a href="mailto:{{$selectedUserDetail['email']}}">{{$selectedUserDetail['email']}}</a>
                                    </p>
                                </li>
                                @endif

                                @if(isset($selectedUserDetail['zip']) && !empty($selectedUserDetail['zip']))
                                <li>
                                    <label>
                                        Pincode
                                    </label>
                                    <p>
                                        {{$selectedUserDetail['zip']}}
                                    </p>
                                </li>
                                @endif
                                @if(isset($selectedUserDetail['user']) && $selectedUserDetail['user'] =='job_seeker')

                                    @if(isset($selectedUserDetail['experience']) && !empty($selectedUserDetail['experience']))
                                    <li>
                                        <label>
                                            Experience
                                        </label>
                                        <p>
                                            {{$selectedUserDetail['experience']}} Year
                                        </p>
                                    </li>
                                    @endif
                                    @if(isset($selectedUserDetail['graduation']) && !empty($selectedUserDetail['graduation']))
                                    <li>
                                        <label>
                                            Graduation
                                        </label>
                                        <p>
                                            {{$selectedUserDetail['graduation']}}
                                        </p>
                                    </li>
                                    @endif
                                    @if(isset($selectedUserDetail['post_graduation']) && !empty($selectedUserDetail['post_graduation']))
                                    <li>
                                        <label>
                                            Post Graduation
                                        </label>
                                        <p>
                                            {{$selectedUserDetail['post_graduation']}}
                                        </p>
                                    </li>
                                    @endif
                                    @if(isset($selectedUserDetail['computer_skill']) && !empty($selectedUserDetail['computer_skill']))
                                    <li>
                                        <label>
                                            Computer Skill
                                        </label>
                                        <p>
                                            {{$selectedUserDetail['computer_skill']}}
                                        </p>
                                    </li>
                                    @endif
                                    @if(isset($selectedUserDetail['project_category_specialization']) && !empty($selectedUserDetail['project_category_specialization']))
                                    <li>
                                        <label>
                                            Project Category Specialization
                                        </label>
                                        <p>
                                            {{$selectedUserDetail['project_category_specialization']}}
                                        </p>
                                    </li>
                                    @endif

                                    @if(isset($selectedUserDetail['portfolio_image']) && !empty($selectedUserDetail['portfolio_image']))
                                    <li>
                                        <label>
                                            Portfolio Image
                                        </label>
                                        <p>
                                            {{--*/$imagePath="";/*--}}
                                            @if(isset($selectedUserDetail['portfolio_image']) && !empty($selectedUserDetail['portfolio_image']))
                                            {{--*/ $base_url=URL::asset('resources/assets/upload/portfolio/'.$selectedUserDetail['portfolio_image']); /*--}}
                                            {{--*/ $file_headers = @get_headers($base_url); /*--}}
                                            @if($file_headers[0] == 'HTTP/1.1 404 Not Found')
                                            {{--*/ $imagePath=URL::asset('resources/assets/images/no-image-found.gif')/*--}}
                                            @else
                                            {{--*/ $imagePath=$base_url; /*--}}
                                            @endif
                                            @else
                                            {{--*/ $imagePath=URL::asset('resources/assets/images/no-image-found.gif') /*--}}
                                            @endif
                                            <a href="javascript:;" class="portfolioImage" data-url="{{$imagePath}}"><img src="{{$imagePath}}" height="width:200px;heigth:200px;" /></a>
                                        </p>
                                    </li>
                                    @endif
                                    @if(isset($selectedUserDetail['portfolio_description']) && !empty($selectedUserDetail['portfolio_description']))
                                    <li>
                                        <label>Portfolio Description</label>
                                        <p>
                                            {{stripslashes($selectedUserDetail['portfolio_description'])}}
                                        </p>
                                    </li>
                                    @endif
                                @endif
                            </ul>
                            <br /><br />

                            @if(isset($user_reviews_ratings) && $selectedUserDetail['user'] =='job_seeker' && count($user_reviews_ratings) > 0)
                                <label>Reviews and Ratings</label>
                                @foreach($user_reviews_ratings as $user_reviews)
                                    <div>

                                        @if(Auth::user()->role != 'JOB_SEEKER')
                                            <p>{{$user_reviews->first_name}} {{$user_reviews->last_name}}
                                                <span id="user_rating_{{$user_reviews->id}}"></span>
                                            </p>
                                            <p>{{$user_reviews->review}}</p>
                                        @endif
                                    </div>
                                @endforeach
                            @endif
                              <br />
                              <br />
                              <br />
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <div id="imageModel" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Portfolio</h4>
                    </div>
                    <form  class="frmReviewRating" name="frmReviewRating" id="frmReviewRating"  role="form" method="POST" >
                        <div class="modal-body">
                            <img class="image-view" src="" style="width:100%;max-width:100%;" />
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <link href="{{URL::asset('resources/assets/css/bootstrap.css')}}" rel="stylesheet" type="text/css"/>
        <script src="{{URL::asset('resources/assets/js/jquery.js')}}"></script>
        <script src="{{URL::asset('resources/assets/js/bootpag.min.js')}}"></script>
        <script src="{{URL::asset('resources/assets/js/bootstrap.min.js')}}"></script>
        <script src="{{URL::asset('resources/assets/js/bootbox.min.js')}}"></script>
        <script src="{{URL::asset('resources/assets/js/pages/seeker_my_account.js')}}"></script>
        <script src="{{URL::asset('resources/assets/js/jQuery-validation-plugin/jquery.validate.js')}}"></script>
        <script src="{{URL::asset('resources/assets/js/jQuery-validation-plugin/additional-methods.js')}}"></script>
        <script type="text/javascript" src="{{URL::asset('resources/assets/js/rated/src/jquery.rateyo.js')}}"></script>
        <script src="{{URL::asset('resources/assets/js/pages/poster/account.js')}}"></script>
        <script src="{{URL::asset('resources/assets/js/pages/bootstrap.min.js')}}"></script>

        <script>
            var rating = 0;
            @if (isset($selectedUserDetail['average_rating']) && !empty($selectedUserDetail['average_rating']))
                    rating = {{$selectedUserDetail['average_rating']}};
            @endif
                    $rateYo = $("#rateYo").rateYo({rating:rating, halfStar:true, ratedFill: "#222638", readOnly:true});
            $(document).on("click", ".portfolioImage", function(event){
                $("#imageModel").modal("show");
                $("#imageModel").find(".image-view").attr("src", $(this).attr("data-url"));
            });

            @foreach($user_reviews_ratings as $user_reviews_ratings)
                var user_rating = {{$user_reviews_ratings->rating}};
                $("#user_rating_{{$user_reviews_ratings->id}}").rateYo({rating:user_rating, halfStar:true, ratedFill: "#222638", readOnly:true});
            @endforeach
        </script>
    </body>
</html>
