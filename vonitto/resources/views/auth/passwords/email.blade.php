<!DOCTYPE html>
<html lang="en-US">
  <head>
    <!-- include header files  -->
    @include('layouts.header')
  </head>
<body class="fixed-header login_page">
  <div id="page" class="hfeed site inner_padding">
    <header id="masthead" class="site-header" role="banner">
      @include('layouts.header_menu')
    </header><!-- #masthead -->
    <div id="main" class="site-main">
      <header class="page-header">
      <h2 class="page-title">Reset Password</h2>
      </header>
      <!-- Main Content -->
      <div class="container">
        <div class="row">
          <div class="col-md-8 col-md-offset-2">

              <div class="panel-body loginsignupmain">
              @if (session('status'))
              <div class="alert alert-success">
              {{ session('status') }}
              </div>
              @endif
                <form class="form-horizontal" role="form" method="POST" action="{{ url('/password/email') }}">
                {!! csrf_field() !!}
                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <!-- <label class="col-md-4 control-label">E-Mail Address</label> -->
                <div class="form-row form-row-wide">
                <input type="email" class="input-text" placeholder="Enter Email" name="email" value="{{ old('email') }}">
                <img src="{{URL::asset('resources/assets/images/icon-3.png')}}" class="icon">
                @if ($errors->has('email'))

                <span class="help-block">
                <strong>{{ $errors->first('email') }}</strong>
                </span>
                @endif
                </div>
                </div>
                  <div class="form-group">

                      <button type="submit" class="button">
                        <i class="fa fa-btn fa-envelope"></i>Send Password Reset Link
                      </button>

                  </div>
                </form>
              </div>
            </div>
          </div>

      </div>
    </div>
  </div>
</body>
</html>
