<!DOCTYPE html>
<html lang="en-US">
  <head>
    <!-- include header files  -->
    @include('layouts.header')
  </head>
<body class="fixed-header login_page">
  <div id="page" class="hfeed site inner_padding">
    <header id="masthead" class="site-header" role="banner">
      @include('layouts.header_menu')
    </header><!-- #masthead -->
    <div id="main" class="site-main">
      <header class="page-header">
      <h2 class="page-title">Reset Password</h2>
      </header>
      <!-- Main Content -->
      <div class="container">
        <div class="row">
          <div class="col-md-8 col-md-offset-2">

              <div class="panel-body loginsignupmain">


                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/password/reset') }}">
                        {!! csrf_field() !!}

                        <input type="hidden" name="token" value="{{ $token }}">

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <!-- <label class="col-md-4 control-label">E-Mail Address</label> -->

                            <div class="form-row form-row-wide">
                                <input type="email" class="input-text" placeholder="Enter Email " name="email" value="{{ $email or old('email') }}">
                                <img src="{{URL::asset('resources/assets/images/icon-3.png')}}" class="icon">
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <!-- <label class="col-md-4 control-label">Password</label> -->

                            <div class="form-row form-row-wide">
                                <input type="password" class="input-text" placeholder="Password" name="password">
                                <img src="{{URL::asset('resources/assets/images/icon-2.png')}}" class="icon">
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            <!-- <label class="col-md-4 control-label">Confirm Password</label> -->
                            <div class="form-row form-row-wide">
                                <input type="password" class="input-text"placeholder="Confirm Password" name="password_confirmation">
                                <img src="{{URL::asset('resources/assets/images/icon-2.png')}}" class="icon">
                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                                <button type="submit" class="button">
                                    <i class="fa fa-btn fa-refresh"></i>Reset Password
                                </button>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
