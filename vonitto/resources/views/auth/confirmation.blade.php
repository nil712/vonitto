@extends('layouts.app')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default loginpage">
				<div class="panel-heading heading">Welcome</div>
					<p class="welcomeNote">Congratulation...!!! Your account is verified successfully. Your account will active soon.</p>
			</div>
		</div>
	</div>
</div>
@endsection
