@if(isset($awardedJobs) && !empty($awardedJobs))
<div class='tablediv'>
    <table class="ajax-completed">
      <thead>
      <tr>
        <th>No.</th>
        <th>Job Title</th>
        <th>Job Description</th>
        <th>Job Poster</th>
        <th>Project Deadline</th>
        <th>Awarded Date</th>
          <th>Status</th>
      </tr>
    </thead>
      {{--*/$i=1/*--}}
      @foreach($awardedJobs as $job)
      <tbody>
        <tr>
          <td>{{$i++}}</td>
          <td><a href="{{url('/job/detail/'.$job->job_id)}}">{{$job->title}}</a></td>
          <td>{{$job->description}}</td>
          <td><a href="{{url('user/profile/'.$job->job_poster_id)}}">{{$job->first_name}} {{$job->last_name}}</a></td>
          <td>{{date('d-M-Y',strtotime($job->project_deadline))}}</td>
          <td>{{date('d-M-Y',strtotime($job->created_at))}}</td>
          <td><select class="dropdown job-status">
                <option data-project="{{$job->job_id}}" data-posterid="{{$job->job_poster_id}}" @if($job->progress_status == 'Assigned')  selected="selected" @endif value="Assigned">Assigned</option>
                <option data-project="{{$job->job_id}}" data-posterid="{{$job->job_poster_id}}" @if($job->progress_status == 'Completed') selected="selected" @endif value="Completed">Completed</option>
              <select>
          </td>
        </tr>
      </tbody>
      @endforeach
    </table>
  </div>
  @else
@endif
