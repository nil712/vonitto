@if(isset($pitchedJobs) && !empty($pitchedJobs))
<div class='tablediv'>
    <table>
      <thead>
      <tr>
        <th>No.</th>
        <th>Job Title</th>
        <th>Job Description</th>
        <th>Project Deadline</th>
        <th>Date</th>
      </tr>
    </thead>
      {{--*/$i=1/*--}}
      @foreach($pitchedJobs as $job)
      <tbody>
        <tr>
          <td>{{$i++}}</td>
          <td><a href="{{url('/job/detail/'.$job->job_id)}}">{{$job->title}}</a></td>
          <td>{{substr($job->description,0,50)}}</td>
          <td>{{date('d-M-Y',strtotime($job->project_deadline))}}</td>
          <td>{{date('d-M-Y',strtotime($job->date))}}</td>
        </tr>
      </tbody>
      @endforeach
    </table>
  </div>
  @else
@endif
