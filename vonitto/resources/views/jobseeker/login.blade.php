<!DOCTYPE html>
<html lang="en-US">
<head>
  <!-- include header files  -->
  @include('layouts.header')
</head>
<body class="fixed-header login_page">

	<div id="page" class="hfeed site inner_padding">
    <header id="masthead" class="site-header" role="banner">
       @include('layouts.header_menu')
    </header><!-- #masthead -->

    <div id="main" class="site-main">
      <header class="page-header">
          <h2 class="page-title">Job Seeker Login</h2>
      </header>
      <div id="primary" class="content-area container" role="main">
      <article id="post-2454" class="post-2454 page type-page status-publish hentry">
      <div class="entry-content">
          <div class="woocommerce">

  <div class="woocommerce-customer-login loginsignupmain">

  		<h2>Login</h2>

  		<form class="login" id="formLoginposter"  role="form" method="POST" action="{{ url('/auth/login') }}">
        <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
  			<div class="form-row form-row-wide">

             <!-- <label for="email">Email<span class="required">*</span></label>-->
  <input type="email" class="input-text" name="email" id="email" placeholder="EMAIL" value="{{ old('email') }}"/>
  <img class="icon" src="{{URL::asset('resources/assets/images/icon-1.png')}}"   />
  @if ($errors->has('email'))<p class="error_message">{!!$errors->first('email')!!}</p>@endif
            </div>

<div class="form-row form-row-wide">
		<!--<label for="password">Password <span class="required">*</span></label>-->
  				<input class="input-text" type="password" name="password" id="password" placeholder="PASSWORD"/>
                <img class="icon" src="{{URL::asset('resources/assets/images/icon-2.png')}}"   />
          @if ($errors->has('password'))<p class="error_message">{!!$errors->first('password')!!}</p>@endif

</div>
<div class="form-row" style="text-align:left;">
  <input type="hidden" id="role" name="role" value="JOB_SEEKER" />
          <input type="submit" class="button" name="login" value="Login" />
  				<label for="rememberme" class="inline">
  				<input name="rememberme" type="checkbox" id="rememberme" value="forever" />Remember me</label>

</div>

  			<div class="lost_password">
  				<a href="{{url('password/email')}}">Forgot your password?</a>
  			</div>

  		</form>

  </div></div>
  </div>
  </article><!-- #post -->
</div><!-- #primary -->
</div><!-- #main -->

<div class="footer-cta">
			<div class="container">
    			<h2>Got a question?</h2>
          <p>We're here to help. Check out our FAQs, send us an email or call us at 1 (800) 555-5555</p>
          <p><a href="#" class="button button--type-action">Contact Us</a></p>
			</div>
</div>
<footer id="colophon" class="site-footer" role="contentinfo">
			@include('layouts.footer')
</footer><!-- #colophon -->
</div><!-- #page -->
</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
<script type='text/javascript' src="{{URL::asset('resources/assets/js/salvattore.min0168.js')}}"></script>
<script type='text/javascript' src="{{URL::asset('resources/assets/js/jquery.validate.js')}}"></script>
<script type='text/javascript' src="{{URL::asset('resources/assets/js/bootbox.min.js')}}"></script>
</html>
