@if(isset($markJobs) && !empty($markJobs))
<div class='tablediv'>
    <table class="">
      <thead>
      <tr>
        <th>No.</th>
        <th>Job Title</th>
        <th>Job Description</th>
        <th>Project Deadline</th>
      </tr>
    </thead>
      {{--*/$i=1/*--}}
      @foreach($markJobs as $job)
      <tbody>
        <tr>
          <td>{{$i++}}</td>
          <td><a href="{{url('/job/detail/'.$job->id)}}">{{substr($job->title,0,50)}}</a></td>
          <td>{{substr($job->description,0,50)}}</td>
          <td>{{date('d-M-Y',strtotime($job->project_deadline))}}</td>
        </tr>
      </tbody>
      @endforeach
    </table>
  </div>
  @else
@endif
