@if(isset($packageHistory) && !empty($packageHistory))
<div class='tablediv'>
  <table>
    <thead>
    <tr>
      <th>No.</th>
      <th>Package Name</th>
      <th>Transaction ID</th>
      <th>Subscription ID</th>
      <th>Payment Status</th>
      <th>Payment Type</th>
      <th>Date</th>
      <th>Action</th>
    </tr>
  </thead>
    {{--*/$i=1/*--}}
    @foreach($packageHistory as $package)
    <tbody>
      <tr>
        <td data-name="No.">{{$i++}}</td>
        <td data-name="Package Name"><a href="{{url('/')}}">{{$package->subscription_type}}</a></td>
        <td data-name="Transaction ID">@if(isset($package->txn_id) && !empty($package->txn_id)) {{$package->txn_id}} @else - @endif</td>
        <td data-name="Subscription ID">@if(isset($package->subscr_id) && !empty($package->subscr_id)) {{$package->subscr_id}} @else - @endif</td>
        <td data-name="Payment Status">@if(isset($package->payer_status) && !empty($package->payer_status)) {{$package->payer_status}} @else - @endif</td>
        <td data-name="Payment Type">@if(isset($package->payment_type) && !empty($package->payment_type)) {{$package->payment_type}} @else Recurring @endif</td>
        <td data-name="Date">{{date('d-M-Y',strtotime($package->payment_date))}}</td>
        @if($package->is_subscr && $package->is_recurring)
          <td data-name="Action"><a href="javascript:;" data-subscr="{{$package->subscr_id}}" class="unsubscribLink">Unsubscribe</a></td>
        @elseif($package->is_recurring)
          <td data-name="Action" class="red">Unsubscribed</td>
        @else
          <td data-name="Action">-</td>
        @endif
      </tr>
    </tbody>
    @endforeach
  </table>
  </div>
  @else
@endif
