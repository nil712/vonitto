<!DOCTYPE html>
<html lang="en-US">
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
   <head>
      @include('layouts.header')

   </head>
   <body class="fixed-header">
   <div id="page" class="hfeed site inner_padding">
     <header id="masthead" class="site-header" role="banner">
       @include('layouts.inner.header_inner_menu')
     </header>
     <section id="vonitto_widget_jobs_spotlight-6" class="widget howitworks widget--home vonitto_widget_jobs_spotlight widget--home-job-spotlights">
     <div class="container">
       <!--tool_tab-->
       <?php //dd($completedJob); ?>
      <div class="tool_tab">
       <ul id="myTab" class="nav nav-tabs">
        <li class="active"><a href='#tab1' data-toggle="tab">My Profile</a></li>
        <li><a href='#tab2' data-toggle="tab">Order History</a></li>
        <li><a href='#tab3' data-toggle="tab">Pitched Jobs</a></li>
        <li><a href='#tab4' data-toggle="tab">Jobs Awarded</a></li>
        <li><a href='#tab5' data-toggle="tab">Jobs Completed</a></li>
        <li><a href='#tab6' data-toggle="tab">Bookmarks</a></li>
        <li><a href='#tab7' data-toggle="tab">Settings</a></li>
      </ul>
       <div class="tab-content" id="myTabContent">
       <div id='tab1' class="tab-pane fade in active">
          <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
               <div class="leftSide">
                 <img src="{{$userDetail['image']}}">
               </div>
           </div>
           <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 pull-right">
               <div class="rightSide">
                 <h4>{{$userDetail['first_name']}} {{$userDetail['last_name']}}</h4>
                 <ul>
                   <li><label class="lbl">Email</label>
                       <span>{{$userDetail['email']}}</span>
                   </li>
                   <li>
                     <label class="lbl">Join Date</label>
                     <span>{{date('d-M-Y',strtotime($userDetail['created_at']))}}</span>
                   </li>
                   <li>
                     <label class="lbl">Remaining Pitch Request</label>
                     <span>{{($userDetail['available_pitch']!=NULL)?$userDetail['available_pitch']:0}}</span>
                    </li>
                 </ul>
               </div>
           </div>
         </div>
        <div id='tab2' class="tab-pane fade">
          <div class="jobsPackages">
            @if($packageHistory->toArray()!=null)
            <div class='tablediv'>
                <table>
                  <thead>
                  <tr>
                    <th>No.</th>
                    <th>Package Name</th>
                    <th>Transaction ID</th>
                    <th>Subscription ID</th>
                    <th>Payment Status</th>
                    <th>Payment Type</th>
                    <th>Date</th>
                    <th>Action</th>
                  </tr>
                </thead>
                  {{--*/$i=1/*--}}
                  @foreach($packageHistory as $package)
                  <tbody>
                    <tr>
                      <td data-name="No.">{{$i++}}</td>
                      <td data-name="Package Name"><a href="{{url('/')}}">{{$package->subscription_type}}</a></td>
                      <td data-name="Transaction ID">@if(isset($package->txn_id) && !empty($package->txn_id)) {{$package->txn_id}} @else - @endif</td>
                      <td data-name="Subscription ID">@if(isset($package->subscr_id) && !empty($package->subscr_id)) {{$package->subscr_id}} @else - @endif</td>
                      <td data-name="Payment Status">@if(isset($package->payer_status) && !empty($package->payer_status)) {{$package->payer_status}} @else - @endif</td>
                      <td data-name="Payment Type">@if(isset($package->payment_type) && !empty($package->payment_type)) {{$package->payment_type}} @else Recurring @endif</td>
                      <td data-name="Date">{{date('d-M-Y',strtotime($package->payment_date))}}</td>
                      @if($package->is_subscr && $package->is_recurring)
                        <td data-name="Action"><a href="javascript:;" data-subscr="{{$package->subscr_id}}" class="unsubscribLink">Unsubscribe</a></td>
                      @elseif($package->is_recurring)
                        <td data-name="Action" class="red">Unsubscribed</td>
                      @else
                        <td data-name="Action">-</td>
                      @endif
                    </tr>
                  </tbody>
                  @endforeach
                </table>
              </div>
              @else
              <p>No record found</p>
            @endif
          </div>
          <div id="page-selection"></div>
        </div>
        <div id='tab3' class="tab-pane fade">
          <div class="pitchedJobs">
            @if($pitchedJobs->toArray()!=null)
            <div class='tablediv'>
                <table>
                  <thead>
                  <tr>
                    <th>No.</th>
                    <th>Job Title</th>
                    <th>Job Description</th>
                    <th>Project Deadline</th>
                    <th>Date</th>
                  </tr>
                </thead>
                  {{--*/$i=1/*--}}
                  @foreach($pitchedJobs as $job)
                  <tbody>
                    <tr>
                      <td>{{$i++}}</td>
                      <td><a href="{{url('/job/detail/'.$job->job_id)}}">{{$job->title}}</a></td>
                      <td>{{substr($job->description,0,50)}}</td>
                      <td>{{date('d-M-Y',strtotime($job->project_deadline))}}</td>
                      <td>{{date('d-M-Y',strtotime($job->date))}}</td>
                    </tr>
                  </tbody>
                  @endforeach
                </table>
              </div>
              @else
              <p>No record found</p>
            @endif
          </div>
          <div id="page-selection1"></div>
        </div>
        <div id='tab4' class="tab-pane fade">
          <div class="messageBox" style="display:none;"></div>

          <div class="awardedJobs">
            @if($awardedJobs->toArray()!=null)
            <div class='tablediv jobaccount_table'>
                <table class="ajax-awarded">
                  <thead>
                  <tr>
                    <th class="acoount_sr" >No.</th>
                    <th class="acoount_title">Job Title</th>
                    <th class="acoount_dis">Job Description</th>
                    <th class="acoount_poster">Job Poster</th>
                    <th class="acoount_dedline">Project Deadline</th>
                    <th class="acoount_date">Awarded Date</th>
                    <th class="acoount_status">Status</th>
                  </tr>
                </thead>
                  {{--*/$i=1/*--}}
                  @foreach($awardedJobs as $job)
                  <tbody>
                    <tr>
                      <td>{{$i++}}</td>
                      <td><a href="{{url('/job/detail/'.$job->job_id)}}">{{$job->title}}</a></td>
                      <td><p>{{$job->description}}</p></td>
                      <td><a href="{{url('user/profile/'.$job->job_poster_id)}}">{{$job->first_name}} {{$job->last_name}}</a></td>
                      <td>{{date('d-M-Y',strtotime($job->project_deadline))}}</td>
                      <td>{{date('d-M-Y',strtotime($job->created_at))}}</td>
                      <td><select class="dropdown job-status">
                            <option data-project="{{$job->job_id}}" data-posterid="{{$job->job_poster_id}}" @if($job->progress_status == 'Assigned')  selected="selected" @endif value="Assigned">Assigned</option>
                            <option data-project="{{$job->job_id}}" data-posterid="{{$job->job_poster_id}}" @if($job->progress_status == 'Completed') selected="selected" @endif value="Completed">Completed</option>
                          <select>
                      </td>
                    </tr>
                  </tbody>
                  @endforeach
                </table>
              </div>
              @else
              <p>No record found</p>
            @endif
          </div>
          <div id="page-selection2"></div>
        </div>
        <div id='tab5' class="tab-pane fade">
          <div class="completedJob">
            @if($completedJob->toArray()!=null)
            <div class='tablediv jobaccount_table'>
                <table class="ajax-completed">
                  <thead>
                  <tr>
                    <th class="acoount_sr">No.</th>
                    <th class="acoount_title">Job Title</th>
                    <th class="acoount_dis">Job Description</th>
                    <th class="acoount_poster">Job Poster</th>
                    <th class="acoount_dedline">Project Deadline</th>
                    <th class="acoount_date">Awarded Date</th>
                    <th class="acoount_status">Email</th>
                    <th class="acoount_status">Mobile</th>
                    <th class="acoount_status">Rating</th>
                  </tr>
                </thead>
                  {{--*/$i=1/*--}}
                  @foreach($completedJob as $job)
                  <tbody>
                    <tr>
                      <td>{{$i++}}</td>
                      <td><a href="{{url('/job/detail/'.$job->job_id)}}">{{$job->title}}</a></td>
                      <td><p>{{$job->description}}</p></td>
                      <td><a href="{{url('user/profile/'.$job->job_poster_id)}}">{{$job->first_name}} {{$job->last_name}}</a></td>
                      <td>{{date('d-M-Y',strtotime($job->project_deadline))}}</td>
                      <td>{{date('d-M-Y',strtotime($job->created_at))}}</td>
                      <td>{{$job->email}}</td>
                      <td>{{$job->phone}}</td>
                      <td><a href="javascript:;" class="reviewRating" data-job="{{$job->job_id}}" data-rating="{{$job->rating}}" data-review="{{stripslashes($job->review)}}" data-seeker="{{$job->job_seeker_id}}" >@if(isset($job->rating) && !empty($job->rating)) {{$job->rating}}  @else 0 @endif star </a></td>
                    </tr>
                  </tbody>
                  @endforeach
                </table>
              </div>
              @else
              <p>No record found</p>
            @endif
          </div>
          <div id="page-selection4"></div>
        </div>
        <div id='tab6' class="tab-pane fade">
          <div class="favouriteJob">
            @if(isset($markJobs) && !empty($markJobs))
            <div class='tablediv'>
                <table>
                  <thead>
                  <tr>
                    <th>No.</th>
                    <th>Job Title</th>
                    <th>Job Description</th>
                    <th>Project Deadline</th>
                  </tr>
                </thead>
                  {{--*/$i=1/*--}}
                  @foreach($markJobs as $job)
                  <tbody>
                    <tr>
                      <td>{{$i++}}</td>
                      <td><a href="{{url('/job/detail/'.$job->id)}}">{{substr($job->title,0,50)}}</a></td>
                      <td>{{substr($job->description,0,50)}}</td>
                      <td>{{date('d-M-Y',strtotime($job->project_deadline))}}</td>
                    </tr>
                  </tbody>
                  @endforeach
                </table>
              </div>
              @else
              <p>No record found</p>
            @endif
          </div>
          <div id="page-selection5"></div>
        </div>

        <div id='tab7' class="tab-pane fade">


            <div class="selection JobSetting">
              <h4>I’d like to be contacted on new job notification based on my area and categories of interest.</h4>
              <ul>
                <li>
                    <input type="radio" name="notificationPreference" id="notificationPreference1" value="Immediately">
                    <label for="notificationPreference1"> Immediately </label>
                </li>

                <li>
                    <input type="radio" name="notificationPreference" id="notificationPreference2" value="Daily">
                    <label for="notificationPreference2"> Daily </label>
                </li>

                <li>
                    <input type="radio" name="notificationPreference" id="notificationPreference3" value="Weekly">
                    <label for="notificationPreference3"> Weekly </label>
                </li>
              </ul>
            </div>

            <div class="selection JobSetting">
              <h4>I’d like to be contacted on job notification if the status or description of the job you pitch has changed.</h4>
              <ul>
                <li>
                    <input type="radio" name="jobNotificationPreference" id="jobNotificationPreference1" value="Immediately">
                    <label for="jobNotificationPreference1"> Immediately </label>
                </li>

                <li>
                    <input type="radio" name="jobNotificationPreference" id="jobNotificationPreference2" value="Daily">
                    <label for="jobNotificationPreference2"> Daily </label>
                </li>

                <li>
                    <input type="radio" name="jobNotificationPreference" id="jobNotificationPreference3" value="Weekly">
                    <label for="jobNotificationPreference3"> Weekly </label>
                </li>
              </ul>
            </div>

            <div class="selection JobSetting">
              <ul>
                <li>
                    <input type="checkbox" name="canContact1" id="canContact1" value="1">
                    <label for="canContact1"> I’d like to be contacted on offers and promotions from pitchyourconcepts.com </label>
                </li>

                <li>
                  <input type="checkbox" name="canContact2" id="canContact2" value="1">
                  <label for="canContact2"> I’d like to be contacted on offers and promotions from affiliated companies by pitchyourconcepts.com </label>
                </li>

              </ul>
            </div>



          <div id="page-selection6"></div>
        </div>
     </div>
      </div>
      </div>
   </section>
     <div id="reviewModel" class="modal fade" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Review & Rating</h4>
          </div>
          <form  class="frmReviewRating" name="frmReviewRating" id="frmReviewRating"  role="form" method="POST" >
            <div class="messageBox" style="display:none;"></div>

            <div class="modal-body">
              <p class="form-row form-row-wide">
                  <label for="comment">Select Rating</label>
                  <div id="rateYo" class="rateyo"></div>
             </p>
             <p class="comment-form-comment">
               <label for="comment">Your Review</label>
               <textarea style="resize:none;" aria-required="true" rows="8" cols="45" name="review" id="review" name="review"></textarea>
             </p>


            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
         </form>
        </div>

      </div>
    </div>
     <input type="hidden" name="_token" value="{{csrf_token()}}" id="token">
 </body>
 </html>
 <!--
 <link href="{{URL::asset('resources/assets/css/pages/seeker_my_account.css')}}" rel="stylesheet" type="text/css">
 -->
<script src="{{URL::asset('resources/assets/js/jquery.js')}}"></script>
<script src="{{URL::asset('resources/assets/js/bootpag.min.js')}}"></script>
<script src="{{URL::asset('resources/assets/js/pages/seeker_my_account.js')}}"></script>
<!-- Ankit Sompura -->
<script src="{{URL::asset('resources/assets/js/pages/seeker/account.js')}}"></script>
<!-- Ankit Sompura -->
<script src="{{URL::asset('resources/assets/js/pages/bootstrap.min.js')}}"></script>
 <!-- Bootstrap TabCollapse-->
<script type="text/javascript" src="{{URL::asset('resources/assets/js/rated/src/jquery.rateyo.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('resources/assets/js/pages/bootstrap-tabcollapse.js')}}"></script>
<script type="text/javascript">
	 $('#myTab').tabCollapse();
</script>
 <!-- Bootstrap TabCollapse-->
<script>
  $('#page-selection').bootpag({
    total: {{$packagePages}}
  }).on("page", function(event,num){
      var token = $("#token").val();
      $.ajax({
        type: "GET",
        headers: {'X-CSRF-TOKEN': token},
        url: "{{url('myaccount/loadJobsPackage')}}"+"/"+num,
        cache: false,
        beforeSend: function () {

        },
       success: function(data) {
        if(data!="0"){
          $(".jobsPackages").html("");
          $(".jobsPackages").html(data);
        }
       }
     });
  });

  $('#page-selection1').bootpag({
    total: {{$pitchPages}}
  }).on("page", function(event,num){
    var token = $("#token").val();
    $.ajax({
      type: "GET",
      headers: {'X-CSRF-TOKEN': token},
      url: "{{url('myaccount/loadPitchJobs')}}"+"/"+num,
      cache: false,
      beforeSend: function () {

      },
     success: function(data) {
      if(data!="0"){
        $(".pitchedJobs").html("");
        $(".pitchedJobs").html(data);
      }
     }
   });
  });

  $('#page-selection2').bootpag({
    total: {{$awardedJobsPages}}
  }).on("page", function(event,num){
    var token = $("#token").val();
    $.ajax({
      type: "GET",
      headers: {'X-CSRF-TOKEN': token},
      url: "{{url('myaccount/loadJobseekerAwardedJobs')}}"+"/"+num,
      cache: false,
      beforeSend: function () {

      },
     success: function(data) {
      if(data!="0"){
        $(".awardedJobs").html("");
        $(".awardedJobs").html(data);
      }
     }
   });
  });

  //Completed Job
  $('#page-selection4').bootpag({
    total: {{$completedJobsPages}}
  }).on("page", function(event,num){
    var token = $("#token").val();
    $.ajax({
      type: "GET",
      headers: {'X-CSRF-TOKEN': token},
      url: "{{url('myaccount/loadJobseekerCompletedJobs')}}"+"/"+num,
      cache: false,
      beforeSend: function () {

      },
     success: function(data) {
      if(data!="0"){
        $(".completedJob").html("");
        $(".completedJob").html(data);
      }
     }
   });
  });

  //favourite Job
  $('#page-selection5').bootpag({
    total: {{$markJobPages}}
  }).on("page", function(event,num){
    var token = $("#token").val();
    $.ajax({
      type: "GET",
      headers: {'X-CSRF-TOKEN': token},
      url: "{{url('jobseeker/myaccount/loadJobseekerFavourite')}}"+"/"+num,
      cache: false,
      beforeSend: function () {

      },
     success: function(data) {
      if(data!="0"){
        $(".favouriteJob").html("");
        $(".favouriteJob").html(data);
      }
     }
   });
  });
  $(document).ready(function(){
    $(document).on("click",".unsubscribLink",function(event){
      event.preventDefault();
      var token = $("#token").val();
      var dataString={subscr_id:$(this).data('subscr')};
      var $currentObj=$(this);
      $.ajax({
        type: "POST",
        headers: {'X-CSRF-TOKEN': token},
        url: BASE_URL+"jobseeker/myaccount/unsubscribe",
        cache: false,
        data: JSON.stringify({ data: dataString }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {

        },
        success: function(data) {
          if(data.status == 1){ // success
            alert("Package unsubscribe successfully.");
            $($currentObj).parents('td').html('Unsubscribed');
          }else if(data.status == -1){ // already unsubscribe
            alert("Package is already unsubscribed.");
            $($currentObj).parents('td').html("-");
          }else { // getting error
            alert("Something went wrong");
          }
        }
      });
    });
  });

  $(document).on("click",".ajax-completed .reviewRating",function(){
      var $rateYo;
      var $model=$("#reviewModel").modal("show");
      var rating=$(this).attr("data-rating");
      $rateYo=$("#rateYo").rateYo({halfStar:true,ratedFill: "#222638"});
      $("#rateYo").rateYo("option", "readOnly", true);
      $("#rateYo").rateYo("option", "rating",rating);
      $model.find("#submitReview").hide();
      $model.find("#review").text($(this).attr("data-review"));
      $model.find("#review").attr('disabled','disabled');
  });

</script>
