@if(isset($completedJob) && !empty($completedJob))
<div class='tablediv'>
    <table>
      <thead>
      <tr>
        <th class="acoount_sr">No.</th>
        <th class="acoount_title">Job Title</th>
        <th class="acoount_dis">Job Description</th>
        <th class="acoount_poster">Job Poster</th>
        <th class="acoount_dedline">Project Deadline</th>
        <th class="acoount_date">Awarded Date</th>
        <th class="acoount_status">Email</th>
        <th class="acoount_status">Mobile</th>
        <th class="acoount_status">Rating</th>
      </tr>
    </thead>
      {{--*/$i=1/*--}}
      @foreach($completedJob as $job)
      <tbody>
        <tr>
          <td>{{$i++}}</td>
          <td><a href="{{url('/job/detail/'.$job->job_id)}}">{{$job->title}}</a></td>
          <td><p>{{$job->description}}</p></td>
          <td><a href="{{url('user/profile/'.$job->job_poster_id)}}">{{$job->first_name}} {{$job->last_name}}</a></td>
          <td>{{date('d-M-Y',strtotime($job->project_deadline))}}</td>
          <td>{{date('d-M-Y',strtotime($job->created_at))}}</td>
          <td>{{$job->email}}</td>
          <td>{{$job->phone}}</td>
          <td><a href="javascript:;" class="reviewRating" data-job="{{$job->job_id}}" data-rating="{{$job->rating}}" data-review="{{stripslashes($job->review)}}" data-seeker="{{$job->job_seeker_id}}" >@if(isset($job->rating) && !empty($job->rating)) {{$job->rating}}  @else 0 @endif star </a></td>
        </tr>
      </tbody>
      @endforeach
    </table>
  </div>
  @else
@endif
