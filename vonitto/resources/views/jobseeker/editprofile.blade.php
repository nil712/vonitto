<!DOCTYPE html>
<html lang="en-US">
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <head>
        <!-- include header files  -->
        @include('layouts.header')
    </head>
    <body class="fixed-header">
        <div id="page" class="hfeed site inner_padding">
            <header id="masthead" class="site-header" role="banner">
                @include('layouts.inner.header_inner_menu')
            </header>
            <!-- #masthead -->
            <div id="main" class="site-main">
                <header class="page-header">
                    <h2 class="page-title">My Account</h2>
                </header>
                @if(session('status') && session('status') == 'Warning')
                <div class="alert">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    {{ session('message') }}
                </div>
                @endif
                @if(session('status') && session('status') == 'Error')
                <div class="alert alert-error">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    {{ session('message') }}
                </div>
                @endif
                @if(session('status') && session('status') == 'Success')
                <div class="alert alert-success">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    {{ session('message') }}
                </div>
                @endif

                <div id="primary" class="content-area container" role="main">


                    <article id="post-1673" class="post-1673 page type-page status-publish hentry">
                        <div class="entry-content">
                            <div class="registration-form woocommerce register_dv loginsignupmain">

                                <form  class="register" id="formRegistrationSeeker" enctype="multipart/form-data" role="form" method="POST" action="{{ url('jobseeker/editprofile/'.Auth::id()) }}">

                                    <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">

                                    <p class="form-row form-row-wide">
                                     <!--<label for="reg_email">FirstName <span class="required">*</span></label>-->
                                        <input type="text" class="input-text" name="first_name" id="first_name" value="{{$EditJobSeekerData['first_name']}}"  placeholder="FIRSTNAME"/>
                                        <img class="icon" src="{{URL::asset('resources/assets/images/icon-1.png')}}"   />
                                    <div class="clearfix"></div>
                                    @if ($errors->has('first_name'))<p class="error_message">{!!$errors->first('first_name')!!}</p>@endif
                                    </p>

                                    <p class="form-row form-row-wide">
                                     <!-- <label for="reg_email">LastName <span class="required">*</span></label> -->
                                        <input type="text" class="input-text" name="last_name" id="last_name" value="{{$EditJobSeekerData['last_name']}}"  placeholder="LASTNAME"/>
                                        <img class="icon" src="{{URL::asset('resources/assets/images/icon-1.png')}}"   />
                                    <div class="clearfix"></div>
                                    @if ($errors->has('last_name'))<p class="error_message">{!!$errors->first('last_name')!!}</p>@endif
                                    </p>

                                    <p class="form-row form-row-wide">
                                     <!-- <label for="reg_email">Email <span class="required">*</span></label> -->
                                        <input type="email" readonly="readonly" class="input-text" name="email" id="email" value="{{$EditJobSeekerData['email']}}"  placeholder="EMAIL"/>
                                        <img class="icon" src="{{URL::asset('resources/assets/images/icon-1.png')}}"   />
                                    <div class="clearfix"></div>
                                    @if ($errors->has('email'))<p class="error_message">{!!$errors->first('email')!!}</p>@endif
                                    </p>
                                    <p class="form-row form-row-wide">
                                     <!-- <label for="reg_password">Profile Image <span class="required">*</span></label> -->
                                        <img style="margin-bottom:10px;" class="img-circle pull-left" src="{{ URL::asset('resources/assets/upload/jobseeker/'.Auth::user()->image) }}">
                                    <div class="clearfix"></div>
                                    <div class="upload">
                                        <label class="brows"><input type="file" name="image" value="{{ old('image') }}" placeholder="">Upload Profile Image</label>
                                    </div>
                                    <!-- <input type="file" class="input-text" name="image" />  -->
                                    <div class="clearfix"></div>
                                    @if ($errors->has('image'))<p class="error_message">{!!$errors->first('image')!!}</p>@endif
                                    </p>

                                    <p class="form-row form-row-wide">
                                        <!-- <label>Phone Number </label> -->
                                        <input type="text" class="input-text" name="phone" id="phone" value="{{$EditJobSeekerData['phone']}}"  placeholder="Phone Number"/>
                                        <img class="icon" src="{{URL::asset('resources/assets/images/icon-3.png')}}"   />
                                    <div class="clearfix"></div>
                                    @if ($errors->has('phone'))<p class="error_message">{!!$errors->first('phone')!!}</p>@endif
                                    </p>


                                    <p class="form-row form-row-wide">
                                        <!-- <label>Street Address1 </label> -->
                                        <input type="text" class="input-text" name="street_address1" id="street_address1" value="{{$EditJobSeekerData['street_address1']}}"  placeholder="Street Address1"/>
                                        <img class="icon" src="{{URL::asset('resources/assets/images/address.png')}}"   />
                                    <div class="clearfix"></div>
                                    @if ($errors->has('street_address1'))<p class="error_message">{!!$errors->first('street_address1')!!}</p>@endif
                                    </p>

                                    <p class="form-row form-row-wide">
                                        <!-- <label>Street Address2 </label> -->
                                        <input type="text" class="input-text" name="street_address2" id="street_address2" value="{{$EditJobSeekerData['street_address2']}}"  placeholder="Street Address2"/>
                                        <img class="icon" src="{{URL::asset('resources/assets/images/address.png')}}"   />
                                    <div class="clearfix"></div>
                                    @if ($errors->has('street_address2'))<p class="error_message">{!!$errors->first('street_address2')!!}</p>@endif
                                    </p>

                                    <p class="form-row form-row-wide">
                                        <!-- <label>City</label> -->
                                        <input type="text" class="input-text" name="city" id="city" value="{{$EditJobSeekerData['city']}}"  placeholder="City"/>
                                        <img class="icon" src="{{URL::asset('resources/assets/images/state.png')}}"   />

                                    <div class="clearfix"></div>
                                    @if ($errors->has('city'))<p class="error_message">{!!$errors->first('city')!!}</p>@endif
                                    </p>

                                    <p class="form-row form-row-wide">
                                        <!-- <label>State</label> -->
                                        <input type="text" class="input-text" name="state" id="state" value="{{$EditJobSeekerData['state']}}"  placeholder="State"/>
                                        <img class="icon" src="{{URL::asset('resources/assets/images/state.png')}}"   />
                                    <div class="clearfix"></div>
                                    @if ($errors->has('state'))<p class="error_message">{!!$errors->first('state')!!}</p>@endif
                                    </p>

                                    <p class="form-row form-row-wide">
                                        <!-- <label>Postcode</label> -->
                                        <input type="text" class="input-text" name="zip" id="zip" value="{{$EditJobSeekerData['zip']}}"  placeholder="Postcode"/>
                                        <img class="icon" src="{{URL::asset('resources/assets/images/pincode.png')}}"   />
                                    <div class="clearfix"></div>
                                    @if ($errors->has('zip'))<p class="error_message">{!!$errors->first('zip')!!}</p>@endif
                                    </p>

                                    <p class="form-row form-row-wide">
                                        <!-- <label>Country</label> -->
                                        <select name="country" class="dropdown" id="country">
                                            <option value=""> Select Country </option>
                                            <option value="UK" <?php if ($EditJobSeekerData['country'] == 'UK') { ?> selected="selected" <?php } ?> >UK</option>
                                            <option value="Malaysia" <?php if ($EditJobSeekerData['country'] == 'Malaysia') { ?> selected="selected" <?php } ?> >Malaysia</option>
                                        </select>
                                    </p>
                                    <p class="form-row form-row-wide">
                                        <!-- <label>Postcode</label> -->
                                        <input maxlength="4" type="text" class="input-text" name="experience" id="experience" value="{{$EditJobSeekerData['experience']}}"  placeholder="Years of Experience"/>
                                        <img class="icon" src="{{URL::asset('resources/assets/images/year-of-exp.png')}}"   />
                                    <div class="clearfix"></div>
                                    @if ($errors->has('experience'))<p class="error_message">{!!$errors->first('experience')!!}</p>@endif

                                    </p>
                                    <p class="form-row form-row-wide">
                                        <!-- <label>Postcode</label> -->
                                        <!-- <input type="text" class="input-text" maxlength="50" name="graduation" id="graduation" value="{{$EditJobSeekerData['graduation']}}"  placeholder="Graduation"/>
                                        <img class="icon" src="{{URL::asset('resources/assets/images/education.png')}}"   /> -->

                                        <select name="graduation" id="graduation">
                                          <option value="">Highest qulification obtained.</option>
                                          <option value="Certificate">Certificate</option>
                                          <option value="Degree">Degree</option>
                                          <option value="Degree">Degree</option>
                                          <option value="Master (Diploma)">Master (Diploma)</option>
                                          <option value="PHD">PHD</option>
                                          <option value="Other">Other</option>

                                        </select>
                                    <div class="clearfix"></div>
                                    @if ($errors->has('graduation'))<p class="error_message">{!!$errors->first('graduation')!!}</p>@endif
                                    </p>
                                    <p class="form-row form-row-wide">
                                        <!-- <label>Postcode</label> -->
                                        <input type="text" class="input-text" maxlength="50" name="post_graduation" id="post_graduation" value="{{$EditJobSeekerData['post_graduation']}}"  placeholder="Member Number / Member Association (RIBA / BIID)"/>
                                        <img class="icon" src="{{URL::asset('resources/assets/images/education.png')}}"   />
                                    <div class="clearfix"></div>
                                    @if ($errors->has('post_graduation'))<p class="error_message">{!!$errors->first('post_graduation')!!}</p>@endif
                                    </p>
                                    <p class="form-row form-row-wide">
                                        <!-- <label>Postcode</label> -->
                                        <textarea style="resize:none" aria-required="true" rows="8" cols="45" name="computer_skill" id="computer_skill" name="computer_skill" placeholder="Computer Skill" >{{$EditJobSeekerData['computer_skill']}}</textarea>
                                    </p>
                                    <p class="form-row form-row-wide">
                                        <!-- <label>Postcode</label> -->
                                        <!-- <textarea style="resize:none" aria-required="true" rows="8" cols="45" name="project_category_specialization" id="project_category_specialization" name="project_category_specialization" placeholder="Project Category Specialisation" >{{$EditJobSeekerData['project_category_specialization']}}</textarea> -->
                                        <select>
                                          <option value="">Project Category</option>
                                          <option value="Civic & Community">Civic & Community</option>
                                          <option value="Culture">Culture</option>
                                          <option value="Education">Education</option>
                                          <option value="Health">Health</option>
                                          <option value="Hospitality">Hospitality</option>
                                          <option value="Office & Workplace">Office & Workplace</option>
                                          <option value="Residential">Residential</option>
                                          <option value="Retail">Retail</option>
                                          <option value="Sports">Sports</option>
                                          <option value="Transportation">Transportation</option>
                                        </select>
                                    </p>
                                    <p class="form-row form-row-wide">
                                     <!-- <label for="reg_password">Profile Image <span class="required">*</span></label> -->
                                    <div class="clearfix"></div>
                                    <div class="upload">
                                        <label class="brows"><input type="file" id="portfolio_image" class="portfolio_image" name="portfolio_image" value="{{ old('portfolio_image') }}" >Portfolio Attachment</label>
                                    </div>
                                    <!-- <input type="file" class="input-text" name="image" />  -->
                                    <div class="clearfix"></div>
                                    </p>
                                    <p class="form-row form-row-wide">
                                        <!-- <label>Postcode</label> -->
                                        <textarea style="resize:none" aria-required="true" rows="8" cols="45" name="portfolio_description" id="portfolio_description" name="portfolio_description" placeholder="Portfolio Description" >{{$EditJobSeekerData['portfolio_description']}}</textarea>
                                    </p>
                                    <!-- Spam Trap -->
                                    <p class="form-row">

                                        <button name="preview" id="preview" data-toggle="modal" data-target="#myModal">Preview</button>
                                        <input type="submit" class="button" name="register" value="Save Changes" />
                                    </p>

                                    <p>
                                      Note that your address, telephone number and email address will not be shown to JOB POSTER.
                                    </p>
                                </form>
                            </div>
                        </div>
                    </article><!-- #post -->
                </div><!-- #primary -->
            </div><!-- #main -->

            <div class="footer-cta">
                <div class="container">
                    <h2>Got a question?</h2>
                    <p>We're here to help. Check out our FAQs, send us an email or call us at 1 (800) 555-5555</p>
                    <p><a href="#" class="button button--type-action">Contact Us</a></p>
                </div>
            </div>

            <div id="myModal" class="modal fade" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h2 class="modal-title">My Account Preview</h2>
                        </div>
                        <div class="modal-body">
                            <p>
                                <img style="margin-bottom:10px;" class="img-circle pull-left" src="{{ URL::asset('resources/assets/upload/jobseeker/'.Auth::user()->image) }}">
                            </p>
                            <br />
                            <div class="row">
                                <div class="col-md-6">
                                    <p><strong>First Name </strong>: <span id="first_name_span"></span></p>
                                    <p><strong>Last Name : </strong><span id="last_name_span"></span></p>
                                    <p><strong>Email : </strong><span id="email_span"></span></p>
                                    <p><strong>Phone : </strong><span id="phone_span"></span> </p>
                                    <p><strong>Street Address 1 : </strong><span id="street_address1_span"></span></p>
                                    <p><strong>Street Address 2 : </strong><span id="street_address2_span"></span></p>
                                </div>
                                <div class="col-md-6">
                                    <p><strong>City : </strong><span id="city_span"></span></p>
                                    <p><strong>State : </strong><span id="state_span"></span></p>
                                    <p><strong>Post Code : </strong><span id="zip_span"></span></p>
                                    <p><strong>Country : </strong><span id="country_span"></span></p>
                                    <p><strong>Years of Experience : </strong><span id="experience_span"></span></p>
                                </div>
                            </div>
                            <p><strong>Graduation : </strong><span id="graduation_span"></span></p>
                            <p><strong>Post Graduation : </strong><span id="post_graduation_span"></span></p>
                            <p><strong>Computer Skill : </strong><span id="computer_skill_span"></span></p>
                            <p><strong>Project Category Specialization : </strong><span id="project_category_specialization_span"></span></p>
                            <p><strong>Portfolio Description : </strong><span id="portfolio_description_span"></span></p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>

            <footer id="colophon" class="site-footer" role="contentinfo">
                @include('layouts.footer')
            </footer><!-- #colophon -->
        </div>
        /*
        * Developer Name : Hitesh Tank
        * Date : 3-Aug-2016
        * Description : validation of portfolio image
        */
        <script>
            $(document).ready(function () {
                $('#preview').on('click', function (event) {
                    event.preventDefault();
                    $('#first_name_span').text($('#first_name').val());
                    $('#last_name_span').text($('#last_name').val());
                    $('#email_span').text($('#email').val());
                    $('#phone_span').text($('#phone').val());
                    $('#street_address1_span').text($('#street_address1').val());
                    $('#street_address2_span').text($('#street_address2').val());
                    $('#city_span').text($('#city').val());
                    $('#state_span').text($('#state').val());
                    $('#zip_span').text($('#zip').val());
                    $('#country_span').text($('#country').val());
                    $('#experience_span').text($('#experience').val());
                    $('#graduation_span').text($('#graduation').val());
                    $('#computer_skill_span').text($('#computer_skill').val());
                    $('#project_category_specialization_span').text($('#project_category_specialization').val());
                    $('#portfolio_description_span').text($('#portfolio_description').val());
                    $('#post_graduation_span').text($('#post_graduation').val());
                });
            });
        </script>
    </body>
</html>
