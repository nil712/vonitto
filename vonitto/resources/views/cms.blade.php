<!DOCTYPE html>
<html lang="en-US">
@section('title',$cmsData[0]->page_name.' |')
<head>
  <!-- include header files  -->
  @include('layouts.header')
</head>
<body class="fixed-header login_page">

	<div id="page" class="hfeed site inner_padding">
    <header id="masthead" class="site-header" role="banner">
       @include('layouts.header_menu')
    </header><!-- #masthead -->

    <div id="main" class="site-main">
      <header class="page-header">
          <h2 class="page-title">{{$cmsData[0]->page_name}}</h2>
      </header>
      <div role="main" class="content-area container" id="primary">
                      <article class="post-1948 page type-page status-publish hentry" id="post-1948">
                          <div class="entry-content">
                            <?php echo htmlspecialchars_decode($cmsData[0]->page_text,ENT_QUOTES|ENT_HTML5); ?>
                          </div>
                      </article><!-- #post -->
      </div>
  </div><!-- #main -->

<div class="footer-cta">
			<div class="container">
    			<h2>Got a question?</h2>
          <p>We're here to help. Check out our FAQs, send us an email or call us at 1 (800) 555-5555</p>
          <p><a href="#" class="button button--type-action">Contact Us</a></p>
			</div>
</div>
<footer id="colophon" class="site-footer" role="contentinfo">
			@include('layouts.footer')
</footer><!-- #colophon -->
</div><!-- #page -->
</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
<script type='text/javascript' src="{{URL::asset('resources/assets/js/salvattore.min0168.js')}}"></script>
<script type='text/javascript' src="{{URL::asset('resources/assets/js/jquery.validate.js')}}"></script>
<script type='text/javascript' src="{{URL::asset('resources/assets/js/bootbox.min.js')}}"></script>
</html>
