<!DOCTYPE html>
<html lang="en-US">
<head>
  <!-- include header files  -->
  @include('layouts.header')
</head>

<body class="fixed-header">
	<div id="page" class="hfeed site inner_padding">
		<header id="masthead" class="site-header" role="banner">
       @include('layouts.header_menu')
    </header><!-- #masthead -->

		<div id="main" class="site-main">

    <div id="primary" class="content-area container" role="main">


<article id="post-1673" class="post-1673 page type-page status-publish hentry">
    <div class="entry-content">
        <div class="registration-form woocommerce lets">


	<h2>Log in and get to work</h2>

	<div method="post" class="register_dv">

		<div class="row devide_row">
          <div class="col-sm-6">
            <div class="jobcolumn">

              <div class="circle_dv">
               <a href="{{ url('/jobseeker/login') }}"> <img src="{{URL::asset('resources/assets/images/signup_ic_1.png')}}"  alt=""/>
                      <p>Job Seeker</p></a>
                </div>

             <a href="{{ url('/jobseeker/login') }}" class="regist_btn">Login</a>

            </div>
          </div>

          <div class="col-sm-6">
            <div class="jobcolumn">

              <div class="circle_dv">
               <a href="{{ url('/jobposter/login') }}"> <img src="{{URL::asset('resources/assets/images/signup_ic_2.png')}}"  alt=""/>
                      <p>Job Poster</p></a>
                </div>

             <a href="{{ url('/jobposter/login') }}" class="regist_btn">Login</a>

            </div>
          </div>
        </div>

</div>



</div>

    </div>
</article><!-- #post -->
                    </div><!-- #primary -->



		</div><!-- #main -->

				<div class="footer-cta">
			<div class="container">
				<h2>Got a question?</h2>
<p>We're here to help. Check out our FAQs, send us an email or call us at 1 (800) 555-5555</p>
<p><a href="#" class="button button--type-action">Contact Us</a></p>
			</div>
		</div>

		<footer id="colophon" class="site-footer" role="contentinfo">
					@include('layouts.footer')
		</footer><!-- #colophon -->
	</div><!-- #page -->



</body>

<script type='text/javascript' src='js/vonitto.minfcbd.js'></script>
<script type='text/javascript' src='js/salvattore.min0168.js'></script>
</html>
