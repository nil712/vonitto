<!DOCTYPE html>
<html lang="en-US">
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
   <head>
      <!-- include header files  -->
      @include('layouts.header')
   </head>
   <body class="fixed-header">
   <div id="page" class="hfeed site inner_padding">
     <header id="masthead" class="site-header" role="banner">
       @include('layouts.inner.header_inner_menu')
     </header>
     <!-- #masthead -->
     <div id="main" class="site-main">


     <header class="page-header">
         <h2 class="page-title">My Account</h2>
     </header>

     @if(session('status') && session('status') == 'Warning')
     <div class="alert">
       <button type="button" class="close" data-dismiss="alert">×</button>
       {{ session('message') }}
     </div>
     @endif

     @if(session('status') && session('status') == 'Error')
     <div class="alert alert-error">
         <button type="button" class="close" data-dismiss="alert">×</button>
         {{ session('message') }}
     </div>
     @endif

     @if(session('status') && session('status') == 'Success')
       <div class="alert alert-success">
           <button type="button" class="close" data-dismiss="alert">×</button>
           {{ session('message') }}
       </div>
     @endif

     <div id="primary" class="content-area container" role="main">


 <article id="post-1673" class="post-1673 page type-page status-publish hentry">
     <div class="entry-content">
         <div class="registration-form woocommerce register_dv loginsignupmain">

          <form  class="register" id="formRegistrationPoster" enctype="multipart/form-data" role="form" method="POST" action="{{ url('jobposter/editprofile/'.Auth::id()) }}">

             <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">

             <p class="form-row form-row-wide">
              <!-- <label for="reg_email">FirstName <span class="required">*</span></label> -->
              <input type="text" class="input-text" name="first_name" id="first_name" value="{{$EditJobPosterData['first_name']}}"  placeholder="FIRSTNAME"/>
              <img class="icon" src="{{URL::asset('resources/assets/images/icon-1.png')}}"   />
               <div class="clearfix"></div>
               @if ($errors->has('first_name'))<p class="error_message">{!!$errors->first('first_name')!!}</p>@endif
            </p>

             <p class="form-row form-row-wide">
              <!-- <label for="reg_email">LastName <span class="required">*</span></label> -->
              <input type="text" class="input-text" name="last_name" id="last_name" value="{{$EditJobPosterData['last_name']}}"  placeholder="LASTNAME"/>
              <img class="icon" src="{{URL::asset('resources/assets/images/icon-1.png')}}"   />
               <div class="clearfix"></div>
               @if ($errors->has('last_name'))<p class="error_message">{!!$errors->first('last_name')!!}</p>@endif
            </p>

             <p class="form-row form-row-wide">
              <!-- <label for="reg_email">Email <span class="required">*</span></label> -->
              <input type="email" readonly="readonly" class="input-text" name="email" id="email" value="{{$EditJobPosterData['email']}}"  placeholder="EMAIL"/>
              <img class="icon" src="{{URL::asset('resources/assets/images/icon-3.png')}}"   />
               <div class="clearfix"></div>
               @if ($errors->has('email'))<p class="error_message">{!!$errors->first('email')!!}</p>@endif
            </p>

            


             <p class="form-row form-row-wide">
              <!-- <label for="reg_password">Profile Image <span class="required">*</span></label> -->
              <img style="margin-bottom:10px;" class="img-circle pull-left" src="{{ URL::asset('resources/assets/upload/jobposter/'.Auth::user()->image) }}">
              <div class="clearfix"></div>
              <!-- <input type="file" class="input-text" name="image" /> -->
              <div class="upload">
               <label class="brows"><input type="file" name="image" value="{{ old('image') }}" placeholder="">Upload Profile Image</label>
              </div>
               <div class="clearfix"></div>
               @if ($errors->has('image'))<p class="error_message">{!!$errors->first('image')!!}</p>@endif
            </p>

            <p class="form-row form-row-wide">
             <!-- <label>Phone Number </label> -->
             <input type="text" class="input-text" name="phone" id="phone" value="{{$EditJobPosterData['phone']}}"  placeholder="Phone Number"/>
             <img class="icon" src="{{URL::asset('resources/assets/images/icon-4.png')}}"   />
              <div class="clearfix"></div>
              @if ($errors->has('phone'))<p class="error_message">{!!$errors->first('phone')!!}</p>@endif
           </p>


           <p class="form-row form-row-wide">
            <!-- <label>Street Address1 </label> -->
            <input type="text" class="input-text" name="street_address1" id="street_address1" value="{{$EditJobPosterData['street_address1']}}"  placeholder="Street Address1"/>
            <img class="icon" src="{{URL::asset('resources/assets/images/address.png')}}"   />
             <div class="clearfix"></div>
             @if ($errors->has('street_address1'))<p class="error_message">{!!$errors->first('street_address1')!!}</p>@endif
          </p>

          <p class="form-row form-row-wide">
           <!-- <label>Street Address2 </label> -->
           <input type="text" class="input-text" name="street_address2" id="street_address2" value="{{$EditJobPosterData['street_address2']}}"  placeholder="Street Address2"/>
           <img class="icon" src="{{URL::asset('resources/assets/images/address.png')}}"   />
            <div class="clearfix"></div>
            @if ($errors->has('street_address2'))<p class="error_message">{!!$errors->first('street_address2')!!}</p>@endif
         </p>

         <p class="form-row form-row-wide">
          <!-- <label>City</label> -->
          <input type="text" class="input-text" name="city" id="city" value="{{$EditJobPosterData['city']}}"  placeholder="City"/>
          <img class="icon" src="{{URL::asset('resources/assets/images/state.png')}}"   />
           <div class="clearfix"></div>
           @if ($errors->has('city'))<p class="error_message">{!!$errors->first('city')!!}</p>@endif
        </p>

        <p class="form-row form-row-wide">
         <!-- <label>State</label> -->
         <input type="text" class="input-text" name="state" id="state" value="{{$EditJobPosterData['state']}}"  placeholder="State"/>
         <img class="icon" src="{{URL::asset('resources/assets/images/state.png')}}"   />
          <div class="clearfix"></div>
          @if ($errors->has('state'))<p class="error_message">{!!$errors->first('state')!!}</p>@endif
       </p>

       <p class="form-row form-row-wide">
        <!-- <label>Postcode</label> -->
        <input type="text" class="input-text" name="zip" id="zip" value="{{$EditJobPosterData['zip']}}"  placeholder="Postcode"/>
        <img class="icon" src="{{URL::asset('resources/assets/images/pincode.png')}}"   />
         <div class="clearfix"></div>
         @if ($errors->has('zip'))<p class="error_message">{!!$errors->first('zip')!!}</p>@endif
      </p>

      <p class="form-row form-row-wide">
       <!-- <label>Country</label> -->
       <select name="country" id="country" class="dropdown">
        <option value=""> Select Country </option>
        <option value="UK" <?php if($EditJobPosterData['country'] == 'UK'){ ?> selected="selected" <?php } ?> >UK</option>
        <option value="Malaysia" <?php if($EditJobPosterData['country'] == 'Malaysia'){ ?> selected="selected" <?php } ?> >Malaysia</option>
       </select>
     </p>

     <p class="form-row form-row-wide">
         <!-- <label for="reg_role">Register As</label> -->
         <select name="register_as" class="vonitto-registration-role dropdown">
             <option value="">Select Register Type </option>
             <option value="individual" <?php if($EditJobPosterData['register_as'] == 'individual'){ ?> selected="selected" <?php } ?> >Individual</option>
             <option value="company" <?php if($EditJobPosterData['register_as'] == 'company'){ ?> selected="selected" <?php } ?>>Company</option>
         </select>
         <div class="clearfix"></div>
         @if ($errors->has('register_as'))<p class="error_message">{!!$errors->first('register_as')!!}</p>@endif
     </p>

     <p class="form-row form-row-wide" id="company_textbox" style="display:none;">
       <!-- <label for="reg_email">Company Name<span class="required"></span></label> -->
       <input type="text" class="input-text" name="company_name" id="company_name" value="{{$EditJobPosterData['company_name']}}" placeholder="COMPANY NAME"/>
       <img class="icon" src="{{URL::asset('resources/assets/images/icon-3.png')}}"   />
       <div class="clearfix"></div>
       @if ($errors->has('company_name'))<p class="error_message">{!!$errors->first('company_name')!!}</p>@endif
     </p>

     <p class="form-row form-row-wide" id="company_link" style="display:none;">
       <!-- <label for="reg_email">Company Link<span class="required"></span></label> -->
       <input type="text" class="input-text" name="company_link" id="company_link" value="{{$EditJobPosterData['company_link']}}" placeholder="http://"/>
       <img class="icon" src="{{URL::asset('resources/assets/images/icon-3.png')}}"   />
       <div class="clearfix"></div>
       @if ($errors->has('company_link'))<p class="error_message">{!!$errors->first('company_link')!!}</p>@endif
     </p>

            <!-- Spam Trap -->
            <p class="form-row">
                 <input type="submit" class="button" name="register" value="Save Changes" />
            </p>
          </form>
       </div>
     </div>
 </article><!-- #post -->
 </div><!-- #primary -->
 </div><!-- #main -->

 <div class="footer-cta">
      <div class="container">
          <h2>Got a question?</h2>
           <p>We're here to help. Check out our FAQs, send us an email or call us at 1 (800) 555-5555</p>
           <p><a href="#" class="button button--type-action">Contact Us</a></p>
      </div>
 </div>
 <footer id="colophon" class="site-footer" role="contentinfo">
      @include('layouts.footer')
 </footer><!-- #colophon -->
 </div>
 </body>
 </html>
<script>
$(document).ready(function() {
  var selectTypeData = $('select.vonitto-registration-role').val();
  if(selectTypeData == 'company'){
      $("#company_link").css('display','block');
      $("#company_textbox").css('display','block');
  }else{
    $("#company_link").css('display','none');
    $("#company_textbox").css('display','none');
  }
  $('select.vonitto-registration-role').change(function(){
       var selectType = $(this).val();
       if(selectType == 'individual'){
          $("#company_link").css('display','none');
          $("#company_textbox").css('display','none');
       }else{
          $("#company_link").css('display','block');
          $("#company_textbox").css('display','block');
       }
  });
});
</script>
