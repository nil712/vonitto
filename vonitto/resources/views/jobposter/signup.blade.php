<!DOCTYPE html>
<html lang="en-US">
<head>
  <!-- include header files  -->
  @include('layouts.header')
</head>
<body class="fixed-header signup_page">

	<div id="page" class="hfeed site inner_padding">
    <header id="masthead" class="site-header" role="banner">
       @include('layouts.header_menu')
    </header><!-- #masthead -->
    <div id="main" class="site-main">


    <header class="page-header">
        <h2 class="page-title">Job Poster Sign Up</h2>
    </header>

    @if(session('status') && session('status') == 'Warning')
    <div class="alert">
      <button type="button" class="close" data-dismiss="alert">×</button>
      {{ session('message') }}
    </div>
    @endif

    @if(session('status') && session('status') == 'Error')
    <div class="alert alert-error">
        <button type="button" class="close" data-dismiss="alert">×</button>
        {{ session('message') }}
    </div>
    @endif

    @if(session('status') && session('status') == 'Success')
      <div class="alert alert-success">
          <button type="button" class="close" data-dismiss="alert">×</button>
          {{ session('message') }}
      </div>
    @endif

    <div id="primary" class="content-area container" role="main">


<article id="post-1673" class="post-1673 page type-page status-publish hentry">
    <div class="entry-content">
        <div class="registration-form woocommerce register_dv loginsignupmain">

         <form class="register" id="formRegistrationPoster" enctype="multipart/form-data" role="form" method="POST" action="{{ url('jobposter') }}">
            <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">

           <div class="form-row form-row-wide">
           		<!--<label for="reg_email">FirstName <span class="required">*</span></label>-->
        			<input type="text" class="input-text" name="first_name" id="first_name" value="{{ old('first_name') }}"  placeholder="FIRSTNAME"/>
                    <img class="icon" src="{{URL::asset('resources/assets/images/icon-1.png')}}"   />
              <div class="clearfix"></div>
              @if ($errors->has('first_name'))<p class="error_message">{!!$errors->first('first_name')!!}</p>@endif</div>


         <div class="form-row form-row-wide">
         <!--<label for="reg_email">LastName <span class="required">*</span></label>-->
        			<input type="text" class="input-text" name="last_name" id="last_name" value="{{ old('last_name') }}"  placeholder="LASTNAME"/>
                    <img class="icon" src="{{URL::asset('resources/assets/images/icon-1.png')}}"   />

              <div class="clearfix"></div>
              @if ($errors->has('last_name'))<p class="error_message">{!!$errors->first('last_name')!!}</p>@endif
         </div>


    <div class="form-row form-row-wide">
    <!--<label for="reg_password">Profile Image <span class="required">*</span></label>-->
        			<!--<input type="file" class="input-text" name="image" value="{{ old('image') }}" />-->
               <div class="upload">
                <label class="brows"><input type="file" name="image" value="{{ old('image') }}" placeholder="">Upload Profile Image</label>
             </div>

              <div class="clearfix"></div>
              @if ($errors->has('image'))<p class="error_message">{!!$errors->first('image')!!}</p>@endif

    </div>


           <div class="form-row form-row-wide" >
           <!--<label for="reg_email">Email <span class="required">*</span></label>-->
        			<input type="email" class="input-text" name="email" id="email" value="{{ old('email') }}"  placeholder="EMAIL"/>
                    <img class="icon" src="{{URL::asset('resources/assets/images/icon-3.png')}}"   />
              <div class="clearfix"></div>
              @if ($errors->has('email'))<p class="error_message">{!!$errors->first('email')!!}</p>@endif


           </div>



  <div class="form-row form-row-wide">

        			<!--<label for="reg_email">Password <span class="required">*</span></label>-->
        			<input type="password" class="input-text" name="password" id="password"  placeholder="PASSWORD"/>
                    <img class="icon" src="{{URL::asset('resources/assets/images/icon-2.png')}}"   />
              <div class="clearfix"></div>
              @if ($errors->has('password'))<p class="error_message">{!!$errors->first('password')!!}</p>@endif


  </div>



<div class="form-row form-row-wide">

<!--<label for="reg_email">Confirm Password <span class="required">*</span></label>-->
        			<input type="password" class="input-text" name="password_confirmation" id="password_confirmation" placeholder="CONFIRM PASSWORD"/>
                    <img class="icon" src="{{URL::asset('resources/assets/images/icon-2.png')}}"   />
              <div class="clearfix"></div>
              @if ($errors->has('password_confirmation'))<p class="error_message">{!!$errors->first('password_confirmation')!!}</p>@endif
</div>



    <div class="form-row form-row-wide" >


     <!-- <label for="reg_role" style="text-align:left;">Register As</label> -->
                <select name="register_as" class="vonitto-registration-role dropdown">
                    <option value="" selected='selected' >Register As </option>
                    <option value="individual" >Individual</option>
                    <option value="company">Company</option>
                </select>
                <div class="clearfix"></div>
                @if ($errors->has('register_as'))<p class="error_message">{!!$errors->first('register_as')!!}</p>@endif

    </div>



        <div class="form-row form-row-wide" id="company_textbox" style="display:none;" >

        			<!-- <label for="reg_email">Company Name<span class="required"></span></label> -->
        			<input type="text" class="input-text" name="company_name" id="company_name" placeholder="COMPANY NAME"/>
              <div class="clearfix"></div>
              @if ($errors->has('company_name'))<p class="error_message">{!!$errors->first('company_name')!!}</p>@endif

        </div>

        <div class="form-row form-row-wide checkbox">
          <label class="pull-left"><input type="checkbox" name="agree" id="agree" value="1">Read and understood the <a href="{{url('/cms/terms-and-conditions')}}" >Terms and Conditions </a> and <a href="{{url('/cms/privacy-policy')}}" >Privacy Policy</a>.</label>
        </div>

        <div class="clear"></div>


          <div class="form-row" >
           <input type="hidden" id="role" name="role" value="JOB_POSTER" />
                <input type="submit" id="register" class="button pull-left" name="register" value="Register" disabled="disabled" />
          </div>

        		<!-- Spam Trap -->

        	</form>
      </div>
    </div>
</article><!-- #post -->
</div><!-- #primary -->
</div><!-- #main -->

<div class="footer-cta">
			<div class="container">
    			<h2>Got a question?</h2>
          <p>We're here to help. Check out our FAQs, send us an email or call us at 1 (800) 555-5555</p>
          <p><a href="#" class="button button--type-action">Contact Us</a></p>
			</div>
</div>
<footer id="colophon" class="site-footer" role="contentinfo">
			@include('layouts.footer')
</footer><!-- #colophon -->
</div><!-- #page -->
</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
<script type='text/javascript' src="{{URL::asset('resources/assets/js/salvattore.min0168.js')}}"></script>
<script type='text/javascript' src="{{URL::asset('resources/assets/js/jquery.validate.js')}}"></script>
<script type='text/javascript' src="{{URL::asset('resources/assets/js/bootbox.min.js')}}"></script>
</html>
<script>
$(document).ready(function() {
  $('select.vonitto-registration-role').change(function(){
       var selectType = $(this).val();
       if(selectType == 'individual'){
         $("#company_name").val('');
         $("#company_textbox").css('display','none');
       }else if(selectType == 'company'){
          $("#company_textbox").css('display','block');
       }else {
         $("#company_name").val('');
         $("#company_textbox").css('display','none');
       }
  });
});
</script>
