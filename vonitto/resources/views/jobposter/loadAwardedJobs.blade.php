@if(isset($awardedJobs) && !empty($awardedJobs))
<div class='tablediv'>
   <table>
  <thead>
  <tr>
    <th>No.</th>
    <th>Job Title</th>
    <th>Seeker Name</th>
    <th>Job Progress Status</th>
    <th>Date</th>
  </tr>
  </thead>
  {{--*/$i=1/*--}}
  <tbody>


  @foreach($awardedJobs as $job)

    <tr>
      <td data-name="No.">{{$i++}}</td>
      <td data-name="Job Title"><a href="{{url('/job/detail/'.$job->job_id)}}">{{$job->title}}</a></td>
      <td data-name="Seeker Name"><a href="{{url('user/profile/'.$job->user_id)}}">{{$job->first_name}} {{$job->last_name}}</a></td>
      <td data-name="Job Progress Status">{{$job->progress_status}}</td>
      <td data-name="Date">{{date('d-M-Y',strtotime($job->created_at))}}</td>
    </tr>

  @endforeach
  </tbody>
</table>
</div>
  @else
@endif
