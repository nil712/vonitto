@if(isset($completedJob) && !empty($completedJob))
<div class='tablediv'>
   <table id="ajax-complete">
  <thead>
  <tr>
    <th>No.</th>
    <th>Job Title</th>
    <th>Seeker Name</th>
    <th>Email</th>
    <th>Mobile</th>
    <th>Rating</th>
  </tr>
  </thead>
  {{--*/$i=1/*--}}
  <tbody>


  @foreach($completedJob as $job)
    <tr>
      <td data-name="No.">{{$i++}}</td>
      <td data-name="Job Title"><a href="{{url('/job/detail/'.$job->job_id)}}">{{$job->title}}</a></td>
      <td data-name="Seeker Name"><a href="{{url('user/profile',array($job->job_seeker_id))}}">{{$job->first_name}} {{$job->last_name}}</a></td>
      <td data-name="Job Progress Status">{{$job->email}}</td>
      <td data-name="Job Progress Status">{{$job->phone}}</td>
      <td data-name="Date"><a href="javascript:;" class="reviewRating" data-job="{{$job->job_id}}" data-rating="{{$job->rating}}" data-review="{{stripslashes($job->review)}}" data-seeker="{{$job->job_seeker_id}}" >@if(isset($job->rating) && !empty($job->rating)) {{$job->rating}}  @else 0 @endif star </a></td>
    </tr>

  @endforeach
  </tbody>
</table>
</div>
  @else
@endif
