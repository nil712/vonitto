<meta charset="UTF-8" />
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="xmlrpc.php" />
<meta name="viewport" content="initial-scale=1">
<meta name="viewport" content="width=device-width" />
<title>@yield('title') Vonitto</title>
<link rel="alternate" type="application/rss+xml" title="Extended &raquo; Feed" href="feed/index.html" />
<link rel="alternate" type="application/rss+xml" title="Extended &raquo; Comments Feed" href="comments/feed/index.html" />
<link rel='shortcut icon' type='image/x-icon' href="{{URL::asset('resources/assets/images/favicon.ico')}}" />

<link rel="stylesheet"   href="https://fonts.googleapis.com/css?family=Montserrat:400,700|Varela+Round&amp;subset=latin" type="text/css" media="all" />
<link rel="stylesheet"  href="{{URL::asset('resources/assets/css/bootstrap-select.css')}}" type="text/css" media="all" />


  <link rel="stylesheet" href="{{URL::asset('resources/assets/js/rated/min/jquery.rateyo.min.css')}}"/>

<link rel="stylesheet"  href="{{URL::asset('resources/assets/css/sharing.css')}}" type="text/css" media="all" />
 <link href="{{URL::asset('resources/assets/css/pages/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
<link rel="stylesheet"  href="{{URL::asset('resources/assets/css/style.css')}}" type="text/css" media="all" />
<link rel="stylesheet"  href="{{URL::asset('resources/assets/css/add_style.css')}}" type="text/css" media="all" />
<link rel="stylesheet"  href="{{URL::asset('resources/assets/css/default.css')}}" type="text/css" media="all" />
<link rel="stylesheet"  href="{{URL::asset('resources/assets/css/responsive.css')}}" type="text/css" media="all" />
<link rel="stylesheet"  href="{{URL::asset('resources/assets/css/custom.css')}}" type="text/css" media="all" />
<script type="text/javascript" src="{{URL::asset('resources/assets/js/jqueryf019.js')}}" ></script>
<style>
span.error{color:red;}
.error_message{color:red;}
</style>
<script> var BASE_URL = '<?php echo URL::to('/').'/'; ?>';</script>
