<?php
  use App\User;
  use App\PageCms;
  $cms_content_model=new PageCms();
  $cmsNames=$cms_content_model->getActiveCmsLink();

  if(Session::get('id') != ''){
      $id = Session::get('id');
  }else{
      $id = Auth::user()->id;
  }
  $userData = User::where('id','=',$id)->get();
  $username = ucfirst($userData[0]->first_name);
  $roleType = ucfirst($userData[0]->role);
?>
<style>
span.error{color:red;}
.error_message{color:red;}
</style>
<div class="container"> <a href="{{ url('/') }}" title="Extended" rel="home" class="site-branding">
  <h1 class="site-title"> <img src="{{URL::asset('resources/assets/images/logo.png')}}" width="" height="" alt="" /> <span>Vonitto</span> </h1>
  <h2 class="site-description">Vonitto</h2>
  </a>
  <nav id="site-navigation" class="site-primary-navigation "> <a href="#site-navigation" class="js-primary-menu-toggle primary-menu-toggle primary-menu-toggle--close">Close</a>
    <form role="search" method="get" id="searchform" class="searchform searchform--header" action="">
      <label class="screen-reader-text" for="s">Search for:</label>
      <input type="text" value="" name="s" id="s" class="searchform__input searchform--header__input" placeholder="Keywords..." />
      <button type="submit" id="searchsubmit" class="searchform__submit searchform--header__submit"><span class="screen-reader-text">Search</button>
    </form>
    <div class="nav-menu nav-menu--primary">
      <ul id="menu-main-menu" class="nav-menu nav-menu--primary">
        @if(Auth::user()->role=="JOB_SEEKER")
          <li><a href="{{ url('/browse') }}">Browse</a></li>
        @else
          <li><a href="{{ url('/browse/seeker') }}">Browse</a></li>
        @endif
        @foreach($cmsNames as $cmsNames)
            @if($cmsNames->page_name == 'How It Works')
              <li><a href="{{url('cms',array($cmsNames->slug))}}">{{$cmsNames->page_name}}</a></li>
            @endif
        @endforeach
        <?php if($roleType == 'JOB_SEEKER') { ?>
              <li class="menu-item-has-children"><a href="javascript:;" class="">Job Seeker</a>
                <ul class="sub-menu">
                  <li><a href="javascript:;">Pricing</a></li>
                  <!-- <li><a href="javascript:;">Find A Job</a></li>
                  <li><a href="javascript:;">Companies</a></li>
                  <li><a href="javascript:;">Submit Your Resume</a></li>
                  <li><a href="javascript:;">Candidate Dashboard</a></li> -->
                  <li><a href="{{url('jobseeker/myaccount')}}">My Account</a></li>
                </ul>
              </li>
              <li><a href="javascript:;"></a>
              <figure class="profile-picture">
      								<?php if(isset(Auth::user()->image) && !empty(Auth::user()->image)) { ?>
      								<img class="img-circle" src="{{ URL::asset('resources/assets/upload/jobseeker/'.Auth::user()->image) }}">
      								<?php }else{ ?>
      								<img src="{{URL::asset('resources/assets/images/user.png')}}" class="img-circle" />
      								<?php } ?>
      				</figure>
             </li>
             <li class="signup"><a href="{{ url('/jobseeker/editprofile/'.Auth::user()->id) }}" >{{ $username }}</a></li>
        <?php }else{ ?>
              <li class="menu-item-has-children"><a href="javascript:;">Job Poster</a>
                <ul class="sub-menu">
                  <li><a href="{{ url('/jobpost/') }}">Post A Job</a></li>
                  <li><a href="{{url('/')}}">My Account</a></li>
                </ul>
              </li>
              <?php if($roleType == 'JOB_POSTER') { ?>
                <li><a href="javascript:;"></a>
                <figure class="profile-picture">
                        <?php if(isset(Auth::user()->image) && !empty(Auth::user()->image)) { ?>
                        <img class="img-circle" src="{{ URL::asset('resources/assets/upload/jobposter/'.Auth::user()->image) }}">
                        <?php }else{ ?>
                        <img src="{{URL::asset('resources/assets/images/user.png')}}" class="img-circle" />
                        <?php } ?>
                </figure>
                </li>
              <?php }else{ ?>
                <li><a href="javascript:;"></a>
                <figure class="profile-picture">
                        <img src="{{URL::asset('resources/assets/images/user.png')}}" class="img-circle" />
                </figure>
                </li>
              <?php } ?>
              <li class="signup"><a href="{{ url('/jobposter/editprofile/'.Auth::user()->id) }}" >{{ $username }}</a></li>
        <?php } ?>
        <li class="login"><a href="{{ url('logout') }}" >logout</a></li>
      </ul>
    </div>
  </nav>
  <a href="#site-navigation" class="js-primary-menu-toggle primary-menu-toggle primary-menu-toggle--open"><span class="screen-reader-text">Menu</span></a> </div>
