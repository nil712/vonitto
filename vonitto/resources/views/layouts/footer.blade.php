<?php
use App\PageCms;
$cms_content_model=new PageCms();
$cmsNames=$cms_content_model->getActiveCmsLink();

?>
<div class="footer-widgets">
    <div class="container">
      <div class="row">
        <div class="col-xs-12 col-md-6 col-lg-3">
          <aside id="text-1" class="widget widget--footer widget_text">
            <div class="textwidget">
              <img src="{{URL::asset('resources/assets/images/logo_black.png')}}" type="image/svg+xml" width="100%" />
              <br>
              </br>
              Job Searching Just Got Easy. Use Pitch Your Concepts to run a hiring site and earn money in the process!</div>
          </aside>
        </div>
        <div class="col-xs-12 col-md-6 col-lg-3">
          <aside id="pages-1" class="widget widget--footer widget_pages">
            <h3 class="widget-title widget-title--footer">Site Map</h3>
            <ul>
              <li class="page_item page-item-99991379"><a href="javascript:;">JobSeeker Dashboard</a></li>
              <li class="page_item page-item-2064"><a href="javascript:;">Companies</a></li>
              <li class="page_item page-item-99991381"><a href="javascript:;">Find A Candidate</a></li>
              <li class="page_item page-item-3940"><a href="javascript:;">Find A Job</a></li>
              <li class="page_item page-item-25 current_page_item"><a href="javascript:;">Home</a></li>
              <li class="page_item page-item-99991394"><a href="javascript:;">How It Works</a></li>
              <li class="page_item page-item-7"><a href="javascript:;">Job Dashboard</a></li>
              <li class="page_item page-item-8"><a href="javascript:;">Jobs</a></li>
              <li class="page_item page-item-2454"><a href="javascript:;">Login</a></li>
              <li class="page_item page-item-99991207"><a href="javascript:;">My Account</a></li>
              <li class="page_item page-item-99991191"><a href="javascript:;">Post A Job</a></li>
              <li class="page_item page-item-99991374"><a href="javascript:;">Pricing</a></li>
              <li class="page_item page-item-1673"><a href="javascript:;">Sign Up</a></li>
              <li class="page_item page-item-99991378"><a href="javascript:;">Submit Your Resume</a></li>
            </ul>
          </aside>
        </div>
        <div class="col-xs-12 col-md-6 col-lg-3">
          <aside id="recent-posts-3" class="widget widget--footer widget_recent_entries">
            <h3 class="widget-title widget-title--footer">Pages</h3>
            <ul>
              @foreach($cmsNames as $cmsNames)
                  <li class="page_item"><a href="{{url('cms',array($cmsNames->slug))}}">{{$cmsNames->page_name}}</a></li>
              @endforeach
            </ul>
          </aside>
        </div>
        <div class="col-xs-12 col-md-6 col-lg-3">
          <aside id="text-2" class="widget widget--footer widget_text">
            <h3 class="widget-title widget-title--footer">Pitch Your Concepts Offices</h3>
            <div class="textwidget">
              Pitch Your Concepts Inc.<br>
              Company Code : 9906029 </div>
              <!-- 555 Madison Avenue, Suite F-2
              Manhattan, New York 10282 <br>
              </br>
              Pitch Your Concepts Inc Canada.
              545 Younge St, Suite 11
              Toronto, Ontario M4K 6F4 -->
              <h3 class="widget-title widget-title--footer">Follow Us</h3>
              <div class="socials">
                <ul>
                    <li><a title="Google+" href="https://plus.google.com/105429965581389546552/posts" target="_blank"> <img src="{{URL::asset('resources/assets/images/social-1.png')}}"> </a> </li>
                    <li><a title="Twitter" href="https://twitter.com/PitchUrConcepts" target="_blank"> <img src="{{URL::asset('resources/assets/images/social-2.png')}}"> </a> </li>
                    <li><a title="Facebook" href="https://www.facebook.com/PitchYourConcepts" target="_blank"> <img src="{{URL::asset('resources/assets/images/social-3.png')}}"> </a> </li>
                    <li><a title="LinkedIn" href="#" target="_blank"> <img src="{{URL::asset('resources/assets/images/social-4.png')}}"> </a> </li>
                    <li><a title="Youtube" href="https://www.youtube.com/channel/UC2_1L2iDekDw6AU2E97vLOA" target="_blank" > <img src="{{URL::asset('resources/assets/images/social-5.png')}}"> </a> </li>
                </ul>
              <div>
          </aside>
        </div>
      </div>
    </div>
  </div>
  <div class="copyright">
    <div class="container">
      <div class="site-info"> &copy; 2016 Pitch Your Concepts &mdash; All Rights Reserved </div>
      <!-- .site-info -->

      <div class="footer-social">
          <a href="javascript:;"><span class="screen-reader-text">Facebook</span></a>
          <a href="javascript:;"><span class="screen-reader-text">Twitter</span></a>
          <a href="javascript:;"><span class="screen-reader-text">Google Plus</span></a>
          <a href="javascript:;"><span class="screen-reader-text">Instagram</span></a>
          <a href="javascript:;"><span class="screen-reader-text">Pinterest</span></a>
          <a href="javascript:;"><span class="screen-reader-text">Vimeo</span></a>
          <a href="javascript:;"><span class="screen-reader-text">Linkedin</span></a>
      </div>
      <a href="#top" class="btt "><span class="screen-reader-text">Back to Top</span></a> </div>
  </div>
<!-- #colophon -->
</div>
<!-- #page -->
<script type='text/javascript' src="{{ URL::asset('resources/assets/js/bootstrap.min.js') }}" type="text/javascript"></script>
<script type='text/javascript' src="{{URL::asset('resources/assets/js/salvattore.min0168.js')}}"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>

<script type='text/javascript' src="{{URL::asset('resources/assets/js/custom.js')}}"></script>
