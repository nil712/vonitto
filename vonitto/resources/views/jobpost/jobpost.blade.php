<!DOCTYPE html>
<html lang="en-US">
<head>
  <!-- include header files  -->
  @include('layouts.header')
  <link rel="stylesheet" media="all" type="text/css" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
	<link rel="stylesheet" media="all" type="text/css" href="{{URL::asset('resources/assets/css/jquery-ui-timepicker-addon.css')}}" />
  <link href="{{URL::asset('resources/assets/js/dropzone/css/dropzone.css')}}" rel="stylesheet"/>
</head>
<body class="fixed-header jobpost">

	<div id="page" class="hfeed site inner_padding">
    <header id="masthead" class="site-header" role="banner">
       @include('layouts.inner.header_inner_menu')
    </header><!-- #masthead -->
    <div id="main" class="site-main">


    <header class="page-header">
        <h2 class="page-title">Post A Job</h2>
    </header>

  <div class="messageBox" style="display:none;"></div>
    <div id="primary" class="content-area container" role="main">


<article id="post-1673" class="post-1673 page type-page status-publish hentry">
    <div class="entry-content">
        <div class="registration-form woocommerce register_dv loginsignupmain jobpostlogin">
         <form  class="register" id="formJobPost" enctype="multipart/form-data" role="form" method="POST" action="{{url('jobpost')}}" >
            <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
            <div class="form-row form-row-wide">
      			 <input type="text" class="input-text" name="title" id="title" value="{{ old('title') }}"  placeholder="JOB TITLE"/>
              <div class="clearfix"></div>
            </div>
         <div class="form-row form-row-wide">
       			<textarea style="resize:none;border-radius:0px;" rows="5"  class="" name="description" id="description" value="{{ old('description') }}"  placeholder="DESCRIPTION"></textarea>
              <div class="clearfix"></div>
         </div>

         <div class="form-row form-row-wide">
       			<textarea style="resize:none;border-radius:0px;" rows="5" class="" name="what_you_want" id="what_you_want" value="{{ old('what_you_want') }}"  placeholder="Tell us what you want to get done "></textarea>
              <div class="clearfix"></div>
         </div>

            <div class="form-row form-row-wide">
            <label for="category" style="text-align:left;">Category <span class="required">*</span></label>
       			<select name="category_id" class="dropdown" id="category_id">
                <option value=""> Select Category </option>
                @foreach($categories as $category)
                  <option value="{{$category['id']}}"> {{$category['name']}} </option>
                @endforeach
              </select>
              <div class="clearfix"></div>
            </div>




          <div class="form-row form-row-wide">
           <!--<label for="budget">Project Deadline</label>-->
                        <input type="text" class="input-text" name="project_deadline" placeholder="Project Deadline Date" id="project_deadline" value="">
                        <div class="clearfix"></div>
          </div>


           <div class="form-row form-row-wide" >

           <!-- <label for="budget">Estimated Job Budget </label>-->
              <input type="text" maxlength="12" class="input-text" name="budget" id="budget"  placeholder="Estimated Job Budget"/>
              <div class="clearfix"></div>
              <select name="currency" class="dropdown" id="currency">
                <option value="GBP">GBP</option>
                <option value="MYR">MYR</option>
              </select>
           </div>
           <div class="form-row form-row-wide" >
             <div class="dropzone" id="add-dropzone">
             </div>
               <div class="clearfix"></div>
               <p class="error_message">Files could include pitch,video,pdf,word,excel,picture etc..</p>
           </div>
         <div class="form-row form-row-wide" >
             <div class="upload">
               <label class="brows" ><input type="file" name="job_logo" placeholder="" id="job_logo" multiple>Logo</label>
            </div>
              <div class="clearfix"></div>
         </div>
         <div class="form-row form-row-wide">
             <!--<label for="what_you_want">What skills should the job seeker have? </label>-->
              <textarea style="resize:none;" class="input-text" name="skills" id="skills" value="{{ old('what_you_want') }}"  placeholder="What skills should the job seeker have? "></textarea>
              <div class="clearfix"></div>
         </div>
         <div class="form-row form-row-wide" >

      <label for="category" style="text-align:left;">How long do you expect this job to last?   <span class="required">*</span></label>
        			<select name="how_long" class="dropdown" id="how_long">
                <option value=""> Select duration </option>
                <option value="More than 6 months"> More than 6 months </option>
                <option value="3 to 6 months"> 3 to 6 months </option>
                <option value="1 to 3 months"> 1 to 3 months </option>
                <option value="Less than 1 month"> Less than 1 month </option>
                <option value="Less than 1 week"> Less than 1 week </option>
              </select>
              <div class="clearfix"></div>
      </div>


        <div class="form-row form-row-wide">
        			<label for="location_country" style="text-align:left;"> Country </label>
        			<select name="location_country"class="dropdown" id="location_country">
                <option value=""> Select Country </option>
                <option value="UK"> UK </option>
                <option value="Malaysia"> Malaysia </option>
              </select>
              <div class="clearfix"></div>
            </div>




           <div class="form-row form-row-wide" >
            <!--<label for="location_city">City </label>-->
                <input type="text" class="input-text" name="location_city" id="location_city"  placeholder="City"/>
                <div class="clearfix"></div>
             </div>
            <div class="form-row form-row-wide" >
                <input type="text" class="input-text" name="postcode" id="postcode"  placeholder="Post code"/>
                <div class="clearfix"></div>
             </div>
            <div class="form-row" >
              <input type="hidden" name="file_names" id="file_names" class="file_names" />
              <input type="submit" class="button" id="submitData" name="register" value="Submit" />
            </div>


        		<!-- Spam Trap -->

        	</form>
      </div>
    </div>
</article><!-- #post -->
</div><!-- #primary -->
</div><!-- #main -->

<div class="footer-cta">
			<div class="container">
    			<h2>Got a question?</h2>
          <p>We're here to help. Check out our FAQs, send us an email or call us at 1 (800) 555-5555</p>
          <p><a href="#" class="button button--type-action">Contact Us</a></p>
			</div>
</div>
<footer id="colophon" class="site-footer" role="contentinfo">
			@include('layouts.footer')
</footer><!-- #colophon -->
</div><!-- #page -->
</body>


<!--<script type='text/javascript' src="{{URL::asset('resources/assets/js/vonitto.minfcbd.js')}}"></script>-->
<script type='text/javascript' src="{{URL::asset('resources/assets/js/salvattore.min0168.js')}}"></script>
<script type='text/javascript' src="{{URL::asset('resources/assets/js/jquery.validate.js')}}"></script>
<script type='text/javascript' src="{{URL::asset('resources/assets/js/bootbox.min.js')}}"></script>

<script type="text/javascript" src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
<script type="text/javascript" src="http://code.jquery.com/ui/1.10.3/jquery-ui.min.js"></script>
<script type="text/javascript" src="{{URL::asset('resources/assets/js/jquery-ui-sliderAccess.js')}}"></script>
<script type='text/javascript' src="{{URL::asset('resources/assets/js/jquery-ui-timepicker-addon.js')}}"></script>
<script src="{{URL::asset('resources/assets/js/jQuery-validation-plugin/jquery.validate.js')}}"></script>
<script src="{{URL::asset('resources/assets/js/jQuery-validation-plugin/additional-methods.js')}}"></script>
<script src="{{URL::asset('resources/assets/js/pages/poster/create-post.js')}}"></script>
	<script type="text/javascript">
		$(function(){
      $('#project_deadline').datetimepicker(
    	{
    		minDate: 0,
    	});
      $.getScript("{{URL::asset('resources/assets/js/dropzone/dropzone.js')}}",function(){
      // instantiate the uploader
        $('#add-dropzone').dropzone({
          url:BASE_URL+'jobposter/job-post/upload-files',
          maxFilesize: 100,
          uploadMultiple: true,
          parallelUploads: 10,
          maxFiles: 10,
          addRemoveLinks: true,
          autoProcessQueue: true,
          dictFileTooBig: 'File should not be bigger than 100MB',
          dictDefaultMessage: "<label class='dropzone-message-container'><i class='fa fa-cloud-upload'></i><div class='clearfix'></div> Drop images to upload</label>",
          headers: {'x-csrf-token': $("#token").val(),},
          acceptedFiles: ".jpeg,.jpg,.png,.gif,.JPEG,.JPG,.PNG,.GIF,.pdf,.PDF,.docx,.DOCX,.docm,.DOCM,.dotx,.DOTX,.dotm,.DOTM,.xlsx,.XLSX,.xlsm,.XLSM,.xltx,.XLTX,.xltm,.XLTM,.xlsb,.XLSB,.xlam,.XLAM,.mp4,.MP4",
          //maxThumbnailFilesize: 5,
          init: function() {

            this.on('success', function(file, json) {
              if(json.success ==true){
                var stringToAppend = $("#file_names").val().length > 0 ? $("#file_names").val() + "," : "";
                $("#file_names") .val( stringToAppend + json.data );
              }
            });
            this.on('addedfile', function(file) {
            });
            this.on('drop', function(file) {

            });
          }
        });
    });
  });



    //DropZone Initialization
	</script>

</html>
