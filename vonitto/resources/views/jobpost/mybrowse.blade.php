<!DOCTYPE html>
<html lang="en-US">

<head>
  <!-- include header files  -->
  @include('layouts.header')
  <link rel="stylesheet" href="{{URL::asset('resources/assets/css/jPages.css')}}">
</head>
<body class="fixed-header browse_page">

	<div id="page" class="hfeed site inner_padding">
    <header id="masthead" class="site-header" role="banner">
       @include('layouts.inner.header_inner_menu')
    </header><!-- #masthead -->
    <div id="main" class="site-main">


    <header class="page-header">
        <h2 class="page-title">Browse your jobs here</h2>
    </header>



    <div id="primary" class="content-area" role="main">


    <div class="content-area">


<div class="entry-content">
<div class="job_listings" >
    <form class="job_filters" id="formJobFilter" role="form" method="POST" action="{{ url('/browse/search') }}">
      <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
        <div class="search_jobs">
          <div class="container">
            <div class="search_keywords">
                <label for="search_keywords">Keywords</label>
                @if(isset($searchKeyWords['search_keywords']) && $searchKeyWords['search_keywords'] != '')
                  <input type="text" name="search_keywords" id="search_keywords" value="{{$searchKeyWords['search_keywords']}}" />
                @else
                    <input type="text" name="search_keywords" id="search_keywords" placeholder="Search By" value="{{old('search_keywords')}}" />
                @endif
            </div>


            <div class="search_location">
                <label for="search_location">Location</label>
                @if(isset($searchKeyWords['search_location']) && $searchKeyWords['search_location'] != '')
                  <input type="text" name="search_location" id="search_location" value="{{$searchKeyWords['search_location']}}" />
                @else
                  <input type="text" name="search_location" id="search_location" placeholder="Location" value="" />
                @endif

            </div>
            <div class="search_categories">

               <select class="selectpicker" name="category_id">
                    <option value="">Select Category</option>
                    @foreach($categories as $category)
                      @if(isset($catid) && !empty($catid))
                          @if($category['id'] == $catid)
                            <option selected value="{{$category['id']}}"> {{$category['name']}} </option>
                          @else
                          <option value="{{$category['id']}}"> {{$category['name']}} </option>
                          @endif
                      @else
                        <option value="{{$category['id']}}"> {{$category['name']}} </option>
                      @endif
                    @endforeach
                </select>
            </div>

            <div class="search_submit">
                <input type="submit" name="submit" value="Search" id="searchBtn" />
            </div>
            <ul class="job_types sorting_list jobfilter">
          <li><span>Sort By | </span>
            @if(isset($searchKeyWords['sort_by']) && $searchKeyWords['sort_by'] == 'ASC')
              <a href="javascript:void(0)" class="DESC" id="sortByDeadline">Deadline </a>
            @else
              <a href="javascript:void(0)" class="ASC" id="sortByDeadline">Deadline </a>
            @endif


          </li>
          <input type="hidden" id="sort_by" name="sort_by" value="" />
			  </ul>
        <div class="clearfix"></div>
            <!--<div class="filter_wide filter_by_tag">Filter by tag: <span class="filter_by_tag_cloud"></span></div>-->
          </div>
        </div>


    </form>
    <?php if($jobs){  ?>
    <ul class="job_listings_new container" id="job_listings">
        @foreach($jobs as $job)
        <li class="job_listing_list">

             <div class="resiexperince_dv">

             <div class="col-sm-12 browse_devide_Dv">

             <div class="rightimg_dv">
              <img src="{{URL::asset('/resources/assets/upload/joblogo/'.$job['job_logo'])}}" class="img_browse">
             <div class="savebtn_dv">
                       <div class="oneday_left">
                       <ul class="">

              <?php
                $diff=strtotime($job['project_deadline'])-time();//time returns current time in seconds
                $days=ceil($diff/(60*60*24));//seconds/minute*minutes/hour*hours/day)
               ?>
               @if($days > 0)
                <li class=""><?php echo "$days days left"; ?></li>
               @else
                <li class="">Expired</li>
               @endif
              </ul>
                      </div>
                      <div class="savebtn">

                            @if(isset($job->bookmark_status) && !empty($job->bookmark_status) && $job->bookmark_status=='bookmark' &&  $job->seeker_id==Auth::id())
                                <a href="javascript:;" class="job_bookmark" data-status='unbookmark' data-seeker="{{Auth::id()}}" data-poster="{{$job->job_poster_id}}" data-job="{{$job->id}}"><i class="icon ion-android-star"></i>Bookmark</a>
                            @else
                                <a href="javascript:;" class="job_bookmark" data-status='bookmark' data-seeker="{{Auth::id()}}" data-poster="{{$job->job_poster_id}}" data-job="{{$job->id}}"><i class="icon ion-android-star-outline"></i>Bookmark</a>
                            @endif
                      </div>

                      </div>

                 </div>
             <div class="left_txt">
              <h3>{{$job['title']}}</h3>

                 <h4> {{$job['location_city']}}  <span>{{$job['currency']}} {{$job['budget']}} @if(isset($job['company_name']) && !empty($job['company_name'])) | {{stripslashes($job['company_name'])}} @endif</span>	</h4>
              <p>  {{$job['description']}}</p>

              <div class="viewdetails_row">
                      <a class="details" href="{{ url('/job/detail/'.$job['id']) }}">View Details</a>

                 </div>
             </div>
             </div>
             </div>
        </li>
        @endforeach
    </ul>
<a class="load_more_jobs" href="#" ></a></div>
<?php }else{ ?>
  <ul class="job_listings">
    <li>No Record Found.</li>
  </ul>
<?php }?>
 <div class="holder container"></div>
</div>

</div>

</div><!-- #primary -->
</div><!-- #main -->

<div class="footer-cta">
			<div class="container">
    			<h2>Got a question?</h2>
          <p>We're here to help. Check out our FAQs, send us an email or call us at 1 (800) 555-5555</p>
          <p><a href="#" class="button button--type-action">Contact Us</a></p>
			</div>
</div>
<footer id="colophon" class="site-footer" role="contentinfo">
			@include('layouts.footer')
</footer><!-- #colophon -->
</div><!-- #page -->
</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
<!--<script type='text/javascript' src="{{URL::asset('resources/assets/js/vonitto.minfcbd.js')}}"></script>-->
<script type='text/javascript' src="{{URL::asset('resources/assets/js/jPages.js')}}"></script>
<script type='text/javascript' src="{{URL::asset('resources/assets/js/salvattore.min0168.js')}}"></script>
<script type='text/javascript' src="{{URL::asset('resources/assets/js/jquery.validate.js')}}"></script>
<script type='text/javascript' src="{{URL::asset('resources/assets/js/bootbox.min.js')}}"></script>
<script type='text/javascript' src="{{URL::asset('resources/assets/js/bootstrap-select.min.js')}}"></script>

<script type="text/javascript">
$(function(){

  /* initiate the plugin */
  $("div.holder").jPages({
    containerID  : "job_listings",
    perPage      : 10,
    startPage    : 1,
    startRange   : 1,
    midRange     : 5,
    endRange     : 1
  });

});

	window.onload=function(){
		$('.selectpicker').selectpicker();
	};
  $(document).ready(function(){
    $('#sortByDeadline').on('click',function(){
      var sortby = $(this).attr('class');
      //alert(sortby);
      $('#sort_by').val(sortby);
      $('#searchBtn').trigger('click');
    })
  });

  /* Developer Name : Hitesh Tank
   * Date : 5-Aug-2016
   */
  $(document).on("click",".job_bookmark",function(event){
      event.preventDefault();
      var token = $("#token").val();
      var dataString={job_id:$(this).data('job'),poster_id:$(this).data('poster'),status:$(this).attr('data-status')};
      var $currentObj=$(this);
      $.ajax({
        type: "POST",
        headers: {'X-CSRF-TOKEN': token},
        url: BASE_URL+"jobseeker/job/mark-action",
        cache: false,
        data: JSON.stringify({ data: dataString }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {

        },
       success: function(data) {
         if(data.status == 1){
           if(data.updated =='bookmark'){
              $currentObj.attr("data-status",'unbookmark');
              $currentObj.children().removeClass("ion-android-star-outline").addClass('ion-android-star');
          }else{
            $currentObj.children().removeClass("ion-android-star").addClass('ion-android-star-outline');
            $currentObj.attr("data-status",'bookmark');
          }
       }
     }
     });
  });

</script>
</html>
