<!DOCTYPE html>

<html lang="en-US">
<head>
    <!-- include header files  -->
    @include('layouts.header')
    <link href="{{URL::asset('resources/assets/js/dropzone/css/dropzone.css')}}" rel="stylesheet"/>
    <style>
    .tablediv input.actionbtn:hover {
          border: 2px solid #000;
          color: #000;
      }
    </style>
</head>
<body class="fixed-header jobdetail_page">
    <div id="page" class="hfeed site inner_padding">
        <header id="masthead" class="site-header" role="banner"> @include('layouts.inner.header_inner_menu') </header>
        <!-- #masthead -->
        <div id="main" class="site-main">
            <header class="page-header">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-9">
                            <h2 class="page-title">{{ $jobDetail['title']}}</h2>
                            <h3 class="page-subtitle">
                              <ul class="job-listing-meta meta">
                                  @if($jobDetail['project_deadline'])
                                      {{--*/ $diff=strtotime($jobDetail['project_deadline'])-time() /*--}}
                                      {{--*/ $days=floor($diff/(60*60*24)) /*--}}
                                  @else
                                      {{--*/ $diff=strtotime($jobDetail['project_deadline'])-time() /*--}}
                                      {{--*/ $days=floor($diff/(60*60*24)) /*--}}
                                  @endif
                                  @if($days > 0)
                                   <li class="job-type part-time" itemprop="employmentType"><?php echo "$days days left"; ?></li>
                                  @else
                                  @endif
                                  <li class="location" itemprop="jobLocation">
                                      @if(strtolower($jobDetail['location_country']) == 'uk')
                                        {{substr($jobDetail['postcode'],2,strlen($jobDetail['postcode']))}}
                                      @elseif(strtolower($jobDetail['location_country']) == 'malaysia')
                                        {{$jobDetail['postcode']}}
                                      @endif

                                  </li>
                                  <li itemprop="datePosted">JOB ID : {{ $jobDetail['job_project_id'] }}</li>
                              </ul>
                            </h3>
                      </div>
                        <div class="col-sm-3">
                            <aside id="jobify_widget_job_apply-2" class="widget widget--job_listing widget--job_listing-top jobify_widget_job_apply">
                                <div class="job_application application">
                                    @if(isset(Auth::user()->id) && Auth::user()->role == "JOB_POSTER")
                                    <p>As a job poster you can not pitch job</p>
                                    @else @if(isset($pitched) && $pitched == "yes")
                                    <p>Thanks for pitching this job.</p>
                                    @else @if($days > 0) @if($is_pitched=="0")
                                    <a href="javascript:;" data-id="{{$jobDetail['id']}}" class="application_button button job_pitch_btn"  id="pitchLink">Pitch</a> @else
                                    <a href="#" class="application_button button">Already Pitched</a> @endif
                                        @if(isset($jobDetail['bookmark_status']) && !empty($jobDetail['bookmark_status']) && $jobDetail['bookmark_status']=='bookmark' &&  $jobDetail['seeker_id']==Auth::id())
                                          <a href="javascript:;"  data-status='unbookmark' data-seeker="{{Auth::id()}}" data-poster="{{$jobDetail['job_poster_id']}}" data-job="{{$jobDetail['id']}}" class="application_button button job_bookmark">
                                            <i class="icon ion-android-star"></i>
                                            Bookmarked
                                          </a>
                                        @else
                                            <a href="javascript:;" data-status='bookmark' data-seeker="{{Auth::id()}}" data-poster="{{$jobDetail['job_poster_id']}}" data-job="{{$jobDetail['id']}}" class="application_button button job_bookmark">
                                              <i class="icon ion-android-star-outline"></i>
                                              Bookmark
                                            </a>
                                        @endif
                                     @else
                                    <a href="#" class="application_button button">Expired</a>
                                            @if(isset($jobDetail['bookmark_status']) && !empty($jobDetail['bookmark_status']) && $jobDetail['bookmark_status']=='bookmark' &&  $jobDetail['seeker_id']==Auth::id())
                                              <a href="javascript:;"  data-status='unbookmark' data-seeker="{{Auth::id()}}" data-poster="{{$jobDetail['job_poster_id']}}" data-job="{{$jobDetail['id']}}" class="application_button button job_bookmark">
                                                <i class="icon ion-android-star"></i>
                                                Bookmarked
                                              </a>
                                            @else
                                                <a href="javascript:;"  data-status='bookmark' data-seeker="{{Auth::id()}}" data-poster="{{$jobDetail['job_poster_id']}}" data-job="{{$jobDetail['id']}}" class="application_button button job_bookmark">
                                                  <i class="icon ion-android-star-outline"></i>
                                                  Bookmark
                                                </a>
                                            @endif
                                    @endif @endif @endif
                                </div>
                            </aside>
                        </div>
                    </div>
                </div>
            </header>
            <div id="primary" class="content-area container" role="main">
                <div id="main" class="site-main">
                    <div class="single_job_listing" itemscope itemtype="http://schema.org/JobPosting">
                        <div id="content" class="container content-area" role="main">
                            <div class="job-overview-content row">
                                <div itemprop="description" class="job_listing-description job-overview col-md-12 col-sm-12">
                                    <!-- <h2 class="widget-title widget-title--job_listing-top job-overview-title">Overview</h2>-->
                                    @if(isset(Auth::user()->id) && Auth::user()->role == "JOB_SEEKER")
                                      <div class="jobdetail_row jobdetail_required">
                                          <h4>Job Poster</h4>
                                          <p><a href="{{url("user/profile/".$jobDetail['getposter']['id'])}}">{{$jobDetail["getposter"]["first_name"]}} {{$jobDetail["getposter"]["last_name"]}}</a></p>
                                      </div>
                                    @endif
                                    <div class="jobdetail_row jobdetail_overview">
                                        <h3>Project Description</h3>
                                        <p>{{ $jobDetail['description']}}</p>
                                    </div>
                                    <div class="jobdetail_row jobdetail_keyobjective">
                                        <h4>Key Objectives</h4>
                                        <p>{{ $jobDetail['what_you_want']}}</p>
                                    </div>
                                    <!-- <div class="jobdetail_row jobdetail_kindof">
                                        <h4>Kind of job</h4>
                                        <p>{{ $jobDetail['what_kind_job']}}</p>
                                    </div> -->
                                    <div class="jobdetail_row jobdetail_required">
                                        <h4>Required Knowledge, Skills, and Abilities</h4>
                                        <p>{{ $jobDetail['skills']}}</p>
                                    </div>
                                    @if($jobDetail['file']!=null && $jobDetail['file']!="")
                                      <div class="jobdetail_row jobdetail_required">
                                          <h4>Attached Document</h4>
                                          <ul>
                                            {{--*/$i=1/*--}}
                                            @foreach($jobDetail['file'] as $file)
                                             <li> <a href="{{url('download/documents/'.$file)}}">Attachment {{$i++}}</a><br></li>
                                            @endforeach
                                        </ul>
                                      </div>
                                    @endif

                                </div>
                            </div>
                        </div>
                        @if(isset(Auth::user()->id) && Auth::user()->role == "JOB_POSTER")
                        <div id="content" class="container content-area pitchedJobs" role="main">
                          @if(isset($pitchedRequest) && !empty($pitchedRequest))
                              <div class='tablediv jobpost_table'>
                                 <table>
                                <thead>
                                <tr>
                                  <th class="sr">No.</th>
                                  <th class="sr_title">Job Title</th>
                                  <th class="sr_discription">Job Description</th>
                                  <th class="sr_name">Documents</th>
                                  <th class="sr_name">Seeker Name</th>
                                  <th class="sr_date">Date</th>
                                  <th class="sr_action">Action</th>
                                </tr>
                              </thead>
                                {{--*/$i=1/*--}}
                                <tbody>
                                @foreach($pitchedRequest as $pitch)
                                  <tr>
                                    <td data-name="No.">{{$i++}}</td>
                                    <td data-name="Job Title"><a href="{{url('/job/detail/'.$pitch->job_id)}}">{{$pitch->title}}</a></td>
                                    <td data-name="Job Description"><p>{{$pitch->description}}</p></td>
                                    <td data-name="Seeker Name">
                                      <p>
                                        {{--*/$files=array();/*--}}
                                        @if((isset($pitch->attached_documents) && !empty($pitch->attached_documents)) || (isset($pitch->comments) && !empty($pitch->comments)))
                                            <a href="javascript:;" class="view_documents" data-comments="{{stripslashes($pitch->comments)}}" data-files="{{$pitch->attached_documents}}"  >Documents</a>
                                        @else
                                           -
                                        @endif
                                      </p>
                                    </td>
                                    <td data-name="Seeker Name"><a href="{{url('user/profile/'.$pitch->job_seeker_id)}}">{{$pitch->first_name}} {{$pitch->last_name}}</a></td>
                                    <td data-name="Date">{{date('d-M-Y',strtotime($pitch->created_at))}}</td>
                                    @if($pitch->is_awarded=="0")
                                     <td data-name="Action"><input type="button" value="Award" class="btnAward actionbtn" data-job="{{$pitch->job_id}}" data-title="{{$jobDetail['title']}}" data-user="{{$pitch->job_seeker_id}}"></td>
                                    @else
                                     <td data-name="Action"><input type="button" value="@if($pitch->is_assigned) Awarded @else Rejected @endif" data-title="{{$jobDetail['title']}}" class="@if($pitch->is_assigned) btnAward  @else btnAssigned @endif actionbtn" data-job="{{$pitch->job_id}}" data-user="{{$pitch->job_seeker_id}}"></td>
                                    @endif
                                  </tr>
                                @endforeach
                                </tbody>
                              </table>
                            @else
                          @endif
                        </div>
                        @endif
                    </div>
                    <div id="page-selection1"></div>
                </div>
            </div>
            <!-- #primary -->
        </div>
        <!-- #main -->
        <div class="footer-cta">
            <div class="container">
                <h2>Got a question?</h2>
                <p>We're here to help. Check out our FAQs, send us an email or call us at 1 (800) 555-5555</p>
                <p><a href="#" class="button button--type-action">Contact Us</a>
                </p>
            </div>
        </div>
        <footer id="colophon" class="site-footer" role="contentinfo"> @include('layouts.footer') </footer>
        <!-- #colophon -->

    <!-- pitch Modal -->
    <div id="pitchModal" class="modal fade" role="dialog">
     <div class="modal-dialog">

       <!-- Modal content-->
       <div class="modal-content">
         <div class="modal-header">
           <button type="button" class="close" data-dismiss="modal">&times;</button>
           <h4 class="modal-title">Pitch on Project</h4>
         </div>
         <form  class="pitchYourProjectForm" name="pitchYourProjectForm" id="pitchYourProjectForm"  role="form" method="POST" enctype="multipart/form-data" >
           <div class="messageBox" style="display:none;"></div>

           <div class="modal-body">
             <p class="form-row form-row-wide">
                 <label for="comment">Upload Documents</label>
                 <div class="dropzone" id="add-dropzone">
                 </div>
            </p>
            <p class="comment-form-comment">
              <label for="comment">Your Comments</label>
              <textarea style="resize:none;" aria-required="true" rows="8" cols="5" name="comments" id="comments" name="comments"></textarea>
            </p>
           </div>
           <div class="modal-footer">
             <input type="hidden" id="file_names" name="file_names" class="file_names" />
             <input type="hidden" id="post_id" name="post_id" class="post_id" />
             <input type="hidden" id="job_title" name="job_title" class="job_title" value="{{$jobDetail['title']}}" />

             <button type="submit" class="btn btn-primary pitchJobButton" id="pitchJobButton" value="Pitch">Pitch</button>
             <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
           </div>
        </form>
       </div>

     </div>
   </div>
   <!-- pitch Modal -->
   <!-- View Document Model -->
       <div id="viewDocumentModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">Seeker Documents & Comments</h4>
            </div>
            <form>
              <div class="messageBox" style="display:none;"></div>

              <div class="modal-body">
                <p class="form-row form-row-wide documents">
                    <label for="comment">Documents</label>
                    <span class="file_download">
                    </span>
               </p>
               <p class="comment-form-comment comments">
                 <label for="comment">Comments</label>
                 <textarea style="resize:none;" class="comment_text" readonly="readonly" aria-required="true" rows="8" cols="5"></textarea>
               </p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
           </form>
          </div>

        </div>
      </div>
      <!-- View Document Model -->
    </div>
    <!-- #page -->
<input type="hidden" value="{{csrf_token()}}" id="token" name="_token">
<!--<script type='text/javascript' src="{{URL::asset('resources/assets/js/vonitto.minfcbd.js')}}"></script>-->

<script src="{{URL::asset('resources/assets/js/jquery.js')}}"></script>
<script type='text/javascript' src="{{URL::asset('resources/assets/js/salvattore.min0168.js')}}"></script>
<script type='text/javascript' src="{{URL::asset('resources/assets/js/jquery.validate.js')}}"></script>
<script src="{{URL::asset('resources/assets/js/bootpag.min.js')}}"></script>
<script src="{{URL::asset('resources/assets/js/bootstrap.min.js')}}"></script>
<script src="{{URL::asset('resources/assets/js/bootbox.min.js')}}"></script>

<script type="text/javascript">
$(document).ready(function(){

  $('#page-selection1').bootpag({
    total: {{$requestPages}}
  }).on("page", function(event,num){
    var token = $("#token").val();
    var dataString = "page="+num+"&job_id="+{{$jobDetail['id']}}
    $.ajax({
      type: "POST",
      headers: {'X-CSRF-TOKEN': token},
      url: "{{url('loadJobsPitcherList')}}",
      data:dataString,
      cache: false,
      beforeSend: function () {

      },
     success: function(data) {
      if(data!="0"){
        $(".pitchedJobs").html("");
        $(".pitchedJobs").html(data);
      }
     }
   });
  });
  $(document).on('click','.btnAward',function(){
    var obj = $(this);
    var btnValue = $(this).val();
    var msg = "";
    if(btnValue=="Awarded"){
      msg = "Are you sure you want to disaward?";
    }else {
      msg = "Are you sure you want to award?";
    }
    bootbox.confirm(msg,function(option){
      if(option){
        var job_id = $(obj).data("job");
        var user_id = $(obj).data("user");
        var job_title = $(obj).data("title");
       //  console.log(job_id+" "+user_id);
        var token = $("#token").val();
        var dataString = "job_id="+job_id+"&job_seeker_id="+user_id+"&job_title="+job_title;
       $.ajax({
         type: "POST",
         headers: {'X-CSRF-TOKEN': token},
         url: "{{url('jobposter/assignJob')}}",
         data:dataString,
         cache: false,
         beforeSend: function () {
         },
        success: function(data) {
         if(data=="1"){

           $(obj).val("Awarded");
           $(obj).removeClass("btnAward");
           $(obj).addClass("btnAssigned");
           $(obj).addClass("green");
           $(".btnAward[data-job='"+job_id+"']").val("Rejected");
           $(".btnAward[data-job='"+job_id+"']").addClass("btnAssigned");
           $(obj).addClass("btnAward");
           $(obj).removeClass("btnAssigned");
           $(".actionbtn").removeClass('btnAward');
           $(".actionbtn").css('cursor','initial');
           $(obj).addClass('btnAward');
           $(obj).css('cursor','pointer');
         }else if(data=="2"){

           $(".actionbtn").css('cursor','pointer');
            $(obj).val("Award");
           $(obj).removeClass("btnAward");
           $(obj).addClass("btnAssigned");
           $(obj).addClass("green");
           $(".btnAssigned[data-job='"+job_id+"']").val("Award");
           $(obj).addClass("btnAward");
           $(obj).removeClass("btnAssigned");
           $(".actionbtn").addClass('btnAward');
         }else {
           //
         }
        }
      });
      }
    });
  });

  /* Developer Name : Hitesh Tank
   * Date : 5-Aug-2016
   */
  $(document).on("click",".job_bookmark",function(event){
      event.preventDefault();
      var token = $("#token").val();
      var dataString={job_id:$(this).data('job'),poster_id:$(this).data('poster'),status:$(this).attr('data-status')};
      var $currentObj=$(this);
      $.ajax({
        type: "POST",
        headers: {'X-CSRF-TOKEN': token},
        url: BASE_URL+"jobseeker/job/mark-action",
        cache: false,
        data: JSON.stringify({ data: dataString }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {

        },
       success: function(data) {
         if(data.status == 1){
           if(data.updated =='bookmark'){
              $currentObj.attr("data-status",'unbookmark');
              $currentObj.html('<i class="icon ion-android-star"></i> Bookmarked');
          }else{

            $currentObj.html('<i class="icon ion-android-star-outline"></i> Bookmark');
            $currentObj.attr("data-status",'bookmark');
          }
       }
     }
     });
  });

  /* Developer Name : Hitesh Tank
   * Date : 29-Aug-2016
   */
  $(document).on("click",".job_pitch_btn",function(event){
      $("#pitchModal").modal("show");
      $("#pitchModal").find(".post_id").val($(this).data("id"));
  });
  $(document).on("click",".jobpost_table .view_documents",function(event){
    var $model=$("#viewDocumentModal").modal("show");
    if($(this).data("comments") === "")
    {
      $model.find(".comments").hide();
    }else{
      $model.find(".comments").show();
      $model.find(".comment_text").val($(this).data("comments"));
    }
    if($(this).data("files") === "")
    {
      $model.find(".documents").hide();
    }else{
      var $files=$(this).data("files");
      var array = $files.split(',');
      var file_string="";
      for (var i=0;i<array.length;i++){
        file_string+="<a href='<?php echo url('download/seeker-documents/'); ?>/"+array[i]+"' >" + array[i] + "</a><br>";
      }
      $model.find(".documents").show();
      $model.find(".file_download").html("<br>"+file_string);

    }
  });
});

$(function(){
  $.getScript("{{URL::asset('resources/assets/js/dropzone/dropzone.js')}}",function(){
  // instantiate the uploader
    $('#add-dropzone').dropzone({
      url:BASE_URL+'jobseeker/job-post/upload-documents',
      maxFilesize: 100,
      uploadMultiple: true,
      parallelUploads: 10,
      maxFiles: 10,
      addRemoveLinks: false,
      autoProcessQueue: true,
      dictFileTooBig: 'File should not be bigger than 100MB',
      dictDefaultMessage: "<label class='dropzone-message-container'><i class='fa fa-cloud-upload'></i><div class='clearfix'></div> Drop images to upload</label>",
      headers: {'x-csrf-token': $("#token").val(),},
      acceptedFiles: ".jpeg,.jpg,.png,.gif,.JPEG,.JPG,.PNG,.GIF,.pdf,.PDF,.docx,.DOCX,.docm,.DOCM,.dotx,.DOTX,.dotm,.DOTM,.xlsx,.XLSX,.xlsm,.XLSM,.xltx,.XLTX,.xltm,.XLTM,.xlsb,.XLSB,.xlam,.XLAM,.mp4,.MP4",
      //maxThumbnailFilesize: 5,
      init: function() {
        this.on('success', function(file, json) {
          if(json.success ==true){
            var stringToAppend = $("#file_names").val().length > 0 ? $("#file_names").val() + "," : "";
            $("#file_names") .val( stringToAppend + json.data );
          }
        });
        this.on('addedfile', function(file) {
        });
        this.on('drop', function(file) {

        });
      }
    });
});
$(document).on("click","#pitchJobButton",function(event){
  var $submitButton=$(this);
  if($("#pitchYourProjectForm").length){
    $("#pitchYourProjectForm").validate({
              focusInvalid: false, // do not focus the last invalid input
              errorElement: 'p',
              errorClass: 'error_message', // default input error message class
              invalidHandler: function(form, validator) {
                  var errors = validator.numberOfInvalids();
                if (errors) {
                   validator.errorList[0].element.focus();
                   $('html, body').animate({
                   scrollTop: $(validator.errorList[0].element).offset().top-150
                }, "fast");
                  }
               } ,
              ignore: [],
                  rules: {

                  },
                  messages: {
                  },
                  errorPlacement: function(error, element) {
                      if(element.attr("name") == "txtBoardContent"){
                          error.insertAfter("#cke_editor1");
                      }else if(element.attr("name") == "content_du"){
                          error.insertAfter("#cke_content_du");
                      }else{
                          error.insertAfter(element);
                      }
                  },
                  highlight: function (element) { // hightlight error inputs
                      $(element)
                          .closest('.form-group').addClass('has-error'); // set error class to the control group
                  },
                  success: function (label) {
                      label.closest('.form-group').removeClass('has-error');
                      label.remove();
                  },
                  submitHandler: function(form) {
                      var formData = new FormData($("#pitchYourProjectForm")[0]);
                      var dataString = formData;

                      $.ajax({
                          type: "POST",
                          cache:false,
                          contentType: false,
                          processData: false,
                          headers: {'X-CSRF-TOKEN': $("#token").val()},
                          url:BASE_URL+'jobseeker/pitch',
                          data:dataString,
                          beforeSend:function(){
                           $submitButton.attr('disabled',true);
                           $submitButton.val('Processing');
                          },
                          success: function (response){
                             $submitButton.val('Pitch');
                             $submitButton.removeAttr('disabled');
                            if(response.status == 1){
                                alert(response.message);
                            }else{
                                alert(response.message);
                            }
                            setTimeout(function(){  $("#pitchModal").modal("hide"); location.reload(1);},1000);
                          }
                      });

                  }
          });
      }
});

});
</script>
</body>
</html>
