<!DOCTYPE html>
<html lang="en-US">
<head>
  <!-- include header files  -->
  @include('layouts.header')
  <link rel="stylesheet" href="{{URL::asset('resources/assets/css/jPages.css')}}">
</head>
<body class="fixed-header browse_page">
	<div id="page" class="hfeed site inner_padding">
    <header id="masthead" class="site-header" role="banner">
       @include('layouts.inner.header_inner_menu')
    </header><!-- #masthead -->
    <div id="main" class="site-main">
    <header class="page-header">
        <h2 class="page-title">Architects and Designers</h2>
    </header>
    <div id="primary" class="content-area" role="main">
    <div class="content-area">
<div class="entry-content">
<div class="job_listings" >
    <form class="job_filters" id="formJobFilter" role="form" method="POST" action="{{ url('browse/seeker') }}">
      <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
        <div class="search_jobs">
          <div class="container">
            <div class="search_keywords comonkeyword">
                <label for="search_keywords">Keywords</label>
                @if(isset($searchKeyWords['search_keywords']) && $searchKeyWords['search_keywords'] != '')
                  <input type="text" name="search_keywords" id="search_keywords" value="{{$searchKeyWords['search_keywords']}}" />
                @else
                    <input type="text" name="search_keywords" id="search_keywords" placeholder="Search By" value="{{old('search_keywords')}}" />
                @endif
                <select class="searchBy" name="searchBy" id="searchBy">
                    <option @if(isset($searchBy['searchBy']) && $searchBy['searchBy'] == 'location') selected @endif value="location">By Location</option>
                    <option  @if(isset($searchBy['searchBy']) && $searchBy['searchBy'] == 'name') selected @endif value="name">By Name</option>
                    <option @if(isset($searchBy['searchBy']) && $searchBy['searchBy'] == 'skill') selected @endif value="skill">By Computer Skill</option>
                    <!-- <option @if(isset($searchBy['searchBy']) && $searchBy['searchBy'] == 'rating') selected @endif value="rating">By Rating</option> -->
                    <option @if(isset($searchBy['searchBy']) && $searchBy['searchBy'] == 'postcode') selected @endif value="postcode">By Post Code</option>
                    <option @if(isset($searchBy['searchBy']) && $searchBy['searchBy'] == 'experience') selected @endif value="experience">By Year Of Experience</option>
                    <option @if(isset($searchBy['searchBy']) && $searchBy['searchBy'] == 'qualification') selected @endif value="qualification">By Highest Qualification Obtained</option>
                    <option @if(isset($searchBy['searchBy']) && $searchBy['searchBy'] == 'project') selected @endif value="project">Project Category</option>
                </select>
            </div>

            <!-- <div class="search_location">
                <label for="search_location">Location</label>
                @if(isset($searchKeyWords['search_location']) && $searchKeyWords['search_location'] != '')
                  <input type="text" name="search_location" id="search_location" value="{{$searchKeyWords['search_location']}}" />
                @else
                  <input type="text" name="search_location" id="search_location" placeholder="Location" value="" />
                @endif
            </div> -->
            <div class="search_submit">
                <input type="submit" name="submit" value="Search" id="searchBtn" />
            </div>
            <!-- <ul class="job_types sorting_list jobfilter">
          <li><span>Sort By | </span>
            @if(isset($searchKeyWords['sort_by']) && $searchKeyWords['sort_by'] == 'ASC')
              <a href="javascript:void(0)" class="DESC" id="sortByDeadline">First name</a>
            @else
              <a href="javascript:void(0)" class="ASC" id="sortByDeadline">First name</a>
            @endif
          </li>
          <input type="hidden" id="sort_by" name="sort_by" value="" />
			  </ul> -->
        <div class="clearfix"></div>
            <!--<div class="filter_wide filter_by_tag">Filter by tag: <span class="filter_by_tag_cloud"></span></div>-->
          </div>
        </div>
    </form>
    @if($seekerDetail->toArray()!=null)
    <ul class="job_listings_new container" id="job_seeker_list">
        @foreach($seekerDetail as $seeker)
        <li class="job_listing_list">
          <div class="resiexperince_dv">
            <div class="col-sm-12">

              <!-- @if($seeker['image']!="")
                <img src="{{URL::asset('resources/assets/upload/jobseeker/'.$seeker['image'])}}">
              @else
                <img src="{{URL::asset('resources/assets/images/signup_ic_1.png')}}">
              @endif -->
              <h3>{{$seeker['first_name']}} {{$seeker['last_name']}}</h3>
              <h4> {{$seeker['city']}}  <span>{{$seeker['country']}}</span> <span>{{$seeker['phone']}}</span>	</h4>
              <p>{{$seeker['description']}}</p>
              <div class="viewdetails_row">
                <a class="details" href="{{ url('user/profile/'.$seeker['id'])}}">View Details</a>
                  <div class="savebtn_dv">
                  </div>
                </div>
            </div>
          </div>
        </li>
        @endforeach
    </ul>
    <a class="load_more_jobs" href="#" ></a></div>
    @else
      <ul class="job_listings">
        <li>No Record Found.</li>
      </ul>
    @endif
    <div class="holder container"></div>
</div>
</div>
</div><!-- #primary -->
</div><!-- #main -->
<div class="footer-cta">
			<div class="container">
    			<h2>Got a question?</h2>
          <p>We're here to help. Check out our FAQs, send us an email or call us at 1 (800) 555-5555</p>
          <p><a href="#" class="button button--type-action">Contact Us</a></p>
			</div>
</div>
<footer id="colophon" class="site-footer" role="contentinfo">
			@include('layouts.footer')
</footer><!-- #colophon -->
</div><!-- #page -->
</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
<!--<script type='text/javascript' src="{{URL::asset('resources/assets/js/vonitto.minfcbd.js')}}"></script>-->
<script type='text/javascript' src="{{URL::asset('resources/assets/js/jPages.js')}}"></script>
<script type='text/javascript' src="{{URL::asset('resources/assets/js/salvattore.min0168.js')}}"></script>
<script type='text/javascript' src="{{URL::asset('resources/assets/js/jquery.validate.js')}}"></script>
<script type='text/javascript' src="{{URL::asset('resources/assets/js/bootbox.min.js')}}"></script>
<script type='text/javascript' src="{{URL::asset('resources/assets/js/bootstrap-select.min.js')}}"></script>
<script type="text/javascript">
$(function(){
  /* initiate the plugin */
  $("div.holder").jPages({
    containerID  : "job_seeker_list",
    perPage      : 4,
    startPage    : 1,
    startRange   : 1,
    midRange     : 5,
    endRange     : 1
  });
});
	window.onload=function(){
		$('.selectpicker').selectpicker();
	};
  $(document).ready(function(){
    $('#sortByDeadline').on('click',function(){
      var sortby = $(this).attr('class');
      //alert(sortby);
      $('#sort_by').val(sortby);
      $('#searchBtn').trigger('click');
    })
  });
</script>
</html>
