@if(isset($myJobs) && !empty($myJobs))
    <table>
      <tr>
        <th>No.</th>
        <th>Title</th>
        <th>Deadline</th>
        <th>Bidget</th>
        <th>Pitch Request</th>
        <th>Progress Status</th>
        <th>Date</th>
        <th>Action</th>
      </tr>
      {{--*/$i=1/*--}}
      @foreach($myJobs as $job)
        <tr>
          <td>{{$i++}}</td>
          <td><a href="{{url('/job/detail/'.$job->job_id)}}">{{$job->title}}</a></td>
          <td>{{$job->project_deadline}}</td>
          <td>{{$job->budget}}{{$job->currency}}</td>
          <td>{{$job->pitched_request}}</td>
          <td>{{$job->progress_status}}</td>
          <td>{{$job->created_at}}</td>
          <td data-name="Date"><a href="{{url('jobposter/jobpost',array($job->job_id))}}/edit">Edit</a></td>
        </tr>
      @endforeach
    </table>
  @else
@endif
