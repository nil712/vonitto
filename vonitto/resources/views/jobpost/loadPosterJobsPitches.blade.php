@if(isset($pitchedRequest) && !empty($pitchedRequest))
    <table>
      <tr>
        <th>No.</th>
        <th>Job Title</th>
        <th>Job Description</th>
        <th class="sr_name">Documents</th>
        <th>Seeker Name</th>
        <th>Date</th>
        <th>Action</th>
      </tr>
      {{--*/$i=1/*--}}
      @foreach($pitchedRequest as $pitch)
        <tr>
          <td>{{$i++}}</td>
          <td><a href="{{url('/job/detail/'.$pitch->job_id)}}">{{$pitch->title}}</a></td>
          <td>{{$pitch->description}}</td>
          <td data-name="Seeker Name">
            <p>
              {{--*/$files=array();/*--}}
              @if((isset($pitch->attached_documents) && !empty($pitch->attached_documents)) || (isset($pitch->comments) && !empty($pitch->comments)))
                  <a href="javascript:;" class="view_documents" data-comments="{{stripslashes($pitch->comments)}}" data-files="{{$pitch->attached_documents}}"  >Documents</a>
              @else
                 -
              @endif
            </p>
          </td>
          <td data-name="Seeker Name"><a href="{{url('user/profile/'.$pitch->job_seeker_id)}}">{{$pitch->first_name}} {{$pitch->last_name}}</a></td>
          <td>{{date('d-M-Y',strtotime($pitch->created_at))}}</td>
          @if($pitch->is_awarded=="0")
           <td><input type="button" value="Award" class="btnAward" data-job="{{$pitch->job_id}}" data-user="{{$pitch->job_seeker_id}}"></td>
          @else
           <td data-name="Action"><input type="button" value="@if($pitch->is_assigned) Awarded @else Rejected @endif" class="@if($pitch->is_assigned) btnAward @else btnAssigned @endif " data-job="{{$pitch->job_id}}" data-user="{{$pitch->job_seeker_id}}"></td>
          @endif
        </tr>
      @endforeach
    </table>
  @else
@endif
