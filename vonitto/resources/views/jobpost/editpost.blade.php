<!DOCTYPE html>
<html lang="en-US">
<head>
  <!-- include header files  -->
  @include('layouts.header')
  <link rel="stylesheet" media="all" type="text/css" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
	<link rel="stylesheet" media="all" type="text/css" href="{{URL::asset('resources/assets/css/jquery-ui-timepicker-addon.css')}}" />
  <link href="{{URL::asset('resources/assets/js/dropzone/css/dropzone.css')}}" rel="stylesheet"/>
</head>
<body class="fixed-header jobpost">

	<div id="page" class="hfeed site inner_padding">
    <header id="masthead" class="site-header" role="banner">
       @include('layouts.inner.header_inner_menu')
    </header><!-- #masthead -->
    <div id="main" class="site-main">


    <header class="page-header">
        <h2 class="page-title">Edit A Job</h2>
    </header>
    <div class="messageBox" style="display:none;"></div>


  @if($isUserPost)
    <div id="primary" class="content-area container" role="main">

        @if(isset($postDetail) && !empty($postDetail) && count($postDetail)>0)
        {{--*/$postDetail=$postDetail->toArray();/*--}}
        <?php //dd($postDetail); ?>
              <article id="post-1673" class="post-1673 page type-page status-publish hentry">
                  <div class="entry-content">
                      <div class="registration-form woocommerce register_dv loginsignupmain jobpostlogin">

                       <form  class="register" id="formJobPost" enctype="multipart/form-data" role="form" method="POST" action="{{ url('jobposter/jobpost',array($postDetail['id'])) }}">
                          <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
                          <input type="hidden" name="_method" value="PUT" >
                          <div class="form-row form-row-wide">
                      		<input type="text" class="input-text" name="title" id="title" value="{{stripslashes($postDetail['title'])}}"  placeholder="JOB TITLE"/>
                                  <div class="clearfix"></div>
                          </div>
                          <div class="form-row form-row-wide">
                         <!--<label for="description">Job Description <span class="required">*</span></label>-->
                       			<textarea style="resize:none;border-radius:0px;" rows="5"  class=""  name="description"  id="description" placeholder="DESCRIPTION">{{stripslashes($postDetail['description'])}}</textarea>
                              <div class="clearfix"></div>
                         </div>
                         <div class="form-row form-row-wide">

                        			<!--<label for="what_you_want">Tell us what you want to get done </label>-->
                       			<textarea style="resize:none;border-radius:0px;" rows="5"  class=""  name="what_you_want" id="what_you_want" value=""  placeholder="Tell us what you want to get done ">{{stripslashes($postDetail['what_you_want'])}}</textarea>
                              <div class="clearfix"></div>
                         </div>

                       <div class="form-row form-row-wide">
                          <label for="category" style="text-align:left;">Category <span class="required">*</span></label>
                     			<select name="category_id" class="dropdown" id="category_id">
                              <option value=""> Select Category </option>
                              @foreach($categories as $category)
                                <option @if($category['id'] == $postDetail['category_id']) selected @endif  value="{{$category['id']}}"> {{$category['name']}} </option>
                              @endforeach
                            </select>
                            <div class="clearfix"></div>
                            @if ($errors->has('category_id'))<p class="error_message">{!!$errors->first('category_id')!!}</p>@endif

                          </div>




                    <div class="form-row form-row-wide">
                     <!--<label for="budget">Project Deadline</label>-->
                                  <input type="text" class="input-text" name="project_deadline" placeholder="Project Deadline Date" id="project_deadline" value="{{date('m/d/Y H:i',strtotime($postDetail['project_deadline']))}}">
                                  <div class="clearfix"></div>
                    </div>


                     <div class="form-row form-row-wide" >
                     <!-- <label for="budget">Estimated Job Budget </label>-->
                        <input type="text" maxlength="12" class="input-text" name="budget" id="budget"  placeholder="Estimated Job Budget" value="{{$postDetail['budget']}}" />
                        <div class="clearfix"></div>
                        <select name="currency" class="dropdown" id="currency">
                          <option @if($postDetail['currency'] == "GBP") selected="selected" @endif value="GBP">GBP</option>
                          <option @if($postDetail['currency'] == "MYR") selected="selected" @endif value="MYR">MYR</option>
                        </select>
                     </div>
                     {{--*/$result=array();/*--}}
                     @if(isset($postDetail['file']) && !empty($postDetail['file']))
                       {{--*/ $files=explode(",",$postDetail['file']); /*--}}
                       @foreach($files as $file)
                         {{--*/ $file_name=URL::asset('resources/assets/upload/jobposter/document/'.$file); /*--}}
                         {{--*/$header=@get_headers($file_name);/*--}}
                         @if($header[0] == 'HTTP/1.0 404 Not Found')
                         {{--*/$obj['name']=$file;/*--}}
                         {{--*/$obj['file_path']=URL::asset('resources/assets/images/not-found.jpg');/*--}}
                         {{--*/$obj['size']=''; /*--}}
                         @else
                           {{--*/$obj['name']=$file;/*--}}
                           {{--*/$obj['file_path']=$file_name;/*--}}
                           {{--*/$obj['size']=''; /*--}}
                         @endif
                         {{--*/$result[]=$obj;/*--}}
                       @endforeach
                     @endif
                     <div class="form-row form-row-wide" >
                       <div class="dropzone" id="add-dropzone">
                       </div>
                         <div class="clearfix"></div>
                         <p class="error_message">Files could include pitch,video,pdf,word,excel,picture etc..</p>
                     </div>
                       <div class="form-row form-row-wide" >
               				     <div class="upload">
                             <label class="brows" ><input type="file" name="job_logo" placeholder="" id="job_logo" multiple>Logo</label>
                          </div>
                            <div class="clearfix"></div>
                       </div>
                      <div class="form-row form-row-wide">
                       <!--<label for="what_you_want">What skills should the job seeker have? </label>-->
                                    <textarea style="resize:none;" class="input-text" name="skills" id="skills" value=""  placeholder="What skills should the job seeker have? ">{{stripslashes($postDetail['skills'])}}</textarea>
                                    <div class="clearfix"></div>
                      </div>




                    <div class="form-row form-row-wide" >

                    <label for="category" style="text-align:left;">How long do you expect this job to last?   <span class="required">*</span></label>
                      			<select name="how_long" class="dropdown" id="how_long">
                              <option value=""> Select duration </option>
                              <option @if($postDetail['how_long'] == "More than 6 months") selected="selected" @endif value="More than 6 months"> More than 6 months </option>
                              <option @if($postDetail['how_long'] == '3 to 6 months') selected="selected" @endif value="3 to 6 months"> 3 to 6 months </option>
                              <option @if($postDetail['how_long'] == '1 to 3 months') selected="selected" @endif value="1 to 3 months"> 1 to 3 months </option>
                              <option @if($postDetail['how_long'] == 'Less than 1 month') selected="selected" @endif value="Less than 1 month"> Less than 1 month </option>
                              <option @if($postDetail['how_long'] == 'Less than 1 week') selected="selected" @endif value="Less than 1 week"> Less than 1 week </option>
                            </select>
                            <div class="clearfix"></div>
                    </div>


                    <div class="form-row form-row-wide">

                            			<label for="location_country" style="text-align:left;"> Country </label>
                            			<select name="location_country"class="dropdown" id="location_country">
                                    <option value=""> Select Country </option>
                                    <option @if($postDetail['location_country'] == 'UK') selected @endif  value="UK"> UK </option>
                                    <option @if($postDetail['location_country'] == 'Malaysia') selected @endif value="Malaysia"> Malaysia </option>
                                  </select>
                                  <div class="clearfix"></div>
                    </div>
                     <div class="form-row form-row-wide" >
                        <!--<label for="location_city">City </label>-->
                            <input type="text" class="input-text" name="location_city" id="location_city"  placeholder="City" value="{{$postDetail['location_city']}}"/>
                            <div class="clearfix"></div>
                     </div>
                     <div class="form-row form-row-wide" >
                         <input type="text" class="input-text" name="postcode" id="postcode"  placeholder="Post code" value="{{$postDetail['postcode']}}" />
                         <div class="clearfix"></div>
                      </div>
                    <div class="form-row" >

                          <input type="hidden" name="post_id" id="post_id" class="post_id" value="{{$postDetail['id']}}" />
                          <input type="hidden" name="file_names" id="file_names" class="file_names" value="{{$postDetail['file']}}" />
                         <input type="submit" id="editPostBtn" class="button" name="register" value="Update" />
                    </div>


                      		<!-- Spam Trap -->

                      	</form>
                    </div>
                  </div>
              </article><!-- #post -->
              @else
                <h3>Job not found</h3>
              @endif
              </div><!-- #primary -->
@else
  <h3>Unable to edit job</h3>
@endif
</div><!-- #main -->

<div class="footer-cta">
			<div class="container">
    			<h2>Got a question?</h2>
          <p>We're here to help. Check out our FAQs, send us an email or call us at 1 (800) 555-5555</p>
          <p><a href="#" class="button button--type-action">Contact Us</a></p>
			</div>
</div>
<footer id="colophon" class="site-footer" role="contentinfo">
			@include('layouts.footer')
</footer><!-- #colophon -->
</div><!-- #page -->
</body>

{{--*/$result=json_encode($result);/*--}}

<!--<script type='text/javascript' src="{{URL::asset('resources/assets/js/vonitto.minfcbd.js')}}"></script>-->
<script type='text/javascript' src="{{URL::asset('resources/assets/js/salvattore.min0168.js')}}"></script>

<script type='text/javascript' src="{{URL::asset('resources/assets/js/bootbox.min.js')}}"></script>
<script type="text/javascript" src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
<script type="text/javascript" src="http://code.jquery.com/ui/1.10.3/jquery-ui.min.js"></script>
<script type="text/javascript" src="{{URL::asset('resources/assets/js/jquery-ui-sliderAccess.js')}}"></script>
<script type='text/javascript' src="{{URL::asset('resources/assets/js/jquery-ui-timepicker-addon.js')}}"></script>
<!-- Hitesh Tank -->
<script src="{{URL::asset('resources/assets/js/jQuery-validation-plugin/jquery.validate.js')}}"></script>
<script src="{{URL::asset('resources/assets/js/jQuery-validation-plugin/additional-methods.js')}}"></script>
<script src="{{URL::asset('resources/assets/js/pages/poster/post.js')}}"></script>
<!-- Hitesh Tank -->

	<script type="text/javascript">
  var data=<?php echo $result; ?>;

  var uploadurl="";
		$(function(){
      $('#project_deadline').datetimepicker(
    	{
    		minDate: 0,
    	});

  $.getScript("{{URL::asset('resources/assets/js/dropzone/dropzone.js')}}",function(){
  // instantiate the uploader
    $('#add-dropzone').dropzone({
      url:BASE_URL+'jobposter/job-post/upload-files',
      maxFilesize: 100,
      uploadMultiple: true,
      parallelUploads: 10,
      maxFiles: 10,
      addRemoveLinks: true,
      autoProcessQueue: true,
      dictFileTooBig: 'File should not be bigger than 100MB',
      dictDefaultMessage: "<label class='dropzone-message-container'><i class='fa fa-cloud-upload'></i><div class='clearfix'></div> Drop images to upload</label>",
      headers: {'x-csrf-token': $("#token").val(),},
      acceptedFiles: ".jpeg,.jpg,.png,.gif,.JPEG,.JPG,.PNG,.GIF,.pdf,.PDF,.docx,.DOCX,.docm,.DOCM,.dotx,.DOTX,.dotm,.DOTM,.xlsx,.XLSX,.xlsm,.XLSM,.xltx,.XLTX,.xltm,.XLTM,.xlsb,.XLSB,.xlam,.XLAM,.mp4,.MP4",
      //maxThumbnailFilesize: 5,
      init: function() {

        thisDropzone=this;

              $.each(data, function(key,value){ //loop through it
                  var mockFile = { name: value.name, size: value.size }; // here we get the file name and size as response
                  thisDropzone.options.addedfile.call(thisDropzone, mockFile);
                  thisDropzone.options.thumbnail.call(thisDropzone, mockFile,value.file_path);//uploadsfolder is the folder where you have all those uploaded files
              });
              $(".dz-details > img").each(function(){
                $(this).removeAttr("alt");
              });


            // var thisDropzone = this;
            // var mockFile = { name: 'ec64f19587cac11ce1ad01ce417a50ff.png', size: 224.9 }; // here we get the file name and size as response
            // thisDropzone.options.addedfile.call(thisDropzone, mockFile);
            // thisDropzone.options.thumbnail.call(thisDropzone, mockFile, uploadurl);//uploadsfolder is the folder where you have all those uploaded files
        this.on('success', function(file, json) {
          if(json.success ==true){
            var stringToAppend = $("#file_names").val().length > 0 ? $("#file_names").val() + "," : "";

            $("#file_names") .val( stringToAppend + json.data );
          }
        });
        this.on('addedfile', function(file) {
            console.log(file);
        });
        this.on('drop', function(file) {
          console.log(file);
        });
        this.on("removedfile", function(file) {
          $.ajax({
                url: BASE_URL+"jobposter/job-post/delete-post-file",
                type: 'post',
                data: {
                    file_name: file.name,
                    post_id:$("#post_id").val()
                },
                headers: {
                  'X-CSRF-TOKEN': $("#token").val()
                },
                dataType: 'json',
                success: function (data) {
                    if(data.status==1){
                      $("#file_names") .val(  data.data );
                    }
                }
            });
        });
      }
    });
});


		});
	</script>

</html>
