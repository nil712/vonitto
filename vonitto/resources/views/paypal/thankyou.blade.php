<!DOCTYPE html>
<html lang="en-US">
<head>
<!-- include header files  -->
@include('layouts.header')
</head>
<body class="fixed-header jobdetail_page">
<div id="page" class="hfeed site inner_padding">
  <header id="masthead" class="site-header" role="banner"> @include('layouts.inner.header_inner_menu') </header>
  <!-- #masthead -->
  <div id="main" class="site-main">
    <header class="page-header">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <h1>Congratulation, your membership package has been activated</h1>
        </div>
      </div>
    </div>
  </header>
</div>
</div>
</body>
</html>
